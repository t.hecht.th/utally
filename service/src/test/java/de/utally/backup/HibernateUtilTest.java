package de.utally.backup;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Tobias Hecht
 *
 */
class HibernateUtilTest {

	@BeforeEach
	void setUp() {
	}

	@AfterEach
	void tearDown() throws Exception {
		FileUtils.deleteDirectory(new File("data"));
	}

	@Test
	void getSessionFactory() {
		var un = "un";
		var pw = "pw";
		var url = "jdbc:hsqldb:file:data/db";
		
		var optSessionFactory = HibernateUtil.getSessionFactory(url, un, pw);

		assertThat(optSessionFactory).isPresent();
	}

}
