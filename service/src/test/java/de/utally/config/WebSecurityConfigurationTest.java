package de.utally.config;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;

class WebSecurityConfigurationTest {
	
	private final WebSecurityConfiguration configuration = new WebSecurityConfiguration();

	@Test
	void passwordEncoder() {
		assertThat(configuration.passwordEncoder()).isInstanceOf(BCryptPasswordEncoder.class);
	}
	
	@Test
	void userDetailsServiceWithUser() {
		var encoder = configuration.passwordEncoder();
		String operatorUsername = "operatorUsername";
		String operatorPassword = "operatorPassword";
		String adminUsername = "adminUsername";
		String adminPassword = "adminPassword";
		String userUsername = "userUsername";
		String userPassword = "userPassword";
		var userDetailsService = configuration.userDetailsService(encoder, operatorUsername, operatorPassword, adminUsername, adminPassword, userUsername, userPassword);
		
		var userDetails = userDetailsService.loadUserByUsername(userUsername);
		assertThat(userDetails.getUsername()).isEqualTo(userUsername);
		assertThat(encoder.matches(userPassword, userDetails.getPassword())).isTrue();

		userDetails = userDetailsService.loadUserByUsername(operatorUsername);
		assertThat(userDetails.getUsername()).isEqualTo(operatorUsername);
		assertThat(encoder.matches(operatorPassword, userDetails.getPassword())).isTrue();

		userDetails = userDetailsService.loadUserByUsername(adminUsername);
		assertThat(userDetails.getUsername()).isEqualTo(adminUsername);
		assertThat(encoder.matches(adminPassword, userDetails.getPassword())).isTrue();
	}
}
