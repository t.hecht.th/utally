package de.utally.data.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class TeamTest {

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsById(long id) {
		var thisObj = new Team();
		var thatObj = new Team();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertTrue(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdWrongClass(long id) {
		var thisObj = new Team();
		var thatObj = new Location();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertFalse(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdNull(long id) {
		var thisObj = new Team();
		thisObj.setId(id);
		
		assertFalse(thisObj.equalsById(null));
	}

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void toStringTest(long id) {
		var team = new Team();
		team.setId(id);
		team.setCity("cityString");
		team.setCreationTime(Instant.now());
		team.setEnabled(true);
		team.setFullName("fullNameString");
		team.setModificationTime(Instant.now());
		team.setShortName("shortNameString");
		
		String toStringResult = String.format(
				"(%s@%s [id=%d, fullName=%s, shortName=%s, city=%s, enabled=%b, creationTime=%s, modificationTime=%s])",
				team.getClass().getSimpleName(), Integer.toHexString(team.hashCode()), team.getId(), team.getFullName(),
				team.getShortName(), team.getCity(), team.getEnabled(),
				team.getCreationTime(), team.getModificationTime());
		
		String result = team.toString();
		System.out.println(result);
		
		assertEquals(toStringResult, result);
	}
	
	@Test
	void contains() {
		var team = new Team();
		team.setId(5L);
		
		var player = new Player();
		player.setTeam(team);
		
		assertTrue(team.contains(player));
	}
	
	@Test
	void containsNot() {
		var team = new Team();
		team.setId(5L);
		
		var team2 = new Team();
		team2.setId(6L);
		var player = new Player();
		player.setTeam(team2);
		
		assertFalse(team.contains(player));
	}
	
	@Test
	void containsNull() {
		var team = new Team();
		team.setId(5L);
		
		assertFalse(team.contains(null));
	}
}
