package de.utally.data.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class TimeSlotTest {

	@Test
	void contains() {
		TimeSlot timeSlot = new TimeSlot();
		Instant now = Instant.now();
		timeSlot.setStart(now.minusMillis(5));
		timeSlot.setEnd(now.plusMillis(5));
		assertTrue(timeSlot.contains(now));
	}

	@Test
	void containsNot() {
		TimeSlot timeSlot = new TimeSlot();
		Instant now = Instant.now();
		timeSlot.setStart(now.minusMillis(10));
		timeSlot.setEnd(now.minusMillis(5));
		assertFalse(timeSlot.contains(now));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void toStringTest(long id) {
		TimeSlot timeSlot = new TimeSlot();
		Instant now = Instant.now();
		timeSlot.setId(id);
		timeSlot.setEnabled(true);
		timeSlot.setLabel("labelString");
		timeSlot.setCreationTime(now);
		timeSlot.setModificationTime(now);
		timeSlot.setStart(now);
		timeSlot.setEnd(now);
		
		String toStringResult = String.format(
				"(%s@%s [id=%d, label=%s, start=%s, end=%s, enabled=%b, creationTime=%s, modificationTime=%s])",
				timeSlot.getClass().getSimpleName(), Integer.toHexString(timeSlot.hashCode()), timeSlot.getId(), timeSlot.getLabel(),
				timeSlot.getStart(), timeSlot.getEnd(), timeSlot.getEnabled(),
				timeSlot.getCreationTime(), timeSlot.getModificationTime());
		
		String result = timeSlot.toString();
		System.out.println(result);
		
		assertEquals(toStringResult, result);
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsById(long id) {
		var thisObj = new TimeSlot();
		var thatObj = new TimeSlot();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertTrue(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdWrongClass(long id) {
		var thisObj = new TimeSlot();
		var thatObj = new Event();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertFalse(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdNull(long id) {
		var thisObj = new TimeSlot();
		thisObj.setId(id);
		
		assertFalse(thisObj.equalsById(null));
	}
}
