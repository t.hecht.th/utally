package de.utally.data.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class TallyTest {

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsById(long id) {
		var thisObj = new Tally();
		var thatObj = new Tally();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertTrue(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdWrongClass(long id) {
		var thisObj = new Tally();
		var thatObj = new Location();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertFalse(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdNull(long id) {
		var thisObj = new Tally();
		thisObj.setId(id);
		
		assertFalse(thisObj.equalsById(null));
	}

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void toStringTest(long id) {
		var tally = new Tally();
		tally.setId(id);
		tally.setCancelled(true);
		tally.setLocation(new Location());
		tally.setPlayer(new Player());
		tally.setProduct(new Product());
		tally.setTime(Instant.now());
		
		String toStringResult = String.format(
				"(%s@%s [id=%d, player=%s, location=%s, time=%s, product=%s, cancelled=%b])",
				tally.getClass().getSimpleName(), Integer.toHexString(tally.hashCode()), tally.getId(), tally.getPlayer(),
				tally.getLocation(), tally.getTime(), tally.getProduct(),
				tally.getCancelled());
		
		String result = tally.toString();
		System.out.println(result);
		
		assertEquals(toStringResult, result);
	}
	
	@Test
	void isValid() {
		var tally = new Tally();
		tally.setCancelled(false);
		
		assertTrue(tally.isValid());
	}
	
	@Test
	void isInvalid() {
		var tally = new Tally();
		tally.setCancelled(true);
		
		assertFalse(tally.isValid());
	}
}
