package de.utally.data.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsById(long id) {
		var thisObj = new Product();
		var thatObj = new Product();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertTrue(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdWrongClass(long id) {
		var thisObj = new Product();
		var thatObj = new Location();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertFalse(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdNull(long id) {
		var thisObj = new Product();
		thisObj.setId(id);
		
		assertFalse(thisObj.equalsById(null));
	}
	
	@ParameterizedTest
	@EnumSource(ProductCategory.class)
	void toStringTest(ProductCategory productCategory) {
		var product = new Product();
		product.setId(5L);
		product.setCategory(productCategory);
		product.setCreationTime(Instant.now());
		product.setEnabled(true);
		product.setImageName("ImageNameString");
		product.setModificationTime(Instant.now());
		product.setName("nameString");
		product.setPrice(400);
		product.setDisplayStatistic(true);
		
		String toStringResult = String.format(
				"(%s@%s [id=%d, name=%s, price=%d, imageName=%s, category=%s, enabled=%b, creationTime=%s, modificationTime=%s, displayStatistic=%b])",
				product.getClass().getSimpleName(), Integer.toHexString(product.hashCode()), product.getId(), product.getName(),
				product.getPrice(), product.getImageName(), product.getCategory(), product.getEnabled(),
				product.getCreationTime(), product.getModificationTime(), product.getDisplayStatistic());
		
		String result = product.toString();
		System.out.println(result);
		
		assertEquals(toStringResult, result);
	}

}
