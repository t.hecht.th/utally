package de.utally.data.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class EventTest {

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsById(long id) {
		var thisObj = new Event();
		var thatObj = new Event();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertTrue(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdWrongClass(long id) {
		var thisObj = new Event();
		var thatObj = new Location();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertFalse(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdNull(long id) {
		var thisObj = new Event();
		thisObj.setId(id);
		
		assertFalse(thisObj.equalsById(null));
	}

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void toStringTest(long id) {
		var event = new Event();
		event.setId(id);
		event.setCreationTime(Instant.now());
		event.setEnabled(true);
		event.setLocations(new HashSet<>());
		event.setModificationTime(Instant.now());
		event.setPlayersFee(4);
		event.setTeams(new HashSet<>());
		event.setTitle("titleString");
		
		String toStringResult = String.format(
				"(%s@%s [id=%d, title=%s, playersFee=%d, enabled=%b, locations=%s, teams=%s, creationTime=%s, modificationTime=%s])",
				event.getClass().getSimpleName(), Integer.toHexString(event.hashCode()), event.getId(), event.getTitle(), event.getPlayersFee(),
				event.getEnabled(), event.getLocations(), event.getTeams(), event.getCreationTime(), event.getModificationTime());
		
		String result = event.toString();
		System.out.println(result);
		
		assertEquals(toStringResult, result);
	}
}
