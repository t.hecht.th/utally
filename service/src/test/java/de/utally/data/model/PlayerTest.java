package de.utally.data.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsById(long id) {
		var thisObj = new Player();
		var thatObj = new Player();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertTrue(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdWrongClass(long id) {
		var thisObj = new Player();
		var thatObj = new Location();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertFalse(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdNull(long id) {
		var thisObj = new Player();
		thisObj.setId(id);
		
		assertFalse(thisObj.equalsById(null));
	}

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void toStringTest(long id) {
		var player = new Player();
		player.setId(id);
		player.setComment("commentString");
		player.setCreationTime(Instant.now());
		player.setEnabled(true);
		player.setEvent(new Event());
		player.setModificationTime(Instant.now());
		player.setName("nameString");
		player.setPaid(true);
		player.setPaidPlayersFee(500);
		player.setPayTime(Instant.now());
		player.setTeam(new Team());
		player.setWithPlayersFee(true);
		
		String toStringResult = String.format(
				"(%s@%s [id=%d, name=%s, team=%s, event=%s, paid=%b, withPlayersFee=%b, comment=%s, payTime=%s, enabled=%b, paidPlayersFee=%d, creationTime=%s, modificationTime=%s, preferences=%s])",
				player.getClass().getSimpleName(), Integer.toHexString(player.hashCode()), player.getId(), player.getName(),
				player.getTeam(), player.getEvent(), player.getPaid(), player.getWithPlayersFee(),
				player.getComment(), player.getPayTime(), player.getEnabled(), player.getPaidPlayersFee(),
				player.getCreationTime(), player.getModificationTime(), player.getPreferences());
		
		String result = player.toString();
		System.out.println(result);
		
		assertEquals(toStringResult, result);
	}

}
