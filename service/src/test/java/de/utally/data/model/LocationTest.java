package de.utally.data.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class LocationTest {

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsById(long id) {
		var thisObj = new Location();
		var thatObj = new Location();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertTrue(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdWrongClass(long id) {
		var thisObj = new Location();
		var thatObj = new Event();
		thisObj.setId(id);
		thatObj.setId(id);
		
		assertFalse(thisObj.equalsById(thatObj));
	}
	
	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void equalsByIdNull(long id) {
		var thisObj = new Location();
		thisObj.setId(id);
		
		assertFalse(thisObj.equalsById(null));
	}

	@ParameterizedTest
	@ValueSource(longs = {3L, 4L, 5L})
	void toStringTest(long id) {
		var location = new Location();
		location.setId(id);
		location.setName("nameString");
		location.setEnabled(true);
		location.setProducts(new HashSet<>());
		location.setCreationTime(Instant.now());
		location.setModificationTime(Instant.now());
		
		String toStringResult = String.format("(%s@%s [id=%d, name=%s, enabled=%b, products=%s, creationTime=%s, modificationTime=%s])",
				location.getClass().getSimpleName(), Integer.toHexString(location.hashCode()), location.getId(), location.getName(),
				location.getEnabled(), location.getProducts(), location.getCreationTime(),
				location.getModificationTime());
		
		String result = location.toString();
		System.out.println(result);
		
		assertEquals(toStringResult, result);
	}
}
