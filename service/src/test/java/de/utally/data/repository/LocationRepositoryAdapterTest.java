package de.utally.data.repository;

import de.utally.data.exception.LocationNotFoundException;
import de.utally.data.exception.ProductNotFoundException;
import de.utally.data.model.Location;
import de.utally.data.repository.internal.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class LocationRepositoryAdapterTest {
	@Mock
	LocationRepository repository;
	
	@Mock
	ProductRepository productRepository;
	
	@Mock
	TallyRepository tallyRepository;
	
	LocationRepositoryAdapter repositoryAdapter;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		repositoryAdapter = new LocationRepositoryAdapter(repository, productRepository, tallyRepository);
		
		doAnswer((Answer<LocationEntity>) invocation -> invocation.getArgument(0)).when(repository).save(any(LocationEntity.class));
	}

	@AfterEach
	void tearDown() {
		reset(repository);
		reset(productRepository);
	}

	@Test
	void insertLocation() {
		String name = "name";
		String description = "description";
		
		var location = repositoryAdapter.insertLocation(name, description, true);
		
		assertEquals(name, location.getName());
		assertEquals(description, location.getDescription());
		assertTrue(location.getEnabled());
		assertNotNull(location.getCreationTime());
		assertNotNull(location.getModificationTime());
		
		verify(repository).save(any(LocationEntity.class));
	}

	@Test
	void insertLocationNullName() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.insertLocation(null, "description", true));

		verify(repository, never()).save(any(LocationEntity.class));
	}

	@Test
	void insertLocationNullEnabledState() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.insertLocation("name", "description", null));

		verify(repository, never()).save(any(LocationEntity.class));
	}

	@Test
	void existsByIdTrue() {
		when(repository.existsById(anyLong())).thenReturn(true);
		assertTrue(repositoryAdapter.existsById(5L));
	}

	@Test
	void existsByIdFalse() {
		when(repository.existsById(anyLong())).thenReturn(false);
		assertFalse(repositoryAdapter.existsById(5L));
	}
	
	@Test
	void updateLocation() {
		Long id = 4L;
		String name = "name";
		
		var location = new Location();
		location.setId(id);
		location.setName(name);
		location.setEnabled(true);
		location.setDescription("description");
		
		var locationEntity = new LocationEntity();
		locationEntity.setId(id);
		locationEntity.setName("name2");
		locationEntity.setEnabled(false);
		
		when(repository.findById(id)).thenReturn(Optional.of(locationEntity));
		
		doAnswer((Answer<Void>) invocation -> {
			LocationEntity l = invocation.getArgument(0);
			assertEquals(location.getName(), l.getName());
			assertTrue(l.getEnabled());
			assertEquals(location.getDescription(), l.getDescription());
			assertNotNull(l.getModificationTime());
			return null;
		}).when(repository).save(any());
		
		repositoryAdapter.updateLocation(location);
		verify(repository).save(any());
	}
	
	@Test
	void updateMissingLocation() {
		Long id = 4L;
		String name = "name";
		
		var location = new Location();
		location.setId(id);
		location.setName(name);
		
		when(repository.findById(id)).thenReturn(Optional.empty());
		
		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.updateLocation(location));
		verify(repository, never()).save(any());
	}

	@Test
	void updateLocationNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.updateLocation(null));
		verify(repository, never()).save(any());
	}

	@Test
	void removeLocation() {
		Long locationId = 6L;
		repositoryAdapter.removeLocation(locationId);
		verify(repository).deleteById(locationId);
		verify(tallyRepository).deleteByLocationId(locationId);
	}
	
	@Test
	void getLocations() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		
		when(repository.findAll()).thenReturn(Collections.singletonList(locationEntity));
		
		var locations = repositoryAdapter.getLocations();
		
		assertEquals(1, locations.size());
		assertEquals(locationId, locations.get(0).getId());
	}
	
	@Test
	void getEnabledLocations() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		
		when(repository.findByEnabled(true)).thenReturn(Collections.singletonList(locationEntity));
		
		var locations = repositoryAdapter.getEnabledLocations();
		
		assertEquals(1, locations.size());
		assertEquals(locationId, locations.get(0).getId());
		
		verify(repository).findByEnabled(true);
	}
	
	@Test
	void getLocationById() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		
		when(repository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		
		var location = repositoryAdapter.getLocation(locationId);
		
		assertEquals(locationId, location.getId());
		verify(repository).findById(locationId);
	}

	@Test
	void getLocationByIdMissing() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);

		when(repository.findById(locationId)).thenReturn(Optional.empty());

		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.getLocation(locationId));

		verify(repository).findById(anyLong());
	}

	@Test
	void getLocationByIdNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getLocation(null));

		verify(repository, never()).findById(anyLong());
	}
	
	@Test
	void addProduct() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		Long productId = 7L;
		var product = new ProductEntity();
		product.setId(productId);
		
		when(productRepository.findById(productId)).thenReturn(Optional.of(product));
		when(repository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		
		assertTrue(locationEntity.getProducts().isEmpty());
		repositoryAdapter.addProduct(locationId, product.getId());
		assertEquals(1, locationEntity.getProducts().size());
		assertNotNull(locationEntity.getModificationTime());
		
		verify(repository).save(any(LocationEntity.class));
	}
	
	@Test
	void addProductWithWrongLocation() {
		Long locationId = 6L;
		Long productId = 7L;
		var product = new ProductEntity();
		product.setId(productId);
		
		when(productRepository.findById(productId)).thenReturn(Optional.of(product));
		when(repository.findById(locationId)).thenReturn(Optional.empty());

		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.addProduct(locationId, productId));
		
		verify(repository, never()).save(any(LocationEntity.class));
	}
	
	@Test
	void addProductWithWrongProduct() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		Long productId = 7L;

		when(productRepository.findById(productId)).thenReturn(Optional.empty());
		when(repository.findById(locationId)).thenReturn(Optional.of(locationEntity));

		assertThrows(ProductNotFoundException.class, () -> repositoryAdapter.addProduct(locationId, productId));
		
		verify(repository, never()).save(any(LocationEntity.class));
	}

	@Test
	void addProductWithNullLocation() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.addProduct(null, 5L));

		verify(repository, never()).save(any(LocationEntity.class));
	}

	@Test
	void addProductWithNullProduct() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.addProduct(5L, null));

		verify(repository, never()).save(any(LocationEntity.class));
	}
	
	@Test
	void getProducts() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		Long productId = 7L;
		var productEntity = new ProductEntity();
		productEntity.setId(productId);
		locationEntity.setProducts(Set.of(productEntity));
		
		when(repository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		
		assertEquals(1, locationEntity.getProducts().size());
		var products = repositoryAdapter.getProducts(locationId);
		
		assertEquals(1, products.size());
		assertEquals(productId, products.get(0).getId());
		verify(repository).findById(locationId);
	}

	@Test
	void getProductsMissing() {
		Long locationId = 6L;
		when(repository.findById(locationId)).thenReturn(Optional.empty());

		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.getProducts(locationId));

		verify(repository).findById(anyLong());
	}

	@Test
	void getProductsNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getProducts(null));

		verify(repository, never()).findById(anyLong());
	}
	
	@Test
	void removeProduct() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		Long productId = 7L;
		var productEntity = new ProductEntity();
		productEntity.setId(productId);
		locationEntity.setProducts(Set.of(productEntity));
		
		when(productRepository.findById(productId)).thenReturn(Optional.of(productEntity));
		when(repository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		
		assertEquals(1, locationEntity.getProducts().size());
		repositoryAdapter.removeProduct(locationId, productEntity.getId());
		assertTrue(locationEntity.getProducts().isEmpty());
		assertNotNull(locationEntity.getModificationTime());
		
		verify(repository).save(any(LocationEntity.class));
	}
	
	@Test
	void removeProductWithWrongLocation() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		Long productId = 7L;
		var productEntity = new ProductEntity();
		productEntity.setId(productId);
		locationEntity.setProducts(Set.of(productEntity));
		
		when(productRepository.findById(productId)).thenReturn(Optional.of(productEntity));
		when(repository.findById(locationId)).thenReturn(Optional.empty());

		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.removeProduct(locationId, productId));
		
		verify(repository, never()).save(any(LocationEntity.class));
	}
	
	@Test
	void removeProductWithWrongProduct() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		Long productId = 7L;
		var productEntity = new ProductEntity();
		productEntity.setId(productId);
		locationEntity.setProducts(Set.of(productEntity));
		
		when(productRepository.findById(productId)).thenReturn(Optional.empty());
		when(repository.findById(locationId)).thenReturn(Optional.of(locationEntity));

		assertThrows(ProductNotFoundException.class, () -> repositoryAdapter.removeProduct(locationId, productId));
		
		verify(repository, never()).save(any(LocationEntity.class));
	}

	@Test
	void removeProductWithNullLocation() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeProduct(null, 5L));

		verify(repository, never()).save(any(LocationEntity.class));
	}

	@Test
	void removeProductWithNullProduct() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeProduct(5L, null));

		verify(repository, never()).save(any(LocationEntity.class));
	}
	
	@Test
	void removeProductFromAllLocations() {
		Long locationId = 6L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		Long productId = 7L;
		var productEntity = new ProductEntity();
		productEntity.setId(productId);
		locationEntity.setProducts(Set.of(productEntity));

		when(productRepository.existsById(productId)).thenReturn(true);
		when(repository.findAll()).thenReturn(Collections.singletonList(locationEntity));
		
		assertEquals(1, locationEntity.getProducts().size());
		repositoryAdapter.removeProductFromAllLocations(productEntity.getId());
		assertEquals(0, locationEntity.getProducts().size());
		
		verify(repository).save(any(LocationEntity.class));
	}
	
	@Test
	void removeProductFromAllLocationsWrongProduct() {
		Long productId = 7L;

		when(productRepository.existsById(productId)).thenReturn(false);

		assertThrows(ProductNotFoundException.class, () -> repositoryAdapter.removeProductFromAllLocations(productId));
		
		verify(repository, never()).save(any(LocationEntity.class));
	}

	@Test
	void removeProductFromAllLocationsNullProduct() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeProductFromAllLocations(null));

		verify(repository, never()).save(any(LocationEntity.class));
	}
}
