package de.utally.data.repository.internal;

import de.utally.data.model.Event;
import de.utally.data.model.TimeSlot;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class TimeSlotEntityTest {
    @Test
    void toTimeSlot() {
        var eventEntity = new EventEntity();
        eventEntity.setId(5L);
        var timeSlotEntity = new TimeSlotEntity();
        timeSlotEntity.setEnd(Instant.now().plusMillis(2000));
        timeSlotEntity.setId(4L);
        timeSlotEntity.setLabel("label");
        timeSlotEntity.setStart(Instant.now());
        timeSlotEntity.setCreationTime(Instant.now().minusSeconds(5));
        timeSlotEntity.setModificationTime(Instant.now().minusSeconds(2));
        timeSlotEntity.setEnabled(true);
        timeSlotEntity.setDescription("description");
        timeSlotEntity.setEvent(eventEntity);

        TimeSlot timeSlot = timeSlotEntity.toTimeSlot();

        assertAll(
                () -> assertEquals(timeSlotEntity.getEnd(), timeSlot.getEnd()),
                () -> assertEquals(timeSlotEntity.getId(), timeSlot.getId()),
                () -> assertEquals(timeSlotEntity.getLabel(), timeSlot.getLabel()),
                () -> assertEquals(timeSlotEntity.getCreationTime(), timeSlot.getCreationTime()),
                () -> assertEquals(timeSlotEntity.getModificationTime(), timeSlot.getModificationTime()),
                () -> assertTrue(timeSlot.getEnabled()),
                () -> assertEquals(timeSlotEntity.getStart(), timeSlot.getStart()),
                () -> assertEquals(timeSlotEntity.getDescription(), timeSlot.getDescription()),
                () -> assertEquals(timeSlotEntity.getEvent().getId(), timeSlot.getEvent().getId())
        );
    }

    @Test
    void toTimeSlots() {
        var timeSlot = new TimeSlotEntity();
        timeSlot.setId(4L);
        var eventEntity = new EventEntity();
        eventEntity.setId(5L);
        timeSlot.setEvent(eventEntity);
        var timeSlots = TimeSlotEntity.toTimeSlots(Collections.singletonList(timeSlot));
        assertEquals(1, timeSlots.size());
        assertEquals(timeSlot.getId(), timeSlots.toArray(new TimeSlot[0])[0].getId());
    }

    @Test
    void fromTimeSlot() {
        var event = new Event();
        event.setId(5L);
        var timeSlot = new TimeSlot();
        timeSlot.setEnd(Instant.now().plusMillis(2000));
        timeSlot.setId(4L);
        timeSlot.setLabel("label");
        timeSlot.setStart(Instant.now());
        timeSlot.setCreationTime(Instant.now().minusSeconds(5));
        timeSlot.setModificationTime(Instant.now().minusSeconds(2));
        timeSlot.setEnabled(true);
        timeSlot.setDescription("description");
        timeSlot.setEvent(event);

        TimeSlotEntity timeSlotEntity = TimeSlotEntity.fromTimeSlot(timeSlot);

        assertAll(
                () -> assertEquals(timeSlotEntity.getEnd(), timeSlot.getEnd()),
                () -> assertEquals(timeSlotEntity.getId(), timeSlot.getId()),
                () -> assertEquals(timeSlotEntity.getLabel(), timeSlot.getLabel()),
                () -> assertEquals(timeSlotEntity.getCreationTime(), timeSlot.getCreationTime()),
                () -> assertEquals(timeSlotEntity.getModificationTime(), timeSlot.getModificationTime()),
                () -> assertTrue(timeSlotEntity.getEnabled()),
                () -> assertEquals(timeSlotEntity.getStart(), timeSlot.getStart()),
                () -> assertEquals(timeSlotEntity.getDescription(), timeSlot.getDescription()),
                () -> assertEquals(timeSlotEntity.getEvent().getId(), timeSlot.getEvent().getId())
        );
    }

    @Test
    void fromTimeSlots() {
        var timeSlot = new TimeSlot();
        timeSlot.setId(4L);
        var event = new Event();
        event.setId(5L);
        timeSlot.setEvent(event);
        var timeSlots = TimeSlotEntity.fromTimeSlots(Collections.singletonList(timeSlot));
        assertEquals(1, timeSlots.size());
        assertEquals(timeSlot.getId(), timeSlots.toArray(new TimeSlotEntity[0])[0].getId());
    }
}
