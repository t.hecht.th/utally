package de.utally.data.repository;

import de.utally.data.exception.*;
import de.utally.data.model.Tally;
import de.utally.data.model.TallyAdapter;
import de.utally.data.repository.internal.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TallyRepositoryAdapterTest {
	@Mock
	TallyRepository tallyRepository;

	@Mock
	PlayerRepository playerRepository;

	@Mock
	ProductRepository productRepository;

	@Mock
	LocationRepository locationRepository;

	@Mock
	EventRepository eventRepository;

	@Mock
	TeamRepository teamRepository;

	@Mock
	TimeSlotRepository timeSlotRepository;

	TallyAdapter tallyAdapter;

	@BeforeEach
	void setUp() {
		tallyAdapter = new TallyRepositoryAdapter(tallyRepository, playerRepository, productRepository,
				locationRepository, eventRepository, teamRepository, timeSlotRepository);
		
		doAnswer((Answer<TallyEntity>) invocation -> invocation.getArgument(0)).when(tallyRepository).save(any(TallyEntity.class));
	}

	@AfterEach
	void tearDown() {
		reset(tallyRepository);
		reset(playerRepository);
		reset(productRepository);
		reset(locationRepository);
		reset(eventRepository);
		reset(teamRepository);
	}

	@Test
	void insertTally() {
		tallyAdapter = new TallyRepositoryAdapter(tallyRepository, playerRepository, productRepository,
				locationRepository, eventRepository, teamRepository, timeSlotRepository);
		var playerId = 7L;
		var player = new PlayerEntity();
		player.setId(playerId);
		var productId = 8L;
		var product = new ProductEntity();
		product.setId(productId);
		var locationId = 9L;
		var location = new LocationEntity();
		location.setId(locationId);
		var teamId = 12L;
		var team = new TeamEntity();
		team.setId(teamId);
		team.setFullName("fullName");
		team.setCity("city");
		team.setShortName("shortName");
		player.setTeam(team);
		var event = new EventEntity();
		event.setId(13L);
		player.setEvent(event);

		when(playerRepository.findById(playerId)).thenReturn(Optional.of(player));
		when(productRepository.findById(productId)).thenReturn(Optional.of(product));
		when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));

		Tally tally = tallyAdapter.insertTally(playerId, productId, locationId);

		assertEquals(Boolean.FALSE, tally.getCancelled());
		assertNotNull(tally.getProduct());
		assertNotNull(tally.getPlayer());
		assertNotNull(tally.getLocation());
		assertNotNull(tally.getPlayer().getTeam());
		assertNotNull(tally.getTime());
		assertEquals(product.getId(), tally.getProduct().getId());
		assertEquals(player.getId(), tally.getPlayer().getId());
		assertEquals(location.getId(), tally.getLocation().getId());
		assertEquals(team.getId(), tally.getPlayer().getTeam().getId());

		verify(tallyRepository).save(any());
	}

	@Test
	void insertTallyWithMissingPlayer() {
		var playerId = 7L;
		var productId = 8L;
		var product = new ProductEntity();
		product.setId(productId);
		var locationId = 9L;
		var location = new LocationEntity();
		location.setId(locationId);

		when(playerRepository.findById(playerId)).thenReturn(Optional.empty());
		when(productRepository.findById(productId)).thenReturn(Optional.of(product));
		when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));

		assertThrows(PlayerNotFoundException.class, () -> tallyAdapter.insertTally(playerId, productId, locationId));

		verify(tallyRepository, never()).save(any());
	}

	@Test
	void insertTallyWithMissingProduct() {
		var playerId = 7L;
		var player = new PlayerEntity();
		player.setId(playerId);
		var productId = 8L;
		var locationId = 9L;
		var location = new LocationEntity();
		location.setId(locationId);

		when(playerRepository.findById(playerId)).thenReturn(Optional.of(player));
		when(productRepository.findById(productId)).thenReturn(Optional.empty());
		when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));

		assertThrows(ProductNotFoundException.class, () -> tallyAdapter.insertTally(playerId, productId, locationId));

		verify(tallyRepository, never()).save(any());
	}

	@Test
	void insertTallyWithMissingLocation() {
		var playerId = 7L;
		var player = new PlayerEntity();
		player.setId(playerId);
		var productId = 8L;
		var product = new ProductEntity();
		product.setId(productId);
		var locationId = 9L;

		when(playerRepository.findById(playerId)).thenReturn(Optional.of(player));
		when(productRepository.findById(productId)).thenReturn(Optional.of(product));
		when(locationRepository.findById(locationId)).thenReturn(Optional.empty());

		assertThrows(LocationNotFoundException.class, () -> tallyAdapter.insertTally(playerId, productId, locationId));

		verify(tallyRepository, never()).save(any());
	}

	@Test
	void insertTallyWithPlayerNull() {
		var productId = 8L;
		var locationId = 9L;

		assertThrows(NullPointerException.class, () -> tallyAdapter.insertTally(null, productId, locationId));

		verify(tallyRepository, never()).save(any());
	}

	@Test
	void insertTallyWithProductNull() {
		var playerId = 7L;
		var locationId = 9L;

		assertThrows(NullPointerException.class, () -> tallyAdapter.insertTally(playerId, null, locationId));

		verify(tallyRepository, never()).save(any());
	}

	@Test
	void insertTallyWithLocationNull() {
		var playerId = 7L;
		var productId = 8L;

		assertThrows(NullPointerException.class, () -> tallyAdapter.insertTally(playerId, productId, null));

		verify(tallyRepository, never()).save(any());
	}

	@Test
	void getTallyByIdNotExisting() {
		when(tallyRepository.findById(anyLong())).thenReturn(Optional.empty());

		assertThrows(TallyNotFoundException.class, () -> tallyAdapter.getTallyById(5L));

		verify(tallyRepository).findById(anyLong());
	}

	@Test
	void getTallyByIdNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTallyById(null));
	}

	@Test
	void getTallyById() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		when(tallyRepository.findById(tallyEntity.getId())).thenReturn(Optional.of(tallyEntity));

		var tally = tallyAdapter.getTallyById(tallyEntity.getId());
		assertEquals(tallyEntity.getId(), tally.getId());

		verify(tallyRepository).findById(tallyEntity.getId());
	}

	@Test
	void getTalliesByPlayer() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		when(playerRepository.existsById(playerEntity.getId())).thenReturn(true);

		when(tallyRepository.findByPlayerId(playerEntity.getId())).thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTalliesByPlayer(playerEntity.getId());
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findByPlayerId(playerEntity.getId());
	}

	@Test
	void getTalliesByPlayerMissing() {
		long playerId = 7L;

		when(playerRepository.existsById(playerId)).thenReturn(false);

		assertThrows(PlayerNotFoundException.class, () -> tallyAdapter.getTalliesByPlayer(playerId));

		verify(tallyRepository, never()).findByPlayerId(anyLong());
	}

	@Test
	void getTalliesByPlayerNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByPlayer(null));
		verify(tallyRepository, never()).findByPlayerId(anyLong());
	}

	@Test
	void getTalliesByProductAndPlayer() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		when(productRepository.existsById(productEntity.getId())).thenReturn(true);
		when(playerRepository.existsById(playerEntity.getId())).thenReturn(true);

		when(tallyRepository.findByProductIdAndPlayerId(productEntity.getId(), playerEntity.getId()))
				.thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTalliesByProductAndPlayer(productEntity.getId(), playerEntity.getId());
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findByProductIdAndPlayerId(productEntity.getId(), playerEntity.getId());
	}

	@Test
	void getTalliesByProductAndPlayerWithMissingProduct() {
		long playerId = 7L;
		long productId = 8L;

		when(productRepository.existsById(productId)).thenReturn(false);

		assertThrows(ProductNotFoundException.class, () -> tallyAdapter.getTalliesByProductAndPlayer(productId, playerId));

		verify(tallyRepository, never()).findByProductIdAndPlayerId(anyLong(), anyLong());
	}

	@Test
	void getTalliesByProductAndPlayerWithMissingPlayer() {
		long playerId = 7L;
		long productId = 8L;

		when(productRepository.existsById(productId)).thenReturn(true);
		when(playerRepository.existsById(playerId)).thenReturn(false);

		assertThrows(PlayerNotFoundException.class, () -> tallyAdapter.getTalliesByProductAndPlayer(productId, playerId));

		verify(tallyRepository, never()).findByProductIdAndPlayerId(anyLong(), anyLong());
	}

	@Test
	void getTalliesByProductAndPlayerWithProductNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByProductAndPlayer(null, 7L));

		verify(tallyRepository, never()).findByProductIdAndPlayerId(anyLong(), anyLong());
	}

	@Test
	void getTalliesByProductAndPlayerWithPlayerNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByProductAndPlayer(7L, null));

		verify(tallyRepository, never()).findByProductIdAndPlayerId(anyLong(), anyLong());
	}

	@Test
	void getTalliesByLocationAndPlayer() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		when(locationRepository.existsById(locationEntity.getId())).thenReturn(true);
		when(playerRepository.existsById(playerEntity.getId())).thenReturn(true);

		when(tallyRepository.findByLocationIdAndPlayerId(locationEntity.getId(), playerEntity.getId()))
				.thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTalliesByLocationAndPlayer(locationEntity.getId(), playerEntity.getId());
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findByLocationIdAndPlayerId(locationEntity.getId(), playerEntity.getId());
	}

	@Test
	void getTalliesByLocationAndPlayerMissingLocation() {
		long playerId = 7L;
		long locationId = 10L;

		when(locationRepository.existsById(locationId)).thenReturn(false);

		assertThrows(LocationNotFoundException.class, () -> tallyAdapter.getTalliesByLocationAndPlayer(locationId, playerId));

		verify(tallyRepository, never()).findByLocationIdAndPlayerId(anyLong(), anyLong());
	}

	@Test
	void getTalliesByLocationAndPlayerMissingPlayer() {
		long playerId = 7L;
		long locationId = 10L;

		when(locationRepository.existsById(locationId)).thenReturn(true);
		when(playerRepository.existsById(playerId)).thenReturn(false);

		assertThrows(PlayerNotFoundException.class, () -> tallyAdapter.getTalliesByLocationAndPlayer(locationId, playerId));

		verify(tallyRepository, never()).findByLocationIdAndPlayerId(anyLong(), anyLong());
	}

	@Test
	void getTalliesByLocationAndPlayerLocationNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByLocationAndPlayer(null, 5L));

		verify(tallyRepository, never()).findByLocationIdAndPlayerId(anyLong(), anyLong());
	}

	@Test
	void getTalliesByLocationAndPlayerPlayerNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByLocationAndPlayer(5L, null));

		verify(tallyRepository, never()).findByLocationIdAndPlayerId(anyLong(), anyLong());
	}
	
	@Test
	void getTalliesByProduct() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		when(productRepository.existsById(productEntity.getId())).thenReturn(true);

		when(tallyRepository.findByProductId(productEntity.getId()))
				.thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTalliesByProduct(productEntity.getId());
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findByProductId(productEntity.getId());
	}

	@Test
	void getTalliesByProductMissing() {
		long productId = 8L;

		when(productRepository.existsById(productId)).thenReturn(false);

		assertThrows(ProductNotFoundException.class, () -> tallyAdapter.getTalliesByProduct(productId));

		verify(tallyRepository, never()).findByProductId(anyLong());
	}

	@Test
	void getTalliesByProductNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByProduct(null));

		verify(tallyRepository, never()).findByProductId(anyLong());
	}
	
	@Test
	void getTalliesByLocation() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		when(locationRepository.existsById(locationEntity.getId())).thenReturn(true);

		when(tallyRepository.findByLocationId(locationEntity.getId()))
				.thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTalliesByLocation(locationEntity.getId());
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findByLocationId(locationEntity.getId());
	}

	@Test
	void getTalliesByLocationMissing() {
		long locationId = 10L;

		when(locationRepository.existsById(locationId)).thenReturn(false);

		assertThrows(LocationNotFoundException.class, () -> tallyAdapter.getTalliesByLocation(locationId));

		verify(tallyRepository, never()).findByLocationId(anyLong());
	}

	@Test
	void getTalliesByLocationNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByLocation(null));

		verify(tallyRepository, never()).findByLocationId(anyLong());
	}
	
	@Test
	void getTalliesByEvent() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);
		
		eventEntity.setLocations(Set.of(locationEntity));
		
		when(eventRepository.findById(eventEntity.getId())).thenReturn(Optional.of(eventEntity));

		when(tallyRepository.findByLocationIdIn(List.of(locationEntity.getId())))
				.thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTalliesByEvent(eventEntity.getId());
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findByLocationIdIn(List.of(locationEntity.getId()));
	}

	@Test
	void getTalliesByEventMissing() {
		long eventId = 11L;

		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> tallyAdapter.getTalliesByEvent(eventId));

		verify(tallyRepository, never()).findByLocationIdIn(anyList());
	}

	@Test
	void getTalliesByEventNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByEvent(null));

		verify(tallyRepository, never()).findByLocationIdIn(anyList());
	}

	@Test
	void getTalliesByTimeslot() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var timeslotEntity = new TimeSlotEntity();
		timeslotEntity.setId(11L);
		timeslotEntity.setStart(Instant.now().minus(5L, ChronoUnit.SECONDS));
		timeslotEntity.setEnd(Instant.now().plus(5L, ChronoUnit.SECONDS));
		timeslotEntity.setEvent(eventEntity);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		eventEntity.setLocations(Set.of(locationEntity));

		when(timeSlotRepository.findById(timeslotEntity.getId())).thenReturn(Optional.of(timeslotEntity));

		when(tallyRepository.findByTimeBetween(timeslotEntity.getStart(), timeslotEntity.getEnd()))
				.thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTalliesByTimeSlot(timeslotEntity.getId());
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findByTimeBetween(any(), any());
	}

	@Test
	void getTalliesByTimeslotMissing() {
		long timeslotId = 11L;

		when(timeSlotRepository.findById(timeslotId)).thenReturn(Optional.empty());

		assertThrows(TimeSlotNotFoundException.class, () -> tallyAdapter.getTalliesByTimeSlot(timeslotId));

		verify(tallyRepository, never()).findByTimeBetween(any(), any());
	}

	@Test
	void getTalliesByTimeslotNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByEvent(null));

		verify(tallyRepository, never()).findByTimeBetween(any(), any());
	}

	@Test
	void getTalliesByTeam() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		eventEntity.setLocations(Set.of(locationEntity));

		when(teamRepository.existsById(teamEntity.getId())).thenReturn(true);

		when(tallyRepository.findByPlayerTeamId(teamEntity.getId()))
				.thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTalliesByTeam(teamEntity.getId());
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findByPlayerTeamId(teamEntity.getId());
	}

	@Test
	void getTalliesByTeamMissing() {
		long teamId = 11L;

		when(teamRepository.existsById(teamId)).thenReturn(false);

		assertThrows(TeamNotFoundException.class, () -> tallyAdapter.getTalliesByTeam(teamId));

		verify(tallyRepository, never()).findByPlayerTeamId(anyLong());
	}

	@Test
	void getTalliesByTeamNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByTeam(null));

		verify(tallyRepository, never()).findByPlayerTeamId(anyLong());
	}

	@Test
	void getTallies() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);

		when(tallyRepository.findAll()).thenReturn(Collections.singletonList(tallyEntity));

		List<Tally> tallies = tallyAdapter.getTallies();
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity.getId(), tallies.get(0).getId());

		verify(tallyRepository).findAll();
	}
	
	@Test
	void getTalliesByLocationAndEvent() {
		var productEntity1 = new ProductEntity();
		var productEntity2 = new ProductEntity();
		var tallyEntity1 = new TallyEntity();
		var tallyEntity2 = new TallyEntity();
		tallyEntity1.setId(9L);
		tallyEntity2.setId(10L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(4L);
		tallyEntity1.setLocation(locationEntity);
		tallyEntity2.setLocation(locationEntity);
		var playerEntity1 = new PlayerEntity();
		var playerEntity2 = new PlayerEntity();
		playerEntity1.setId(6L);
		playerEntity2.setId(7L);
		var teamEntity1 = new TeamEntity();
		var teamEntity2 = new TeamEntity();
		teamEntity1.setId(4L);
		teamEntity2.setId(5L);
		playerEntity1.setTeam(teamEntity1);
		playerEntity2.setTeam(teamEntity2);
		var eventEntity1 = new EventEntity();
		var eventEntity2 = new EventEntity();
		eventEntity1.setId(8L);
		eventEntity2.setId(9L);
		playerEntity1.setEvent(eventEntity1);
		playerEntity2.setEvent(eventEntity2);
		tallyEntity1.setPlayer(playerEntity1);
		tallyEntity2.setPlayer(playerEntity2);
		tallyEntity1.setProduct(productEntity1);
		tallyEntity2.setProduct(productEntity2);

		when(locationRepository.existsById(locationEntity.getId())).thenReturn(true);
		when(eventRepository.existsById(eventEntity1.getId())).thenReturn(true);

		when(tallyRepository.findByLocationId(locationEntity.getId())).thenReturn(List.of(tallyEntity1, tallyEntity2));
		
		var tallies = tallyAdapter.getTalliesByLocationAndEvent(locationEntity.getId(), eventEntity1.getId());
		
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity1.getId(), tallies.get(0).getId());
		assertEquals(tallyEntity1.getLocation().getId(), tallies.get(0).getLocation().getId());
		assertEquals(tallyEntity1.getPlayer().getId(), tallies.get(0).getPlayer().getId());
		assertEquals(tallyEntity1.getPlayer().getEvent().getId(), tallies.get(0).getPlayer().getEvent().getId());
		
		verify(tallyRepository).findByLocationId(locationEntity.getId());
	}

	@Test
	void getTalliesByLocationAndEventMissingLocation() {
		long locationId = 4L;
		long eventId = 8L;

		when(locationRepository.existsById(locationId)).thenReturn(false);

		assertThrows(LocationNotFoundException.class, () -> tallyAdapter.getTalliesByLocationAndEvent(locationId, eventId));

		verify(tallyRepository, never()).findByLocationId(anyLong());
	}

	@Test
	void getTalliesByLocationAndEventMissingEvent() {
		long locationId = 4L;
		long eventId = 8L;

		when(locationRepository.existsById(locationId)).thenReturn(true);
		when(eventRepository.existsById(eventId)).thenReturn(false);

		assertThrows(EventNotFoundException.class, () -> tallyAdapter.getTalliesByLocationAndEvent(locationId, eventId));

		verify(tallyRepository, never()).findByLocationId(anyLong());
	}

	@Test
	void getTalliesByLocationAndEventLocationNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByLocationAndEvent(null, 5L));

		verify(tallyRepository, never()).findByLocationId(anyLong());
	}

	@Test
	void getTalliesByLocationAndEventEventNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByLocationAndEvent(5L, null));

		verify(tallyRepository, never()).findByLocationId(anyLong());
	}
	
	@Test
	void getTalliesByProductAndEvent() {
		var tallyEntity1 = new TallyEntity();
		var tallyEntity2 = new TallyEntity();
		tallyEntity1.setId(9L);
		tallyEntity2.setId(10L);
		var productEntity = new ProductEntity();
		productEntity.setId(4L);
		tallyEntity1.setProduct(productEntity);
		tallyEntity2.setProduct(productEntity);
		var locationEntity = new LocationEntity();
		locationEntity.setId(4L);
		tallyEntity1.setLocation(locationEntity);
		tallyEntity2.setLocation(locationEntity);
		var playerEntity1 = new PlayerEntity();
		var playerEntity2 = new PlayerEntity();
		playerEntity1.setId(6L);
		playerEntity2.setId(7L);
		var teamEntity1 = new TeamEntity();
		var teamEntity2 = new TeamEntity();
		teamEntity1.setId(4L);
		teamEntity2.setId(5L);
		playerEntity1.setTeam(teamEntity1);
		playerEntity2.setTeam(teamEntity2);
		var eventEntity1 = new EventEntity();
		var eventEntity2 = new EventEntity();
		eventEntity1.setId(8L);
		eventEntity2.setId(9L);
		playerEntity1.setEvent(eventEntity1);
		playerEntity2.setEvent(eventEntity2);
		tallyEntity1.setPlayer(playerEntity1);
		tallyEntity2.setPlayer(playerEntity2);

		when(productRepository.existsById(productEntity.getId())).thenReturn(true);
		when(eventRepository.existsById(eventEntity1.getId())).thenReturn(true);

		when(tallyRepository.findByProductId(productEntity.getId())).thenReturn(List.of(tallyEntity1, tallyEntity2));
		
		var tallies = tallyAdapter.getTalliesByProductAndEvent(productEntity.getId(), eventEntity1.getId());
		
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity1.getId(), tallies.get(0).getId());
		assertEquals(tallyEntity1.getProduct().getId(), tallies.get(0).getProduct().getId());
		assertEquals(tallyEntity1.getPlayer().getId(), tallies.get(0).getPlayer().getId());
		assertEquals(tallyEntity1.getPlayer().getEvent().getId(), tallies.get(0).getPlayer().getEvent().getId());
		
		verify(tallyRepository).findByProductId(productEntity.getId());
	}

	@Test
	void getTalliesByProductAndEventMissingProduct() {
		long productId = 4L;
		long eventId = 8L;

		when(productRepository.existsById(productId)).thenReturn(false);

		assertThrows(ProductNotFoundException.class, () -> tallyAdapter.getTalliesByProductAndEvent(productId, eventId));

		verify(tallyRepository, never()).findByProductId(anyLong());
	}

	@Test
	void getTalliesByProductAndEventMissingEvent() {
		long productId = 4L;
		long eventId = 8L;

		when(productRepository.existsById(productId)).thenReturn(true);
		when(eventRepository.existsById(eventId)).thenReturn(false);

		assertThrows(EventNotFoundException.class, () -> tallyAdapter.getTalliesByProductAndEvent(productId, eventId));

		verify(tallyRepository, never()).findByProductId(anyLong());
	}

	@Test
	void getTalliesByProductAndEventProductNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByProductAndEvent(null, 5L));

		verify(tallyRepository, never()).findByProductId(anyLong());
	}

	@Test
	void getTalliesByProductAndEventEventNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByProductAndEvent(5L, null));

		verify(tallyRepository, never()).findByProductId(anyLong());
	}
	
	@Test
	void getTalliesByTeamAndEvent() {
		var productEntity1 = new ProductEntity();
		var productEntity2 = new ProductEntity();
		var tallyEntity1 = new TallyEntity();
		var tallyEntity2 = new TallyEntity();
		tallyEntity1.setId(9L);
		tallyEntity2.setId(10L);
		var playerEntity1 = new PlayerEntity();
		var playerEntity2 = new PlayerEntity();
		playerEntity1.setId(6L);
		playerEntity2.setId(7L);
		var teamEntity1 = new TeamEntity();
		var teamEntity2 = new TeamEntity();
		teamEntity1.setId(4L);
		teamEntity2.setId(5L);
		playerEntity1.setTeam(teamEntity1);
		playerEntity2.setTeam(teamEntity2);
		var eventEntity = new EventEntity();
		eventEntity.setId(8L);
		var locationEntity1 = new LocationEntity();
		var locationEntity2 = new LocationEntity();
		locationEntity1.setId(1L);
		locationEntity2.setId(2L);
		eventEntity.setLocations(Set.of(locationEntity1, locationEntity2));
		playerEntity1.setEvent(eventEntity);
		playerEntity2.setEvent(eventEntity);
		tallyEntity1.setPlayer(playerEntity1);
		tallyEntity2.setPlayer(playerEntity2);
		tallyEntity1.setLocation(locationEntity1);
		tallyEntity2.setLocation(locationEntity2);
		tallyEntity1.setProduct(productEntity1);
		tallyEntity2.setProduct(productEntity2);

		when(teamRepository.existsById(teamEntity1.getId())).thenReturn(true);
		when(eventRepository.existsById(eventEntity.getId())).thenReturn(true);

		when(eventRepository.findById(eventEntity.getId())).thenReturn(Optional.of(eventEntity));
		when(tallyRepository.findByLocationIdIn(anyList())).thenReturn(List.of(tallyEntity1, tallyEntity2));
		
		var tallies = tallyAdapter.getTalliesByTeamAndEvent(teamEntity1.getId(), eventEntity.getId());
		
		assertEquals(1, tallies.size());
		assertEquals(tallyEntity1.getId(), tallies.get(0).getId());
		assertEquals(tallyEntity1.getLocation().getId(), tallies.get(0).getLocation().getId());
		assertEquals(tallyEntity1.getPlayer().getId(), tallies.get(0).getPlayer().getId());
		assertEquals(tallyEntity1.getPlayer().getEvent().getId(), tallies.get(0).getPlayer().getEvent().getId());
		assertEquals(tallyEntity1.getPlayer().getTeam().getId(), tallies.get(0).getPlayer().getTeam().getId());
		
		verify(tallyRepository).findByLocationIdIn(anyList());
	}

	@Test
	void getTalliesByTeamAndEventMissingTeam() {
		long teamId = 4L;
		long eventId = 8L;

		when(teamRepository.existsById(teamId)).thenReturn(false);

		assertThrows(TeamNotFoundException.class, () -> tallyAdapter.getTalliesByTeamAndEvent(teamId, eventId));

		verify(tallyRepository, never()).findByLocationIdIn(anyList());
	}

	@Test
	void getTalliesByTeamAndEventMissingEvent() {
		long teamId = 4L;
		long eventId = 8L;

		when(teamRepository.existsById(teamId)).thenReturn(true);
		when(eventRepository.existsById(eventId)).thenReturn(false);

		assertThrows(EventNotFoundException.class, () -> tallyAdapter.getTalliesByTeamAndEvent(teamId, eventId));

		verify(tallyRepository, never()).findByLocationIdIn(anyList());
	}

	@Test
	void getTalliesByTeamAndEventTeamNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByTeamAndEvent(null, 5L));

		verify(tallyRepository, never()).findByLocationIdIn(anyList());
	}

	@Test
	void getTalliesByTeamAndEventEventNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.getTalliesByTeamAndEvent(5L, null));

		verify(tallyRepository, never()).findByLocationIdIn(anyList());
	}

	@Test
	void cancelTally() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);
		tallyEntity.setCancelled(Boolean.FALSE);

		when(tallyRepository.findById(tallyEntity.getId())).thenReturn(Optional.of(tallyEntity));

		tallyAdapter.cancelTally(tallyEntity.getId());

		assertTrue(tallyEntity.getCancelled());

		verify(tallyRepository).findById(tallyEntity.getId());
		verify(tallyRepository).save(any());
	}

	@Test
	void cancelTallyMissing() {
		long tallyId = 9L;

		when(tallyRepository.findById(tallyId)).thenReturn(Optional.empty());

		assertThrows(TallyNotFoundException.class, () -> tallyAdapter.cancelTally(tallyId));

		verify(tallyRepository).findById(anyLong());
		verify(tallyRepository, never()).save(any());
	}

	@Test
	void revokeCancellation() {
		var playerEntity = new PlayerEntity();
		playerEntity.setId(7L);
		var eventEntity = new EventEntity();
		eventEntity.setId(13L);
		playerEntity.setEvent(eventEntity);
		var teamEntity = new TeamEntity();
		teamEntity.setId(12L);
		playerEntity.setTeam(teamEntity);
		var productEntity = new ProductEntity();
		productEntity.setId(8L);
		var locationEntity = new LocationEntity();
		locationEntity.setId(10L);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		tallyEntity.setProduct(productEntity);
		tallyEntity.setLocation(locationEntity);
		tallyEntity.setCancelled(Boolean.TRUE);

		when(tallyRepository.findById(tallyEntity.getId())).thenReturn(Optional.of(tallyEntity));

		tallyAdapter.revokeCancellation(tallyEntity.getId());

		assertFalse(tallyEntity.getCancelled());

		verify(tallyRepository).findById(tallyEntity.getId());
		verify(tallyRepository).save(any());
	}

	@Test
	void revokeCancellationMissing() {
		long tallyId = 9L;

		when(tallyRepository.findById(tallyId)).thenReturn(Optional.empty());

		assertThrows(TallyNotFoundException.class, () -> tallyAdapter.revokeCancellation(tallyId));

		verify(tallyRepository).findById(anyLong());
		verify(tallyRepository, never()).save(any());
	}

	@Test
	void removeTally() {
		Long tallyId = 7L;
		tallyAdapter.removeTally(tallyId);
		verify(tallyRepository).deleteById(tallyId);
	}

	@Test
	void removeTalliesByProduct() {
		Long productId = 7L;
		when(productRepository.existsById(productId)).thenReturn(true);
		tallyAdapter.removeTalliesByProduct(productId);
		verify(tallyRepository).deleteByProductId(productId);
	}

	@Test
	void removeTalliesByProductMissing() {
		Long productId = 7L;
		when(productRepository.existsById(productId)).thenReturn(false);
		assertThrows(ProductNotFoundException.class, () -> tallyAdapter.removeTalliesByProduct(productId));
		verify(tallyRepository, never()).deleteByProductId(anyLong());
	}

	@Test
	void removeTalliesByProductNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.removeTalliesByProduct(null));
	}

	@Test
	void removeTalliesByPlayer() {
		Long playerId = 7L;
		when(playerRepository.existsById(playerId)).thenReturn(true);
		tallyAdapter.removeTalliesByPlayer(playerId);
		verify(tallyRepository).deleteByPlayerId(playerId);
	}

	@Test
	void removeTalliesByPlayerMissing() {
		Long playerId = 7L;
		when(playerRepository.existsById(playerId)).thenReturn(false);
		assertThrows(PlayerNotFoundException.class, () -> tallyAdapter.removeTalliesByPlayer(playerId));
		verify(tallyRepository, never()).deleteByPlayerId(anyLong());
	}

	@Test
	void removeTalliesByPlayerNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.removeTalliesByPlayer(null));
	}

	@Test
	void removeTalliesByLocation() {
		Long locationId = 7L;
		when(locationRepository.existsById(locationId)).thenReturn(true);
		tallyAdapter.removeTalliesByLocation(locationId);
		verify(tallyRepository).deleteByLocationId(locationId);
	}

	@Test
	void removeTalliesByLocationMissing() {
		Long locationId = 7L;
		when(locationRepository.existsById(locationId)).thenReturn(false);
		assertThrows(LocationNotFoundException.class, () -> tallyAdapter.removeTalliesByLocation(locationId));
		verify(tallyRepository, never()).deleteByLocationId(anyLong());
	}

	@Test
	void removeTalliesByLocationNull() {
		assertThrows(NullPointerException.class, () -> tallyAdapter.removeTalliesByLocation(null));
	}
}
