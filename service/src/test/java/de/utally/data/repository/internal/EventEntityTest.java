package de.utally.data.repository.internal;

import de.utally.data.model.Event;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class EventEntityTest {
    @Test
    void toEvent() {
        var eventEntity = new EventEntity();
        eventEntity.setId(5L);
        eventEntity.setLocations(Collections.emptySet());
        eventEntity.setTitle("title");
        eventEntity.setCreationTime(Instant.now().minusSeconds(5));
        eventEntity.setModificationTime(Instant.now().minusSeconds(2));
        eventEntity.setEnabled(true);

        Event event = eventEntity.toEvent();

        assertAll(
                () -> assertEquals(eventEntity.getId(), event.getId()),
                () -> assertEquals(eventEntity.getTitle(), event.getTitle()),
                () -> assertTrue(event.getLocations().isEmpty()),
                () -> assertEquals(eventEntity.getCreationTime(), event.getCreationTime()),
                () -> assertEquals(eventEntity.getModificationTime(), event.getModificationTime()),
                () -> assertTrue(event.getEnabled())
        );
    }

    @Test
    void toEvents() {
        var event = new EventEntity();
        event.setId(4L);
        var events = EventEntity.toEvents(Collections.singletonList(event));
        assertEquals(1, events.size());
        assertEquals(event.getId(), events.toArray(new Event[0])[0].getId());
    }

    @Test
    void fromEvent() {
        var event = new Event();
        event.setId(5L);
        event.setLocations(Collections.emptySet());
        event.setTitle("title");
        event.setCreationTime(Instant.now().minusSeconds(5));
        event.setModificationTime(Instant.now().minusSeconds(2));
        event.setEnabled(true);

        EventEntity eventEntity = EventEntity.fromEvent(event);

        assertAll(
                () -> assertEquals(eventEntity.getId(), event.getId()),
                () -> assertEquals(eventEntity.getTitle(), event.getTitle()),
                () -> assertTrue(eventEntity.getLocations().isEmpty()),
                () -> assertEquals(eventEntity.getCreationTime(), event.getCreationTime()),
                () -> assertEquals(eventEntity.getModificationTime(), event.getModificationTime()),
                () -> assertTrue(eventEntity.getEnabled())
        );
    }

    @Test
    void fromEvents() {
        var event = new Event();
        event.setId(4L);
        var events = EventEntity.fromEvents(Collections.singletonList(event));
        assertEquals(1, events.size());
        assertEquals(event.getId(), events.toArray(new EventEntity[0])[0].getId());
    }
}
