package de.utally.data.repository;

import de.utally.data.exception.EventNotFoundException;
import de.utally.data.exception.PlayerNotFoundException;
import de.utally.data.exception.PlayersNotOfSameEventException;
import de.utally.data.exception.TeamNotFoundException;
import de.utally.data.model.Player;
import de.utally.data.model.PlayerAdapter;
import de.utally.data.repository.internal.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PlayerRepositoryAdapterTest {
	@Mock
	PlayerRepository playerRepository;
	
	@Mock
	TeamRepository teamRepository;
	
	@Mock
	EventRepository eventRepository;
	
	@Mock
	TallyRepository tallyRepository;
	
	PlayerAdapter playerAdapter;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		playerAdapter = new PlayerRepositoryAdapter(playerRepository, teamRepository, eventRepository, tallyRepository);
		
		doAnswer((Answer<PlayerEntity>) invocation -> invocation.getArgument(0)).when(playerRepository).save(any(PlayerEntity.class));
	}

	@AfterEach
	void tearDown() {
		reset(playerRepository);
		reset(teamRepository);
		reset(eventRepository);
	}

	@Test
	void insertPlayer() {
		var teamId = 9L;
		var team = new TeamEntity();
		team.setId(teamId);
		team.setShortName("shortName");
		team.setFullName("fullName");
		team.setCity("city");
		var eventId = 7L;
		var event = new EventEntity();
		event.setId(eventId);
		event.setTitle("eventTitle");
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.of(team));
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(event));
		
		String name = "playerName";
		Player player = playerAdapter.insertPlayer(name, teamId, eventId, true);
		assertEquals(name, player.getName());
		assertNotNull(player.getTeam());
		assertEquals(Long.valueOf(teamId), player.getTeam().getId());
		assertNotNull(player.getEvent());
		assertEquals(Long.valueOf(eventId), player.getEvent().getId());
		assertTrue(player.getEnabled());
		assertNotNull(player.getCreationTime());
		assertNotNull(player.getModificationTime());
		assertEquals(Integer.valueOf(0), player.getPaidPlayersFee());
		
		verify(playerRepository).save(any());
	}
	
	@Test
	void insertPlayerWithMissingTeam() {
		var teamId = 9L;
		var eventId = 7L;
		var event = new EventEntity();
		event.setId(eventId);
		event.setTitle("eventTitle");
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.empty());
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(event));

		assertThrows(TeamNotFoundException.class, () -> playerAdapter.insertPlayer("playerName", teamId, eventId, true));
		
		verify(playerRepository, never()).save(any());
	}
	
	@Test
	void insertPlayerWithMissingEvent() {
		var teamId = 9L;
		var team = new TeamEntity();
		team.setId(teamId);
		team.setShortName("shortName");
		team.setFullName("fullName");
		team.setCity("city");
		var eventId = 7L;

		when(teamRepository.findById(teamId)).thenReturn(Optional.of(team));
		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> playerAdapter.insertPlayer("playerName", teamId, eventId, true));
		
		verify(playerRepository, never()).save(any());
	}

	@Test
	void insertPlayerWithTeamNull() {
		assertThrows(NullPointerException.class, () -> playerAdapter.insertPlayer("playerName", null, 5L, true));

		verify(playerRepository, never()).save(any());
	}

	@Test
	void insertPlayerWithEventNull() {
		assertThrows(NullPointerException.class, () -> playerAdapter.insertPlayer("playerName", 5L, null, true));

		verify(playerRepository, never()).save(any());
	}

	@Test
	void existsByIdTrue() {
		when(playerRepository.existsById(anyLong())).thenReturn(true);
		assertTrue(playerAdapter.existsById(5L));
	}

	@Test
	void existsByIdFalse() {
		when(playerRepository.existsById(anyLong())).thenReturn(false);
		assertFalse(playerAdapter.existsById(5L));
	}
	
	@Test
	void updatePlayer() {
		Long id = 7L;
		String name = "playerName";
		var player = new Player();
		player.setId(id);
		player.setName(name);
		player.setEnabled(true);
		player.setComment("comment");
		player.setPreferences(Map.of("key", "value"));
		
		var playerEntity = new PlayerEntity();
		playerEntity.setId(id);
		playerEntity.setName("name2");
		playerEntity.setEnabled(false);
		
		when(playerRepository.findById(id)).thenReturn(Optional.of(playerEntity));
		
		doAnswer((Answer<Void>) invocation -> {
			PlayerEntity p = invocation.getArgument(0);
			assertEquals(player.getName(), p.getName());
			assertEquals(player.getComment(), p.getComment());
			assertEquals(player.getPreferences(), p.getPreferences());
			assertTrue(p.getEnabled());
			assertNotNull(p.getModificationTime());
			return null;
		}).when(playerRepository).save(any());
		
		playerAdapter.updatePlayer(player);
		
		verify(playerRepository).save(any());
	}
	
	@Test
	void updateMissingPlayer() {
		Long id = 7L;
		String name = "playerName";
		var player = new Player();
		player.setId(id);
		player.setName(name);
		player.setEnabled(true);
		
		when(playerRepository.findById(id)).thenReturn(Optional.empty());
		
		assertThrows(PlayerNotFoundException.class, () -> playerAdapter.updatePlayer(player));
		
		verify(playerRepository, never()).save(any());
	}

	@Test
	void updatePlayerNull() {
		assertThrows(NullPointerException.class, () -> playerAdapter.updatePlayer(null));

		verify(playerRepository, never()).save(any());
	}

	@Test
	void getPlayersByTeamAndEvent() {
		Long eventId = 8L;
		Long teamId = 9L;
		Long playerId = 7L;
		PlayerEntity playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		var teamEntity = new TeamEntity();
		teamEntity.setId(5L);
		playerEntity.setTeam(teamEntity);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		playerEntity.setEvent(eventEntity);

		when(teamRepository.existsById(teamId)).thenReturn(true);
		when(eventRepository.existsById(eventId)).thenReturn(true);

		when(playerRepository.findByTeamIdAndEventId(teamId, eventId)).thenReturn(Collections.singletonList(playerEntity));
		
		List<Player> players = playerAdapter.getPlayersByTeamAndEvent(teamId, eventId);
		assertEquals(1, players.size());
		assertEquals(playerId, players.get(0).getId());
		
		verify(playerRepository).findByTeamIdAndEventId(teamId, eventId);
	}

	@Test
	void getPlayersByTeamAndEventMissingTeam() {
		Long eventId = 8L;
		Long teamId = 9L;

		when(teamRepository.existsById(teamId)).thenReturn(false);

		assertThrows(TeamNotFoundException.class, () -> playerAdapter.getPlayersByTeamAndEvent(teamId, eventId));

		verify(playerRepository, never()).findByTeamIdAndEventId(anyLong(), anyLong());
	}

	@Test
	void getPlayersByTeamAndEventMissingEvent() {
		Long eventId = 8L;
		Long teamId = 9L;

		when(teamRepository.existsById(teamId)).thenReturn(true);
		when(eventRepository.existsById(eventId)).thenReturn(false);

		assertThrows(EventNotFoundException.class, () -> playerAdapter.getPlayersByTeamAndEvent(teamId, eventId));

		verify(playerRepository, never()).findByTeamIdAndEventId(anyLong(), anyLong());
	}

	@Test
	void getPlayersByTeamAndEventNullTeam() {
		assertThrows(NullPointerException.class, () -> playerAdapter.getPlayersByTeamAndEvent(null, 5L));

		verify(playerRepository, never()).findByTeamIdAndEventId(anyLong(), anyLong());
	}

	@Test
	void getPlayersByTeamAndEventNullEvent() {
		assertThrows(NullPointerException.class, () -> playerAdapter.getPlayersByTeamAndEvent(5L, null));

		verify(playerRepository, never()).findByTeamIdAndEventId(anyLong(), anyLong());
	}
	
	@Test
	void getPlayersByTeamsAndEvent() {
		Long eventId = 8L;
		Long teamId = 9L;
		Long playerId = 7L;
		PlayerEntity playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		var teamEntity = new TeamEntity();
		teamEntity.setId(5L);
		playerEntity.setTeam(teamEntity);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		playerEntity.setEvent(eventEntity);

		when(eventRepository.existsById(eventId)).thenReturn(true);

		when(playerRepository.findByTeamIdInAndEventId(List.of(teamId), eventId)).thenReturn(Collections.singletonList(playerEntity));
		
		List<Player> players = playerAdapter.getPlayersByTeamsAndEvent(List.of(teamId), eventId);
		assertEquals(1, players.size());
		assertEquals(playerId, players.get(0).getId());
		
		verify(playerRepository).findByTeamIdInAndEventId(List.of(teamId), eventId);
	}

	@Test
	void getPlayersByTeamsAndEventMissingEvent() {
		Long eventId = 8L;
		Long teamId = 9L;

		when(eventRepository.existsById(eventId)).thenReturn(false);

		var teams = List.of(teamId);
		assertThrows(EventNotFoundException.class, () -> playerAdapter.getPlayersByTeamsAndEvent(teams, eventId));

		verify(playerRepository, never()).findByTeamIdInAndEventId(anyList(), anyLong());
	}

	@Test
	void getPlayersByTeamsAndEventNullTeams() {
		assertThrows(NullPointerException.class, () -> playerAdapter.getPlayersByTeamsAndEvent(null, 5L));

		verify(playerRepository, never()).findByTeamIdInAndEventId(anyList(), anyLong());
	}

	@Test
	void getPlayersByTeamsAndEventNullEvent() {
		var teams = List.of(5L);
		assertThrows(NullPointerException.class, () -> playerAdapter.getPlayersByTeamsAndEvent(teams, null));

		verify(playerRepository, never()).findByTeamIdInAndEventId(anyList(), anyLong());
	}
	
	@Test
	void getPlayersByEvent() {
		Long eventId = 8L;
		Long playerId = 7L;
		PlayerEntity playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		var teamEntity = new TeamEntity();
		teamEntity.setId(5L);
		playerEntity.setTeam(teamEntity);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		playerEntity.setEvent(eventEntity);

		when(eventRepository.existsById(eventId)).thenReturn(true);

		when(playerRepository.findByEventId(eventId)).thenReturn(Collections.singletonList(playerEntity));
		
		List<Player> players = playerAdapter.getPlayersByEvent(eventId);
		assertEquals(1, players.size());
		assertEquals(playerId, players.get(0).getId());
		
		verify(playerRepository).findByEventId(eventId);
	}

	@Test
	void getPlayersByEventMissing() {
		Long eventId = 8L;

		when(eventRepository.existsById(eventId)).thenReturn(false);

		assertThrows(EventNotFoundException.class, () -> playerAdapter.getPlayersByEvent(eventId));

		verify(playerRepository, never()).findByEventId(anyLong());
	}

	@Test
	void getPlayersByEventNull() {
		assertThrows(NullPointerException.class, () -> playerAdapter.getPlayersByEvent(null));

		verify(playerRepository, never()).findByEventId(anyLong());
	}
	
	@Test
	void getPlayersByTeam() {
		Long teamId = 9L;
		Long playerId = 7L;
		PlayerEntity playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		var teamEntity = new TeamEntity();
		teamEntity.setId(5L);
		playerEntity.setTeam(teamEntity);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		playerEntity.setEvent(eventEntity);

		when(teamRepository.existsById(teamId)).thenReturn(true);

		when(playerRepository.findByTeamId(teamId)).thenReturn(Collections.singletonList(playerEntity));
		
		List<Player> players = playerAdapter.getPlayersByTeam(teamId);
		assertEquals(1, players.size());
		assertEquals(playerId, players.get(0).getId());
		
		verify(playerRepository).findByTeamId(teamId);
	}

	@Test
	void getPlayersByTeamMissing() {
		Long teamId = 9L;

		when(teamRepository.existsById(teamId)).thenReturn(false);

		assertThrows(TeamNotFoundException.class, () -> playerAdapter.getPlayersByTeam(teamId));

		verify(playerRepository, never()).findByTeamId(anyLong());
	}

	@Test
	void getPlayersByTeamNull() {
		assertThrows(NullPointerException.class, () -> playerAdapter.getPlayersByTeam(null));

		verify(playerRepository, never()).findByTeamId(anyLong());
	}
	
	@Test
	void getPlayers() {
		Long playerId = 7L;
		var playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		var teamEntity = new TeamEntity();
		teamEntity.setId(5L);
		playerEntity.setTeam(teamEntity);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		playerEntity.setEvent(eventEntity);
		when(playerRepository.findAll()).thenReturn(Collections.singletonList(playerEntity));
		
		List<Player> players = playerAdapter.getPlayers();
		assertEquals(1, players.size());
		assertEquals(playerId, players.get(0).getId());
		
		verify(playerRepository).findAll();
	}
	
	@Test
	void getEnabledPlayers() {
		Long playerId = 7L;
		var playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		var teamEntity = new TeamEntity();
		teamEntity.setId(5L);
		playerEntity.setTeam(teamEntity);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		playerEntity.setEvent(eventEntity);
		when(playerRepository.findByEnabled(true)).thenReturn(Collections.singletonList(playerEntity));
		
		List<Player> players = playerAdapter.getEnabledPlayers();
		assertEquals(1, players.size());
		assertEquals(playerId, players.get(0).getId());
		
		verify(playerRepository).findByEnabled(true);
	}
	
	@Test
	void getPlayerById() {
		Long playerId = 7L;
		var playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		var teamEntity = new TeamEntity();
		teamEntity.setId(5L);
		playerEntity.setTeam(teamEntity);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		playerEntity.setEvent(eventEntity);
		when(playerRepository.findById(playerId)).thenReturn(Optional.of(playerEntity));
		
		Player player = playerAdapter.getPlayerById(playerId);
		
		assertEquals(playerId, player.getId());
	}

	@Test
	void getPlayerByIdMissing() {
		Long playerId = 7L;
		when(playerRepository.findById(playerId)).thenReturn(Optional.empty());

		assertThrows(PlayerNotFoundException.class, () -> playerAdapter.getPlayerById(playerId));
	}

	@Test
	void getPlayerByIdNull() {
		assertThrows(NullPointerException.class, () -> playerAdapter.getPlayerById(null));
	}
	
	@Test
	void pay() {
		Long playerId = 7L;
		Integer paidPlayersFee = 100;
		var playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		when(playerRepository.findById(playerId)).thenReturn(Optional.of(playerEntity));
		playerEntity.setPaid(Boolean.FALSE);
		playerEntity.setWithPlayersFee(Boolean.FALSE);
		playerEntity.setComment("");
		
		String comment = "comment";
		
		playerAdapter.pay(playerId, comment, Boolean.TRUE, paidPlayersFee);

		assertThat(playerEntity.getPayTime()).isNotNull();
		assertTrue(playerEntity.getPaid());
		assertTrue(playerEntity.getWithPlayersFee());
		assertEquals(comment, playerEntity.getComment());
		assertEquals(paidPlayersFee, playerEntity.getPaidPlayersFee());
		assertNotNull(playerEntity.getModificationTime());
		
		verify(playerRepository).findById(anyLong());
		verify(playerRepository).save(any());
	}

	@Test
	void payMissingPlayer() {
		Long playerId = 7L;
		Integer paidPlayersFee = 100;

		when(playerRepository.findById(playerId)).thenReturn(Optional.empty());

		assertThrows(PlayerNotFoundException.class , () -> playerAdapter.pay(playerId, "comment", Boolean.TRUE, paidPlayersFee));

		verify(playerRepository).findById(anyLong());
		verify(playerRepository, never()).save(any());
	}

	@Test
	void payNullPlayer() {
		assertThrows(NullPointerException.class , () -> playerAdapter.pay(null, "comment", Boolean.TRUE, 100));

		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).save(any());
	}

	@Test
	void payNullComment() {
		assertThrows(NullPointerException.class , () -> playerAdapter.pay(5L, null, Boolean.TRUE, 100));

		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).save(any());
	}

	@Test
	void payNullWithPlayersFee() {
		assertThrows(NullPointerException.class , () -> playerAdapter.pay(5L, "comment", null, 100));

		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).save(any());
	}

	@Test
	void payNullWithPaidPlayersFee() {
		assertThrows(NullPointerException.class , () -> playerAdapter.pay(5L, "comment", Boolean.TRUE, null));

		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).save(any());
	}
	
	@Test
	void revertPayment() {
		String comment = "comment";
		Long playerId = 7L;
		var playerEntity = new PlayerEntity();
		playerEntity.setId(playerId);
		when(playerRepository.findById(playerId)).thenReturn(Optional.of(playerEntity));
		playerEntity.setPaid(Boolean.TRUE);
		playerEntity.setWithPlayersFee(Boolean.TRUE);
		playerEntity.setComment(comment);
		playerEntity.setPaidPlayersFee(100);
		
		playerAdapter.revertPayment(playerId);

		assertThat(playerEntity.getPayTime()).isNull();
		assertFalse(playerEntity.getPaid());
		assertFalse(playerEntity.getWithPlayersFee());
		assertEquals(comment, playerEntity.getComment());
		assertEquals(Integer.valueOf(0), playerEntity.getPaidPlayersFee());
		assertNotNull(playerEntity.getModificationTime());
		
		verify(playerRepository).findById(anyLong());
		verify(playerRepository).save(any());
	}

	@Test
	void revertPaymentMissing() {
		Long playerId = 7L;

		when(playerRepository.findById(playerId)).thenReturn(Optional.empty());

		assertThrows(PlayerNotFoundException.class, () -> playerAdapter.revertPayment(playerId));

		verify(playerRepository).findById(anyLong());
		verify(playerRepository, never()).save(any());
	}

	@Test
	void revertPaymentNull() {
		assertThrows(NullPointerException.class, () -> playerAdapter.revertPayment(null));

		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).save(any());
	}
	
	@Test
	void removePlayer() {
		Long playerId = 7L;
		playerAdapter.removePlayer(playerId);
		verify(playerRepository).deleteById(playerId);
		verify(tallyRepository).deleteByPlayerId(playerId);
	}

	@Test
	void removePlayerNull() {
		assertThrows(NullPointerException.class, () -> playerAdapter.removePlayer(null));
		verify(playerRepository, never()).deleteById(anyLong());
		verify(tallyRepository, never()).deleteByPlayerId(anyLong());
	}
	
	@Test
	void mergePlayerIntoPlayer() {
		Long eventId = 10L;
		var event = new EventEntity();
		event.setId(eventId);

		Long playerId1 = 7L;
		var playerEntity1 = new PlayerEntity();
		playerEntity1.setId(playerId1);
		playerEntity1.setEvent(event);
		var teamEntity1 = new TeamEntity();
		teamEntity1.setId(5L);
		playerEntity1.setTeam(teamEntity1);
		
		Long playerId2 = 8L;
		var playerEntity2 = new PlayerEntity();
		playerEntity2.setId(playerId2);
		playerEntity2.setEvent(event);
		var teamEntity2 = new TeamEntity();
		teamEntity2.setId(15L);
		playerEntity2.setTeam(teamEntity2);
		
		when(playerRepository.findById(playerId1)).thenReturn(Optional.of(playerEntity1));
		when(playerRepository.findById(playerId2)).thenReturn(Optional.of(playerEntity2));
		
		var tally1 = new TallyEntity();
		tally1.setId(5L);
		tally1.setPlayer(playerEntity1);
		var tally2 = new TallyEntity();
		tally2.setId(6L);
		tally2.setPlayer(playerEntity2);
		
		when(tallyRepository.findByPlayerId(playerEntity1.getId())).thenReturn(List.of(tally1, tally2));
		
		doAnswer((Answer<Iterable<TallyEntity>>) invocation -> {
			boolean anyTally = false;
			Iterable<TallyEntity> tallies = invocation.getArgument(0);
			for(TallyEntity tally : tallies) {
				anyTally = true;
				assertEquals(playerEntity2.getId(), tally.getPlayer().getId());
			}
			assertTrue(anyTally);
			return tallies;
		}).when(tallyRepository).saveAll(anyIterable());
		
		playerAdapter.mergePlayerIntoPlayer(playerId1, playerId2);
		
		verify(playerRepository).findById(playerId1);
		verify(playerRepository).findById(playerId2);
		verify(playerRepository).delete(playerEntity1);
		verify(tallyRepository).saveAll(anyIterable());
	}

	@Test
	void mergePlayerIntoPlayerNotOfSameEvent() {
		Long eventId1 = 10L;
		var event1 = new EventEntity();
		event1.setId(eventId1);

		Long eventId2 = 11L;
		var event2 = new EventEntity();
		event2.setId(eventId2);

		Long playerId1 = 7L;
		var playerEntity1 = new PlayerEntity();
		playerEntity1.setId(playerId1);
		playerEntity1.setEvent(event1);

		Long playerId2 = 8L;
		var playerEntity2 = new PlayerEntity();
		playerEntity2.setId(playerId2);
		playerEntity2.setEvent(event2);

		when(playerRepository.findById(playerId1)).thenReturn(Optional.of(playerEntity1));
		when(playerRepository.findById(playerId2)).thenReturn(Optional.of(playerEntity2));

		assertThrows(PlayersNotOfSameEventException.class,() -> playerAdapter.mergePlayerIntoPlayer(playerId1, playerId2));

		verify(playerRepository).findById(playerId1);
		verify(playerRepository).findById(playerId2);
		verify(playerRepository, never()).delete(any());
		verify(tallyRepository, never()).saveAll(anyIterable());
	}
	
	@Test
	void mergePlayerIntoPlayerMissingSource() {
		Long playerId1 = 7L;
		
		Long playerId2 = 8L;
		var playerEntity2 = new PlayerEntity();
		playerEntity2.setId(playerId2);
		
		when(playerRepository.findById(playerId1)).thenReturn(Optional.empty());
		when(playerRepository.findById(playerId2)).thenReturn(Optional.of(playerEntity2));

		assertThrows(PlayerNotFoundException.class, () -> playerAdapter.mergePlayerIntoPlayer(playerId1, playerId2));
		
		verify(playerRepository).findById(playerId1);
		verify(playerRepository, never()).findById(playerId2);
		verify(playerRepository, never()).delete(any());
		verify(tallyRepository, never()).saveAll(anyIterable());
	}
	
	@Test
	void mergePlayerIntoPlayerMissingDestination() {
		Long playerId1 = 7L;
		var playerEntity1 = new PlayerEntity();
		playerEntity1.setId(playerId1);
		
		Long playerId2 = 8L;
		
		when(playerRepository.findById(playerId1)).thenReturn(Optional.of(playerEntity1));
		when(playerRepository.findById(playerId2)).thenReturn(Optional.empty());

		assertThrows(PlayerNotFoundException.class, () -> playerAdapter.mergePlayerIntoPlayer(playerId1, playerId2));
		
		verify(playerRepository).findById(playerId1);
		verify(playerRepository).findById(playerId2);
		verify(playerRepository, never()).delete(any());
		verify(tallyRepository, never()).saveAll(anyIterable());
	}

	@Test
	void mergePlayerIntoPlayerNullSource() {
		assertThrows(NullPointerException.class, () -> playerAdapter.mergePlayerIntoPlayer( null, 5L));

		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).delete(any());
		verify(tallyRepository, never()).saveAll(anyIterable());
	}

	@Test
	void mergePlayerIntoPlayerNullDestination() {
		assertThrows(NullPointerException.class, () -> playerAdapter.mergePlayerIntoPlayer(5L, null));

		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).findById(anyLong());
		verify(playerRepository, never()).delete(any());
		verify(tallyRepository, never()).saveAll(anyIterable());
	}
}
