package de.utally.data.repository;

import de.utally.data.exception.EventNotFoundException;
import de.utally.data.exception.TimeSlotNotFoundException;
import de.utally.data.model.TimeSlot;
import de.utally.data.repository.internal.*;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TimeSlotRepositoryAdapterTest {

	private static final EasyRandom EASY_RANDOM = new EasyRandom();

	@Mock
	TimeSlotRepository repository;

	@Mock
	EventRepository eventRepository;

	@Mock
	TallyRepository tallyRepository;
	
	TimeSlotRepositoryAdapter repositoryAdapter;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		repositoryAdapter = new TimeSlotRepositoryAdapter(repository, eventRepository, tallyRepository);
		
		doAnswer((Answer<TimeSlotEntity>) invocation -> invocation.getArgument(0)).when(repository).save(any(TimeSlotEntity.class));
	}

	@AfterEach
	void tearDown() {
		reset(repository, eventRepository);
	}

	@Test
	void insertTimeSlot() {
		var event = new EventEntity();
		event.setId(7L);
		when(eventRepository.findById(event.getId())).thenReturn(Optional.of(event));

		String label = "label";
		Instant start = Instant.now();
		Instant end = start.plusMillis(2000);
		String description = "description";
		
		var timeSlot = repositoryAdapter.insertTimeSlot(label, description, start, end, true, event.getId());

		assertEquals(label, timeSlot.getLabel());
		assertEquals(description, timeSlot.getDescription());
		assertEquals(start, timeSlot.getStart());
		assertEquals(end, timeSlot.getEnd());
		assertTrue(timeSlot.getEnabled());
		assertNotNull(timeSlot.getCreationTime());
		assertNotNull(timeSlot.getModificationTime());
		assertNotNull(timeSlot.getEvent());
		
		verify(repository).save(any());
	}

	@Test
	void insertTimeSlotMissingEvent() {
		var event = new EventEntity();
		long eventId = 7L;
		event.setId(eventId);
		when(eventRepository.findById(event.getId())).thenReturn(Optional.empty());

		String label = "label";
		Instant start = Instant.now();
		Instant end = start.plusMillis(2000);
		String description = "description";

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.insertTimeSlot(label, description, start, end, true, eventId));

		verify(repository, never()).save(any());
	}

	@Test
	void insertTimeSlotNull() {
		String label = "label";
		Instant start = Instant.now();
		Instant end = start.plusMillis(2000);
		String description = "description";

		assertThrows(NullPointerException.class, () -> repositoryAdapter.insertTimeSlot(label, description, start, end, true, null));

		verify(repository, never()).save(any());
	}
	
	@Test
	void updateTimeSlot() {
		Long id = 5L;
		String label = "label";
		Instant start = Instant.now();
		Instant end = start.plusMillis(2000);
		
		var timeSlot = new TimeSlot();
		timeSlot.setId(id);
		timeSlot.setEnd(end);
		timeSlot.setStart(start);
		timeSlot.setLabel(label);
		timeSlot.setEnabled(true);
		timeSlot.setDescription("description");
		
		var timeSlotEntity = new TimeSlotEntity();
		timeSlotEntity.setId(id);
		timeSlotEntity.setLabel("label2");
		timeSlotEntity.setStart(Instant.now());
		timeSlotEntity.setEnd(Instant.now());
		timeSlotEntity.setEnabled(false);
		
		when(repository.findById(id)).thenReturn(Optional.of(timeSlotEntity));
		
		doAnswer((Answer<Void>) invocation -> {
			TimeSlotEntity t = invocation.getArgument(0);
			assertEquals(timeSlot.getEnd(), t.getEnd());
			assertEquals(timeSlot.getStart(), t.getStart());
			assertEquals(timeSlot.getLabel(), t.getLabel());
			assertEquals(timeSlot.getDescription(), t.getDescription());
			assertTrue(t.getEnabled());
			assertNotNull(t.getModificationTime());
			return null;
		}).when(repository).save(any());
		
		repositoryAdapter.updateTimeSlot(timeSlot);
		
		verify(repository).save(any());
	}
	
	@Test
	void updateMissingTimeSlot() {
		Long id = 5L;
		String label = "label";
		Instant start = Instant.now();
		Instant end = start.plusMillis(2000);
		
		var timeSlot = new TimeSlot();
		timeSlot.setId(id);
		timeSlot.setEnd(end);
		timeSlot.setStart(start);
		timeSlot.setLabel(label);
		
		when(repository.findById(id)).thenReturn(Optional.empty());

		assertThrows(TimeSlotNotFoundException.class, () -> repositoryAdapter.updateTimeSlot(timeSlot));
		
		verify(repository, never()).save(any());
	}

	@Test
	void updateTimeSlotNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.updateTimeSlot(null));

		verify(repository, never()).save(any());
	}
	
	@Test
	void removeTimeSlot() {
		Long timeSlotId = 5L;
		repositoryAdapter.removeTimeSlot(timeSlotId);
		verify(repository).deleteById(timeSlotId);
	}

	@Test
	void removeTimeSlotNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeTimeSlot(null));
		verify(repository, never()).deleteById(anyLong());
	}

	@Test
	void getTimeSlots() {
		Long timeSlotId = 5L;
		var timeSlotEntity = new TimeSlotEntity();
		timeSlotEntity.setId(timeSlotId);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		timeSlotEntity.setEvent(eventEntity);
		
		when(repository.findAll()).thenReturn(Collections.singletonList(timeSlotEntity));
		
		var timeSlots = repositoryAdapter.getTimeSlots();
		
		assertEquals(1, timeSlots.size());
		assertEquals(timeSlotId, timeSlots.get(0).getId());
	}

	@Test
	void getTimeSlotsByEvent() {
		long eventId = 7L;
		Long timeSlotId = 5L;
		var timeSlotEntity = new TimeSlotEntity();
		timeSlotEntity.setId(timeSlotId);
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		timeSlotEntity.setEvent(eventEntity);

		when(eventRepository.existsById(eventId)).thenReturn(true);
		when(repository.findByEventIdOrderByStart(eventId)).thenReturn(Collections.singletonList(timeSlotEntity));

		var timeSlots = repositoryAdapter.getTimeSlotsByEvent(eventId);

		assertEquals(1, timeSlots.size());
		assertEquals(timeSlotId, timeSlots.get(0).getId());
	}

	@Test
	void getTimeSlotsByEventMissing() {
		long eventId = 7L;

		when(eventRepository.existsById(eventId)).thenReturn(false);

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.getTimeSlotsByEvent(eventId));
	}

	@Test
	void getTimeSlotsByEventNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getTimeSlotsByEvent(null));
	}
	
	@Test
	void getEnabledTimeSlots() {
		Long timeSlotId = 5L;
		var timeSlotEntity = new TimeSlotEntity();
		timeSlotEntity.setId(timeSlotId);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		timeSlotEntity.setEvent(eventEntity);
		
		when(repository.findByEnabled(true)).thenReturn(Collections.singletonList(timeSlotEntity));
		
		var timeSlots = repositoryAdapter.getEnabledTimeSlots();

		assertEquals(1, timeSlots.size());
		assertEquals(timeSlotId, timeSlots.get(0).getId());
		
		verify(repository).findByEnabled(true);
	}
	
	@Test
	void getTimeSlotById() {
		Long timeSlotId = 5L;
		var timeSlotEntity = new TimeSlotEntity();
		timeSlotEntity.setId(timeSlotId);
		var eventEntity = new EventEntity();
		eventEntity.setId(6L);
		timeSlotEntity.setEvent(eventEntity);
		
		when(repository.findById(timeSlotId)).thenReturn(Optional.of(timeSlotEntity));
		
		var timeSlot = repositoryAdapter.getTimeSlotById(timeSlotId);
		
		assertEquals(timeSlotId, timeSlot.getId());
	}

	@Test
	void getTimeSlotByIdMissing() {
		Long timeSlotId = 5L;

		when(repository.findById(timeSlotId)).thenReturn(Optional.empty());

		assertThrows(TimeSlotNotFoundException.class, () -> repositoryAdapter.getTimeSlotById(timeSlotId));
	}

	@Test
	void getTimeSlotByIdNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getTimeSlotById(null));
	}

	@Test
	void getNumberOfTalliesByTeamInEventForStatistics() {
		var p1 = EASY_RANDOM.nextObject(ProductEntity.class);
		var p2 = EASY_RANDOM.nextObject(ProductEntity.class);
		var l = EASY_RANDOM.nextObject(LocationEntity.class);
		var t1 = EASY_RANDOM.nextObject(TeamEntity.class);
		var t2 = EASY_RANDOM.nextObject(TeamEntity.class);
		var e = EASY_RANDOM.nextObject(EventEntity.class);
		var timeSlot = EASY_RANDOM.nextObject(TimeSlotEntity.class);
		p1.setDisplayStatistic(true);
		p2.setDisplayStatistic(true);
		l.setProducts(Set.of(p1, p2));
		e.setTeams(Set.of(t1, t2));
		e.setLocations(Set.of(l));
		timeSlot.setEvent(e);
		timeSlot.setStart(Instant.now().minus(5L, ChronoUnit.HOURS));
		timeSlot.setEnd(Instant.now().plus(5L, ChronoUnit.HOURS));

		when(repository.findById(timeSlot.getId())).thenReturn(Optional.of(timeSlot));
		when(tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalseAndTimeBetween(t1.getId(), p1.getId(), timeSlot.getStart(), timeSlot.getEnd())).thenReturn(4L);
		when(tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalseAndTimeBetween(t2.getId(), p1.getId(), timeSlot.getStart(), timeSlot.getEnd())).thenReturn(6L);
		when(tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalseAndTimeBetween(t1.getId(), p2.getId(), timeSlot.getStart(), timeSlot.getEnd())).thenReturn(8L);
		when(tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalseAndTimeBetween(t2.getId(), p2.getId(), timeSlot.getStart(), timeSlot.getEnd())).thenReturn(9L);

		var result = repositoryAdapter.getNumberOfTalliesByTeamInTimeSlotForStatistics(timeSlot.getId());

		assertThat(result)
				.hasSize(2)
				.containsEntry(p1.getId(), Map.of(t1.getId(), 4L, t2.getId(), 6L))
				.containsEntry(p2.getId(), Map.of(t1.getId(), 8L, t2.getId(), 9L));
	}

	@Test
	void getNumberOfTalliesByTeamInEventForStatisticsEventNotFound() {
		when(repository.findById(anyLong())).thenReturn(Optional.empty());

		assertThatThrownBy(() -> repositoryAdapter.getNumberOfTalliesByTeamInTimeSlotForStatistics(5L)).isInstanceOf(TimeSlotNotFoundException.class);
	}

	@Test
	void getNumberOfTalliesByTeamInEventForStatisticsNull() {
		assertThatThrownBy(() -> repositoryAdapter.getNumberOfTalliesByTeamInTimeSlotForStatistics(null)).isInstanceOf(NullPointerException.class);
	}
}
