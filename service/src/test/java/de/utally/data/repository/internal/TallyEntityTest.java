package de.utally.data.repository.internal;

import de.utally.data.model.*;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class TallyEntityTest {
    @Test
    void toTally() {
        TallyEntity tallyEntity = new TallyEntity();
        tallyEntity.setCancelled(Boolean.TRUE);
        tallyEntity.setId(6L);
        tallyEntity.setPlayer(null);
        tallyEntity.setProduct(null);
        tallyEntity.setLocation(null);
        tallyEntity.setTime(Instant.now());
        var event = new EventEntity();
        var team = new TeamEntity();
        var player = new PlayerEntity();
        var product = new ProductEntity();
        var location = new LocationEntity();
        event.setId(7L);
        team.setId(8L);
        player.setId(9L);
        product.setId(10L);
        location.setId(11L);
        player.setEvent(event);
        player.setTeam(team);
        tallyEntity.setPlayer(player);
        tallyEntity.setProduct(product);
        tallyEntity.setLocation(location);

        Tally tally = tallyEntity.toTally();

        assertAll(
                () -> assertEquals(tallyEntity.getCancelled(), tally.getCancelled()),
                () -> assertEquals(tallyEntity.getId(), tally.getId()),
                () -> assertNotNull(tally.getPlayer()),
                () -> assertNotNull(tally.getProduct()),
                () -> assertNotNull(tally.getLocation()),
                () -> assertEquals(player.getId(), tally.getPlayer().getId()),
                () -> assertEquals(product.getId(), tally.getProduct().getId()),
                () -> assertEquals(location.getId(), tally.getLocation().getId()),
                () -> assertEquals(tallyEntity.getTime(), tally.getTime())
        );
    }

    @Test
    void toTallies() {
        var tally = new TallyEntity();
        tally.setId(4L);
        var event = new EventEntity();
        var team = new TeamEntity();
        var player = new PlayerEntity();
        var product = new ProductEntity();
        var location = new LocationEntity();
        event.setId(7L);
        team.setId(8L);
        player.setId(9L);
        product.setId(10L);
        location.setId(11L);
        player.setEvent(event);
        player.setTeam(team);
        tally.setPlayer(player);
        tally.setProduct(product);
        tally.setLocation(location);
        var tallies = TallyEntity.toTallies(Collections.singletonList(tally));
        assertEquals(1, tallies.size());
        assertEquals(tally.getId(), tallies.toArray(new Tally[0])[0].getId());
    }

    @Test
    void fromTally() {
        Tally tally = new Tally();
        tally.setCancelled(Boolean.TRUE);
        tally.setId(6L);
        tally.setTime(Instant.now());
        var event = new Event();
        var team = new Team();
        var player = new Player();
        var product = new Product();
        var location = new Location();
        event.setId(7L);
        team.setId(8L);
        player.setId(9L);
        product.setId(10L);
        location.setId(11L);
        player.setEvent(event);
        player.setTeam(team);
        tally.setPlayer(player);
        tally.setProduct(product);
        tally.setLocation(location);

        TallyEntity tallyEntity = TallyEntity.fromTally(tally);

        assertAll(
                () -> assertEquals(tallyEntity.getCancelled(), tally.getCancelled()),
                () -> assertEquals(tallyEntity.getId(), tally.getId()),
                () -> assertNotNull(tallyEntity.getPlayer()),
                () -> assertNotNull(tallyEntity.getProduct()),
                () -> assertNotNull(tallyEntity.getLocation()),
                () -> assertEquals(player.getId(), tallyEntity.getPlayer().getId()),
                () -> assertEquals(product.getId(), tallyEntity.getProduct().getId()),
                () -> assertEquals(location.getId(), tallyEntity.getLocation().getId()),
                () -> assertEquals(tallyEntity.getTime(), tally.getTime())
        );
    }

    @Test
    void fromTallies() {
        var tally = new Tally();
        tally.setId(4L);
        var event = new Event();
        var team = new Team();
        var player = new Player();
        var product = new Product();
        var location = new Location();
        event.setId(7L);
        team.setId(8L);
        player.setId(9L);
        product.setId(10L);
        location.setId(11L);
        player.setEvent(event);
        player.setTeam(team);
        tally.setPlayer(player);
        tally.setProduct(product);
        tally.setLocation(location);
        var tallies = TallyEntity.fromTallies(Collections.singletonList(tally));
        assertEquals(1, tallies.size());
        assertEquals(tally.getId(), tallies.toArray(new TallyEntity[0])[0].getId());
    }
}
