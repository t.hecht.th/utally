package de.utally.data.repository.internal;

import de.utally.data.model.Event;
import de.utally.data.model.Player;
import de.utally.data.model.Team;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PlayerEntityTest {
    @Test
    void toPlayer() {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setComment("comment");
        playerEntity.setId(4L);
        playerEntity.setName("name");
        playerEntity.setPaid(Boolean.TRUE);
        playerEntity.setPaidPlayersFee(120);
        playerEntity.setPayTime(Instant.now());
        playerEntity.setCreationTime(Instant.now().minusSeconds(5));
        playerEntity.setModificationTime(Instant.now().minusSeconds(2));
        playerEntity.setEnabled(true);
        playerEntity.setTeam(null);
        playerEntity.setWithPlayersFee(Boolean.TRUE);
        playerEntity.setPreferences(Map.of("key", "value"));
        var teamEntity = new TeamEntity();
        teamEntity.setId(5L);
        playerEntity.setTeam(teamEntity);
        var eventEntity = new EventEntity();
        eventEntity.setId(6L);
        playerEntity.setEvent(eventEntity);

        Player player = playerEntity.toPlayer();

        assertAll(
                () -> assertEquals(playerEntity.getComment(), player.getComment()),
                () -> assertEquals(playerEntity.getId(), player.getId()),
                () -> assertEquals(playerEntity.getName(), player.getName()),
                () -> assertEquals(playerEntity.getPaid(), player.getPaid()),
                () -> assertEquals(playerEntity.getPaidPlayersFee(), player.getPaidPlayersFee()),
                () -> assertEquals(playerEntity.getPayTime(), player.getPayTime()),
                () -> assertEquals(playerEntity.getCreationTime(), player.getCreationTime()),
                () -> assertEquals(playerEntity.getModificationTime(), player.getModificationTime()),
                () -> assertTrue(player.getEnabled()),
                () -> assertNotNull(player.getTeam()),
                () -> assertEquals(teamEntity.getId(), player.getTeam().getId()),
                () -> assertNotNull(player.getEvent()),
                () -> assertEquals(eventEntity.getId(), player.getEvent().getId()),
                () -> assertEquals(playerEntity.getWithPlayersFee(), player.getWithPlayersFee()),
                () -> assertEquals(playerEntity.getPreferences(), player.getPreferences())
        );
    }

    @Test
    void toPlayers() {
        var player = new PlayerEntity();
        player.setId(4L);
        var teamEntity = new TeamEntity();
        teamEntity.setId(5L);
        player.setTeam(teamEntity);
        var eventEntity = new EventEntity();
        eventEntity.setId(6L);
        player.setEvent(eventEntity);
        var players = PlayerEntity.toPlayers(Collections.singletonList(player));
        assertEquals(1, players.size());
        var playerResult = players.toArray(new Player[0])[0];
        assertEquals(player.getId(), playerResult.getId());
        assertEquals(player.getTeam().getId(), playerResult.getTeam().getId());
        assertEquals(player.getEvent().getId(), playerResult.getEvent().getId());
    }

    @Test
    void fromPlayer() {
        Player player = new Player();
        player.setComment("comment");
        player.setId(4L);
        player.setName("name");
        player.setPaid(Boolean.TRUE);
        player.setPaidPlayersFee(120);
        player.setPayTime(Instant.now());
        player.setCreationTime(Instant.now().minusSeconds(5));
        player.setModificationTime(Instant.now().minusSeconds(2));
        player.setEnabled(true);
        player.setTeam(null);
        player.setWithPlayersFee(Boolean.TRUE);
        player.setPreferences(Map.of("key", "value"));
        var team = new Team();
        team.setId(5L);
        player.setTeam(team);
        var event = new Event();
        event.setId(6L);
        player.setEvent(event);

        PlayerEntity playerEntity = PlayerEntity.fromPlayer(player);

        assertAll(
                () -> assertEquals(playerEntity.getComment(), player.getComment()),
                () -> assertEquals(playerEntity.getId(), player.getId()),
                () -> assertEquals(playerEntity.getName(), player.getName()),
                () -> assertEquals(playerEntity.getPaid(), player.getPaid()),
                () -> assertEquals(playerEntity.getPaidPlayersFee(), player.getPaidPlayersFee()),
                () -> assertEquals(playerEntity.getPayTime(), player.getPayTime()),
                () -> assertEquals(playerEntity.getCreationTime(), player.getCreationTime()),
                () -> assertEquals(playerEntity.getModificationTime(), player.getModificationTime()),
                () -> assertTrue(playerEntity.getEnabled()),
                () -> assertNotNull(playerEntity.getTeam()),
                () -> assertNotNull(playerEntity.getEvent()),
                () -> assertEquals(player.getTeam().getId(), playerEntity.getTeam().getId()),
                () -> assertEquals(player.getEvent().getId(), playerEntity.getEvent().getId()),
                () -> assertEquals(playerEntity.getWithPlayersFee(), player.getWithPlayersFee()),
                () -> assertEquals(playerEntity.getPreferences(), player.getPreferences())
        );
    }

    @Test
    void fromPlayers() {
        var player = new Player();
        player.setId(4L);
        var team = new Team();
        team.setId(5L);
        player.setTeam(team);
        var event = new Event();
        event.setId(6L);
        player.setEvent(event);
        var players = PlayerEntity.fromPlayers(Collections.singletonList(player));
        assertEquals(1, players.size());
        var playerResult= players.toArray(new PlayerEntity[0])[0];
        assertEquals(player.getId(), playerResult.getId());
        assertEquals(player.getTeam().getId(), playerResult.getTeam().getId());
        assertEquals(player.getEvent().getId(), playerResult.getEvent().getId());
    }
}
