package de.utally.data.repository;

import de.utally.data.exception.EventNotFoundException;
import de.utally.data.exception.ProductNotFoundException;
import de.utally.data.model.Product;
import de.utally.data.model.ProductAdapter;
import de.utally.data.model.ProductCategory;
import de.utally.data.repository.internal.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductRepositoryAdapterTest {
	@Mock
	ProductRepository productRepository;
	
	@Mock
	TallyRepository tallyRepository;

	@Mock
	EventRepository eventRepository;
	
	ProductAdapter productAdapter;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		productAdapter = new ProductRepositoryAdapter(productRepository, tallyRepository, eventRepository);
		
		doAnswer((Answer<ProductEntity>) invocation -> invocation.getArgument(0)).when(productRepository).save(any(ProductEntity.class));
	}

	@AfterEach
	void tearDown() {
		reset(productRepository);
	}

	@ParameterizedTest
	@EnumSource(ProductCategory.class)
	void insertProduct(ProductCategory productCategory) {
		String name = "name";
		Integer price = 6;
		String imageName = "imageName";
		String description = "description";

		Product product = productAdapter.insertProduct(name, description, price, imageName, productCategory, true, false);

		assertEquals(name, product.getName());
		assertEquals(description, product.getDescription());
		assertEquals(price, product.getPrice());
		assertEquals(imageName, product.getImageName());
		assertEquals(productCategory, product.getCategory());
		assertTrue(product.getEnabled());
		assertNotNull(product.getCreationTime());
		assertNotNull(product.getModificationTime());
		assertFalse(product.getDisplayStatistic());
		
		verify(productRepository).save(any());
	}

	@Test
	void insertProductNullName() {
		assertThrows(NullPointerException.class, () -> productAdapter.insertProduct(null, "description", 500, "imageName", ProductCategory.DRINK, true, false));

		verify(productRepository, never()).save(any());
	}

	@Test
	void insertProductNullPrice() {
		assertThrows(NullPointerException.class, () -> productAdapter.insertProduct("name", "description", null, "imageName", ProductCategory.DRINK, true, false));

		verify(productRepository, never()).save(any());
	}

	@Test
	void insertProductNullImageName() {
		assertThrows(NullPointerException.class, () -> productAdapter.insertProduct("name", "description", 500, null, ProductCategory.DRINK, true, false));

		verify(productRepository, never()).save(any());
	}

	@Test
	void insertProductNullCategory() {
		assertThrows(NullPointerException.class, () -> productAdapter.insertProduct("name", "description", 500, "imageName", null, true, false));

		verify(productRepository, never()).save(any());
	}

	@Test
	void existsByIdTrue() {
		when(productRepository.existsById(anyLong())).thenReturn(true);
		assertTrue(productAdapter.existsById(5L));
	}

	@Test
	void existsByIdFalse() {
		when(productRepository.existsById(anyLong())).thenReturn(false);
		assertFalse(productAdapter.existsById(5L));
	}
	
	@ParameterizedTest
	@EnumSource(ProductCategory.class)
	void updateProduct(ProductCategory productCategory) {
		Long id = 4L;
		String name = "name";
		Integer price = 6;
		String imageName = "imageName";
		
		Product product = new Product();
		product.setId(id);
		product.setCategory(productCategory);
		product.setImageName(imageName);
		product.setName(name);
		product.setPrice(price);
		product.setEnabled(true);
		product.setDescription("description");
		product.setDisplayStatistic(true);
		
		ProductEntity p2 = new ProductEntity();
		p2.setId(id);
		p2.setCategory(ProductCategory.DRINK);
		p2.setImageName("imageName2");
		p2.setPrice(300);
		p2.setName("oldName");
		p2.setEnabled(false);
		
		when(productRepository.findById(id)).thenReturn(Optional.of(p2));
		
		doAnswer((Answer<Void>) invocation -> {
			ProductEntity productEntity = invocation.getArgument(0);
			assertEquals(productEntity.getId(), product.getId());
			assertEquals(productEntity.getName(), product.getName());
			assertEquals(productEntity.getPrice(), product.getPrice());
			assertEquals(productEntity.getImageName(), product.getImageName());
			assertEquals(productEntity.getCategory(), product.getCategory());
			assertEquals(productEntity.getDescription(), product.getDescription());
			assertTrue(productEntity.getEnabled());
			assertTrue(productEntity.getDisplayStatistic());
			assertNotNull(productEntity.getModificationTime());
			return null;
		}).when(productRepository).save(any());
		
		productAdapter.updateProduct(product);
		verify(productRepository).save(any());
	}
	
	@Test
	void updateMissingProduct() {
		Long id = 4L;
		String name = "name";
		Integer price = 6;
		String imageName = "imageName";
		
		Product product = new Product();
		product.setId(id);
		product.setCategory(ProductCategory.DRINK);
		product.setImageName(imageName);
		product.setName(name);
		product.setPrice(price);

		when(productRepository.findById(product.getId())).thenReturn(Optional.empty());
		
		assertThrows(ProductNotFoundException.class, () -> productAdapter.updateProduct(product));
		verify(productRepository, never()).save(any());
	}

	@Test
	void updateProductNull() {
		assertThrows(NullPointerException.class, () -> productAdapter.updateProduct(null));
		verify(productRepository, never()).save(any());
	}

	@ParameterizedTest
	@EnumSource(ProductCategory.class)
	void getProducts(ProductCategory productCategory) {
		ProductEntity p1 = new ProductEntity();
		p1.setId(4L);
		p1.setCategory(productCategory);
		p1.setImageName("imageName1");
		p1.setPrice(300);
		
		ProductEntity p2 = new ProductEntity();
		p2.setId(5L);
		p2.setCategory(productCategory);
		p2.setImageName("imageName2");
		p2.setPrice(300);
		
		when(productRepository.findAll()).thenReturn(Arrays.asList(p1, p2));
		
		List<Product> products = productAdapter.getProducts();
		
		assertEquals(2, products.size());
		assertEquals(1, products.stream().filter(p -> p.getId().equals(p1.getId())).count());
		assertEquals(1, products.stream().filter(p -> p.getId().equals(p2.getId())).count());
	}

	@ParameterizedTest
	@EnumSource(ProductCategory.class)
	void getProductsByEvent(ProductCategory productCategory) {
		ProductEntity p1 = new ProductEntity();
		p1.setId(4L);
		p1.setCategory(productCategory);
		p1.setImageName("imageName1");
		p1.setPrice(300);

		ProductEntity p2 = new ProductEntity();
		p2.setId(p1.getId());
		p2.setCategory(p1.getCategory());
		p2.setImageName(p1.getImageName());
		p2.setPrice(p1.getPrice());

		ProductEntity p3 = new ProductEntity();
		p3.setId(5L);
		p3.setCategory(productCategory);
		p3.setImageName("imageName2");
		p3.setPrice(300);

		LocationEntity l1 = new LocationEntity();
		l1.setId(6L);
		l1.setProducts(Set.of(p1));

		LocationEntity l2 = new LocationEntity();
		l2.setId(7L);
		l2.setProducts(Set.of(p2, p3));

		EventEntity event = new EventEntity();
		event.setId(8L);
		event.setLocations(Set.of(l1, l2));

		when(eventRepository.findById(event.getId())).thenReturn(Optional.of(event));

		var products = productAdapter.getProductsByEvent(event.getId());

		assertThat(products).hasSize(2).anyMatch(p -> p.getId() == 4L).anyMatch(p -> p.getId() == 5L);
	}

	@Test
	void getProductsByEventEventNotFound() {
		var id = 8L;
		when(eventRepository.findById(id)).thenReturn(Optional.empty());

		assertThatThrownBy(() -> productAdapter.getProductsByEvent(id)).isInstanceOf(EventNotFoundException.class);
	}
	
	@ParameterizedTest
	@EnumSource(ProductCategory.class)
	void getEnabledProducts(ProductCategory productCategory) {
		ProductEntity p1 = new ProductEntity();
		p1.setId(4L);
		p1.setCategory(productCategory);
		p1.setImageName("imageName1");
		p1.setPrice(300);
		
		ProductEntity p2 = new ProductEntity();
		p2.setId(5L);
		p2.setCategory(productCategory);
		p2.setImageName("imageName2");
		p2.setPrice(300);
		
		when(productRepository.findByEnabled(true)).thenReturn(Arrays.asList(p1, p2));
		
		List<Product> products = productAdapter.getEnabledProducts();
		
		assertEquals(2, products.size());
		assertEquals(1, products.stream().filter(p -> p.getId().equals(p1.getId())).count());
		assertEquals(1, products.stream().filter(p -> p.getId().equals(p2.getId())).count());
		
		verify(productRepository).findByEnabled(true);
	}
	
	@Test
	void getProductById() {
		Long productId = 4L;
		ProductEntity productEntity = new ProductEntity();
		productEntity.setId(productId);
		
		when(productRepository.findById(productId)).thenReturn(Optional.of(productEntity));
		
		var product = productAdapter.getProductById(productId);
		
		assertEquals(productId, product.getId());
		verify(productRepository).findById(productId);
	}

	@Test
	void getProductByIdMissing() {
		Long productId = 4L;
		when(productRepository.findById(productId)).thenReturn(Optional.empty());

		assertThrows(ProductNotFoundException.class, () -> productAdapter.getProductById(productId));

		verify(productRepository).findById(productId);
	}
	
	@Test
	void getProductByIds() {
		Long productId = 4L;
		ProductEntity product = new ProductEntity();
		product.setId(productId);
		
		when(productRepository.findAllById(anyList())).thenReturn(List.of(product));
		
		List<Product> products = productAdapter.getProductByIds(List.of(productId));
		
		assertFalse(products.isEmpty());
		assertEquals(productId, products.get(0).getId());
	}
	
	@Test
	void removeProduct() {
		Long productId = 7L;
		productAdapter.removeProduct(productId);
		verify(productRepository).deleteById(productId);
		verify(tallyRepository).deleteByProductId(productId);
	}
}
