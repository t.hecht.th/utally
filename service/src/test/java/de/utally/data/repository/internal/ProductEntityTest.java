package de.utally.data.repository.internal;

import de.utally.data.model.Product;
import de.utally.data.model.ProductCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductEntityTest {
    @ParameterizedTest
    @EnumSource(ProductCategory.class)
    void toProduct(ProductCategory productCategory) {
        var now = Instant.now();
        ProductEntity productEntity = new ProductEntity();
        productEntity.setCategory(productCategory);
        productEntity.setId(6L);
        productEntity.setImageName("imageName");
        productEntity.setName("name");
        productEntity.setPrice(8);
        productEntity.setDescription("description");
        productEntity.setEnabled(Boolean.TRUE);
        productEntity.setDisplayStatistic(Boolean.TRUE);
        productEntity.setCreationTime(now);
        productEntity.setModificationTime(now);

        Product product = productEntity.toProduct();

        assertAll(
                () -> assertEquals(productEntity.getCategory(), product.getCategory()),
                () -> assertEquals(productEntity.getId(), product.getId()),
                () -> assertEquals(productEntity.getImageName(), product.getImageName()),
                () -> assertEquals(productEntity.getName(), product.getName()),
                () -> assertEquals(productEntity.getPrice(), product.getPrice()),
                () -> assertEquals(productEntity.getCreationTime(), product.getCreationTime()),
                () -> assertEquals(productEntity.getModificationTime(), product.getModificationTime()),
                () -> assertEquals(productEntity.getEnabled(), product.getEnabled()),
                () -> assertEquals(productEntity.getDisplayStatistic(), product.getDisplayStatistic()),
                () -> assertEquals(productEntity.getDescription(), product.getDescription())
        );
    }

    @Test
    void toProducts() {
        var product = new ProductEntity();
        product.setId(4L);
        var products = ProductEntity.toProducts(Collections.singletonList(product));
        assertEquals(1, products.size());
        assertEquals(product.getId(), products.toArray(new Product[0])[0].getId());
    }

    @ParameterizedTest
    @EnumSource(ProductCategory.class)
    void fromProduct(ProductCategory productCategory) {
        var now = Instant.now();
        Product product = new Product();
        product.setCategory(productCategory);
        product.setId(6L);
        product.setImageName("imageName");
        product.setName("name");
        product.setPrice(8);
        product.setDescription("description");
        product.setEnabled(Boolean.TRUE);
        product.setDisplayStatistic(Boolean.TRUE);
        product.setCreationTime(now);
        product.setModificationTime(now);

        ProductEntity productEntity = ProductEntity.fromProduct(product);

        assertAll(
                () -> assertEquals(productEntity.getCategory(), product.getCategory()),
                () -> assertEquals(productEntity.getId(), product.getId()),
                () -> assertEquals(productEntity.getImageName(), product.getImageName()),
                () -> assertEquals(productEntity.getName(), product.getName()),
                () -> assertEquals(productEntity.getPrice(), product.getPrice()),
                () -> assertEquals(productEntity.getCreationTime(), product.getCreationTime()),
                () -> assertEquals(productEntity.getModificationTime(), product.getModificationTime()),
                () -> assertEquals(productEntity.getEnabled(), product.getEnabled()),
                () -> assertEquals(productEntity.getDisplayStatistic(), product.getDisplayStatistic()),
                () -> assertEquals(productEntity.getDescription(), product.getDescription())
        );
    }

    @Test
    void fromProducts() {
        var product = new Product();
        product.setId(4L);
        var products = ProductEntity.fromProducts(Collections.singletonList(product));
        assertEquals(1, products.size());
        assertEquals(product.getId(), products.toArray(new ProductEntity[0])[0].getId());
    }
}
