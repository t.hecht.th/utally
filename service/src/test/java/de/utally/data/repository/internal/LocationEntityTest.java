package de.utally.data.repository.internal;

import de.utally.data.model.Location;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class LocationEntityTest {
    @Test
    void toLocation() {
        var locationEntity = new LocationEntity();
        locationEntity.setId(6L);
        locationEntity.setName("name");
        locationEntity.setProducts(Collections.emptySet());
        locationEntity.setCreationTime(Instant.now().minusSeconds(5));
        locationEntity.setModificationTime(Instant.now().minusSeconds(2));
        locationEntity.setEnabled(true);
        locationEntity.setDescription("description");

        Location location = locationEntity.toLocation();

        assertAll(
                () -> assertEquals(location.getId(), locationEntity.getId()),
                () -> assertEquals(location.getName(), locationEntity.getName()),
                () -> assertEquals(location.getCreationTime(), locationEntity.getCreationTime()),
                () -> assertEquals(location.getModificationTime(), locationEntity.getModificationTime()),
                () -> assertEquals(location.getDescription(), locationEntity.getDescription()),
                () -> assertTrue(location.getEnabled()),
                () -> assertTrue(location.getProducts().isEmpty())
        );
    }

    @Test
    void toLocations() {
        var location = new LocationEntity();
        location.setId(4L);
        var locations = LocationEntity.toLocations(Collections.singletonList(location));
        assertEquals(1, locations.size());
        assertEquals(location.getId(), locations.toArray(new Location[0])[0].getId());
    }

    @Test
    void fromLocation() {
        var location = new Location();
        location.setId(6L);
        location.setName("name");
        location.setProducts(Collections.emptySet());
        location.setCreationTime(Instant.now().minusSeconds(5));
        location.setModificationTime(Instant.now().minusSeconds(2));
        location.setEnabled(true);
        location.setDescription("description");

        LocationEntity locationEntity = LocationEntity.fromLocation(location);

        assertAll(
                () -> assertEquals(location.getId(), locationEntity.getId()),
                () -> assertEquals(location.getName(), locationEntity.getName()),
                () -> assertEquals(location.getCreationTime(), locationEntity.getCreationTime()),
                () -> assertEquals(location.getModificationTime(), locationEntity.getModificationTime()),
                () -> assertEquals(location.getDescription(), locationEntity.getDescription()),
                () -> assertTrue(locationEntity.getEnabled()),
                () -> assertTrue(locationEntity.getProducts().isEmpty())
        );
    }

    @Test
    void fromLocations() {
        var location = new Location();
        location.setId(4L);
        var locations = LocationEntity.fromLocations(Collections.singletonList(location));
        assertEquals(1, locations.size());
        assertEquals(location.getId(), locations.toArray(new LocationEntity[0])[0].getId());
    }
}
