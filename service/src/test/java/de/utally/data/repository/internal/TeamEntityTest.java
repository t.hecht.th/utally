package de.utally.data.repository.internal;

import de.utally.data.model.Team;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TeamEntityTest {
    @Test
    void toTeam() {
        var now = Instant.now();
        TeamEntity teamEntity = new TeamEntity();
        teamEntity.setCity("city");
        teamEntity.setFullName("fullName");
        teamEntity.setId(4L);
        teamEntity.setShortName("shortName");
        teamEntity.setCreationTime(now);
        teamEntity.setEnabled(Boolean.TRUE);
        teamEntity.setModificationTime(now);
        teamEntity.setDescription("description");

        Team team = teamEntity.toTeam();

        assertAll(
                () -> assertEquals(teamEntity.getCity(), team.getCity()),
                () -> assertEquals(teamEntity.getFullName(), team.getFullName()),
                () -> assertEquals(teamEntity.getId(), team.getId()),
                () -> assertEquals(teamEntity.getShortName(), team.getShortName()),
                () -> assertEquals(teamEntity.getCreationTime(), team.getCreationTime()),
                () -> assertEquals(teamEntity.getModificationTime(), team.getModificationTime()),
                () -> assertEquals(teamEntity.getDescription(), team.getDescription()),
                () -> assertEquals(teamEntity.getEnabled(), team.getEnabled())
        );
    }

    @Test
    void toTeams() {
        var team = new TeamEntity();
        team.setId(4L);
        var teams = TeamEntity.toTeams(Collections.singletonList(team));
        assertEquals(1, teams.size());
        assertEquals(team.getId(), teams.toArray(new Team[0])[0].getId());
    }

    @Test
    void fromTeam() {
        var now = Instant.now();
        Team team = new Team();
        team.setCity("city");
        team.setFullName("fullName");
        team.setId(4L);
        team.setShortName("shortName");
        team.setCreationTime(now);
        team.setEnabled(Boolean.TRUE);
        team.setModificationTime(now);
        team.setDescription("description");

        TeamEntity teamEntity = TeamEntity.fromTeam(team);

        assertAll(
                () -> assertEquals(teamEntity.getCity(), team.getCity()),
                () -> assertEquals(teamEntity.getFullName(), team.getFullName()),
                () -> assertEquals(teamEntity.getId(), team.getId()),
                () -> assertEquals(teamEntity.getShortName(), team.getShortName()),
                () -> assertEquals(teamEntity.getCreationTime(), team.getCreationTime()),
                () -> assertEquals(teamEntity.getModificationTime(), team.getModificationTime()),
                () -> assertEquals(teamEntity.getDescription(), team.getDescription()),
                () -> assertEquals(teamEntity.getEnabled(), team.getEnabled())
        );
    }

    @Test
    void fromTeams() {
        var team = new Team();
        team.setId(4L);
        var teams = TeamEntity.fromTeams(Collections.singletonList(team));
        assertEquals(1, teams.size());
        assertEquals(team.getId(), teams.toArray(new TeamEntity[0])[0].getId());
    }
}
