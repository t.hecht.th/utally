package de.utally.data.repository;

import de.utally.data.exception.TeamNotFoundException;
import de.utally.data.model.Team;
import de.utally.data.model.TeamAdapter;
import de.utally.data.repository.internal.TeamEntity;
import de.utally.data.repository.internal.TeamRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TeamRepositoryAdapterTest {
	@Mock
	TeamRepository teamRepository;
	
	TeamAdapter teamAdapter;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		teamAdapter = new TeamRepositoryAdapter(teamRepository);
		doAnswer((Answer<TeamEntity>) invocation -> invocation.getArgument(0)).when(teamRepository).save(any(TeamEntity.class));
	}

	@AfterEach
	void tearDown() {
		reset(teamRepository);
	}
	
	@Test
	void insertTeam() {
		String city = "city";
		String fullName = "fullName";
		String shortName = "shortName";
		String description = "description";

		Team team = teamAdapter.insertTeam(fullName, description, city, shortName, true);
		
		assertEquals(city, team.getCity());
		assertEquals(description, team.getDescription());
		assertEquals(fullName, team.getFullName());
		assertEquals(shortName, team.getShortName());
		assertTrue(team.getEnabled());
		assertNotNull(team.getCreationTime());
		assertNotNull(team.getModificationTime());
		
		verify(teamRepository).save(any());
	}

	@Test
	void insertTeamFullNameNull() {
		String city = "city";
		String shortName = "shortName";
		String description = "description";

		assertThrows(NullPointerException.class, () -> teamAdapter.insertTeam(null, description, city, shortName, true));

		verify(teamRepository, never()).save(any());
	}

	@Test
	void insertTeamShortNameNull() {
		String city = "city";
		String fullName = "fullName";
		String description = "description";

		assertThrows(NullPointerException.class, () -> teamAdapter.insertTeam(fullName, description, city, null, true));

		verify(teamRepository, never()).save(any());
	}

	@Test
	void insertTeamCityNull() {
		String fullName = "fullName";
		String shortName = "shortName";
		String description = "description";

		assertThrows(NullPointerException.class, () -> teamAdapter.insertTeam(fullName, description, null, shortName, true));

		verify(teamRepository, never()).save(any());
	}
	
	@Test
	void updateTeam() {
		Long id = 4L;
		String city = "city";
		String fullName = "fullName";
		String shortName = "shortName";
		var team = new Team();
		team.setCity(city);
		team.setFullName(fullName);
		team.setId(id);
		team.setShortName(shortName);
		team.setEnabled(true);
		team.setDescription("description");
		
		var teamEntity = new TeamEntity();
		teamEntity.setId(id);
		teamEntity.setCity("city2");
		teamEntity.setFullName("fullname2");
		teamEntity.setShortName("shortName2");
		teamEntity.setEnabled(false);
		
		when(teamRepository.findById(id)).thenReturn(Optional.of(teamEntity));
		
		doAnswer((Answer<Void>) invocation -> {
			TeamEntity t = invocation.getArgument(0);
			assertEquals(team.getCity(), t.getCity());
			assertEquals(team.getFullName(), t.getFullName());
			assertEquals(team.getShortName(), t.getShortName());
			assertEquals(team.getDescription(), t.getDescription());
			assertTrue(t.getEnabled());
			assertNotNull(t.getModificationTime());
			return null;
		}).when(teamRepository).save(any());
		
		teamAdapter.updateTeam(team);
		
		verify(teamRepository).save(any());
	}
	
	@Test
	void updateMissingTeam() {
		Long id = 4L;
		String city = "city";
		String fullName = "fullName";
		String shortName = "shortName";
		var team = new Team();
		team.setCity(city);
		team.setFullName(fullName);
		team.setId(id);
		team.setShortName(shortName);
		
		when(teamRepository.findById(id)).thenReturn(Optional.empty());
		
		assertThrows(TeamNotFoundException.class, () -> teamAdapter.updateTeam(team));
		
		verify(teamRepository, never()).save(any());
	}

	@Test
	void updateTeamNull() {
		assertThrows(NullPointerException.class, () -> teamAdapter.updateTeam(null));

		verify(teamRepository, never()).save(any());
	}
	
	@Test
	void getTeams() {
		TeamEntity teamEntity = new TeamEntity();
		teamEntity.setId(3L);
		
		when(teamRepository.findAll()).thenReturn(Collections.singletonList(teamEntity));
		
		List<Team> teams = teamAdapter.getTeams();
		
		assertEquals(1, teams.size());
		
		Team team = teams.get(0);
		assertEquals(team.getId(), teamEntity.getId());
	}
	
	@Test
	void getEnabledTeams() {
		TeamEntity teamEntity = new TeamEntity();
		teamEntity.setId(3L);
		
		when(teamRepository.findByEnabled(true)).thenReturn(Collections.singletonList(teamEntity));
		
		List<Team> teams = teamAdapter.getEnabledTeams();
		
		assertEquals(1, teams.size());
		
		Team team = teams.get(0);
		assertEquals(team.getId(), teamEntity.getId());
		
		verify(teamRepository).findByEnabled(true);
	}
	
	@Test
	void getTeamById() {
		Long teamId = 3L;
		TeamEntity teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.of(teamEntity));
		
		var team = teamAdapter.getTeamById(teamId);
		
		assertEquals(teamId, team.getId());
	}

	@Test
	void getTeamByIdNull() {
		assertThrows(NullPointerException.class, () -> teamAdapter.removeTeam(null));
	}

	@Test
	void removeTeam() {
		teamAdapter.removeTeam(6L);
		verify(teamRepository).deleteById(6L);
	}

	@Test
	void removeTeamNull() {
		assertThrows(NullPointerException.class, () -> teamAdapter.removeTeam(null));
		verify(teamRepository, never()).deleteById(anyLong());
	}
}
