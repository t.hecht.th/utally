package de.utally.data.repository;

import de.utally.data.exception.*;
import de.utally.data.model.Event;
import de.utally.data.repository.internal.*;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.*;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EventRepositoryAdapterTest {
	private static final EasyRandom EASY_RANDOM = new EasyRandom();

	@Mock
	EventRepository eventRepository;
	
	@Mock
	LocationRepository locationRepository;
	
	@Mock
	TimeSlotRepository timeSlotRepository;
	
	@Mock
	TeamRepository teamRepository;
	
	@Mock
	ProductRepository productRepository;
	
	@Mock
	PlayerRepository playerRepository;
	
	@Mock
	TallyRepository tallyRepository;
	
	EventRepositoryAdapter repositoryAdapter;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		repositoryAdapter = new EventRepositoryAdapter(eventRepository, locationRepository, timeSlotRepository, teamRepository, productRepository, playerRepository, tallyRepository);
		
		doAnswer((Answer<EventEntity>) invocation -> invocation.getArgument(0)).when(eventRepository).save(any(EventEntity.class));
	}

	@AfterEach
	void tearDown() {
		reset(eventRepository);
	}

	@Test
	void insertEvent() {
		String title = "title";
		Integer playersFee = 120;
		String description = "description";
		
		var event = repositoryAdapter.insertEvent(title, description, true, playersFee);
		assertEquals(title, event.getTitle());
		assertEquals(description, event.getDescription());
		assertTrue(event.getEnabled());
		assertEquals(playersFee, event.getPlayersFee());
		assertNotNull(event.getCreationTime());
		assertNotNull(event.getModificationTime());
		
		verify(eventRepository).save(any(EventEntity.class));
	}

	@Test
	void insertEventNullTitle() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.insertEvent(null, "description", true, 100));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void insertEventNullEnabledState() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.insertEvent("title", "description", null, 100));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void insertEventNullPlayersFee() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.insertEvent("title", "description", true, null));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void updateEvent() {
		Long id = 4L;
		String title = "title";
		Integer playersFee = 120;
		var event = new Event();
		event.setId(id);
		event.setTitle(title);
		event.setEnabled(true);
		event.setPlayersFee(playersFee);
		event.setDescription("description");
		
		var eventEntity = new EventEntity();
		eventEntity.setId(id);
		eventEntity.setTitle("title2");
		eventEntity.setEnabled(false);
		eventEntity.setPlayersFee(140);
		
		when(eventRepository.findById(id)).thenReturn(Optional.of(eventEntity));
		
		doAnswer((Answer<Void>) invocation -> {
			EventEntity e = invocation.getArgument(0);
			assertEquals(event.getTitle(), e.getTitle());
			assertTrue(e.getEnabled());
			assertEquals(event.getPlayersFee(), e.getPlayersFee());
			assertEquals(event.getDescription(), e.getDescription());
			assertNotNull(e.getModificationTime());
			return null;
		}).when(eventRepository).save(any());
		
		repositoryAdapter.updateEvent(event);
		
		verify(eventRepository).save(any());
	}
	
	@Test
	void updateMissingEvent() {
		Long id = 4L;
		String title = "title";
		var event = new Event();
		event.setId(id);
		event.setTitle(title);
		
		when(eventRepository.findById(id)).thenReturn(Optional.empty());
		
		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.updateEvent(event));
		
		verify(eventRepository, never()).save(any());
	}

	@Test
	void updateEventNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.updateEvent(null));

		verify(eventRepository, never()).save(any());
	}

	@Test
	void removeEvent() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		eventEntity.setTitle("title2");
		eventEntity.setEnabled(false);
		eventEntity.setPlayersFee(140);
		var teamEntity = new TeamEntity();
		teamEntity.setId(7L);
		var playerEntity = new PlayerEntity();
		playerEntity.setId(8L);
		playerEntity.setTeam(teamEntity);
		var tallyEntity = new TallyEntity();
		tallyEntity.setId(9L);
		tallyEntity.setPlayer(playerEntity);
		eventEntity.setTeams(Set.of(teamEntity));
		
		List<PlayerEntity> players = List.of(playerEntity);
		
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));
		when(playerRepository.findByEventId(eventId)).thenReturn(players);
		
		repositoryAdapter.removeEvent(eventId);
		verify(eventRepository).deleteById(eventId);
		verify(timeSlotRepository).deleteAllByEventId(eventEntity.getId());
		verify(tallyRepository).deleteByPlayerId(playerEntity.getId());
		verify(playerRepository).deleteAll(players);
	}

	@Test
	void removeEventNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeEvent(null));
		verify(eventRepository, never()).deleteById(anyLong());
		verify(timeSlotRepository, never()).deleteAllByEventId(anyLong());
		verify(tallyRepository, never()).deleteByPlayerId(anyLong());
		verify(playerRepository, never()).deleteAll(anyList());
	}
	
	@Test
	void getEventByTimeSlot() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long timeSlotId = 7L;
		var timeSlotEntity = new TimeSlotEntity();
		timeSlotEntity.setId(timeSlotId);
		timeSlotEntity.setEvent(eventEntity);

		when(timeSlotRepository.findById(timeSlotId)).thenReturn(Optional.of(timeSlotEntity));

		var event = repositoryAdapter.getEventByTimeSlot(timeSlotId);
		
		assertEquals(eventEntity.getId(), event.getId());
		
		verify(timeSlotRepository).findById(timeSlotId);
	}
	
	@Test
	void getEventMissingTimeSlot() {
		Long timeSlotId = 7L;
		when(timeSlotRepository.findById(timeSlotId)).thenReturn(Optional.empty());

		assertThrows(TimeSlotNotFoundException.class, () -> repositoryAdapter.getEventByTimeSlot(timeSlotId));
		
		verify(timeSlotRepository).findById(timeSlotId);
	}

	@Test
	void getEventNullTimeSlot() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getEventByTimeSlot(null));

		verify(timeSlotRepository, never()).findById(anyLong());
	}
	
	@Test
	void getEventsByLocation() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		eventEntity.setLocations(Set.of(locationEntity));
		
		when(locationRepository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		when(eventRepository.findAllByLocation(locationEntity)).thenReturn(List.of(eventEntity));
		
		var event = repositoryAdapter.getEventsByLocation(locationId);
		
		assertFalse(event.isEmpty());
		assertEquals(eventEntity.getId(), event.get(0).getId());
		
		verify(locationRepository).findById(locationId);
		verify(eventRepository).findAllByLocation(locationEntity);
	}

	@Test
	void getEventsMissingLocation() {
		Long locationId = 7L;
		when(locationRepository.findById(locationId)).thenReturn(Optional.empty());

		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.getEventsByLocation(locationId));

		verify(locationRepository).findById(locationId);
		verify(eventRepository, never()).findAllByLocation(any());
	}

	@Test
	void getEventsNullLocation() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getEventsByLocation(null));

		verify(locationRepository, never()).findById(anyLong());
		verify(eventRepository, never()).findAllByLocation(any());
	}

	@Test
	void getEventsByProduct() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		Long productId = 8L;
		var productEntity = new ProductEntity();
		productEntity.setId(productId);
		locationEntity.setProducts(Set.of(productEntity));
		eventEntity.setLocations(Set.of(locationEntity));

		when(productRepository.findById(productId)).thenReturn(Optional.of(productEntity));
		when(eventRepository.findAllByProduct(productEntity)).thenReturn(List.of(eventEntity));

		var event = repositoryAdapter.getEventsByProduct(productId);

		assertFalse(event.isEmpty());
		assertEquals(eventEntity.getId(), event.get(0).getId());

		verify(productRepository).findById(productId);
		verify(eventRepository).findAllByProduct(productEntity);
	}

	@Test
	void getEventsByProductMissing() {
		Long productId = 8L;

		when(productRepository.findById(productId)).thenReturn(Optional.empty());

		assertThrows(ProductNotFoundException.class, () -> repositoryAdapter.getEventsByProduct(productId));

		verify(productRepository).findById(productId);
		verify(eventRepository, never()).findAllByProduct(any());
	}

	@Test
	void getEventsByProductNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getEventsByProduct(null));

		verify(productRepository, never()).findById(anyLong());
		verify(eventRepository, never()).findAllByProduct(any());
	}

	@Test
	void getEventsByTeam() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long teamId = 7L;
		var teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		eventEntity.setTeams(Set.of(teamEntity));
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.of(teamEntity));
		when(eventRepository.findAllByTeam(teamEntity)).thenReturn(List.of(eventEntity));
		
		var event = repositoryAdapter.getEventsByTeam(teamId);
		
		assertFalse(event.isEmpty());
		assertEquals(eventEntity.getId(), event.get(0).getId());
		
		verify(teamRepository).findById(teamId);
		verify(eventRepository).findAllByTeam(teamEntity);
	}
	
	@Test
	void getEventsMissingTeam() {
		Long teamId = 7L;
		when(teamRepository.findById(teamId)).thenReturn(Optional.empty());

		assertThrows(TeamNotFoundException.class, () -> repositoryAdapter.getEventsByTeam(teamId));
		
		verify(teamRepository).findById(teamId);
		verify(eventRepository, never()).findAllByTeam(any());
	}

	@Test
	void getEventsNullTeam() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getEventsByTeam(null));

		verify(teamRepository, never()).findById(anyLong());
		verify(eventRepository, never()).findAllByTeam(any());
	}
	
	@Test
	void getEvents() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		
		when(eventRepository.findAll()).thenReturn(Collections.singletonList(eventEntity));
		
		var events = repositoryAdapter.getEvents();
		
		assertEquals(1, events.size());
		assertEquals(eventId, events.get(0).getId());
	}
	
	@Test
	void getEnabledEvents() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		
		when(eventRepository.findByEnabled(true)).thenReturn(Collections.singletonList(eventEntity));
		
		var events = repositoryAdapter.getEnabledEvents();
		
		assertEquals(1, events.size());
		assertEquals(eventId, events.get(0).getId());
		
		verify(eventRepository).findByEnabled(true);
	}
	
	@Test
	void getEventById() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));
		
		var event = repositoryAdapter.getEventById(eventId);
		
		assertEquals(eventId, event.getId());
		verify(eventRepository).findById(eventId);
	}

	@Test
	void getEventByIdMissing() {
		Long eventId = 6L;

		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.getEventById(eventId));

		verify(eventRepository).findById(anyLong());
	}

	@Test
	void getEventByIdNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getEventById(null));

		verify(eventRepository, never()).existsById(anyLong());
	}
	
	@Test
	void addLocation() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		
		when(locationRepository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));
		
		assertTrue(eventEntity.getLocations().isEmpty());
		repositoryAdapter.addLocation(eventId, locationEntity.getId());
		assertEquals(1, eventEntity.getLocations().size());
		assertNotNull(eventEntity.getModificationTime());
		
		verify(eventRepository).save(any(EventEntity.class));
	}
	
	@Test
	void addLocationWithWrongEvent() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		
		when(locationRepository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.addLocation(eventId, locationId));
		
		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void addLocationWithWrongLocation() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		
		when(locationRepository.findById(locationId)).thenReturn(Optional.empty());
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));

		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.addLocation(eventId, locationId));
		
		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void addLocationWithNullEvent() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.addLocation(null, 5L));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void addLocationWithNullLocation() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.addLocation(5L, null));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void getLocations() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		eventEntity.setLocations(Set.of(locationEntity));
		
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));
		
		assertEquals(1, eventEntity.getLocations().size());
		var locations = repositoryAdapter.getLocations(eventId);

		assertEquals(1, locations.size());
		assertEquals(locationId, locations.get(0).getId());
		verify(eventRepository).findById(eventId);
	}

	@Test
	void getLocationsNoSuchEvent() {
		Long eventId = 6L;
		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.getLocations(eventId));

		verify(eventRepository).findById(eventId);
	}

	@Test
	void getLocationsNullEvent() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getLocations(null));

		verify(eventRepository, never()).existsById(anyLong());
	}
	
	@Test
	void removeLocation() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		eventEntity.setLocations(Set.of(locationEntity));
		
		when(locationRepository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));
		
		assertEquals(1, eventEntity.getLocations().size());
		repositoryAdapter.removeLocation(eventId, locationEntity.getId());
		assertTrue(eventEntity.getLocations().isEmpty());
		assertNotNull(eventEntity.getModificationTime());
		
		verify(eventRepository).save(any(EventEntity.class));
	}
	
	@Test
	void removeLocationWithWrongEvent() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		eventEntity.setLocations(Set.of(locationEntity));
		
		when(locationRepository.findById(locationId)).thenReturn(Optional.of(locationEntity));
		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.removeLocation(eventId, locationId));
		
		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void removeLocationWithWrongLocation() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		eventEntity.setLocations(Set.of(locationEntity));
		
		when(locationRepository.findById(locationId)).thenReturn(Optional.empty());
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));

		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.removeLocation(eventId, locationId));
		
		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void removeLocationWithNullEvent() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeLocation(null, 5L));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void removeLocationWithNullLocation() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeLocation(5L, null));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void addTeam() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long teamId = 7L;
		var teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.of(teamEntity));
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));
		
		assertTrue(eventEntity.getTeams().isEmpty());
		repositoryAdapter.addTeam(eventId, teamEntity.getId());
		assertEquals(1, eventEntity.getTeams().size());
		assertNotNull(eventEntity.getModificationTime());
		
		verify(eventRepository).save(any(EventEntity.class));
	}
	
	@Test
	void addTeamWithWrongEvent() {
		Long eventId = 6L;
		Long teamId = 7L;
		var teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.of(teamEntity));
		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.addTeam(eventId, teamId));
		
		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void addTeamWithWrongTeam() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long teamId = 7L;
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.empty());
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));

		assertThrows(TeamNotFoundException.class, () -> repositoryAdapter.addTeam(eventId, teamId));
		
		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void addTeamWithNullEvent() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.addTeam(null, 5L));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void addTeamWithNullTeam() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.addTeam(5L, null));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void getTeams() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long teamId = 7L;
		var teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		eventEntity.setTeams(Set.of(teamEntity));
		
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));
		
		assertEquals(1, eventEntity.getTeams().size());
		var teams = repositoryAdapter.getTeams(eventId);

		assertEquals(1, teams.size());
		assertEquals(teamId, teams.get(0).getId());
		verify(eventRepository).findById(eventId);
	}

	@Test
	void getTeamsNoSuchEvent() {
		Long eventId = 6L;
		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.getTeams(eventId));

		verify(eventRepository).findById(anyLong());
	}

	@Test
	void getTeamsNullEvent() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.getTeams(null));

		verify(eventRepository, never()).findById(anyLong());
	}
	
	@Test
	void removeTeam() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long teamId = 7L;
		var teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		eventEntity.setTeams(Set.of(teamEntity));
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.of(teamEntity));
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));
		
		assertEquals(1, eventEntity.getTeams().size());
		repositoryAdapter.removeTeam(eventId, teamEntity.getId());
		assertTrue(eventEntity.getTeams().isEmpty());
		assertNotNull(eventEntity.getModificationTime());
		
		verify(eventRepository).save(any(EventEntity.class));
	}
	
	@Test
	void removeTeamWithWrongEvent() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long teamId = 7L;
		var teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		eventEntity.setTeams(Set.of(teamEntity));
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.of(teamEntity));
		when(eventRepository.findById(eventId)).thenReturn(Optional.empty());

		assertThrows(EventNotFoundException.class, () -> repositoryAdapter.removeTeam(eventId, teamId));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void removeTeamWithWrongTeam() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long teamId = 7L;
		var teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		eventEntity.setTeams(Set.of(teamEntity));
		
		when(teamRepository.findById(teamId)).thenReturn(Optional.empty());
		when(eventRepository.findById(eventId)).thenReturn(Optional.of(eventEntity));

		assertThrows(TeamNotFoundException.class, () -> repositoryAdapter.removeTeam(eventId, teamId));
		
		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void removeTeamWithNullEvent() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeTeam(null, 5L));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void removeTeamWithNullTeam() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeTeam(5L, null));

		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void removeLocationFromAllEvents() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long locationId = 7L;
		var locationEntity = new LocationEntity();
		locationEntity.setId(locationId);
		eventEntity.setLocations(Set.of(locationEntity));

		when(locationRepository.existsById(locationId)).thenReturn(true);
		
		when(eventRepository.findAll()).thenReturn(Collections.singletonList(eventEntity));
		
		assertEquals(1, eventEntity.getLocations().size());
		repositoryAdapter.removeLocationFromAllEvents(locationEntity.getId());
		assertTrue(eventEntity.getLocations().isEmpty());
		assertNotNull(eventEntity.getModificationTime());

		verify(eventRepository).findAll();
		verify(eventRepository).save(any(EventEntity.class));
	}
	
	@Test
	void removeLocationFromAllEventsWithWrongLocation() {
		Long locationId = 7L;

		when(locationRepository.existsById(locationId)).thenReturn(false);
		
		assertThrows(LocationNotFoundException.class, () -> repositoryAdapter.removeLocationFromAllEvents(8L));

		verify(eventRepository, never()).findAll();
		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void removeLocationFromAllEventsNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeLocationFromAllEvents(null));

		verify(eventRepository, never()).findAll();
		verify(eventRepository, never()).save(any(EventEntity.class));
	}
	
	@Test
	void removeTeamFromAllEvents() {
		Long eventId = 6L;
		var eventEntity = new EventEntity();
		eventEntity.setId(eventId);
		Long teamId = 7L;
		var teamEntity = new TeamEntity();
		teamEntity.setId(teamId);
		eventEntity.setTeams(Set.of(teamEntity));

		when(teamRepository.existsById(teamId)).thenReturn(true);
		
		when(eventRepository.findAll()).thenReturn(Collections.singletonList(eventEntity));
		
		assertEquals(1, eventEntity.getTeams().size());
		repositoryAdapter.removeTeamFromAllEvents(teamEntity.getId());
		assertTrue(eventEntity.getTeams().isEmpty());
		assertNotNull(eventEntity.getModificationTime());
		
		verify(eventRepository).findAll();
		verify(eventRepository).save(any(EventEntity.class));
	}
	
	@Test
	void removeTeamFromAllEventsWithWrongTeam() {
		Long teamId = 7L;

		when(teamRepository.existsById(teamId)).thenReturn(false);
		
		assertThrows(TeamNotFoundException.class, () -> repositoryAdapter.removeTeamFromAllEvents(8L));

		verify(eventRepository, never()).findAll();
		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void removeTeamFromAllEventsNull() {
		assertThrows(NullPointerException.class, () -> repositoryAdapter.removeTeamFromAllEvents(null));

		verify(eventRepository, never()).findAll();
		verify(eventRepository, never()).save(any(EventEntity.class));
	}

	@Test
	void getNumberOfTalliesByTeamInEventForStatistics() {
		var p1 = EASY_RANDOM.nextObject(ProductEntity.class);
		var p2 = EASY_RANDOM.nextObject(ProductEntity.class);
		var l = EASY_RANDOM.nextObject(LocationEntity.class);
		var t1 = EASY_RANDOM.nextObject(TeamEntity.class);
		var t2 = EASY_RANDOM.nextObject(TeamEntity.class);
		var e = EASY_RANDOM.nextObject(EventEntity.class);
		p1.setDisplayStatistic(true);
		p2.setDisplayStatistic(true);
		l.setProducts(Set.of(p1, p2));
		e.setTeams(Set.of(t1, t2));
		e.setLocations(Set.of(l));

		when(eventRepository.findById(e.getId())).thenReturn(Optional.of(e));
		when(tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalse(t1.getId(), p1.getId())).thenReturn(4L);
		when(tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalse(t2.getId(), p1.getId())).thenReturn(6L);
		when(tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalse(t1.getId(), p2.getId())).thenReturn(8L);
		when(tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalse(t2.getId(), p2.getId())).thenReturn(9L);

		var result = repositoryAdapter.getNumberOfTalliesByTeamInEventForStatistics(e.getId());

		assertThat(result)
				.hasSize(2)
				.containsEntry(p1.getId(), Map.of(t1.getId(), 4L, t2.getId(), 6L))
				.containsEntry(p2.getId(), Map.of(t1.getId(), 8L, t2.getId(), 9L));
	}

	@Test
	void getNumberOfTalliesByTeamInEventForStatisticsEventNotFound() {
		when(eventRepository.findById(anyLong())).thenReturn(Optional.empty());

		assertThatThrownBy(() -> repositoryAdapter.getNumberOfTalliesByTeamInEventForStatistics(5L)).isInstanceOf(EventNotFoundException.class);
	}

	@Test
	void getNumberOfTalliesByTeamInEventForStatisticsNull() {
		assertThatThrownBy(() -> repositoryAdapter.getNumberOfTalliesByTeamInEventForStatistics(null)).isInstanceOf(NullPointerException.class);
	}
}
