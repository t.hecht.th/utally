package de.utally.webservice.rest;

import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ErrorCodeDTOTest {
    @ParameterizedTest
    @EnumSource(ErrorCode.class)
    void errorCodeDtoContent(ErrorCode errorCode) {
        var dto = new ErrorCodeDTO(errorCode);
        assertAll(
                () -> assertEquals(errorCode.name(), dto.getError()),
                () -> assertEquals(errorCode.getMessage(), dto.getMessage()),
                () -> assertEquals(errorCode.getDetail(), dto.getDetail())
        );
    }

    @ParameterizedTest
    @EnumSource(ErrorCode.class)
    void responseBodyContent(ErrorCode errorCode) throws JSONException {
        JSONObject expected = new JSONObject();
        expected.put("error", errorCode.name());
        expected.put("message", errorCode.getMessage());
        expected.put("detail", errorCode.getDetail());
        var dto = new ErrorCodeDTO(errorCode);
        assertEquals(expected.toString(), dto.toJson());
    }
}
