package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PatchPlayerAsOperatorDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    @Test
    void validateCommentMinValid() {
        var cut = new PatchPlayerAsOperatorDTO(true, "", 130);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateCommentMaxValid() {
        var cut = new PatchPlayerAsOperatorDTO(true, StringUtils.repeat('a', 64), 130);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateCommentMaxInvalid() {
        var cut = new PatchPlayerAsOperatorDTO(true, StringUtils.repeat('a', 65), 130);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateCommentNull() {
        var cut = new PatchPlayerAsOperatorDTO(true, null, 130);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateWithPlayersFeeNull() {
        var cut = new PatchPlayerAsOperatorDTO(null, "comment", 130);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validatePaidPlayersFeeValid() {
        var cut = new PatchPlayerAsOperatorDTO(true, "comment", 0);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validatePaidPlayersFeeInvalid() {
        var cut = new PatchPlayerAsOperatorDTO(true, "comment", -1);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validatePaidPlayersFeeNull() {
        var cut = new PatchPlayerAsOperatorDTO(true, "comment", null);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
