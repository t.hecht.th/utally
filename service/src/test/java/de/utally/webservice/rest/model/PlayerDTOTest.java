package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import java.time.Instant;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PlayerDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    // Validation Tests
    @Test
    void validateNameMinValid() {
        var cut = new PlayerDTO(
                5L,
                "a",
                Instant.now(),
                Instant.now(),
                true,
                true,
                "comment",
                Instant.now(),
                true,
                130,
                Map.of("key", "value"),
                7L,
                6L
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMinInvalid() {
        var cut = new PlayerDTO(
                5L,
                "",
                Instant.now(),
                Instant.now(),
                true,
                true,
                "comment",
                Instant.now(),
                true,
                130,
                Map.of("key", "value"),
                7L,
                6L
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameMaxValid() {
        var cut = new PlayerDTO(
                5L,
                StringUtils.repeat('a', 64),
                Instant.now(),
                Instant.now(),
                true,
                true,
                "comment",
                Instant.now(),
                true,
                130,
                Map.of("key", "value"),
                7L,
                6L
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMaxInvalid() {
        var cut = new PlayerDTO(
                5L,
                StringUtils.repeat('a', 65),
                Instant.now(),
                Instant.now(),
                true,
                true,
                "comment",
                Instant.now(),
                true,
                130,
                Map.of("key", "value"),
                7L,
                6L
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameNull() {
        var cut = new PlayerDTO(
                5L,
                null,
                Instant.now(),
                Instant.now(),
                true,
                true,
                "comment",
                Instant.now(),
                true,
                130,
                Map.of("key", "value"),
                7L,
                6L
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEnabledNull() {
        var cut = new PlayerDTO(
                5L,
                "name",
                Instant.now(),
                Instant.now(),
                true,
                true,
                "comment",
                Instant.now(),
                null,
                130,
                Map.of("key", "value"),
                7L,
                6L
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateTeamIdNull() {
        var cut = new PlayerDTO(
                5L,
                "name",
                Instant.now(),
                Instant.now(),
                true,
                true,
                "comment",
                Instant.now(),
                true,
                130,
                Map.of("key", "value"),
                null,
                6L
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEventIdNull() {
        var cut = new PlayerDTO(
                5L,
                "name",
                Instant.now(),
                Instant.now(),
                true,
                true,
                "comment",
                Instant.now(),
                true,
                130,
                Map.of("key", "value"),
                7L,
                null
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
