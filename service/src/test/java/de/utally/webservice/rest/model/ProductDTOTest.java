package de.utally.webservice.rest.model;

import de.utally.data.model.ProductCategory;
import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class ProductDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    // Validation Tests
    @Test
    void validateNameMinValid() {
        var cut = new ProductDTO(
                5L,
                "a",
                120,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMinInvalid() {
        var cut = new ProductDTO(
                5L,
                "",
                120,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameMaxValid() {
        var cut = new ProductDTO(
                5L,
                StringUtils.repeat('a', 64),
                120,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMaxInvalid() {
        var cut = new ProductDTO(
                5L,
                StringUtils.repeat('a', 65),
                120,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameNull() {
        var cut = new ProductDTO(
                5L,
                null,
                120,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validatePriceValid() {
        var cut = new ProductDTO(
                5L,
                "name",
                0,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validatePriceInvalid() {
        var cut = new ProductDTO(
                5L,
                "name",
                -1,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validatePriceNull() {
        var cut = new ProductDTO(
                5L,
                "name",
                null,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateImageNameMinValid() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                "a",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateImageNameMinInvalid() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                "",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateImageNameMaxValid() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                StringUtils.repeat('a', 128),
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateImageNameMaxInvalid() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                StringUtils.repeat('a', 129),
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateImageNameNull() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                null,
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateProductCategoryNull() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                "imagename",
                null,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEnabledNull() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                "imagename",
                ProductCategory.FOOD,
                null,
                Instant.now(),
                Instant.now(),
                "description",
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxValid() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                StringUtils.repeat('a', 2048),
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxInvalid() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                StringUtils.repeat('a', 2049),
                true
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateDisplayStatisticsNull() {
        var cut = new ProductDTO(
                5L,
                "name",
                120,
                "imagename",
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                null
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
