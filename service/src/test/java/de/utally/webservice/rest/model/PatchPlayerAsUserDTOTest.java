package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PatchPlayerAsUserDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    @Test
    void validateNameMinValid() {
        var cut = new PatchPlayerAsUserDTO("a", Map.of("key", "value"));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMinInvalid() {
        var cut = new PatchPlayerAsUserDTO("", Map.of("key", "value"));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameMaxValid() {
        var cut = new PatchPlayerAsUserDTO(StringUtils.repeat('a', 64), Map.of("key", "value"));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMaxInvalid() {
        var cut = new PatchPlayerAsUserDTO(StringUtils.repeat('a', 65), Map.of("key", "value"));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameNull() {
        var cut = new PatchPlayerAsUserDTO(null, Map.of("key", "value"));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validatePreferencesNull() {
        var cut = new PatchPlayerAsUserDTO("name", null);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
