package de.utally.webservice.rest.validation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import jakarta.validation.ConstraintValidatorContext;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CurrencyCodeValidatorTest {

    @Mock
    private ConstraintValidatorContext context;

    private final CurrencyCodeValidator validator = new CurrencyCodeValidator();

    @Test
    void isValid() {
        assertThat(validator.isValid("EUR", context)).isTrue();
    }

    @Test
    void isNotValid() {
        assertThat(validator.isValid("ABCDE", context)).isFalse();
    }

    @Test
    void isValidNull() {
        assertThat(validator.isValid(null, context)).isTrue();
    }
}