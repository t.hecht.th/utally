package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import de.utally.data.model.Team;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.model.TeamDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectInvalidArgumentError;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import({TestAdapters.class, WebSecurityConfiguration.class, TestMapperConfig.class})
@WebMvcTest(value = TeamService.class)
class TeamServiceTest extends UtallyServiceTest {

    @BeforeEach
    void setUp() {
        super.setUp();
    }

    @AfterEach
    void tearDown() {
        super.tearDown();
    }

    @Test
    @WithAdmin
    void getTeamsAsAdmin() throws Exception {
        performGetAllTeams();
    }

    @Test
    @WithOperator
    void getTeamsAsOperator() throws Exception {
        performGetAllTeams();
    }

    @Test
    @WithUser
    void getTeamsAsUser() throws Exception {
        performGetAllTeams();
    }

    private void performGetAllTeams() throws Exception {
        mockMvc.perform(
                get("/api/teams/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(team1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(team1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(team1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(team1.getEnabled()))
                .andExpect(jsonPath("$[0].fullName").value(team1.getFullName()))
                .andExpect(jsonPath("$[0].description").value(team1.getDescription()))
                .andExpect(jsonPath("$[0].shortName").value(team1.getShortName()))
                .andExpect(jsonPath("$[0].city").value(team1.getCity()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(team2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(team2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(team2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(team2.getEnabled()))
                .andExpect(jsonPath("$[1].fullName").value(team2.getFullName()))
                .andExpect(jsonPath("$[1].description").value(team2.getDescription()))
                .andExpect(jsonPath("$[1].shortName").value(team2.getShortName()))
                .andExpect(jsonPath("$[1].city").value(team2.getCity()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(team3.getId()))
                .andExpect(jsonPath("$[2].creationTime").value(team3.getCreationTime().toString()))
                .andExpect(jsonPath("$[2].modificationTime").value(team3.getModificationTime().toString()))
                .andExpect(jsonPath("$[2].enabled").value(team3.getEnabled()))
                .andExpect(jsonPath("$[2].fullName").value(team3.getFullName()))
                .andExpect(jsonPath("$[2].description").value(team3.getDescription()))
                .andExpect(jsonPath("$[2].shortName").value(team3.getShortName()))
                .andExpect(jsonPath("$[2].city").value(team3.getCity()))

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(team4.getId()))
                .andExpect(jsonPath("$[3].creationTime").value(team4.getCreationTime().toString()))
                .andExpect(jsonPath("$[3].modificationTime").value(team4.getModificationTime().toString()))
                .andExpect(jsonPath("$[3].enabled").value(team4.getEnabled()))
                .andExpect(jsonPath("$[3].fullName").value(team4.getFullName()))
                .andExpect(jsonPath("$[3].description").value(team4.getDescription()))
                .andExpect(jsonPath("$[3].shortName").value(team4.getShortName()))
                .andExpect(jsonPath("$[3].city").value(team4.getCity()));
    }

    @Test
    void getTeamsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/teams/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTeamsByEventAsAdmin() throws Exception {
        performGetTeamsByEvent();
    }

    @Test
    @WithOperator
    void getTeamsByEventAsOperator() throws Exception {
        performGetTeamsByEvent();
    }

    @Test
    @WithUser
    void getTeamsByEventAsUser() throws Exception {
        performGetTeamsByEvent();
    }

    void performGetTeamsByEvent() throws Exception {
        mockMvc.perform(
                get("/api/teams/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(team1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(team1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(team1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(team1.getEnabled()))
                .andExpect(jsonPath("$[0].fullName").value(team1.getFullName()))
                .andExpect(jsonPath("$[0].description").value(team1.getDescription()))
                .andExpect(jsonPath("$[0].shortName").value(team1.getShortName()))
                .andExpect(jsonPath("$[0].city").value(team1.getCity()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(team2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(team2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(team2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(team2.getEnabled()))
                .andExpect(jsonPath("$[1].fullName").value(team2.getFullName()))
                .andExpect(jsonPath("$[1].description").value(team2.getDescription()))
                .andExpect(jsonPath("$[1].shortName").value(team2.getShortName()))
                .andExpect(jsonPath("$[1].city").value(team2.getCity()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(team3.getId()))
                .andExpect(jsonPath("$[2].creationTime").value(team3.getCreationTime().toString()))
                .andExpect(jsonPath("$[2].modificationTime").value(team3.getModificationTime().toString()))
                .andExpect(jsonPath("$[2].enabled").value(team3.getEnabled()))
                .andExpect(jsonPath("$[2].fullName").value(team3.getFullName()))
                .andExpect(jsonPath("$[2].description").value(team3.getDescription()))
                .andExpect(jsonPath("$[2].shortName").value(team3.getShortName()))
                .andExpect(jsonPath("$[2].city").value(team3.getCity()));
    }

    @Test
    void getTeamsByEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/teams/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getTeamsByEventNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/teams/event/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithUser
    void getTeamsByEventTypeMismatch() throws Exception {
        var actions = mockMvc.perform(
                get("/api/teams/event/abc").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.BADR0002);
    }

    @Test
    @WithAdmin
    void getTeamAsAdmin() throws Exception {
        performGetTeam();
    }

    @Test
    @WithOperator
    void getTeamAsOperator() throws Exception {
        performGetTeam();
    }

    @Test
    @WithUser
    void getTeamAsUser() throws Exception {
        performGetTeam();
    }

    void performGetTeam() throws Exception {
        mockMvc.perform(
                get("/api/teams/{id}", team1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(team1.getId()))
                .andExpect(jsonPath("$.creationTime").value(team1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(team1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(team1.getEnabled()))
                .andExpect(jsonPath("$.fullName").value(team1.getFullName()))
                .andExpect(jsonPath("$.description").value(team1.getDescription()))
                .andExpect(jsonPath("$.shortName").value(team1.getShortName()))
                .andExpect(jsonPath("$.city").value(team1.getCity()));
    }

    @Test
    void getTeamAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/teams/{id}", team1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getTeamNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/teams/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);
    }

    @Test
    @WithAdmin
    void postTeamAsAdmin() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        injectPostSaveCalls(r);

        performPostTeam(r);
    }

    private void performPostTeam(TeamDTO r) throws Exception {
        mockMvc.perform(
                post("/api/teams/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.fullName").value(r.fullName()))
                .andExpect(jsonPath("$.shortName").value(r.shortName()))
                .andExpect(jsonPath("$.city").value(r.city()))
                .andExpect(jsonPath("$.description").value(r.description()));

        verify(teamAdapter).insertTeam(anyString(), anyString(), anyString(), anyString(), anyBoolean());
    }

    private void injectPostSaveCalls(TeamDTO r) {
        doAnswer(i -> {
            var team = new Team();
            team.setId(idSequence++);
            team.setFullName(i.getArgument(0, String.class));
            team.setDescription(i.getArgument(1, String.class));
            team.setCity(i.getArgument(2, String.class));
            team.setShortName(i.getArgument(3, String.class));
            team.setEnabled(i.getArgument(4, Boolean.class));
            team.setCreationTime(nextInstant());
            team.setModificationTime(nextInstant());
            assertAll(
                    () -> assertEquals(r.fullName(), team.getFullName()),
                    () -> assertEquals(r.description(), team.getDescription()),
                    () -> assertEquals(r.city(), team.getCity()),
                    () -> assertEquals(r.shortName(), team.getShortName()),
                    () -> assertEquals(r.enabled(), team.getEnabled())
            );
            return team;
        }).when(teamAdapter).insertTeam(anyString(), anyString(), anyString(), anyString(), anyBoolean());
    }

    @Test
    @WithOperator
    void postTeamAsOperator() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/teams/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void postTeamAsUser() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/teams/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void postTeamAsAnonymous() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/teams/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void postTeamInvalid() throws Exception {
        var r = createTeamResource(null);

        var actions = mockMvc.perform(
                post("/api/teams/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void putTeamAsAdmin() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        injectPutSaveCalls(r);

        performPutTeam(r);
    }

    private void performPutTeam(TeamDTO r) throws Exception {
        mockMvc.perform(
                put("/api/teams/{id}", team1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(team1.getId()))
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.fullName").value(r.fullName()))
                .andExpect(jsonPath("$.shortName").value(r.shortName()))
                .andExpect(jsonPath("$.city").value(r.city()))
                .andExpect(jsonPath("$.description").value(r.description()));

        verify(teamAdapter).updateTeam(any());
    }

    private void injectPutSaveCalls(TeamDTO r) {
        doAnswer(i -> {
            var team = i.getArgument(0, Team.class);
            assertAll(
                    () -> assertEquals(r.fullName(), team.getFullName()),
                    () -> assertEquals(r.description(), team.getDescription()),
                    () -> assertEquals(r.city(), team.getCity()),
                    () -> assertEquals(r.shortName(), team.getShortName()),
                    () -> assertEquals(r.enabled(), team.getEnabled())
            );
            return null;
        }).when(teamAdapter).updateTeam(any());
    }

    @Test
    @WithOperator
    void putTeamAsOperator() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/teams/{id}", team1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void putTeamAsUser() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/teams/{id}", team1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void putTeamAsAnonymous() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/teams/{id}", team1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void putTeamNotFound() throws Exception {
        var r = createTeamResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/teams/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);
    }

    @Test
    @WithAdmin
    void putTeamInvalid() throws Exception {
        var r = createTeamResource(null);

        var actions = mockMvc.perform(
                put("/api/teams/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void deleteTeamAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/teams/{id}", team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(teamAdapter).removeTeam(team1.getId());
    }

    @Test
    @WithOperator
    void deleteTeamAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/teams/{id}", team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteTeamAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/teams/{id}", team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteTeamAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/teams/{id}", team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    private TeamDTO createTeamResource(String shortName) {
        return new TeamDTO(null, randomStringGenerator(), randomStringGenerator(), shortName, true, null, null, randomStringGenerator());
    }
}
