package de.utally.webservice.rest.model;

import de.utally.data.model.*;
import de.utally.settings.Settings;
import de.utally.testutils.EasyRandomProvider;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Currency;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DtoMapperTest {

    private final DtoMapper mapper = Mappers.getMapper(DtoMapper.class);

    private static final EasyRandom r = EasyRandomProvider.easyRandom();

    @Test
    void toPlayerDto() {
        var m = new Player();
        m.setId(5L);
        m.setComment("comment");
        m.setEnabled(true);
        m.setCreationTime(Instant.now());
        var event = new Event();
        event.setId(6L);
        m.setEvent(event);
        var team = new Team();
        team.setId(7L);
        m.setTeam(team);
        m.setName("name");
        m.setPaidPlayersFee(130);
        m.setPaid(true);
        m.setModificationTime(Instant.now());
        m.setPayTime(Instant.now());
        m.setWithPlayersFee(true);
        m.setPreferences(Map.of("key", "value"));
        var r = mapper.toDto(m);
        assertAll(
                () -> assertEquals(m.getComment(), r.comment()),
                () -> assertEquals(m.getEnabled(), r.enabled()),
                () -> assertEquals(m.getCreationTime(), r.creationTime()),
                () -> assertEquals(m.getModificationTime(), r.modificationTime()),
                () -> assertEquals(m.getName(), r.name()),
                () -> assertEquals(m.getPaid(), r.paid()),
                () -> assertEquals(m.getPaidPlayersFee(), r.paidPlayersFee()),
                () -> assertEquals(m.getWithPlayersFee(), r.withPlayersFee()),
                () -> assertEquals(m.getPayTime(), r.payTime()),
                () -> assertEquals(m.getId(), r.id()),
                () -> assertEquals(m.getPreferences(), r.preferences()),
                () -> assertEquals(event.getId(), r.eventId()),
                () -> assertEquals(team.getId(), r.teamId())
        );
    }

    @Test
    void toProductDto() {
        var m = new Product();
        m.setId(5L);
        m.setCategory(ProductCategory.FOOD);
        m.setCreationTime(Instant.now());
        m.setDescription("description");
        m.setEnabled(true);
        m.setDisplayStatistic(true);
        m.setPrice(120);
        m.setImageName("imagename");
        m.setModificationTime(Instant.now());
        m.setName("name");
        var r = mapper.toDto(m);
        assertAll(
                () -> assertEquals(m.getName(), r.name()),
                () -> assertEquals(m.getModificationTime(), r.modificationTime()),
                () -> assertEquals(m.getCategory(), r.category()),
                () -> assertEquals(m.getCreationTime(), r.creationTime()),
                () -> assertEquals(m.getEnabled(), r.enabled()),
                () -> assertEquals(m.getId(), r.id()),
                () -> assertEquals(m.getPrice(), r.price()),
                () -> assertEquals(m.getDescription(), r.description()),
                () -> assertEquals(m.getImageName(), r.imageName()),
                () -> assertEquals(m.getDisplayStatistic(), r.displayStatistic())
        );
    }

    @Test
    void toCurrencyDto() {
        var currency = Currency.getInstance("EUR");

        var result = mapper.toDto(currency);

        assertThat(result.currencyCode()).isEqualTo(currency.getCurrencyCode());
        assertThat(result.displayName()).isEqualTo(currency.getDisplayName());
        assertThat(result.symbol()).isEqualTo(currency.getSymbol());
    }

    @Test
    void toEventDto() {
        var team = new Team();
        team.setId(9L);
        var location = new Location();
        location.setId(11L);
        var m = new Event();
        m.setDescription("description");
        m.setCreationTime(Instant.now());
        m.setEnabled(true);
        m.setId(5L);
        m.setModificationTime(Instant.now());
        m.setPlayersFee(140);
        m.setTitle("title");
        m.setTeams(Set.of(team));
        m.setLocations(Set.of(location));
        var r = mapper.toDto(m);
        assertAll(
                () -> assertEquals(m.getId(), r.id()),
                () -> assertEquals(m.getCreationTime(), r.creationTime()),
                () -> assertEquals(m.getEnabled(), r.enabled()),
                () -> assertEquals(m.getPlayersFee(), r.playersFee()),
                () -> assertEquals(m.getModificationTime(), r.modificationTime()),
                () -> assertEquals(m.getDescription(), r.description()),
                () -> assertEquals(m.getTitle(), r.title()),
                () -> assertTrue(r.locationIds().contains(location.getId())),
                () -> assertTrue(r.teamIds().contains(team.getId()))
        );
    }

    @Test
    void toLocationDto() {
        var product = new Product();
        product.setId(7L);
        var m = new Location();
        m.setId(5L);
        m.setDescription("description");
        m.setCreationTime(Instant.now());
        m.setModificationTime(Instant.now());
        m.setEnabled(true);
        m.setName("name");
        m.setProducts(Set.of(product));
        var r = mapper.toDto(m);
        assertAll(
                () -> assertEquals(m.getModificationTime(), r.modificationTime()),
                () -> assertEquals(m.getCreationTime(), r.creationTime()),
                () -> assertEquals(m.getEnabled(), r.enabled()),
                () -> assertEquals(m.getId(), r.id()),
                () -> assertEquals(m.getName(), r.name()),
                () -> assertEquals(m.getDescription(), r.description()),
                () -> assertTrue(r.productIds().contains(product.getId()))
        );
    }

    @Test
    void toTimeZoneDto() {
        var zoneId = ZoneId.of("Europe/Berlin");

        var dto = mapper.toDto(zoneId);

        assertThat(dto.zoneId()).isEqualTo("Europe/Berlin");
        assertThat(dto.abbreviation()).isEqualTo("CET");
        assertThat(dto.offset()).isEqualTo(zoneId.getRules().getOffset(Instant.now()).getId());
    }

    @Test
    void toSettingsInternal() {
        var settingsDto = new SettingsDTO(4, "EUR", mapper.toDto(ZoneId.of("Australia/Sydney")), "abdnk", 45, true, 80, 400);
        var settings = mapper.toInternal(settingsDto);
        assertThat(settingsDto.currencyCode()).isEqualTo(settings.getCurrency().getCurrencyCode());
        assertThat(settingsDto.dataPoints()).isEqualTo(settings.getDataPoints());
        assertThat(settingsDto.growlDelay()).isEqualTo(settings.getGrowlDelay());
        assertThat(settingsDto.dateTimeFormat()).isEqualTo(settings.getDateTimeFormat());
        assertThat(settingsDto.selfRegistration()).isEqualTo(settings.getSelfRegistration());
        assertThat(settingsDto.liveStatisticsInterval()).isEqualTo(settings.getLiveStatisticsInterval());
        assertThat(settingsDto.keepAliveTimeout()).isEqualTo(settings.getKeepAliveTimeout());
        assertThat(settingsDto.timeZone().zoneId()).isEqualTo(settings.getTimeZone().getId());
    }

    @Test
    void toSettingsDto() {
        var settings = r.nextObject(Settings.class);
        var settingsDto = mapper.toDto(settings);
        assertThat(settingsDto.currencyCode()).isEqualTo(settings.getCurrency().getCurrencyCode());
        assertThat(settingsDto.dataPoints()).isEqualTo(settings.getDataPoints());
        assertThat(settingsDto.growlDelay()).isEqualTo(settings.getGrowlDelay());
        assertThat(settingsDto.dateTimeFormat()).isEqualTo(settings.getDateTimeFormat());
        assertThat(settingsDto.selfRegistration()).isEqualTo(settings.getSelfRegistration());
        assertThat(settingsDto.liveStatisticsInterval()).isEqualTo(settings.getLiveStatisticsInterval());
        assertThat(settingsDto.keepAliveTimeout()).isEqualTo(settings.getKeepAliveTimeout());
        assertThat(settingsDto.timeZone().zoneId()).isEqualTo(settings.getTimeZone().getId());
    }

    @Test
    void toSettingsInternalNull() {
        var settingsDto = new SettingsDTO(null, null, null, null, null, null, null, null);
        var settings = mapper.toInternal(settingsDto);
        assertThat(settings.getCurrency()).isNull();
        assertThat(settings.getDataPoints()).isNull();
        assertThat(settings.getGrowlDelay()).isNull();
        assertThat(settings.getDateTimeFormat()).isNull();
        assertThat(settings.getSelfRegistration()).isNull();
        assertThat(settings.getLiveStatisticsInterval()).isNull();
        assertThat(settings.getKeepAliveTimeout()).isNull();
        assertThat(settings.getTimeZone()).isNull();
    }

    @Test
    void toTimeSlotDto() {
        var e = new Event();
        e.setId(7L);
        var m = new TimeSlot();
        m.setCreationTime(Instant.now());
        m.setDescription("description");
        m.setEnabled(true);
        m.setId(5L);
        m.setEnd(Instant.now());
        m.setModificationTime(Instant.now());
        m.setLabel("label");
        m.setStart(Instant.now());
        m.setEvent(e);
        var r = mapper.toDto(m);
        assertAll(
                () -> assertEquals(m.getId(), r.id()),
                () -> assertEquals(m.getCreationTime(), r.creationTime()),
                () -> assertEquals(m.getEnabled(), r.enabled()),
                () -> assertEquals(m.getModificationTime(), r.modificationTime()),
                () -> assertEquals(m.getEnd(), r.end()),
                () -> assertEquals(m.getLabel(), r.label()),
                () -> assertEquals(m.getDescription(), r.description()),
                () -> assertEquals(m.getStart(), r.start()),
                () -> assertEquals(m.getEvent().getId(), r.eventId())
        );
    }

    @Test
    void toTallyDto() {
        var m = new Tally();
        m.setId(5L);
        m.setCancelled(true);
        m.setTime(Instant.now());
        var location = new Location();
        location.setId(6L);
        m.setLocation(location);
        var product = new Product();
        product.setId(7L);
        m.setProduct(product);
        var player = new Player();
        player.setId(8L);
        m.setPlayer(player);
        var r = mapper.toDto(m);
        assertAll(
                () -> assertEquals(location.getId(), r.locationId()),
                () -> assertEquals(product.getId(), r.productId()),
                () -> assertEquals(player.getId(), r.playerId()),
                () -> assertEquals(m.getId(), r.id()),
                () -> assertEquals(m.getCancelled(), r.cancelled()),
                () -> assertEquals(m.getTime(), r.time())
        );
    }

    @Test
    void toTeamDto() {
        var m = new Team();
        m.setId(5L);
        m.setCity("city");
        m.setCreationTime(Instant.now());
        m.setDescription("description");
        m.setEnabled(true);
        m.setFullName("fullname");
        m.setModificationTime(Instant.now());
        m.setShortName("shortname");
        var r = mapper.toDto(m);
        assertAll(
                () -> assertEquals(m.getModificationTime(), r.modificationTime()),
                () -> assertEquals(m.getCity(), r.city()),
                () -> assertEquals(m.getCreationTime(), r.creationTime()),
                () -> assertEquals(m.getEnabled(), r.enabled()),
                () -> assertEquals(m.getFullName(), r.fullName()),
                () -> assertEquals(m.getDescription(), r.description()),
                () -> assertEquals(m.getShortName(), r.shortName()),
                () -> assertEquals(m.getId(), r.id())
        );
    }
}