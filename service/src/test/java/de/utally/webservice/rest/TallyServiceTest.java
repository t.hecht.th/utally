package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import de.utally.data.exception.LocationNotFoundException;
import de.utally.data.exception.PlayerNotFoundException;
import de.utally.data.exception.ProductNotFoundException;
import de.utally.data.model.Tally;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.model.TallyDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectInvalidArgumentError;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import({TestAdapters.class, WebSecurityConfiguration.class, TestMapperConfig.class})
@WebMvcTest(value = TallyService.class)
class TallyServiceTest extends UtallyServiceTest {

    @BeforeEach
    void setUp() {
        super.setUp();
    }

    @AfterEach
    void tearDown() {
        super.tearDown();
    }

    @Test
    @WithAdmin
    void getTalliesAsAdmin() throws Exception {
        performGetAllTallies();
    }

    @Test
    @WithOperator
    void getTalliesAsOperator() throws Exception {
        performGetAllTallies();
    }

    @Test
    @WithUser
    void getTalliesAsUser() throws Exception {
        performGetAllTallies();
    }

    private void performGetAllTallies() throws Exception {
        mockMvc.perform(
                get("/api/tallies/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(tally3.getId()))
                .andExpect(jsonPath("$[2].playerId").value(tally3.getPlayer().getId()))
                .andExpect(jsonPath("$[2].locationId").value(tally3.getLocation().getId()))
                .andExpect(jsonPath("$[2].productId").value(tally3.getProduct().getId()))
                .andExpect(jsonPath("$[2].cancelled").value(tally3.getCancelled()))

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(tally4.getId()))
                .andExpect(jsonPath("$[3].playerId").value(tally4.getPlayer().getId()))
                .andExpect(jsonPath("$[3].locationId").value(tally4.getLocation().getId()))
                .andExpect(jsonPath("$[3].productId").value(tally4.getProduct().getId()))
                .andExpect(jsonPath("$[3].cancelled").value(tally4.getCancelled()))

                .andExpect(jsonPath("$[4]").exists())
                .andExpect(jsonPath("$[4].id").value(tally5.getId()))
                .andExpect(jsonPath("$[4].playerId").value(tally5.getPlayer().getId()))
                .andExpect(jsonPath("$[4].locationId").value(tally5.getLocation().getId()))
                .andExpect(jsonPath("$[4].productId").value(tally5.getProduct().getId()))
                .andExpect(jsonPath("$[4].cancelled").value(tally5.getCancelled()))

                .andExpect(jsonPath("$[5]").exists())
                .andExpect(jsonPath("$[5].id").value(tally6.getId()))
                .andExpect(jsonPath("$[5].playerId").value(tally6.getPlayer().getId()))
                .andExpect(jsonPath("$[5].locationId").value(tally6.getLocation().getId()))
                .andExpect(jsonPath("$[5].productId").value(tally6.getProduct().getId()))
                .andExpect(jsonPath("$[5].cancelled").value(tally6.getCancelled()))

                .andExpect(jsonPath("$[6]").exists())
                .andExpect(jsonPath("$[6].id").value(tally7.getId()))
                .andExpect(jsonPath("$[6].playerId").value(tally7.getPlayer().getId()))
                .andExpect(jsonPath("$[6].locationId").value(tally7.getLocation().getId()))
                .andExpect(jsonPath("$[6].productId").value(tally7.getProduct().getId()))
                .andExpect(jsonPath("$[6].cancelled").value(tally7.getCancelled()))

                .andExpect(jsonPath("$[7]").exists())
                .andExpect(jsonPath("$[7].id").value(tally8.getId()))
                .andExpect(jsonPath("$[7].playerId").value(tally8.getPlayer().getId()))
                .andExpect(jsonPath("$[7].locationId").value(tally8.getLocation().getId()))
                .andExpect(jsonPath("$[7].productId").value(tally8.getProduct().getId()))
                .andExpect(jsonPath("$[7].cancelled").value(tally8.getCancelled()));
    }

    @Test
    void getTalliesAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByEventAsAdmin() throws Exception {
        performGetAllTalliesByEvent();
    }

    @Test
    @WithOperator
    void getTalliesByEventAsOperator() throws Exception {
        performGetAllTalliesByEvent();
    }

    @Test
    @WithUser
    void getTalliesByEventAsUser() throws Exception {
        performGetAllTalliesByEvent();
    }

    private void performGetAllTalliesByEvent() throws Exception {
        mockMvc.perform(
                get("/api/tallies/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(tally3.getId()))
                .andExpect(jsonPath("$[2].playerId").value(tally3.getPlayer().getId()))
                .andExpect(jsonPath("$[2].locationId").value(tally3.getLocation().getId()))
                .andExpect(jsonPath("$[2].productId").value(tally3.getProduct().getId()))
                .andExpect(jsonPath("$[2].cancelled").value(tally3.getCancelled()))

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(tally4.getId()))
                .andExpect(jsonPath("$[3].playerId").value(tally4.getPlayer().getId()))
                .andExpect(jsonPath("$[3].locationId").value(tally4.getLocation().getId()))
                .andExpect(jsonPath("$[3].productId").value(tally4.getProduct().getId()))
                .andExpect(jsonPath("$[3].cancelled").value(tally4.getCancelled()))

                .andExpect(jsonPath("$[4]").exists())
                .andExpect(jsonPath("$[4].id").value(tally5.getId()))
                .andExpect(jsonPath("$[4].playerId").value(tally5.getPlayer().getId()))
                .andExpect(jsonPath("$[4].locationId").value(tally5.getLocation().getId()))
                .andExpect(jsonPath("$[4].productId").value(tally5.getProduct().getId()))
                .andExpect(jsonPath("$[4].cancelled").value(tally5.getCancelled()))

                .andExpect(jsonPath("$[5]").exists())
                .andExpect(jsonPath("$[5].id").value(tally6.getId()))
                .andExpect(jsonPath("$[5].playerId").value(tally6.getPlayer().getId()))
                .andExpect(jsonPath("$[5].locationId").value(tally6.getLocation().getId()))
                .andExpect(jsonPath("$[5].productId").value(tally6.getProduct().getId()))
                .andExpect(jsonPath("$[5].cancelled").value(tally6.getCancelled()));
    }

    @Test
    void getTalliesByEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByEventNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/event/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getTalliesByTimeslotAsAdmin() throws Exception {
        performGetAllTalliesByTimeslot();
    }

    @Test
    @WithOperator
    void getTalliesByTimeslotAsOperator() throws Exception {
        performGetAllTalliesByTimeslot();
    }

    @Test
    @WithUser
    void getTalliesByTimeslotAsUser() throws Exception {
        performGetAllTalliesByTimeslot();
    }

    private void performGetAllTalliesByTimeslot() throws Exception {
        mockMvc.perform(
                        get("/api/tallies/timeslot/{id}", timeSlot1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(tally3.getId()))
                .andExpect(jsonPath("$[2].playerId").value(tally3.getPlayer().getId()))
                .andExpect(jsonPath("$[2].locationId").value(tally3.getLocation().getId()))
                .andExpect(jsonPath("$[2].productId").value(tally3.getProduct().getId()))
                .andExpect(jsonPath("$[2].cancelled").value(tally3.getCancelled()))

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(tally4.getId()))
                .andExpect(jsonPath("$[3].playerId").value(tally4.getPlayer().getId()))
                .andExpect(jsonPath("$[3].locationId").value(tally4.getLocation().getId()))
                .andExpect(jsonPath("$[3].productId").value(tally4.getProduct().getId()))
                .andExpect(jsonPath("$[3].cancelled").value(tally4.getCancelled()))

                .andExpect(jsonPath("$[4]").exists())
                .andExpect(jsonPath("$[4].id").value(tally5.getId()))
                .andExpect(jsonPath("$[4].playerId").value(tally5.getPlayer().getId()))
                .andExpect(jsonPath("$[4].locationId").value(tally5.getLocation().getId()))
                .andExpect(jsonPath("$[4].productId").value(tally5.getProduct().getId()))
                .andExpect(jsonPath("$[4].cancelled").value(tally5.getCancelled()))

                .andExpect(jsonPath("$[5]").exists())
                .andExpect(jsonPath("$[5].id").value(tally6.getId()))
                .andExpect(jsonPath("$[5].playerId").value(tally6.getPlayer().getId()))
                .andExpect(jsonPath("$[5].locationId").value(tally6.getLocation().getId()))
                .andExpect(jsonPath("$[5].productId").value(tally6.getProduct().getId()))
                .andExpect(jsonPath("$[5].cancelled").value(tally6.getCancelled()));
    }

    @Test
    void getTalliesByTimeslotAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/timeslot/{id}", timeSlot1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByTimeslotNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/timeslot/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0006);
    }

    @Test
    @WithAdmin
    void getTalliesByLocationAsAdmin() throws Exception {
        performGetAllTalliesByLocation();
    }

    @Test
    @WithOperator
    void getTalliesByLocationAsOperator() throws Exception {
        performGetAllTalliesByLocation();
    }

    @Test
    @WithUser
    void getTalliesByLocationAsUser() throws Exception {
        performGetAllTalliesByLocation();
    }

    private void performGetAllTalliesByLocation() throws Exception {
        mockMvc.perform(
                get("/api/tallies/location/{id}", location1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()));
    }

    @Test
    void getTalliesByLocationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/location/{id}", location1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByLocationNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/location/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void getTalliesByPlayerAsAdmin() throws Exception {
        performGetAllTalliesByPlayer();
    }

    @Test
    @WithOperator
    void getTalliesByPlayerAsOperator() throws Exception {
        performGetAllTalliesByPlayer();
    }

    @Test
    @WithUser
    void getTalliesByPlayerAsUser() throws Exception {
        performGetAllTalliesByPlayer();
    }

    private void performGetAllTalliesByPlayer() throws Exception {
        mockMvc.perform(
                get("/api/tallies/player/{id}", player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()));
    }

    @Test
    void getTalliesByPlayerAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/player/{id}", player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByPlayerNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/player/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void getTalliesByProductAsAdmin() throws Exception {
        performGetAllTalliesByProduct();
    }

    @Test
    @WithOperator
    void getTalliesByProductAsOperator() throws Exception {
        performGetAllTalliesByProduct();
    }

    @Test
    @WithUser
    void getTalliesByProductAsUser() throws Exception {
        performGetAllTalliesByProduct();
    }

    private void performGetAllTalliesByProduct() throws Exception {
        mockMvc.perform(
                get("/api/tallies/product/{id}", product1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally7.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally7.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally7.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally7.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally7.getCancelled()));
    }

    @Test
    void getTalliesByProductAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/product/{id}", product1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByProductNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/product/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    @Test
    @WithAdmin
    void getTalliesByTeamAsAdmin() throws Exception {
        performGetAllTalliesByTeam();
    }

    @Test
    @WithOperator
    void getTalliesByTeamAsOperator() throws Exception {
        performGetAllTalliesByTeam();
    }

    @Test
    @WithUser
    void getTalliesByTeamAsUser() throws Exception {
        performGetAllTalliesByTeam();
    }

    private void performGetAllTalliesByTeam() throws Exception {
        mockMvc.perform(
                get("/api/tallies/team/{id}", team1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(tally3.getId()))
                .andExpect(jsonPath("$[2].playerId").value(tally3.getPlayer().getId()))
                .andExpect(jsonPath("$[2].locationId").value(tally3.getLocation().getId()))
                .andExpect(jsonPath("$[2].productId").value(tally3.getProduct().getId()))
                .andExpect(jsonPath("$[2].cancelled").value(tally3.getCancelled()))

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(tally4.getId()))
                .andExpect(jsonPath("$[3].playerId").value(tally4.getPlayer().getId()))
                .andExpect(jsonPath("$[3].locationId").value(tally4.getLocation().getId()))
                .andExpect(jsonPath("$[3].productId").value(tally4.getProduct().getId()))
                .andExpect(jsonPath("$[3].cancelled").value(tally4.getCancelled()));
    }

    @Test
    void getTalliesByTeamAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/team/{id}", team1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByTeamNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/team/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);
    }

    @Test
    @WithAdmin
    void getTalliesByProductAndPlayerAsAdmin() throws Exception {
        performGetAllTalliesByProductAndPlayer();
    }

    @Test
    @WithOperator
    void getTalliesByProductAndPlayerAsOperator() throws Exception {
        performGetAllTalliesByProductAndPlayer();
    }

    @Test
    @WithUser
    void getTalliesByProductAndPlayerAsUser() throws Exception {
        performGetAllTalliesByProductAndPlayer();
    }

    private void performGetAllTalliesByProductAndPlayer() throws Exception {
        mockMvc.perform(
                get("/api/tallies/product/{productId}/player/{playerId}", product1.getId(), player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()));
    }

    @Test
    void getTalliesByProductAndPlayerAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/product/{productId}/player/{playerId}", product1.getId(), player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByProductAndPlayerProductNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/product/{productId}/player/{playerId}", idSequence++, player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    @Test
    @WithAdmin
    void getTalliesByProductAndPlayerPlayerNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/product/{productId}/player/{playerId}", product1.getId(), idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void getTalliesByLocationAndPlayerAsAdmin() throws Exception {
        performGetAllTalliesByLocationAndPlayer();
    }

    @Test
    @WithOperator
    void getTalliesByLocationAndPlayerAsOperator() throws Exception {
        performGetAllTalliesByLocationAndPlayer();
    }

    @Test
    @WithUser
    void getTalliesByLocationAndPlayerAsUser() throws Exception {
        performGetAllTalliesByLocationAndPlayer();
    }

    private void performGetAllTalliesByLocationAndPlayer() throws Exception {
        mockMvc.perform(
                get("/api/tallies/location/{locationId}/player/{playerId}", location1.getId(), player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()));
    }

    @Test
    void getTalliesByLocationAndPlayerAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/location/{locationId}/player/{playerId}", location1.getId(), player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByLocationAndPlayerLocationNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/location/{locationId}/player/{playerId}", idSequence++, player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void getTalliesByLocationAndPlayerPlayerNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/location/{locationId}/player/{playerId}", location1.getId(), idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void getTalliesByLocationAndEventAsAdmin() throws Exception {
        performGetAllTalliesByLocationAndEvent();
    }

    @Test
    @WithOperator
    void getTalliesByLocationAndEventAsOperator() throws Exception {
        performGetAllTalliesByLocationAndEvent();
    }

    @Test
    @WithUser
    void getTalliesByLocationAndEventAsUser() throws Exception {
        performGetAllTalliesByLocationAndEvent();
    }

    private void performGetAllTalliesByLocationAndEvent() throws Exception {
        mockMvc.perform(
                get("/api/tallies/location/{locationId}/event/{eventId}", location1.getId(), event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()));
    }

    @Test
    void getTalliesByLocationAndEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/location/{locationId}/event/{eventId}", location1.getId(), event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByLocationAndEventLocationNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/location/{locationId}/event/{eventId}", idSequence++, event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void getTalliesByLocationAndEventEventNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/location/{locationId}/event/{eventId}", location1.getId(), idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getTalliesByProductAndEventAsAdmin() throws Exception {
        performGetAllTalliesByProductAndEvent();
    }

    @Test
    @WithOperator
    void getTalliesByProductAndEventAsOperator() throws Exception {
        performGetAllTalliesByProductAndEvent();
    }

    @Test
    @WithUser
    void getTalliesByProductAndEventAsUser() throws Exception {
        performGetAllTalliesByProductAndEvent();
    }

    private void performGetAllTalliesByProductAndEvent() throws Exception {
        mockMvc.perform(
                get("/api/tallies/product/{productId}/event/{eventId}", product1.getId(), event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()));
    }

    @Test
    void getTalliesByProductAndEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/product/{productId}/event/{eventId}", product1.getId(), event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByProductAndEventProductNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/product/{productId}/event/{eventId}", idSequence++, event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    @Test
    @WithAdmin
    void getTalliesByProductAndEventEventNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/product/{productId}/event/{eventId}", product1.getId(), idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getTalliesByTeamAndEventAsAdmin() throws Exception {
        performGetAllTalliesByTeamAndEvent();
    }

    @Test
    @WithOperator
    void getTalliesByTeamAndEventAsOperator() throws Exception {
        performGetAllTalliesByTeamAndEvent();
    }

    @Test
    @WithUser
    void getTalliesByTeamAndEventAsUser() throws Exception {
        performGetAllTalliesByTeamAndEvent();
    }

    private void performGetAllTalliesByTeamAndEvent() throws Exception {
        mockMvc.perform(
                get("/api/tallies/team/{teamId}/event/{eventId}", team1.getId(), event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(tally1.getId()))
                .andExpect(jsonPath("$[0].playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$[0].locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$[0].productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$[0].cancelled").value(tally1.getCancelled()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(tally2.getId()))
                .andExpect(jsonPath("$[1].playerId").value(tally2.getPlayer().getId()))
                .andExpect(jsonPath("$[1].locationId").value(tally2.getLocation().getId()))
                .andExpect(jsonPath("$[1].productId").value(tally2.getProduct().getId()))
                .andExpect(jsonPath("$[1].cancelled").value(tally2.getCancelled()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(tally3.getId()))
                .andExpect(jsonPath("$[2].playerId").value(tally3.getPlayer().getId()))
                .andExpect(jsonPath("$[2].locationId").value(tally3.getLocation().getId()))
                .andExpect(jsonPath("$[2].productId").value(tally3.getProduct().getId()))
                .andExpect(jsonPath("$[2].cancelled").value(tally3.getCancelled()))

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(tally4.getId()))
                .andExpect(jsonPath("$[3].playerId").value(tally4.getPlayer().getId()))
                .andExpect(jsonPath("$[3].locationId").value(tally4.getLocation().getId()))
                .andExpect(jsonPath("$[3].productId").value(tally4.getProduct().getId()))
                .andExpect(jsonPath("$[3].cancelled").value(tally4.getCancelled()));
    }

    @Test
    void getTalliesByTeamAndEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/team/{teamId}/event/{eventId}", team1.getId(), event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTalliesByTeamAndEventTeamNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/team/{teamId}/event/{eventId}", idSequence++, event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);
    }

    @Test
    @WithAdmin
    void getTalliesByTeamAndEventEventNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/team/{teamId}/event/{eventId}", team1.getId(), idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getTallyAsAdmin() throws Exception {
        performGetTally();
    }

    @Test
    @WithOperator
    void getTallyAsOperator() throws Exception {
        performGetTally();
    }

    @Test
    @WithUser
    void getTallyAsUser() throws Exception {
        performGetTally();
    }

    private void performGetTally() throws Exception {
        mockMvc.perform(
                get("/api/tallies/{id}", tally1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(tally1.getId()))
                .andExpect(jsonPath("$.playerId").value(tally1.getPlayer().getId()))
                .andExpect(jsonPath("$.locationId").value(tally1.getLocation().getId()))
                .andExpect(jsonPath("$.productId").value(tally1.getProduct().getId()))
                .andExpect(jsonPath("$.cancelled").value(tally1.getCancelled()));
    }

    @Test
    void getTallyAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/{id}", tally1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTallyNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/tallies/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0007);
    }

    @Test
    @WithAdmin
    void postTallyAsAdmin() throws Exception {
        var r = createTallyDTO();

        injectPostSaveCalls();

        performPostTally(r);
    }

    @Test
    @WithOperator
    void postTallyAsOperator() throws Exception {
        var r = createTallyDTO();

        injectPostSaveCalls();

        performPostTally(r);
    }

    @Test
    @WithUser
    void postTallyAsUser() throws Exception {
        var r = createTallyDTO();

        injectPostSaveCalls();

        performPostTally(r);
    }

    private void performPostTally(TallyDTO r) throws Exception {
        mockMvc.perform(
                post("/api/tallies/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.playerId").value(player1.getId()))
                .andExpect(jsonPath("$.locationId").value(location1.getId()))
                .andExpect(jsonPath("$.productId").value(product1.getId()))
                .andExpect(jsonPath("$.cancelled").value(false));

        verify(tallyAdapter).insertTally(anyLong(), anyLong(), anyLong());
    }

    private void injectPostSaveCalls() {
        doAnswer(i -> {
            assertAll(
                    () -> assertEquals(player1.getId(), i.getArgument(0, Long.class)),
                    () -> assertEquals(product1.getId(), i.getArgument(1, Long.class)),
                    () -> assertEquals(location1.getId(), i.getArgument(2, Long.class))
            );
            var tally = new Tally();
            tally.setId(idSequence++);
            tally.setLocation(location1);
            tally.setProduct(product1);
            tally.setPlayer(player1);
            tally.setCancelled(false);
            return tally;
        }).when(tallyAdapter).insertTally(anyLong(), anyLong(), anyLong());
    }

    @Test
    void postTallyAsAnonymous() throws Exception {
        var r = createTallyDTO();

        var actions = mockMvc.perform(
                post("/api/tallies/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void postTallyPlayerNotFound() throws Exception {
        var r = new TallyDTO(null, idSequence++, location1.getId(), nextInstant(), product1.getId(), false);
        when(tallyAdapter.insertTally(anyLong(), anyLong(), anyLong())).thenThrow(PlayerNotFoundException.class);

        var actions = mockMvc.perform(
                        post("/api/tallies/")
                                .content(objectMapper.writeValueAsString(r))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void postTallyProductNotFound() throws Exception {
        var r = new TallyDTO(null, player1.getId(), location1.getId(), nextInstant(), idSequence++, false);
        when(tallyAdapter.insertTally(anyLong(), anyLong(), anyLong())).thenThrow(ProductNotFoundException.class);

        var actions = mockMvc.perform(
                        post("/api/tallies/")
                                .content(objectMapper.writeValueAsString(r))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    @Test
    @WithAdmin
    void postTallyLocationNotFound() throws Exception {
        var r = new TallyDTO(null, player1.getId(), idSequence++, nextInstant(), product1.getId(), false);
        when(tallyAdapter.insertTally(anyLong(), anyLong(), anyLong())).thenThrow(LocationNotFoundException.class);

        var actions = mockMvc.perform(
                        post("/api/tallies/")
                                .content(objectMapper.writeValueAsString(r))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void postTallyInvalid() throws Exception {
        var r = new TallyDTO(null, player1.getId(), null, nextInstant(), product1.getId(), false);

        var actions = mockMvc.perform(
                post("/api/tallies/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void patchTallyCancelAsAdmin() throws Exception {
        performPatchTally(true);

        verify(tallyAdapter).cancelTally(tally1.getId());
    }

    @Test
    @WithOperator
    void patchTallyCancelAsOperator() throws Exception {
        performPatchTally(true);

        verify(tallyAdapter).cancelTally(tally1.getId());
    }

    @Test
    @WithUser
    void patchTallyCancelAsUser() throws Exception {
        performPatchTally(true);

        verify(tallyAdapter).cancelTally(tally1.getId());
    }

    private void performPatchTally(boolean cancelled) throws Exception {
        mockMvc.perform(
                patch("/api/tallies/{id}/cancelled/{cancelled}", tally1.getId(), cancelled)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.playerId").value(player1.getId()))
                .andExpect(jsonPath("$.locationId").value(location1.getId()))
                .andExpect(jsonPath("$.productId").value(product1.getId()))
                .andExpect(jsonPath("$.cancelled").exists());
    }

    @Test
    void patchTallyCancelAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/tallies/{id}/cancelled/{cancelled}", tally1.getId(), true)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void patchTallyCancelNotExisting() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/tallies/{id}/cancelled/{cancelled}", idSequence++, true)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0007);
    }

    @Test
    @WithAdmin
    void patchTallyRevokeAsAdmin() throws Exception {
        performPatchTally(false);

        verify(tallyAdapter).revokeCancellation(tally1.getId());
    }

    @Test
    @WithOperator
    void patchTallyRevokeAsOperator() throws Exception {
        performPatchTally(false);

        verify(tallyAdapter).revokeCancellation(tally1.getId());
    }

    @Test
    @WithUser
    void patchTallyRevokeAsUser() throws Exception {
        performPatchTally(false);

        verify(tallyAdapter).revokeCancellation(tally1.getId());
    }

    @Test
    void patchTallyRevokeAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/tallies/{id}/cancelled/{cancelled}", tally1.getId(), false)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void patchTallyRevokeNotExisting() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/tallies/{id}/cancelled/{cancelled}", idSequence++, false)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0007);
    }

    @Test
    @WithAdmin
    void deleteTallyAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/tallies/{id}", tally1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(tallyAdapter).removeTally(tally1.getId());
    }

    @Test
    @WithOperator
    void deleteTallyAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/{id}", tally1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteTallyAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/{id}", tally1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteTallyAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/{id}", tally1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void deleteTalliesByProductAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/tallies/product/{id}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(tallyAdapter).removeTalliesByProduct(product1.getId());
    }

    @Test
    @WithOperator
    void deleteTalliesByProductAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/product/{id}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteTalliesByProductAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/product/{id}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteTalliesByProductAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/product/{id}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void deleteTalliesByProductNotFound() throws Exception {
        doThrow(ProductNotFoundException.class).when(tallyAdapter).removeTalliesByProduct(anyLong());

        var actions = mockMvc.perform(
                delete("/api/tallies/product/{id}", idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    @Test
    @WithAdmin
    void deleteTalliesByPlayerAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/tallies/player/{id}", player1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(tallyAdapter).removeTalliesByPlayer(player1.getId());
    }

    @Test
    @WithOperator
    void deleteTalliesByPlayerAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/player/{id}", player1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteTalliesByPlayerAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/player/{id}", player1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteTalliesByPlayerAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/player/{id}", player1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void deleteTalliesByPlayerNotFound() throws Exception {
        doThrow(PlayerNotFoundException.class).when(tallyAdapter).removeTalliesByPlayer(anyLong());

        var actions = mockMvc.perform(
                delete("/api/tallies/player/{id}", idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void deleteTalliesByLocationAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/tallies/location/{id}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(tallyAdapter).removeTalliesByLocation(location1.getId());
    }

    @Test
    @WithOperator
    void deleteTalliesByLocationAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/location/{id}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteTalliesByLocationAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/location/{id}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteTalliesByLocationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/tallies/location/{id}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void deleteTalliesByLocationNotFound() throws Exception {
        doThrow(LocationNotFoundException.class).when(tallyAdapter).removeTalliesByLocation(anyLong());

        var actions = mockMvc.perform(
                delete("/api/tallies/location/{id}", idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    private TallyDTO createTallyDTO() {
        return new TallyDTO(null, player1.getId(), location1.getId(), nextInstant(), product1.getId(), false);
    }
}
