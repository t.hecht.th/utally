package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;

import static org.junit.jupiter.api.Assertions.*;

class PatchPlayerCommentDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    @Test
    void validateCommentMinValid() {
        var cut = createPatchPlayerCommentDTO("");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateCommentMaxValid() {
        var cut = createPatchPlayerCommentDTO(StringUtils.repeat('a', 64));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateCommentMaxInvalid() {
        var cut = createPatchPlayerCommentDTO(StringUtils.repeat('a', 65));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateCommentNull() {
        var cut = createPatchPlayerCommentDTO(null);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    private PatchPlayerCommentDTO createPatchPlayerCommentDTO(String comment) {
        return new PatchPlayerCommentDTO(comment);
    }
}