package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import de.utally.data.exception.EventNotFoundException;
import de.utally.data.exception.TimeSlotNotFoundException;
import de.utally.data.model.TimeSlot;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.model.TimeSlotDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

import java.time.Instant;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectInvalidArgumentError;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import({TestAdapters.class, WebSecurityConfiguration.class, TestMapperConfig.class})
@WebMvcTest(value = TimeSlotService.class)
class TimeSlotServiceTest extends UtallyServiceTest {

    @BeforeEach
    void setUp() {
        super.setUp();
    }

    @AfterEach
    void tearDown() {
        super.tearDown();
    }

    @Test
    @WithAdmin
    void getTimeSlotsAsAdmin() throws Exception {
        performGetAllTimeSlots();
    }

    @Test
    @WithOperator
    void getTimeSlotsAsOperator() throws Exception {
        performGetAllTimeSlots();
    }

    @Test
    @WithUser
    void getTimeSlotsAsUser() throws Exception {
        performGetAllTimeSlots();
    }

    private void performGetAllTimeSlots() throws Exception {
        mockMvc.perform(
                get("/api/timeslots/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(timeSlot1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(timeSlot1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(timeSlot1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].label").value(timeSlot1.getLabel()))
                .andExpect(jsonPath("$[0].end").value(timeSlot1.getEnd().toString()))
                .andExpect(jsonPath("$[0].description").value(timeSlot1.getDescription()))
                .andExpect(jsonPath("$[0].start").value(timeSlot1.getStart().toString()))
                .andExpect(jsonPath("$[0].enabled").value(timeSlot1.getEnabled()))
                .andExpect(jsonPath("$[0].eventId").value(timeSlot1.getEvent().getId()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(timeSlot2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(timeSlot2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(timeSlot2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].label").value(timeSlot2.getLabel()))
                .andExpect(jsonPath("$[1].end").value(timeSlot2.getEnd().toString()))
                .andExpect(jsonPath("$[1].description").value(timeSlot2.getDescription()))
                .andExpect(jsonPath("$[1].start").value(timeSlot2.getStart().toString()))
                .andExpect(jsonPath("$[1].enabled").value(timeSlot2.getEnabled()))
                .andExpect(jsonPath("$[1].eventId").value(timeSlot2.getEvent().getId()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(timeSlot3.getId()))
                .andExpect(jsonPath("$[2].creationTime").value(timeSlot3.getCreationTime().toString()))
                .andExpect(jsonPath("$[2].modificationTime").value(timeSlot3.getModificationTime().toString()))
                .andExpect(jsonPath("$[2].label").value(timeSlot3.getLabel()))
                .andExpect(jsonPath("$[2].end").value(timeSlot3.getEnd().toString()))
                .andExpect(jsonPath("$[2].description").value(timeSlot3.getDescription()))
                .andExpect(jsonPath("$[2].start").value(timeSlot3.getStart().toString()))
                .andExpect(jsonPath("$[2].enabled").value(timeSlot3.getEnabled()))
                .andExpect(jsonPath("$[2].eventId").value(timeSlot3.getEvent().getId()))

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(timeSlot4.getId()))
                .andExpect(jsonPath("$[3].creationTime").value(timeSlot4.getCreationTime().toString()))
                .andExpect(jsonPath("$[3].modificationTime").value(timeSlot4.getModificationTime().toString()))
                .andExpect(jsonPath("$[3].label").value(timeSlot4.getLabel()))
                .andExpect(jsonPath("$[3].end").value(timeSlot4.getEnd().toString()))
                .andExpect(jsonPath("$[3].description").value(timeSlot4.getDescription()))
                .andExpect(jsonPath("$[3].start").value(timeSlot4.getStart().toString()))
                .andExpect(jsonPath("$[3].enabled").value(timeSlot4.getEnabled()))
                .andExpect(jsonPath("$[3].eventId").value(timeSlot4.getEvent().getId()));
    }

    @Test
    void getTimeSlotsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/timeslots/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTimeSlotsByEventAsAdmin() throws Exception {
        performGetTimeSlotsByEvent();
    }

    @Test
    @WithOperator
    void getTimeSlotsByEventAsOperator() throws Exception {
        performGetTimeSlotsByEvent();
    }

    @Test
    @WithUser
    void getTimeSlotsByEventAsUser() throws Exception {
        performGetTimeSlotsByEvent();
    }

    private void performGetTimeSlotsByEvent() throws Exception {
        mockMvc.perform(
                get("/api/timeslots/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(timeSlot1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(timeSlot1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(timeSlot1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].label").value(timeSlot1.getLabel()))
                .andExpect(jsonPath("$[0].end").value(timeSlot1.getEnd().toString()))
                .andExpect(jsonPath("$[0].description").value(timeSlot1.getDescription()))
                .andExpect(jsonPath("$[0].start").value(timeSlot1.getStart().toString()))
                .andExpect(jsonPath("$[0].enabled").value(timeSlot1.getEnabled()))
                .andExpect(jsonPath("$[0].eventId").value(timeSlot1.getEvent().getId()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(timeSlot2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(timeSlot2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(timeSlot2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].label").value(timeSlot2.getLabel()))
                .andExpect(jsonPath("$[1].end").value(timeSlot2.getEnd().toString()))
                .andExpect(jsonPath("$[1].description").value(timeSlot2.getDescription()))
                .andExpect(jsonPath("$[1].start").value(timeSlot2.getStart().toString()))
                .andExpect(jsonPath("$[1].enabled").value(timeSlot2.getEnabled()))
                .andExpect(jsonPath("$[1].eventId").value(timeSlot2.getEvent().getId()));
    }

    @Test
    void getTimeSlotsByEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/timeslots/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getTimeSlotsByEventNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/timeslots/event/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getTimeSlotAsAdmin() throws Exception {
        performGetTimeSlot();
    }

    @Test
    @WithOperator
    void getTimeSlotAsOperator() throws Exception {
        performGetTimeSlot();
    }

    @Test
    @WithUser
    void getTimeSlotAsUser() throws Exception {
        performGetTimeSlot();
    }

    private void performGetTimeSlot() throws Exception {
        mockMvc.perform(
                get("/api/timeslots/{id}", timeSlot1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(timeSlot1.getId()))
                .andExpect(jsonPath("$.creationTime").value(timeSlot1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(timeSlot1.getModificationTime().toString()))
                .andExpect(jsonPath("$.label").value(timeSlot1.getLabel()))
                .andExpect(jsonPath("$.end").value(timeSlot1.getEnd().toString()))
                .andExpect(jsonPath("$.description").value(timeSlot1.getDescription()))
                .andExpect(jsonPath("$.start").value(timeSlot1.getStart().toString()))
                .andExpect(jsonPath("$.enabled").value(timeSlot1.getEnabled()))
                .andExpect(jsonPath("$.eventId").value(timeSlot1.getEvent().getId()));
    }

    @Test
    void getTimeSlotAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/timeslots/{id}", timeSlot1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getTimeSlotNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/timeslots/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0006);
    }

    @Test
    @WithAdmin
    void postTimeSlotAsAdmin() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        injectPostSaveCalls(r);

        performPostTimeSlot(r);
    }

    private void performPostTimeSlot(TimeSlotDTO r) throws Exception {
        mockMvc.perform(
                post("/api/timeslots/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.label").value(r.label()))
                .andExpect(jsonPath("$.description").value(r.description()))
                .andExpect(jsonPath("$.start").value(r.start().toString()))
                .andExpect(jsonPath("$.end").value(r.end().toString()))
                .andExpect(jsonPath("$.eventId").value(r.eventId()));

        verify(timeSlotAdapter).insertTimeSlot(anyString(), anyString(), any(), any(), anyBoolean(), anyLong());
    }

    private void injectPostSaveCalls(TimeSlotDTO r) {
        doAnswer(i -> {
            var timeSlot = new TimeSlot();
            timeSlot.setId(idSequence++);
            timeSlot.setLabel(i.getArgument(0, String.class));
            timeSlot.setDescription(i.getArgument(1, String.class));
            timeSlot.setStart(i.getArgument(2, Instant.class));
            timeSlot.setEnd(i.getArgument(3, Instant.class));
            timeSlot.setEnabled(i.getArgument(4, Boolean.class));
            timeSlot.setEvent(event1);
            timeSlot.setCreationTime(nextInstant());
            timeSlot.setModificationTime(nextInstant());
            assertAll(
                    () -> assertEquals(r.enabled(), timeSlot.getEnabled()),
                    () -> assertEquals(r.label(), timeSlot.getLabel()),
                    () -> assertEquals(r.start(), timeSlot.getStart()),
                    () -> assertEquals(r.end(), timeSlot.getEnd()),
                    () -> assertEquals(r.description(), timeSlot.getDescription()),
                    () -> assertEquals(r.eventId(), i.getArgument(5, Long.class))
            );
            return timeSlot;
        }).when(timeSlotAdapter).insertTimeSlot(anyString(), anyString(), any(), any(), anyBoolean(), anyLong());
    }

    @Test
    @WithOperator
    void postTimeSlotAsOperator() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/timeslots/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void postTimeSlotAsUser() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/timeslots/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void postTimeSlotAsAnonymous() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/timeslots/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void postTimeSlotEventNotFound() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        when(timeSlotAdapter.insertTimeSlot(anyString(), anyString(), any(), any(), anyBoolean(), anyLong())).thenThrow(EventNotFoundException.class);

        var actions = mockMvc.perform(
                post("/api/timeslots/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void postTimeSlotInvalid() throws Exception {
        var r = createTimeSlotResource(null);

        var actions = mockMvc.perform(
                post("/api/timeslots/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void putTimeSlotAsAdmin() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        injectPutSaveCalls(r);

        performPutTimeSlot(r);
    }

    private void performPutTimeSlot(TimeSlotDTO r) throws Exception {
        mockMvc.perform(
                put("/api/timeslots/{id}", timeSlot1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(timeSlot1.getId()))
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.label").value(r.label()))
                .andExpect(jsonPath("$.description").value(r.description()))
                .andExpect(jsonPath("$.start").value(r.start().toString()))
                .andExpect(jsonPath("$.end").value(r.end().toString()))
                .andExpect(jsonPath("$.eventId").value(r.eventId()));

        verify(timeSlotAdapter).updateTimeSlot(any());
    }

    private void injectPutSaveCalls(TimeSlotDTO r) {
        doAnswer(i -> {
            var timeSlot = i.getArgument(0, TimeSlot.class);
            assertAll(
                    () -> assertEquals(r.enabled(), timeSlot.getEnabled()),
                    () -> assertEquals(r.label(), timeSlot.getLabel()),
                    () -> assertEquals(r.start(), timeSlot.getStart()),
                    () -> assertEquals(r.end(), timeSlot.getEnd()),
                    () -> assertEquals(r.description(), timeSlot.getDescription())
            );
            return null;
        }).when(timeSlotAdapter).updateTimeSlot(any());
    }

    @Test
    @WithOperator
    void putTimeSlotAsOperator() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/timeslots/{id}", timeSlot1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void putTimeSlotAsUser() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/timeslots/{id}", timeSlot1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void putTimeSlotAsAnonymous() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/timeslots/{id}", timeSlot1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void putTimeSlotNotFound() throws Exception {
        var r = createTimeSlotResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/timeslots/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0006);
    }

    @Test
    @WithAdmin
    void putTimeSlotInvalid() throws Exception {
        var r = createTimeSlotResource(null);

        var actions = mockMvc.perform(
                put("/api/timeslots/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void deleteTimeSlotAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/timeslots/{id}", timeSlot1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(timeSlotAdapter).removeTimeSlot(timeSlot1.getId());
    }

    @Test
    @WithOperator
    void deleteTimeSlotAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/timeslots/{id}", timeSlot1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteTimeSlotAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/timeslots/{id}", timeSlot1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteTimeSlotAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/timeslots/{id}", timeSlot1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getStatisticsAsAdmin() throws Exception {
        performGetStatistics();
    }

    @Test
    @WithOperator
    void getStatisticsAsOperator() throws Exception {
        performGetStatistics();
    }

    @Test
    @WithUser
    void getStatisticsAsUser() throws Exception {
        performGetStatistics();
    }

    private void performGetStatistics() throws Exception {
        mockMvc.perform(
                        get("/api/timeslots/statistics/{id}", timeSlot4.getId())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[?(@.productId == " + product1.getId() + " && @.teamId == " + team3.getId() + ")].count").value(0))
                .andExpect(jsonPath("$[?(@.productId == " + product1.getId() + " && @.teamId == " + team4.getId() + ")].count").value(1))
                .andExpect(jsonPath("$[?(@.productId == " + product3.getId() + " && @.teamId == " + team3.getId() + ")].count").value(0))
                .andExpect(jsonPath("$[?(@.productId == " + product3.getId() + " && @.teamId == " + team4.getId() + ")].count").value(1));

        verify(timeSlotAdapter).getNumberOfTalliesByTeamInTimeSlotForStatistics(timeSlot4.getId());
    }

    @Test
    void getStatisticsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/timeslots/statistics/{Id}", timeSlot4.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getStatisticsNotFound() throws Exception {
        doThrow(TimeSlotNotFoundException.class).when(timeSlotAdapter).getNumberOfTalliesByTeamInTimeSlotForStatistics(anyLong());
        var actions = mockMvc.perform(
                get("/api/timeslots/statistics/{Id}", idSequence++)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0006);
    }

    private TimeSlotDTO createTimeSlotResource(String label) {
        return new TimeSlotDTO(null, label, nextInstant(), nextInstant(), true, null, null, randomStringGenerator(), event1.getId());
    }
}
