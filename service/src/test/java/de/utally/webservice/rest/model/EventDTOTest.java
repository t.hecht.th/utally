package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import java.time.Instant;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class EventDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    // Validation Tests
    @Test
    void validateTitleMinValid() {
        var cut = new EventDTO(
                5L,
                "a",
                140,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateTitleMinInvalid() {
        var cut = new EventDTO(
                5L,
                "",
                140,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateTitleMaxValid() {
        var cut = new EventDTO(
                5L,
                StringUtils.repeat('a', 64),
                140,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateTitleMaxInvalid() {
        var cut = new EventDTO(
                5L,
                StringUtils.repeat('a', 65),
                140,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateTitleNull() {
        var cut = new EventDTO(
                5L,
                null,
                140,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validatePlayersFeeValid() {
        var cut = new EventDTO(
                5L,
                "title",
                0,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validatePlayersFeeInvalid() {
        var cut = new EventDTO(
                5L,
                "title",
                -1,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validatePlayersFeeNull() {
        var cut = new EventDTO(
                5L,
                "title",
                null,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEnabledNull() {
        var cut = new EventDTO(
                5L,
                "title",
                140,
                null,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxValid() {
        var cut = new EventDTO(
                5L,
                "title",
                140,
                true,
                Instant.now(),
                Instant.now(),
                StringUtils.repeat('a', 2048),
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxInvalid() {
        var cut = new EventDTO(
                5L,
                "title",
                140,
                true,
                Instant.now(),
                Instant.now(),
                StringUtils.repeat('a', 2049),
                Set.of(7L),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
