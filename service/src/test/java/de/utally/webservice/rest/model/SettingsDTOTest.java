package de.utally.webservice.rest.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.mapstruct.factory.Mappers;

import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;

class SettingsDTOTest {

    private Validator validator;

    private final DtoMapper mapper = Mappers.getMapper(DtoMapper.class);

    @BeforeEach
    void setUp() {
        try (var validatorFactory = Validation.buildDefaultValidatorFactory()) {
            validator = validatorFactory.getValidator();
        }
    }

    @Test
    void validateNullSettings() {
        var settingsDto = new SettingsDTO(null, null, null, null, null, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isEmpty();
    }

    @Test
    void validateGrowlDelayMinValid() {
        var settingsDto = new SettingsDTO(0, null, null, null, null, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isEmpty();
    }

    @Test
    void validateGrowlDelayMinInvalid() {
        var settingsDto = new SettingsDTO(-1, null, null, null, null, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isNotEmpty();
    }

    @Test
    void validateKeepAliveTimeoutMinValid() {
        var settingsDto = new SettingsDTO(null, null, null, null, 0, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isEmpty();
    }

    @Test
    void validateKeepAliveTimeoutMinInvalid() {
        var settingsDto = new SettingsDTO(null, null, null, null, -1, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isNotEmpty();
    }

    @Test
    void validateDataPointsMinValid() {
        var settingsDto = new SettingsDTO(null, null, null, null, null, null, 0, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isEmpty();
    }

    @Test
    void validateDataPointsMinInvalid() {
        var settingsDto = new SettingsDTO(null, null, null, null, null, null, -1, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isNotEmpty();
    }

    @Test
    void validateLiveStatisticsIntervalMinValid() {
        var settingsDto = new SettingsDTO(null, null, null, null, null, null, null, 0);

        var result = validator.validate(settingsDto);

        assertThat(result).isEmpty();
    }

    @Test
    void validateLiveStatisticsIntervalInvalid() {
        var settingsDto = new SettingsDTO(null, null, null, null, null, null, null, -1);

        var result = validator.validate(settingsDto);

        assertThat(result).isNotEmpty();
    }

    @Test
    void validateCurrencyCodeValid() {
        var settingsDto = new SettingsDTO(null, "EUR", null, null, null, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isEmpty();
    }

    @Test
    void validateCurrencyCodeInvalid() {
        var settingsDto = new SettingsDTO(null, "ABCDE", null, null, null, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isNotEmpty();
    }

    @Test
    void validateTimeZoneValid() {
        var settingsDto = new SettingsDTO(null, null, mapper.toDto(ZoneId.of("Europe/Berlin")), null, null, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isEmpty();
    }

    @Test
    void validateTimeZoneInvalid() {
        var settingsDto = new SettingsDTO(null, null, new TimeZoneDTO("ABCDE", null, null), null, null, null, null, null);

        var result = validator.validate(settingsDto);

        assertThat(result).isNotEmpty();
    }
}