package de.utally.webservice.rest;

import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TestImageHandler {
    @Bean
    public RestImageHandler imageHandler() {
        return Mockito.mock(RestImageHandler.class);
    }
}
