package de.utally.webservice.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.utally.config.PropertiesConfiguration;
import de.utally.config.WebSecurityConfiguration;
import de.utally.settings.Settings;
import de.utally.settings.SettingsAdapter;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.model.DtoMapper;
import de.utally.webservice.rest.model.SettingsDTO;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Currency;
import java.util.Locale;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectInvalidArgumentError;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Import({WebSecurityConfiguration.class, TestMapperConfig.class, PropertiesConfiguration.class})
@TestPropertySource(properties = {
        "payment.bank.recipient=Ultimate Frisbee Lüneburg e.V.",
        "payment.bank.name=Volksbank Lüneburger Heide",
        "payment.bank.iban=DE28240603008537433400",
        "payment.bank.bic=GENODEF1NBU",
        "payment.paypal.me=TbsHcht",
        "payment.paypal.account=tobi3000@gmx.net"
})
@WebMvcTest(value = SettingsService.class)
class SettingsServiceTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @MockBean
    private SettingsAdapter settingsAdapter;

    @Autowired
    private DtoMapper mapper;

    @Test
    @WithAdmin
    void getSettingsAsAdmin() throws Exception {
        performGetSettings();
    }

    @Test
    @WithOperator
    void getSettingsAsOperator() throws Exception {
        performGetSettings();
    }

    @Test
    @WithUser
    void getSettingsAsUser() throws Exception {
        performGetSettings();
    }

    private void performGetSettings() throws Exception {
        var settings = new Settings();
        when(settingsAdapter.getSettings()).thenReturn(settings);

        mockMvc.perform(
                        get("/api/settings/").accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.growlDelay").value(settings.getGrowlDelay()))
                .andExpect(jsonPath("$.currencyCode").value(settings.getCurrency().getCurrencyCode()))
                .andExpect(jsonPath("$.timeZone.zoneId").value(settings.getTimeZone().toString()))
                .andExpect(jsonPath("$.timeZone.abbreviation").value(settings.getTimeZone().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.ENGLISH)))
                .andExpect(jsonPath("$.timeZone.offset").value(settings.getTimeZone().getRules().getOffset(Instant.now()).getId()))
                .andExpect(jsonPath("$.dateTimeFormat").value(settings.getDateTimeFormat()))
                .andExpect(jsonPath("$.keepAliveTimeout").value(settings.getKeepAliveTimeout()))
                .andExpect(jsonPath("$.selfRegistration").value(settings.getSelfRegistration()))
                .andExpect(jsonPath("$.dataPoints").value(settings.getDataPoints()))
                .andExpect(jsonPath("$.liveStatisticsInterval").value(settings.getLiveStatisticsInterval()))
        ;
    }

    @Test
    void getSettingsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getTimeZonesAsAdmin() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/timezones").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        for (var timezone : ZoneId.getAvailableZoneIds()) {
            actions = actions
                    .andExpect(jsonPath("$[?(@.zoneId=='" + timezone + "')]").exists())
                    .andExpect(jsonPath("$[?(@.abbreviation=='" + ZoneId.of(timezone).getDisplayName(TextStyle.SHORT_STANDALONE, Locale.ENGLISH) + "')]").exists());
        }
    }

    @Test
    @WithOperator
    void getTimeZonesAsOperator() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/timezones").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void getTimeZonesAsUser() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/timezones").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void getTimeZonesAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/timezones").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getCurrenciesAsAdmin() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/currencies").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        for (var currency : Currency.getAvailableCurrencies()) {
            actions = actions
                    .andExpect(jsonPath("$[?(@.currencyCode == '" + currency.getCurrencyCode() + "')].displayName").value(currency.getDisplayName()))
                    .andExpect(jsonPath("$[?(@.currencyCode == '" + currency.getCurrencyCode() + "')].symbol").value(currency.getSymbol()));
        }
    }

    @Test
    @WithOperator
    void getCurrenciesAsOperator() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/currencies").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void getCurrenciesAsUser() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/currencies").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void getCurrenciesAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/currencies").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void patchSettingsAsAdmin() throws Exception {
        performPatchSettings();
    }

    private void performPatchSettings() throws Exception {
        var settings = new SettingsDTO(
                7,
                "USD",
                mapper.toDto(ZoneId.of("Australia/Sydney")),
                "yyyy-MM-ddTHH:mm",
                4,
                true,
                5,
                3
        );
        when(settingsAdapter.getSettings()).thenReturn(mapper.toInternal(settings));

        mockMvc.perform(
                        patch("/api/settings/")
                                .content(objectMapper.writeValueAsString(settings))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.growlDelay").value(settings.growlDelay()))
                .andExpect(jsonPath("$.currencyCode").value(settings.currencyCode()))
                .andExpect(jsonPath("$.timeZone.zoneId").value(settings.timeZone().zoneId()))
                .andExpect(jsonPath("$.timeZone.abbreviation").value(settings.timeZone().abbreviation()))
                .andExpect(jsonPath("$.timeZone.offset").value(settings.timeZone().offset()))
                .andExpect(jsonPath("$.dateTimeFormat").value(settings.dateTimeFormat()))
                .andExpect(jsonPath("$.keepAliveTimeout").value(settings.keepAliveTimeout()))
                .andExpect(jsonPath("$.selfRegistration").value(settings.selfRegistration()))
                .andExpect(jsonPath("$.dataPoints").value(settings.dataPoints()))
                .andExpect(jsonPath("$.liveStatisticsInterval").value(settings.liveStatisticsInterval()))
        ;

        verify(settingsAdapter).updateSettings(any());
    }

    @Test
    @WithOperator
    void patchSettingsAsOperator() throws Exception {
        var settings = new SettingsDTO(null, null, null, null, null, null, null, null);

        var actions = mockMvc.perform(
                patch("/api/settings/")
                        .content(objectMapper.writeValueAsString(settings))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void patchSettingsAsUser() throws Exception {
        var settings = new SettingsDTO(null, null, null, null, null, null, null, null);

        var actions = mockMvc.perform(
                patch("/api/settings/")
                        .content(objectMapper.writeValueAsString(settings))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void patchSettingsAsAnonymous() throws Exception {
        var settings = new SettingsDTO(null, null, null, null, null, null, null, null);

        var actions = mockMvc.perform(
                patch("/api/settings/")
                        .content(objectMapper.writeValueAsString(settings))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void patchSettingsInvalidInteger() throws Exception {
        var settings = new SettingsDTO(null, null, null, null, null, null, -5, null);

        var actions = mockMvc.perform(
                patch("/api/settings/")
                        .content(objectMapper.writeValueAsString(settings))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, settings);
    }

    @Test
    @WithAdmin
    void patchSettingsInvalidCurrency() throws Exception {
        var payload = new JSONObject();
        payload.put("currencyCode", "ABCDD");

        var actions = mockMvc.perform(
                patch("/api/settings/")
                        .content(payload.toString())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        var errorCode = ErrorCode.BADR0003;

        actions
                .andExpect(status().is(errorCode.getStatus().value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.error").value(errorCode.name()))
                .andExpect(jsonPath("$.message").value(errorCode.getMessage()))
                .andExpect(jsonPath("$.detail").value(errorCode.getDetail() + "\n" + "- Invalid currency code"));
    }

    @Test
    @WithAdmin
    void patchSettingsInvalidZoneId() throws Exception {
        var payload = new JSONObject();
        var timeZone = new JSONObject();
        timeZone.put("zoneId", "ABCDE");
        payload.put("timeZone", timeZone);

        var actions = mockMvc.perform(
                patch("/api/settings/")
                        .content(payload.toString())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        var errorCode = ErrorCode.BADR0003;

        actions
                .andExpect(status().is(errorCode.getStatus().value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.error").value(errorCode.name()))
                .andExpect(jsonPath("$.message").value(errorCode.getMessage()))
                .andExpect(jsonPath("$.detail").value(errorCode.getDetail() + "\n" + "- Invalid zone id"));
    }

    @Test
    @WithAdmin
    void patchSettingsInvalidTimeZoneStructure() throws Exception {
        var payload = new JSONObject();
        payload.put("timeZone", "ABCDE");

        var actions = mockMvc.perform(
                patch("/api/settings/")
                        .content(payload.toString())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.BADR0006);
    }

    @Test
    @WithAdmin
    void patchSettingsInvalidBoolean() throws Exception {
        var payload = new JSONObject();
        payload.put("selfRegistration", "ABCDD");

        var actions = mockMvc.perform(
                patch("/api/settings/")
                        .content(payload.toString())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        var errorCode = ErrorCode.BADR0003;

        actions
                .andExpect(status().is(errorCode.getStatus().value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.error").value(errorCode.name()))
                .andExpect(jsonPath("$.message").value(errorCode.getMessage()))
                .andExpect(jsonPath("$.detail").value(errorCode.getDetail() + "\n" + "Cannot deserialize value of type `java.lang.Boolean` from String \"ABCDD\": only \"true\" or \"false\" recognized\n" +
                        " at [Source: (org.springframework.util.StreamUtils$NonClosingInputStream); line: 1, column: 21] (through reference chain: de.utally.webservice.rest.model.SettingsDTO[\"selfRegistration\"])"));
    }

    @Test
    @WithAdmin
    void getPaymentInformationAsAdmin() throws Exception {
        performGetPaymentInformation();
    }

    @Test
    @WithOperator
    void getPaymentInformationAsOperator() throws Exception {
        performGetPaymentInformation();
    }

    private void performGetPaymentInformation() throws Exception {
        mockMvc.perform(
                        get("/api/settings/payment").accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(jsonPath("$.bankRecipient").value("Ultimate Frisbee Lüneburg e.V."))
                .andExpect(jsonPath("$.bankName").value("Volksbank Lüneburger Heide"))
                .andExpect(jsonPath("$.bankIban").value("DE28240603008537433400"))
                .andExpect(jsonPath("$.bankBic").value("GENODEF1NBU"))
                .andExpect(jsonPath("$.paypalMe").value("TbsHcht"))
                .andExpect(jsonPath("$.paypalAccount").value("tobi3000@gmx.net"));
    }

    @Test
    @WithUser
    void getPaymentInformationAsUser() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/payment").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void getPaymentInformationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/settings/payment").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }
}