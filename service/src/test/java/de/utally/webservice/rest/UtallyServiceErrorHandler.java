package de.utally.webservice.rest;

import de.utally.webservice.rest.exception.ErrorCode;
import jakarta.validation.ValidatorFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import jakarta.validation.Validation;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class UtallyServiceErrorHandler {

    private static final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    static void expectError(ResultActions resultActions, ErrorCode errorCode) throws Exception {
        resultActions
                .andExpect(status().is(errorCode.getStatus().value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.error").value(errorCode.name()))
                .andExpect(jsonPath("$.message").value(errorCode.getMessage()))
                .andExpect(jsonPath("$.detail").value(errorCode.getDetail()));
    }

    static void expectInvalidArgumentError(ResultActions resultActions, Object requestBody) throws Exception {
        var errorCode = ErrorCode.BADR0003;

        var validator = validatorFactory.getValidator();
        var result = validator.validate(requestBody);
        String detailedMessageAppendix = result.stream().map(c -> "- " + c.getMessage()).collect(Collectors.joining("\n"));

        resultActions
                .andExpect(status().is(errorCode.getStatus().value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.error").value(errorCode.name()))
                .andExpect(jsonPath("$.message").value(errorCode.getMessage()))
                .andExpect(jsonPath("$.detail").value(errorCode.getDetail() + "\n" + detailedMessageAppendix));
    }
}
