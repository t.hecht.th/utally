package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class TeamDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    @Test
    void validateShortNameMinValid() {
        var cut = new TeamDTO(5L, "fullname", "city", "a", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateShortNameMinInvalid() {
        var cut = new TeamDTO(5L, "fullname", "city", "", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateShortNameMaxValid() {
        var cut = new TeamDTO(5L, "fullname", "city", StringUtils.repeat('a', 64), true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateShortNameMaxInvalid() {
        var cut = new TeamDTO(5L, "fullname", "city", StringUtils.repeat('a', 65), true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateShortNameNull() {
        var cut = new TeamDTO(5L, "fullname", "city", null, true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateFullNameMinValid() {
        var cut = new TeamDTO(5L, "a", "city", "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateFullNameMinInvalid() {
        var cut = new TeamDTO(5L, "", "city", "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateFullNameMaxValid() {
        var cut = new TeamDTO(5L, StringUtils.repeat('a', 128), "city", "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateFullNameMaxInvalid() {
        var cut = new TeamDTO(5L, StringUtils.repeat('a', 129), "city", "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateFullNameNull() {
        var cut = new TeamDTO(5L, null, "city", "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateCityMinValid() {
        var cut = new TeamDTO(5L, "fullname", "a", "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateCityMinInvalid() {
        var cut = new TeamDTO(5L, "fullname", "", "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateCityMaxValid() {
        var cut = new TeamDTO(5L, "fullname", StringUtils.repeat('a', 64), "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateCityMaxInvalid() {
        var cut = new TeamDTO(5L, "fullname", StringUtils.repeat('a', 65), "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateCityNull() {
        var cut = new TeamDTO(5L, "fullname", null, "shortname", true, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEnabledNull() {
        var cut = new TeamDTO(5L, "fullname", "city", "shortname", null, Instant.now(), Instant.now(), "description");

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxValid() {
        var cut = new TeamDTO(5L, "fullname", "city", "shortname", true, Instant.now(), Instant.now(), StringUtils.repeat('a', 2048));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxInvalid() {
        var cut = new TeamDTO(5L, "fullname", "city", "shortname", true, Instant.now(), Instant.now(), StringUtils.repeat('a', 2049));

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
