package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import org.springframework.security.test.context.support.WithMockUser;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@WithMockUser(username = "userUN", password = "userPW", roles = WebSecurityConfiguration.USER_ROLE)
public @interface WithUser {
}
