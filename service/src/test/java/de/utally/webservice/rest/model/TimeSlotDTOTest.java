package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class TimeSlotDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    @Test
    void validateLabelMinValid() {
        var cut = new TimeSlotDTO(5L, "a", Instant.now(), Instant.now(), true, Instant.now(), Instant.now(), "description", 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateLabelMinInvalid() {
        var cut = new TimeSlotDTO(5L, "", Instant.now(), Instant.now(), true, Instant.now(), Instant.now(), "description", 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateLabelMaxValid() {
        var cut = new TimeSlotDTO(5L, StringUtils.repeat('a', 64), Instant.now(), Instant.now(), true, Instant.now(), Instant.now(), "description", 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateLabelMaxInvalid() {
        var cut = new TimeSlotDTO(5L, StringUtils.repeat('a', 65), Instant.now(), Instant.now(), true, Instant.now(), Instant.now(), "description", 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateLabelNull() {
        var cut = new TimeSlotDTO(5L, null, Instant.now(), Instant.now(), true, Instant.now(), Instant.now(), "description", 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateStartNull() {
        var cut = new TimeSlotDTO(5L, "label", null, Instant.now(), true, Instant.now(), Instant.now(), "description", 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEndNull() {
        var cut = new TimeSlotDTO(5L, "label", Instant.now(), null, true, Instant.now(), Instant.now(), "description", 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEnabledNull() {
        var cut = new TimeSlotDTO(5L, "label", Instant.now(), Instant.now(), null, Instant.now(), Instant.now(), "description", 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxValid() {
        var cut = new TimeSlotDTO(5L, "label", Instant.now(), Instant.now(), true, Instant.now(), Instant.now(), StringUtils.repeat('a', 2048), 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxInvalid() {
        var cut = new TimeSlotDTO(5L, "label", Instant.now(), Instant.now(), true, Instant.now(), Instant.now(), StringUtils.repeat('a', 2049), 6L);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEventIdNull() {
        var cut = new TimeSlotDTO(5L, "label", Instant.now(), Instant.now(), true, Instant.now(), Instant.now(), "description", null);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
