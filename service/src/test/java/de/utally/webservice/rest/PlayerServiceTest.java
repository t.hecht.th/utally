package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import de.utally.data.exception.PlayerNotFoundException;
import de.utally.data.exception.PlayersNotOfSameEventException;
import de.utally.data.model.Player;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.model.PatchPlayerAsAdminDTO;
import de.utally.webservice.rest.model.PatchPlayerAsOperatorDTO;
import de.utally.webservice.rest.model.PatchPlayerAsUserDTO;
import de.utally.webservice.rest.model.PatchPlayerCommentDTO;
import de.utally.webservice.rest.model.PlayerDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

import java.util.Map;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectInvalidArgumentError;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import({TestAdapters.class, WebSecurityConfiguration.class, TestMapperConfig.class})
@WebMvcTest(value = PlayerService.class)
class PlayerServiceTest extends UtallyServiceTest {

    @BeforeEach
    void setUp() {
        super.setUp();
    }

    @AfterEach
    void tearDown() {
        super.tearDown();
    }

    @Test
    @WithAdmin
    void getPlayersAsAdmin() throws Exception {
        performGetAllPlayers();
    }

    @Test
    @WithOperator
    void getPlayersAsOperator() throws Exception {
        performGetAllPlayers();
    }

    @Test
    @WithUser
    void getPlayersAsUser() throws Exception {
        performGetAllPlayers();
    }

    private void performGetAllPlayers() throws Exception {
        mockMvc.perform(
                get("/api/players/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(player1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(player1.getEnabled()))
                .andExpect(jsonPath("$[0].name").value(player1.getName()))
                .andExpect(jsonPath("$[0].comment").value(player1.getComment()))
                .andExpect(jsonPath("$[0].eventId").value(player1.getEvent().getId()))
                .andExpect(jsonPath("$[0].teamId").value(player1.getTeam().getId()))
                .andExpect(jsonPath("$[0].paid").value(player1.getPaid()))
                .andExpect(jsonPath("$[0].paidPlayersFee").value(player1.getPaidPlayersFee()))
                .andExpect(jsonPath("$[0].withPlayersFee").value(player1.getWithPlayersFee()))
                .andExpect(jsonPath("$[0].payTime").value(player1.getPayTime().toString()))
                .andExpect(jsonPath("$[0].preferences").exists())
                .andExpect(jsonPath("$[0].preferences").isMap())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(player2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(player2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(player2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(player2.getEnabled()))
                .andExpect(jsonPath("$[1].name").value(player2.getName()))
                .andExpect(jsonPath("$[1].comment").value(player2.getComment()))
                .andExpect(jsonPath("$[1].eventId").value(player2.getEvent().getId()))
                .andExpect(jsonPath("$[1].teamId").value(player2.getTeam().getId()))
                .andExpect(jsonPath("$[1].paid").value(player2.getPaid()))
                .andExpect(jsonPath("$[1].paidPlayersFee").value(player2.getPaidPlayersFee()))
                .andExpect(jsonPath("$[1].withPlayersFee").value(player2.getWithPlayersFee()))
                .andExpect(jsonPath("$[1].payTime").value(player2.getPayTime().toString()))
                .andExpect(jsonPath("$[1].preferences").exists())
                .andExpect(jsonPath("$[1].preferences").isMap())

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(player3.getId()))
                .andExpect(jsonPath("$[2].creationTime").value(player3.getCreationTime().toString()))
                .andExpect(jsonPath("$[2].modificationTime").value(player3.getModificationTime().toString()))
                .andExpect(jsonPath("$[2].enabled").value(player3.getEnabled()))
                .andExpect(jsonPath("$[2].name").value(player3.getName()))
                .andExpect(jsonPath("$[2].comment").value(player3.getComment()))
                .andExpect(jsonPath("$[2].eventId").value(player3.getEvent().getId()))
                .andExpect(jsonPath("$[2].teamId").value(player3.getTeam().getId()))
                .andExpect(jsonPath("$[2].paid").value(player3.getPaid()))
                .andExpect(jsonPath("$[2].paidPlayersFee").value(player3.getPaidPlayersFee()))
                .andExpect(jsonPath("$[2].withPlayersFee").value(player3.getWithPlayersFee()))
                .andExpect(jsonPath("$[2].payTime").value(player3.getPayTime().toString()))
                .andExpect(jsonPath("$[2].preferences").exists())
                .andExpect(jsonPath("$[2].preferences").isMap())

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(player4.getId()))
                .andExpect(jsonPath("$[3].creationTime").value(player4.getCreationTime().toString()))
                .andExpect(jsonPath("$[3].modificationTime").value(player4.getModificationTime().toString()))
                .andExpect(jsonPath("$[3].enabled").value(player4.getEnabled()))
                .andExpect(jsonPath("$[3].name").value(player4.getName()))
                .andExpect(jsonPath("$[3].comment").value(player4.getComment()))
                .andExpect(jsonPath("$[3].eventId").value(player4.getEvent().getId()))
                .andExpect(jsonPath("$[3].teamId").value(player4.getTeam().getId()))
                .andExpect(jsonPath("$[3].paid").value(player4.getPaid()))
                .andExpect(jsonPath("$[3].paidPlayersFee").value(player4.getPaidPlayersFee()))
                .andExpect(jsonPath("$[3].withPlayersFee").value(player4.getWithPlayersFee()))
                .andExpect(jsonPath("$[3].payTime").value(player4.getPayTime().toString()))
                .andExpect(jsonPath("$[3].preferences").exists())
                .andExpect(jsonPath("$[3].preferences").isMap());
    }

    @Test
    void getPlayersAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getPlayersByEventAsAdmin() throws Exception {
        performGetAllPlayersByEvent();
    }

    @Test
    @WithOperator
    void getPlayersByEventAsOperator() throws Exception {
        performGetAllPlayersByEvent();
    }

    @Test
    @WithUser
    void getPlayersByEventAsUser() throws Exception {
        performGetAllPlayersByEvent();
    }

    private void performGetAllPlayersByEvent() throws Exception {
        mockMvc.perform(
                get("/api/players/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(player1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(player1.getEnabled()))
                .andExpect(jsonPath("$[0].name").value(player1.getName()))
                .andExpect(jsonPath("$[0].comment").value(player1.getComment()))
                .andExpect(jsonPath("$[0].eventId").value(player1.getEvent().getId()))
                .andExpect(jsonPath("$[0].teamId").value(player1.getTeam().getId()))
                .andExpect(jsonPath("$[0].paid").value(player1.getPaid()))
                .andExpect(jsonPath("$[0].paidPlayersFee").value(player1.getPaidPlayersFee()))
                .andExpect(jsonPath("$[0].withPlayersFee").value(player1.getWithPlayersFee()))
                .andExpect(jsonPath("$[0].payTime").value(player1.getPayTime().toString()))
                .andExpect(jsonPath("$[0].preferences").exists())
                .andExpect(jsonPath("$[0].preferences").isMap())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(player2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(player2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(player2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(player2.getEnabled()))
                .andExpect(jsonPath("$[1].name").value(player2.getName()))
                .andExpect(jsonPath("$[1].comment").value(player2.getComment()))
                .andExpect(jsonPath("$[1].eventId").value(player2.getEvent().getId()))
                .andExpect(jsonPath("$[1].teamId").value(player2.getTeam().getId()))
                .andExpect(jsonPath("$[1].paid").value(player2.getPaid()))
                .andExpect(jsonPath("$[1].paidPlayersFee").value(player2.getPaidPlayersFee()))
                .andExpect(jsonPath("$[1].withPlayersFee").value(player2.getWithPlayersFee()))
                .andExpect(jsonPath("$[1].payTime").value(player2.getPayTime().toString()))
                .andExpect(jsonPath("$[1].preferences").exists())
                .andExpect(jsonPath("$[1].preferences").isMap())

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(player3.getId()))
                .andExpect(jsonPath("$[2].creationTime").value(player3.getCreationTime().toString()))
                .andExpect(jsonPath("$[2].modificationTime").value(player3.getModificationTime().toString()))
                .andExpect(jsonPath("$[2].enabled").value(player3.getEnabled()))
                .andExpect(jsonPath("$[2].name").value(player3.getName()))
                .andExpect(jsonPath("$[2].comment").value(player3.getComment()))
                .andExpect(jsonPath("$[2].eventId").value(player3.getEvent().getId()))
                .andExpect(jsonPath("$[2].teamId").value(player3.getTeam().getId()))
                .andExpect(jsonPath("$[2].paid").value(player3.getPaid()))
                .andExpect(jsonPath("$[2].paidPlayersFee").value(player3.getPaidPlayersFee()))
                .andExpect(jsonPath("$[2].withPlayersFee").value(player3.getWithPlayersFee()))
                .andExpect(jsonPath("$[2].payTime").value(player3.getPayTime().toString()))
                .andExpect(jsonPath("$[2].preferences").exists())
                .andExpect(jsonPath("$[2].preferences").isMap());
    }

    @Test
    void getPlayersByEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getPlayersByEventNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/event/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getPlayersByTeamAsAdmin() throws Exception {
        performGetAllPlayersByTeam();
    }

    @Test
    @WithOperator
    void getPlayersByTeamAsOperator() throws Exception {
        performGetAllPlayersByTeam();
    }

    @Test
    @WithUser
    void getPlayersByTeamAsUser() throws Exception {
        performGetAllPlayersByTeam();
    }

    private void performGetAllPlayersByTeam() throws Exception {
        mockMvc.perform(
                get("/api/players/team/{id}", team1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(player1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(player1.getEnabled()))
                .andExpect(jsonPath("$[0].name").value(player1.getName()))
                .andExpect(jsonPath("$[0].comment").value(player1.getComment()))
                .andExpect(jsonPath("$[0].eventId").value(player1.getEvent().getId()))
                .andExpect(jsonPath("$[0].teamId").value(player1.getTeam().getId()))
                .andExpect(jsonPath("$[0].paid").value(player1.getPaid()))
                .andExpect(jsonPath("$[0].paidPlayersFee").value(player1.getPaidPlayersFee()))
                .andExpect(jsonPath("$[0].withPlayersFee").value(player1.getWithPlayersFee()))
                .andExpect(jsonPath("$[0].payTime").value(player1.getPayTime().toString()))
                .andExpect(jsonPath("$[0].preferences").exists())
                .andExpect(jsonPath("$[0].preferences").isMap())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(player2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(player2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(player2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(player2.getEnabled()))
                .andExpect(jsonPath("$[1].name").value(player2.getName()))
                .andExpect(jsonPath("$[1].comment").value(player2.getComment()))
                .andExpect(jsonPath("$[1].eventId").value(player2.getEvent().getId()))
                .andExpect(jsonPath("$[1].teamId").value(player2.getTeam().getId()))
                .andExpect(jsonPath("$[1].paid").value(player2.getPaid()))
                .andExpect(jsonPath("$[1].paidPlayersFee").value(player2.getPaidPlayersFee()))
                .andExpect(jsonPath("$[1].withPlayersFee").value(player2.getWithPlayersFee()))
                .andExpect(jsonPath("$[1].payTime").value(player2.getPayTime().toString()))
                .andExpect(jsonPath("$[1].preferences").exists())
                .andExpect(jsonPath("$[1].preferences").isMap());
    }

    @Test
    void getPlayersByTeamAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/team/{id}", team1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getPlayersByTeamNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/team/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);
    }

    @Test
    @WithAdmin
    void getPlayersByTeamAndEventAsAdmin() throws Exception {
        performGetAllPlayersByTeamAndEvent();
    }

    @Test
    @WithOperator
    void getPlayersByTeamAndEventAsOperator() throws Exception {
        performGetAllPlayersByTeamAndEvent();
    }

    @Test
    @WithUser
    void getPlayersByTeamAndEventAsUser() throws Exception {
        performGetAllPlayersByTeamAndEvent();
    }

    private void performGetAllPlayersByTeamAndEvent() throws Exception {
        mockMvc.perform(
                get("/api/players/team/{teamId}/event/{eventId}", team1.getId(), event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(player1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(player1.getEnabled()))
                .andExpect(jsonPath("$[0].name").value(player1.getName()))
                .andExpect(jsonPath("$[0].comment").value(player1.getComment()))
                .andExpect(jsonPath("$[0].eventId").value(player1.getEvent().getId()))
                .andExpect(jsonPath("$[0].teamId").value(player1.getTeam().getId()))
                .andExpect(jsonPath("$[0].paid").value(player1.getPaid()))
                .andExpect(jsonPath("$[0].paidPlayersFee").value(player1.getPaidPlayersFee()))
                .andExpect(jsonPath("$[0].withPlayersFee").value(player1.getWithPlayersFee()))
                .andExpect(jsonPath("$[0].payTime").value(player1.getPayTime().toString()))
                .andExpect(jsonPath("$[0].preferences").exists())
                .andExpect(jsonPath("$[0].preferences").isMap())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(player2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(player2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(player2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(player2.getEnabled()))
                .andExpect(jsonPath("$[1].name").value(player2.getName()))
                .andExpect(jsonPath("$[1].comment").value(player2.getComment()))
                .andExpect(jsonPath("$[1].eventId").value(player2.getEvent().getId()))
                .andExpect(jsonPath("$[1].teamId").value(player2.getTeam().getId()))
                .andExpect(jsonPath("$[1].paid").value(player2.getPaid()))
                .andExpect(jsonPath("$[1].paidPlayersFee").value(player2.getPaidPlayersFee()))
                .andExpect(jsonPath("$[1].withPlayersFee").value(player2.getWithPlayersFee()))
                .andExpect(jsonPath("$[1].payTime").value(player2.getPayTime().toString()))
                .andExpect(jsonPath("$[1].preferences").exists())
                .andExpect(jsonPath("$[1].preferences").isMap());
    }

    @Test
    void getPlayersByTeamAndEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/team/{teamId}/event/{eventId}", team1.getId(), event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getPlayersByTeamAndEventTeamNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/team/{teamId}/event/{eventId}", idSequence++, event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);
    }

    @Test
    @WithAdmin
    void getPlayersByTeamAndEventEventNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/team/{teamId}/event/{eventId}", team1.getId(), idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getPlayerAsAdmin() throws Exception {
        performGetPlayer();
    }

    @Test
    @WithOperator
    void getPlayerAsOperator() throws Exception {
        performGetPlayer();
    }

    @Test
    @WithUser
    void getPlayerAsUser() throws Exception {
        performGetPlayer();
    }

    private void performGetPlayer() throws Exception {
        mockMvc.perform(
                get("/api/players/{id}", player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(player1.getId()))
                .andExpect(jsonPath("$.creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(player1.getEnabled()))
                .andExpect(jsonPath("$.name").value(player1.getName()))
                .andExpect(jsonPath("$.comment").value(player1.getComment()))
                .andExpect(jsonPath("$.eventId").value(player1.getEvent().getId()))
                .andExpect(jsonPath("$.teamId").value(player1.getTeam().getId()))
                .andExpect(jsonPath("$.paid").value(player1.getPaid()))
                .andExpect(jsonPath("$.paidPlayersFee").value(player1.getPaidPlayersFee()))
                .andExpect(jsonPath("$.withPlayersFee").value(player1.getWithPlayersFee()))
                .andExpect(jsonPath("$.payTime").value(player1.getPayTime().toString()))
                .andExpect(jsonPath("$.preferences").exists())
                .andExpect(jsonPath("$.preferences").isMap());
    }

    @Test
    void getPlayerAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/{id}", player1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getPlayerNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/players/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void postPlayerAsUserAsAdmin() throws Exception {
        var r = createPlayerResource();

        injectPostAsUserSaveCalls(r);

        performPostPlayerAsUser(r);
    }

    @Test
    @WithOperator
    void postPlayerAsUserAsOperator() throws Exception {
        var r = createPlayerResource();

        injectPostAsUserSaveCalls(r);

        performPostPlayerAsUser(r);
    }

    @Test
    @WithUser
    void postPlayerAsUserAsUser() throws Exception {
        var r = createPlayerResource();

        injectPostAsUserSaveCalls(r);

        performPostPlayerAsUser(r);
    }

    private void performPostPlayerAsUser(PlayerDTO r) throws Exception {
        mockMvc.perform(
                post("/api/players/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(true))
                .andExpect(jsonPath("$.name").value(r.name()))
                .andExpect(jsonPath("$.eventId").value(event1.getId()))
                .andExpect(jsonPath("$.teamId").value(team1.getId()));

        verify(playerAdapter).insertPlayer(anyString(), anyLong(), anyLong(), anyBoolean());
    }

    private void injectPostAsUserSaveCalls(PlayerDTO r) {
        doAnswer(i -> {
            assertAll(
                    () -> assertEquals(r.name(), i.getArgument(0, String.class)),
                    () -> assertEquals(r.teamId(), i.getArgument(1, Long.class)),
                    () -> assertEquals(r.eventId(), i.getArgument(2, Long.class)),
                    () -> assertTrue(i.getArgument(3, Boolean.class))
            );
            var player = new Player();
            player.setId(idSequence);
            player.setName(i.getArgument(0, String.class));
            player.setTeam(team1);
            player.setEvent(event1);
            player.setEnabled( i.getArgument(3, Boolean.class));
            player.setCreationTime(nextInstant());
            player.setModificationTime(nextInstant());
            return player;
        }).when(playerAdapter).insertPlayer(anyString(), anyLong(), anyLong(), anyBoolean());
    }

    @Test
    void postPlayerAsUserAsAnonymous() throws Exception {
        var r = createPlayerResource();

        var actions = mockMvc.perform(
                post("/api/players/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void postPlayerInvalid() throws Exception {
        var r = createPlayerResource();
        var inv = new PlayerDTO(
                r.id(),
                null,
                r.creationTime(),
                r.modificationTime(),
                r.paid(),
                r.withPlayersFee(),
                r.comment(),
                r.payTime(),
                r.enabled(),
                r.paidPlayersFee(),
                r.preferences(),
                r.teamId(),
                r.eventId()
        );

        var actions = mockMvc.perform(
                post("/api/players/")
                        .content(objectMapper.writeValueAsString(inv))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, inv);
    }

    @Test
    @WithAdmin
    void patchPlayerAsAdminAsAdmin() throws Exception {
        var r = createPatchPlayerAsAdminDTO(randomStringGenerator());

        injectPatchAsAdminSaveCalls(r);

        performPatchPlayerAsAdmin(r);
    }

    private void performPatchPlayerAsAdmin(PatchPlayerAsAdminDTO r) throws Exception {
        mockMvc.perform(
                patch("/api/players/{id}", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(player1.getId()))
                .andExpect(jsonPath("$.creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.name").value(r.name()))
                .andExpect(jsonPath("$.eventId").value(event1.getId()))
                .andExpect(jsonPath("$.teamId").value(team1.getId()));

        verify(playerAdapter).updatePlayer(any());
    }

    private void injectPatchAsAdminSaveCalls(PatchPlayerAsAdminDTO r) {
        doAnswer(i -> {
            var player = i.getArgument(0, Player.class);
            assertAll(
                    () -> assertEquals(r.name(), player.getName()),
                    () -> assertEquals(r.enabled(), player.getEnabled())
            );
            return null;
        }).when(playerAdapter).updatePlayer(any());
    }

    @Test
    @WithOperator
    void patchPlayerAsAdminAsOperator() throws Exception {
        var r = createPatchPlayerAsAdminDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void patchPlayerAsAdminAsUser() throws Exception {
        var r = createPatchPlayerAsAdminDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void patchPlayerAsAdminAsAnonymous() throws Exception {
        var r = createPatchPlayerAsAdminDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void patchPlayerAsAdminNotFound() throws Exception {
        var r = createPatchPlayerAsAdminDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void patchPlayerAsAdminInvalid() throws Exception {
        var r = createPatchPlayerAsAdminDTO(null);

        var actions = mockMvc.perform(
                patch("/api/players/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void patchPlayerAsUserAsAdmin() throws Exception {
        var r = createPatchPlayerAsUserDTO(randomStringGenerator());

        injectPatchAsUserSaveCalls(r);

        performPatchPlayerAsUser(r);
    }

    @Test
    @WithOperator
    void patchPlayerAsUserAsOperator() throws Exception {
        var r = createPatchPlayerAsUserDTO(randomStringGenerator());

        injectPatchAsUserSaveCalls(r);

        performPatchPlayerAsUser(r);
    }

    @Test
    @WithUser
    void patchPlayerAsUserAsUser() throws Exception {
        var r = createPatchPlayerAsUserDTO(randomStringGenerator());

        injectPatchAsUserSaveCalls(r);

        performPatchPlayerAsUser(r);
    }

    private void performPatchPlayerAsUser(PatchPlayerAsUserDTO r) throws Exception {
        mockMvc.perform(
                patch("/api/players/{id}/update", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(player1.getId()))
                .andExpect(jsonPath("$.creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(player1.getEnabled()))
                .andExpect(jsonPath("$.name").value(r.name()))
                .andExpect(jsonPath("$.eventId").value(event1.getId()))
                .andExpect(jsonPath("$.teamId").value(team1.getId()))
                .andExpect(jsonPath("$.preferences").exists())
                .andExpect(jsonPath("$.preferences").isMap())
                .andExpect(jsonPath("$.preferences." + r.preferences().keySet().iterator().next()).value(r.preferences().values().iterator().next()));

        verify(playerAdapter).updatePlayer(any());
    }

    private void injectPatchAsUserSaveCalls(PatchPlayerAsUserDTO r) {
        doAnswer(i -> {
            var player = i.getArgument(0, Player.class);
            assertAll(
                    () -> assertEquals(r.name(), player.getName()),
                    () -> assertEquals(r.preferences(), player.getPreferences())
            );
            return null;
        }).when(playerAdapter).updatePlayer(any());
    }

    @Test
    void patchPlayerAsUserAsAnonymous() throws Exception {
        var r = createPatchPlayerAsUserDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}/update", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void patchPlayerAsUserNotFound() throws Exception {
        var r = createPatchPlayerAsUserDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}/update", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void patchPlayerAsUserInvalid() throws Exception {
        var r = createPatchPlayerAsUserDTO(null);

        var actions = mockMvc.perform(
                patch("/api/players/{id}/update", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void patchPlayerCommentAsAdmin() throws Exception {
        var r = createPatchPlayerCommentDTO(randomStringGenerator());

        injectPatchPlayerCommentSaveCalls(r);

        performPatchPlayerComment(r);
    }

    @Test
    @WithOperator
    void patchPlayerCommentAsOperator() throws Exception {
        var r = createPatchPlayerCommentDTO(randomStringGenerator());

        injectPatchPlayerCommentSaveCalls(r);

        performPatchPlayerComment(r);
    }

    private void performPatchPlayerComment(PatchPlayerCommentDTO r) throws Exception {
        mockMvc.perform(
                        patch("/api/players/{id}/comment", player1.getId())
                                .content(objectMapper.writeValueAsString(r))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(player1.getId()))
                .andExpect(jsonPath("$.creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$.comment").value(player1.getComment()))
                .andExpect(jsonPath("$.eventId").value(event1.getId()))
                .andExpect(jsonPath("$.teamId").value(team1.getId()));

        verify(playerAdapter).updatePlayer(any());
    }

    private void injectPatchPlayerCommentSaveCalls(PatchPlayerCommentDTO r) {
        doAnswer(i -> {
            var player = i.getArgument(0, Player.class);
            assertAll(
                    () -> assertEquals(r.comment(), player.getComment())
            );
            return null;
        }).when(playerAdapter).updatePlayer(any());
    }

    @Test
    @WithUser
    void patchPlayerCommentAsUser() throws Exception {
        var r = createPatchPlayerCommentDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}/comment", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void patchPlayerCommentAsAnonymous() throws Exception {
        var r = createPatchPlayerCommentDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}/comment", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void patchPlayerCommentNotFound() throws Exception {
        var r = createPatchPlayerCommentDTO(randomStringGenerator());

        var actions = mockMvc.perform(
                patch("/api/players/{id}/comment", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void patchPlayerCommentInvalid() throws Exception {
        var r = createPatchPlayerCommentDTO(null);

        var actions = mockMvc.perform(
                patch("/api/players/{id}/comment", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void patchPlayerAsOperatorAsAdmin() throws Exception {
        var r = createPatchPlayerAsOperatorDTO(integerSequence++);

        injectPatchAsOperatorSaveCalls(r);

        performPatchPlayerAsOperator(r);
    }

    @Test
    @WithOperator
    void patchPlayerAsOperatorAsOperator() throws Exception {
        var r = createPatchPlayerAsOperatorDTO(integerSequence++);

        injectPatchAsOperatorSaveCalls(r);

        performPatchPlayerAsOperator(r);
    }

    private void performPatchPlayerAsOperator(PatchPlayerAsOperatorDTO r) throws Exception {
        mockMvc.perform(
                patch("/api/players/{id}/pay", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(player1.getId()))
                .andExpect(jsonPath("$.creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(player1.getEnabled()))
                .andExpect(jsonPath("$.name").value(player1.getName()))
                .andExpect(jsonPath("$.eventId").value(event1.getId()))
                .andExpect(jsonPath("$.teamId").value(team1.getId()));

        verify(playerAdapter).pay(anyLong(), anyString(), anyBoolean(), anyInt());
    }

    private void injectPatchAsOperatorSaveCalls(PatchPlayerAsOperatorDTO r) {
        doAnswer(i -> {
            assertAll(
                    () -> assertEquals(player1.getId(), i.getArgument(0, Long.class)),
                    () -> assertEquals(r.comment(), i.getArgument(1, String.class)),
                    () -> assertEquals(r.withPlayersFee(), i.getArgument(2, Boolean.class)),
                    () -> assertEquals(r.paidPlayersFee(), i.getArgument(3, Integer.class))
            );
            return null;
        }).when(playerAdapter).pay(anyLong(), anyString(), anyBoolean(), anyInt());
    }

    @Test
    @WithUser
    void patchPlayerAsOperatorAsUser() throws Exception {
        var r = createPatchPlayerAsOperatorDTO(integerSequence++);

        var actions = mockMvc.perform(
                patch("/api/players/{id}/pay", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void patchPlayerAsOperatorAsAnonymous() throws Exception {
        var r = createPatchPlayerAsOperatorDTO(integerSequence++);

        var actions = mockMvc.perform(
                patch("/api/players/{id}/pay", player1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void patchPlayerAsOperatorNotFound() throws Exception {
        var r = createPatchPlayerAsOperatorDTO(integerSequence++);

        var actions = mockMvc.perform(
                patch("/api/players/{id}/pay", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void patchPlayerAsOperatorInvalid() throws Exception {
        var r = createPatchPlayerAsOperatorDTO(null);

        var actions = mockMvc.perform(
                patch("/api/players/{id}/pay", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void revertPaymentAsAdmin() throws Exception {
        performRevertPayment();
    }

    @Test
    @WithOperator
    void revertPaymentAsOperator() throws Exception {
        performRevertPayment();
    }

    private void performRevertPayment() throws Exception {
        mockMvc.perform(
                        patch("/api/players/{id}/revert", player1.getId())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(player1.getId()))
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").exists())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.eventId").exists())
                .andExpect(jsonPath("$.teamId").exists());

        verify(playerAdapter).revertPayment(player1.getId());
    }

    @Test
    @WithUser
    void revertPaymentAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/players/{id}/revert", player1.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void revertPaymentAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/players/{id}/revert", player1.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void revertPaymentAsOperatorNotFound() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/players/{id}/revert", idSequence++)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void mergePlayersAsAdmin() throws Exception {
        performMergePlayers();
    }

    @Test
    @WithOperator
    void mergePlayersAsOperator() throws Exception {
        performMergePlayers();
    }

    private void performMergePlayers() throws Exception {
        mockMvc.perform(
                patch("/api/players/merge/{source}/{destination}", player2.getId(), player1.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(player1.getId()))
                .andExpect(jsonPath("$.creationTime").value(player1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(player1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(player1.getEnabled()))
                .andExpect(jsonPath("$.name").value(player1.getName()))
                .andExpect(jsonPath("$.eventId").value(event1.getId()))
                .andExpect(jsonPath("$.teamId").value(team1.getId()));

        verify(playerAdapter).mergePlayerIntoPlayer(player2.getId(), player1.getId());
    }

    @Test
    @WithUser
    void mergePlayersAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/players/merge/{source}/{destination}", player2.getId(), player1.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void mergePlayersAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/players/merge/{source}/{destination}", player2.getId(), player1.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void mergePlayersAsAdminBadRequest() throws Exception {
        doThrow(PlayersNotOfSameEventException.class).when(playerAdapter).mergePlayerIntoPlayer(anyLong(), anyLong());

        var actions = mockMvc.perform(
                        patch("/api/players/merge/{source}/{destination}", player4.getId(), player1.getId())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print());

        expectError(actions, ErrorCode.BADR0001);
    }

    @Test
    @WithAdmin
    void mergePlayersNotFound() throws Exception {
        doThrow(PlayerNotFoundException.class).when(playerAdapter).mergePlayerIntoPlayer(anyLong(), anyLong());

        var actions = mockMvc.perform(
                patch("/api/players/merge/{source}/{destination}", idSequence++, idSequence++)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0005);
    }

    @Test
    @WithAdmin
    void deletePlayerAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/players/{id}", player1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(playerAdapter).removePlayer(player1.getId());
    }

    @Test
    @WithOperator
    void deletePlayerAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/players/{id}", player1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deletePlayerAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/players/{id}", player1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deletePlayerAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/products/{id}", player1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    private PlayerDTO createPlayerResource() {
        return new PlayerDTO(
                null,
                randomStringGenerator(),
                nextInstant(),
                nextInstant(),
                true,
                true,
                randomStringGenerator(),
                nextInstant(),
                true,
                integerSequence++,
                Map.of(randomStringGenerator(), randomStringGenerator()),
                team1.getId(),
                event1.getId()
        );
    }

    private PatchPlayerAsOperatorDTO createPatchPlayerAsOperatorDTO(Integer pf) {
        return new PatchPlayerAsOperatorDTO(true, randomStringGenerator(), pf);
    }

    private PatchPlayerCommentDTO createPatchPlayerCommentDTO(String comment) {
        return new PatchPlayerCommentDTO(comment);
    }

    private PatchPlayerAsAdminDTO createPatchPlayerAsAdminDTO(String name) {
        return new PatchPlayerAsAdminDTO(name, true);
    }

    private PatchPlayerAsUserDTO createPatchPlayerAsUserDTO(String name) {
        return new PatchPlayerAsUserDTO(name, Map.of(randomStringGenerator(), randomStringGenerator()));
    }
}
