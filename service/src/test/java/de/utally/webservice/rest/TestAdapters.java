package de.utally.webservice.rest;

import de.utally.data.model.*;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TestAdapters {
    @Bean
    public LocationAdapter locationAdapter() {
        return Mockito.mock(LocationAdapter.class);
    }

    @Bean
    public EventAdapter eventAdapter() {
        return Mockito.mock(EventAdapter.class);
    }

    @Bean
    public ProductAdapter productAdapter() {
        return Mockito.mock(ProductAdapter.class);
    }

    @Bean
    public TimeSlotAdapter timeSlotAdapter() {
        return Mockito.mock(TimeSlotAdapter.class);
    }

    @Bean
    public TeamAdapter teamAdapter() {
        return Mockito.mock(TeamAdapter.class);
    }

    @Bean
    public PlayerAdapter playerAdapter() {
        return Mockito.mock(PlayerAdapter.class);
    }

    @Bean
    public TallyAdapter tallyAdapter() {
        return Mockito.mock(TallyAdapter.class);
    }
}
