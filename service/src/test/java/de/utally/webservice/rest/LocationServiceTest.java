package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import de.utally.data.exception.LocationNotFoundException;
import de.utally.data.exception.ProductNotFoundException;
import de.utally.data.model.Location;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.model.LocationDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectInvalidArgumentError;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import({TestAdapters.class, WebSecurityConfiguration.class, TestMapperConfig.class})
@WebMvcTest(value = LocationService.class)
class LocationServiceTest extends UtallyServiceTest {

    @BeforeEach
    void setUp() {
        super.setUp();
    }

    @AfterEach
    void tearDown() {
        super.tearDown();
    }

    @Test
    @WithUser
    void getLocationsAsUser() throws Exception {
        performGetAllLocations();
    }

    @Test
    @WithOperator
    void getLocationsAsOperator() throws Exception {
        performGetAllLocations();
    }

    @Test
    @WithAdmin
    void getLocationsAsAdmin() throws Exception {
        performGetAllLocations();
    }

    private void performGetAllLocations() throws Exception {
        mockMvc.perform(
                get("/api/locations/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(location1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(location1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(location1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(location1.getEnabled()))
                .andExpect(jsonPath("$[0].name").value(location1.getName()))
                .andExpect(jsonPath("$[0].description").value(location1.getDescription()))
                .andExpect(jsonPath("$[0].productIds").exists())
                .andExpect(jsonPath("$[0].productIds").isArray())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(location2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(location2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(location2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(location2.getEnabled()))
                .andExpect(jsonPath("$[1].name").value(location2.getName()))
                .andExpect(jsonPath("$[1].description").value(location2.getDescription()))
                .andExpect(jsonPath("$[1].productIds").exists())
                .andExpect(jsonPath("$[1].productIds").isArray())

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(location3.getId()))
                .andExpect(jsonPath("$[2].creationTime").value(location3.getCreationTime().toString()))
                .andExpect(jsonPath("$[2].modificationTime").value(location3.getModificationTime().toString()))
                .andExpect(jsonPath("$[2].enabled").value(location3.getEnabled()))
                .andExpect(jsonPath("$[2].name").value(location3.getName()))
                .andExpect(jsonPath("$[2].description").value(location3.getDescription()))
                .andExpect(jsonPath("$[2].productIds").exists())
                .andExpect(jsonPath("$[2].productIds").isArray())

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(location4.getId()))
                .andExpect(jsonPath("$[3].creationTime").value(location4.getCreationTime().toString()))
                .andExpect(jsonPath("$[3].modificationTime").value(location4.getModificationTime().toString()))
                .andExpect(jsonPath("$[3].enabled").value(location4.getEnabled()))
                .andExpect(jsonPath("$[3].name").value(location4.getName()))
                .andExpect(jsonPath("$[3].description").value(location4.getDescription()))
                .andExpect(jsonPath("$[3].productIds").exists())
                .andExpect(jsonPath("$[3].productIds").isArray());
    }

    @Test
    void getLocationsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/locations/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getLocationsByEventAsAdmin() throws Exception {
        performGetLocationsByEvent();
    }

    @Test
    @WithOperator
    void getLocationsByEventAsOperator() throws Exception {
        performGetLocationsByEvent();
    }

    @Test
    @WithUser
    void getLocationsByEventAsUser() throws Exception {
        performGetLocationsByEvent();
    }

    void performGetLocationsByEvent() throws Exception {
        mockMvc.perform(
                get("/api/locations/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(location1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(location1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(location1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(location1.getEnabled()))
                .andExpect(jsonPath("$[0].name").value(location1.getName()))
                .andExpect(jsonPath("$[0].description").value(location1.getDescription()))
                .andExpect(jsonPath("$[0].productIds").exists())
                .andExpect(jsonPath("$[0].productIds").isArray())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(location2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(location2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(location2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(location2.getEnabled()))
                .andExpect(jsonPath("$[1].name").value(location2.getName()))
                .andExpect(jsonPath("$[1].description").value(location2.getDescription()))
                .andExpect(jsonPath("$[1].productIds").exists())
                .andExpect(jsonPath("$[1].productIds").isArray())

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(location3.getId()))
                .andExpect(jsonPath("$[2].creationTime").value(location3.getCreationTime().toString()))
                .andExpect(jsonPath("$[2].modificationTime").value(location3.getModificationTime().toString()))
                .andExpect(jsonPath("$[2].enabled").value(location3.getEnabled()))
                .andExpect(jsonPath("$[2].name").value(location3.getName()))
                .andExpect(jsonPath("$[2].description").value(location3.getDescription()))
                .andExpect(jsonPath("$[2].productIds").exists())
                .andExpect(jsonPath("$[2].productIds").isArray());
    }

    @Test
    void getLocationsByEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/locations/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getLocationsByEventNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/locations/event/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getLocationAsAdmin() throws Exception {
        performGetLocation();
    }

    @Test
    @WithOperator
    void getLocationAsOperator() throws Exception {
        performGetLocation();
    }

    @Test
    @WithUser
    void getLocationAsUser() throws Exception {
        performGetLocation();
    }

    void performGetLocation() throws Exception {
        mockMvc.perform(
                get("/api/locations/{id}", location1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(location1.getId()))
                .andExpect(jsonPath("$.creationTime").value(location1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(location1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(location1.getEnabled()))
                .andExpect(jsonPath("$.name").value(location1.getName()))
                .andExpect(jsonPath("$.description").value(location1.getDescription()))
                .andExpect(jsonPath("$.productIds").exists())
                .andExpect(jsonPath("$.productIds").isArray());
    }

    @Test
    void getLocationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/locations/{id}", location1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getLocationNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/locations/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void postLocationAsAdmin() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        injectPostSaveCalls(r);

        performPostLocation(r);
    }

    private void performPostLocation(LocationDTO r) throws Exception {
        mockMvc.perform(
                post("/api/locations/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.name").value(r.name()))
                .andExpect(jsonPath("$.description").value(r.description()))
                .andExpect(jsonPath("$.productIds").exists())
                .andExpect(jsonPath("$.productIds").isArray());

        verify(locationAdapter).insertLocation(anyString(), anyString(), anyBoolean());
    }

    private void injectPostSaveCalls(LocationDTO r) {
        doAnswer(i -> {
            var location = new Location();
            location.setId(idSequence++);
            location.setName(i.getArgument(0, String.class));
            location.setDescription(i.getArgument(1, String.class));
            location.setEnabled(i.getArgument(2, Boolean.class));
            location.setCreationTime(nextInstant());
            location.setModificationTime(nextInstant());
            assertAll(
                    () -> assertEquals(r.name(), location.getName()),
                    () -> assertEquals(r.description(), location.getDescription()),
                    () -> assertEquals(r.enabled(), location.getEnabled())
            );
            return location;
        }).when(locationAdapter).insertLocation(anyString(), anyString(), anyBoolean());
    }

    @Test
    @WithOperator
    void postLocationAsOperator() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/locations/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void postLocationAsUser() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/locations/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void postLocationAsAnonymous() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/locations/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void postLocationInvalid() throws Exception {
        var r = createLocationResource(null);

        var actions = mockMvc.perform(
                post("/api/locations/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void putLocationAsAdmin() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        injectPutSaveCalls(r);

        performPutLocation(r);
    }

    private void performPutLocation(LocationDTO r) throws Exception {
        mockMvc.perform(
                put("/api/locations/{id}", location1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(location1.getId()))
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.name").value(r.name()))
                .andExpect(jsonPath("$.description").value(r.description()))
                .andExpect(jsonPath("$.productIds").exists())
                .andExpect(jsonPath("$.productIds").isArray());

        verify(locationAdapter).updateLocation(any());
    }

    private void injectPutSaveCalls(LocationDTO r) {
        doAnswer(i -> {
            var l = i.getArgument(0, Location.class);
            assertAll(
                    () -> assertEquals(r.name(), l.getName()),
                    () -> assertEquals(r.description(), l.getDescription()),
                    () -> assertEquals(r.enabled(), l.getEnabled())
            );
            return null;
        }).when(locationAdapter).updateLocation(any());
    }

    @Test
    @WithOperator
    void putLocationAsOperator() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/locations/{id}", location1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void putLocationAsUser() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/locations/{id}", location1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void putLocationAsAnonymous() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/locations/{id}", location1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void putLocationNotExisting() throws Exception {
        var r = createLocationResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/locations/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void putLocationInvalid() throws Exception {
        var r = createLocationResource(null);

        var actions = mockMvc.perform(
                put("/api/locations/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void deleteLocationAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/locations/{id}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(locationAdapter).removeLocation(location1.getId());
    }

    @Test
    @WithOperator
    void deleteLocationAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/locations/{id}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteLocationAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/locations/{id}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteLocationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/locations/{id}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void addProductToLocationAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/locations/{id}/addproduct/{productId}", location1.getId(), product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(location1.getId()))
                .andExpect(jsonPath("$.creationTime").value(location1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(location1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(location1.getEnabled()))
                .andExpect(jsonPath("$.name").value(location1.getName()))
                .andExpect(jsonPath("$.description").value(location1.getDescription()))
                .andExpect(jsonPath("$.productIds").exists())
                .andExpect(jsonPath("$.productIds").isArray());

        verify(locationAdapter).addProduct(location1.getId(), product1.getId());
    }

    @Test
    @WithOperator
    void addProductToLocationAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/addproduct/{productId}", location1.getId(), product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void addProductToLocationAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/addproduct/{productId}", location1.getId(), product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void addProductToLocationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/addproduct/{productId}", location1.getId(), product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void addProductToLocationLocationNotFound() throws Exception {
        doThrow(LocationNotFoundException.class).when(locationAdapter).addProduct(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/addproduct/{productId}", idSequence++, product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);

        verify(locationAdapter).addProduct(anyLong(), eq(product1.getId()));
    }

    @Test
    @WithAdmin
    void addProductToLocationProductNotFound() throws Exception {
        doThrow(ProductNotFoundException.class).when(locationAdapter).addProduct(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/addproduct/{productId}", location1.getId(), idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);

        verify(locationAdapter).addProduct(eq(location1.getId()), anyLong());
    }

    @Test
    @WithAdmin
    void removeProductFromLocationAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/locations/{id}/removeproduct/{productId}", location1.getId(), product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(location1.getId()))
                .andExpect(jsonPath("$.creationTime").value(location1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(location1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(location1.getEnabled()))
                .andExpect(jsonPath("$.name").value(location1.getName()))
                .andExpect(jsonPath("$.description").value(location1.getDescription()))
                .andExpect(jsonPath("$.productIds").exists())
                .andExpect(jsonPath("$.productIds").isArray());

        verify(locationAdapter).removeProduct(location1.getId(), product1.getId());
    }

    @Test
    @WithOperator
    void removeProductFromLocationAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/removeproduct/{productId}", location1.getId(), product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void removeProductFromLocationAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/removeproduct/{productId}", location1.getId(), product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void removeProductFromLocationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/removeproduct/{productId}", location1.getId(), product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void removeProductFromLocationLocationNotFound() throws Exception {
        doThrow(LocationNotFoundException.class).when(locationAdapter).removeProduct(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/removeproduct/{productId}", idSequence++, product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);

        verify(locationAdapter).removeProduct(anyLong(), eq(product1.getId()));
    }

    @Test
    @WithAdmin
    void removeProductFromLocationProductNotFound() throws Exception {
        doThrow(ProductNotFoundException.class).when(locationAdapter).removeProduct(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/locations/{id}/removeproduct/{productId}", location1.getId(), idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);

        verify(locationAdapter).removeProduct(eq(location1.getId()), anyLong());
    }

    @Test
    @WithAdmin
    void removeProductFromLocationsAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/locations/removeproduct/{productId}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(locationAdapter).removeProductFromAllLocations(product1.getId());
    }

    @Test
    @WithOperator
    void removeProductFromLocationsAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/removeproduct/{productId}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void removeProductFromLocationsAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/removeproduct/{productId}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void removeProductFromLocationsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/locations/removeproduct/{productId}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void removeProductFromLocationsNotFound() throws Exception {
        doThrow(ProductNotFoundException.class).when(locationAdapter).removeProductFromAllLocations(anyLong());
        var actions = mockMvc.perform(
                patch("/api/locations/removeproduct/{productId}", idSequence)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    private LocationDTO createLocationResource(String name) {
        return new LocationDTO(
                null,
                name,
                true,
                null,
                null,
                randomStringGenerator(),
                null
        );
    }
}
