package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import(WebSecurityConfiguration.class)
@WebMvcTest(value = AngularService.class)
class AngularServiceTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void forwardToIndexAdmin() throws Exception {
        mockMvc.perform(
                        get("/admin")
                ).andDo(print())
                .andExpect(status().isMovedPermanently())
                .andExpect(header().string("Location", "/"));
    }

    @Test
    void forwardToIndexOperator() throws Exception {
        mockMvc.perform(
                get("/operator")
        ).andDo(print())
                .andExpect(status().isMovedPermanently())
                .andExpect(header().string("Location", "/"));
    }

    @Test
    void forwardToIndexUser() throws Exception {
        mockMvc.perform(
                        get("/user")
                ).andDo(print())
                .andExpect(status().isMovedPermanently())
                .andExpect(header().string("Location", "/"));
    }

    @Test
    void forwardToIndexLogin() throws Exception {
        mockMvc.perform(
                        get("/login")
                ).andDo(print())
                .andExpect(status().isMovedPermanently())
                .andExpect(header().string("Location", "/"));
    }

    @Test
    void forwardToIndexParty() throws Exception {
        mockMvc.perform(
                        get("/party")
                ).andDo(print())
                .andExpect(status().isMovedPermanently())
                .andExpect(header().string("Location", "/"));
    }
}