package de.utally.webservice.rest.validation;

import de.utally.webservice.rest.model.DtoMapper;
import de.utally.webservice.rest.model.TimeZoneDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import jakarta.validation.ConstraintValidatorContext;

import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ZoneIdValidatorTest {
    @Mock
    private ConstraintValidatorContext context;

    private final ZoneIdValidator validator = new ZoneIdValidator();

    private final DtoMapper mapper = Mappers.getMapper(DtoMapper.class);

    @Test
    void isValid() {
        assertThat(validator.isValid(mapper.toDto(ZoneId.systemDefault()), context)).isTrue();
    }

    @Test
    void isNotValid() {
        var timeZone = new TimeZoneDTO("ABCDE", null, null);
        assertThat(validator.isValid(timeZone, context)).isFalse();
    }

    @Test
    void isNotValidNull() {
        var timeZone = new TimeZoneDTO(null, null, null);
        assertThat(validator.isValid(timeZone, context)).isFalse();
    }

    @Test
    void isValidNull() {
        assertThat(validator.isValid(null, context)).isTrue();
    }
}