package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import de.utally.data.model.Product;
import de.utally.data.model.ProductCategory;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.model.ProductDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

import java.time.Instant;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectInvalidArgumentError;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import({TestAdapters.class, WebSecurityConfiguration.class, TestMapperConfig.class})
@WebMvcTest(value = ProductService.class)
class ProductServiceTest extends UtallyServiceTest {

    @BeforeEach
    void setUp() {
        super.setUp();
    }

    @AfterEach
    void tearDown() {
        super.tearDown();
    }

    @Test
    @WithAdmin
    void getProductsAsAdmin() throws Exception {
        performGetAllProducts();
    }

    @Test
    @WithOperator
    void getProductsAsOperator() throws Exception {
        performGetAllProducts();
    }

    @Test
    @WithUser
    void getProductsAsUser() throws Exception {
        performGetAllProducts();
    }

    private void performGetAllProducts() throws Exception {
        mockMvc.perform(
                get("/api/products/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(product1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(product1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(product1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(product1.getEnabled()))
                .andExpect(jsonPath("$[0].name").value(product1.getName()))
                .andExpect(jsonPath("$[0].description").value(product1.getDescription()))
                .andExpect(jsonPath("$[0].displayStatistic").value(product1.getDisplayStatistic()))
                .andExpect(jsonPath("$[0].price").value(product1.getPrice()))
                .andExpect(jsonPath("$[0].category").value(product1.getCategory().toString()))
                .andExpect(jsonPath("$[0].imageName").value(product1.getImageName()))

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(product2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(product2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(product2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(product2.getEnabled()))
                .andExpect(jsonPath("$[1].name").value(product2.getName()))
                .andExpect(jsonPath("$[1].description").value(product2.getDescription()))
                .andExpect(jsonPath("$[1].displayStatistic").value(product2.getDisplayStatistic()))
                .andExpect(jsonPath("$[1].price").value(product2.getPrice()))
                .andExpect(jsonPath("$[1].category").value(product2.getCategory().toString()))
                .andExpect(jsonPath("$[1].imageName").value(product2.getImageName()))

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value(product3.getId()))
                .andExpect(jsonPath("$[2].creationTime").value(product3.getCreationTime().toString()))
                .andExpect(jsonPath("$[2].modificationTime").value(product3.getModificationTime().toString()))
                .andExpect(jsonPath("$[2].enabled").value(product3.getEnabled()))
                .andExpect(jsonPath("$[2].name").value(product3.getName()))
                .andExpect(jsonPath("$[2].description").value(product3.getDescription()))
                .andExpect(jsonPath("$[2].displayStatistic").value(product3.getDisplayStatistic()))
                .andExpect(jsonPath("$[2].price").value(product3.getPrice()))
                .andExpect(jsonPath("$[2].category").value(product3.getCategory().toString()))
                .andExpect(jsonPath("$[2].imageName").value(product3.getImageName()))

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value(product4.getId()))
                .andExpect(jsonPath("$[3].creationTime").value(product4.getCreationTime().toString()))
                .andExpect(jsonPath("$[3].modificationTime").value(product4.getModificationTime().toString()))
                .andExpect(jsonPath("$[3].enabled").value(product4.getEnabled()))
                .andExpect(jsonPath("$[3].name").value(product4.getName()))
                .andExpect(jsonPath("$[3].description").value(product4.getDescription()))
                .andExpect(jsonPath("$[3].displayStatistic").value(product4.getDisplayStatistic()))
                .andExpect(jsonPath("$[3].price").value(product4.getPrice()))
                .andExpect(jsonPath("$[3].category").value(product4.getCategory().toString()))
                .andExpect(jsonPath("$[3].imageName").value(product4.getImageName()));
    }

    @Test
    void getProductsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/products/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getProductsByEventAsAdmin() throws Exception {
        performGetProductsByEvent();
    }

    @Test
    @WithOperator
    void getProductsByEventAsOperator() throws Exception {
        performGetProductsByEvent();
    }

    @Test
    @WithUser
    void getProductsByEventAsUser() throws Exception {
        performGetProductsByEvent();
    }

    private void performGetProductsByEvent() throws Exception {
        mockMvc.perform(
                        get("/api/products/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")]").exists())
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].creationTime").value(product1.getCreationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].modificationTime").value(product1.getModificationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].enabled").value(product1.getEnabled()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].name").value(product1.getName()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].description").value(product1.getDescription()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].displayStatistic").value(product1.getDisplayStatistic()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].price").value(product1.getPrice()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].category").value(product1.getCategory().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].imageName").value(product1.getImageName()))

                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")]").exists())
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].creationTime").value(product2.getCreationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].modificationTime").value(product2.getModificationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].enabled").value(product2.getEnabled()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].name").value(product2.getName()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].description").value(product2.getDescription()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].displayStatistic").value(product2.getDisplayStatistic()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].price").value(product2.getPrice()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].category").value(product2.getCategory().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].imageName").value(product2.getImageName()))

                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")]").exists())
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].creationTime").value(product3.getCreationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].modificationTime").value(product3.getModificationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].enabled").value(product3.getEnabled()))
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].name").value(product3.getName()))
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].description").value(product3.getDescription()))
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].displayStatistic").value(product3.getDisplayStatistic()))
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].price").value(product3.getPrice()))
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].category").value(product3.getCategory().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product3.getId() + ")].imageName").value(product3.getImageName()))

                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")]").exists())
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].creationTime").value(product4.getCreationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].modificationTime").value(product4.getModificationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].enabled").value(product4.getEnabled()))
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].name").value(product4.getName()))
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].description").value(product4.getDescription()))
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].displayStatistic").value(product4.getDisplayStatistic()))
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].price").value(product4.getPrice()))
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].category").value(product4.getCategory().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product4.getId() + ")].imageName").value(product4.getImageName()));
    }

    @Test
    void getProductsByEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/products/event/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getProductsByEventNotExists() throws Exception {
        var actions = mockMvc.perform(
                get("/api/products/event/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getProductsByLocationAsAdmin() throws Exception {
        performGetProductsByLocation();
    }

    @Test
    @WithOperator
    void getProductsByLocationAsOperator() throws Exception {
        performGetProductsByLocation();
    }

    @Test
    @WithUser
    void getProductsByLocationAsUser() throws Exception {
        performGetProductsByLocation();
    }

    private void performGetProductsByLocation() throws Exception {
        mockMvc.perform(
                        get("/api/products/location/{id}", location1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")]").exists())
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].creationTime").value(product1.getCreationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].modificationTime").value(product1.getModificationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].enabled").value(product1.getEnabled()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].name").value(product1.getName()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].description").value(product1.getDescription()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].displayStatistic").value(product1.getDisplayStatistic()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].price").value(product1.getPrice()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].category").value(product1.getCategory().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product1.getId() + ")].imageName").value(product1.getImageName()))

                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")]").exists())
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].creationTime").value(product2.getCreationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].modificationTime").value(product2.getModificationTime().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].enabled").value(product2.getEnabled()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].name").value(product2.getName()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].description").value(product2.getDescription()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].displayStatistic").value(product2.getDisplayStatistic()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].price").value(product2.getPrice()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].category").value(product2.getCategory().toString()))
                .andExpect(jsonPath("$[?(@.id == " + product2.getId() + ")].imageName").value(product2.getImageName()));
    }

    @Test
    void getProductsByLocationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/products/location/{id}", location1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getProductsByLocationNotExists() throws Exception {
        var actions = mockMvc.perform(
                get("/api/products/location/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void getProductAsAdmin() throws Exception {
        performGetProduct();
    }

    @Test
    @WithOperator
    void getProductAsOperator() throws Exception {
        performGetProduct();
    }

    @Test
    @WithUser
    void getProductAsUser() throws Exception {
        performGetProduct();
    }

    void performGetProduct() throws Exception {
        mockMvc.perform(
                get("/api/products/{id}", product1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(product1.getId()))
                .andExpect(jsonPath("$.creationTime").value(product1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(product1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(product1.getEnabled()))
                .andExpect(jsonPath("$.name").value(product1.getName()))
                .andExpect(jsonPath("$.description").value(product1.getDescription()))
                .andExpect(jsonPath("$.displayStatistic").value(product1.getDisplayStatistic()))
                .andExpect(jsonPath("$.price").value(product1.getPrice()))
                .andExpect(jsonPath("$.category").value(product1.getCategory().toString()))
                .andExpect(jsonPath("$.imageName").value(product1.getImageName()));
    }

    @Test
    void getProductAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/products/{id}", product1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getProductNotExisting() throws Exception {
        var actions = mockMvc.perform(
                get("/api/products/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    @Test
    @WithAdmin
    void postProductAsAdmin() throws Exception {
        var r = createProductResource(randomStringGenerator());

        injectPostSaveCalls(r);

        performPostProduct(r);
    }

    private void performPostProduct(ProductDTO r) throws Exception {
        mockMvc.perform(
                post("/api/products/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.name").value(r.name()))
                .andExpect(jsonPath("$.description").value(r.description()))
                .andExpect(jsonPath("$.displayStatistic").value(r.displayStatistic()))
                .andExpect(jsonPath("$.price").value(r.price()))
                .andExpect(jsonPath("$.category").value(r.category().toString()))
                .andExpect(jsonPath("$.imageName").value(r.imageName()));

        verify(productAdapter).insertProduct(anyString(), anyString(), anyInt(), anyString(), any(), anyBoolean(), anyBoolean());
    }

    private void injectPostSaveCalls(ProductDTO r) {
        doAnswer(i -> {
            var product = new Product();
            product.setId(idSequence++);
            product.setName(i.getArgument(0, String.class));
            product.setDescription(i.getArgument(1, String.class));
            product.setPrice(i.getArgument(2, Integer.class));
            product.setImageName(i.getArgument(3, String.class));
            product.setCategory(i.getArgument(4, ProductCategory.class));
            product.setEnabled(i.getArgument(5, Boolean.class));
            product.setDisplayStatistic(i.getArgument(6, Boolean.class));
            product.setCreationTime(nextInstant());
            product.setModificationTime(nextInstant());
            assertAll(
                    () -> assertEquals(r.name(), product.getName()),
                    () -> assertEquals(r.description(), product.getDescription()),
                    () -> assertEquals(r.imageName(), product.getImageName()),
                    () -> assertEquals(r.category(), product.getCategory()),
                    () -> assertEquals(r.price(), product.getPrice()),
                    () -> assertEquals(r.displayStatistic(), product.getDisplayStatistic()),
                    () -> assertEquals(r.enabled(), product.getEnabled())
            );
            return product;
        }).when(productAdapter).insertProduct(anyString(), anyString(), anyInt(), anyString(), any(), anyBoolean(), anyBoolean());
    }

    @Test
    @WithOperator
    void postProductAsOperator() throws Exception {
        var r = createProductResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/products/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void postProductAsUser() throws Exception {
        var r = createProductResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/products/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void postProductAsAnonymous() throws Exception {
        var r = createProductResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/products/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void postProductInvalid() throws Exception {
        var r = createProductResource(null);

        var actions = mockMvc.perform(
                post("/api/products/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void putProduct() throws Exception {
        var r = createProductResource(randomStringGenerator());

        injectPutSaveCalls(r);

        performPutProduct(r);
    }

    private void performPutProduct(ProductDTO r) throws Exception {
        mockMvc.perform(
                put("/api/products/{id}", product1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(product1.getId()))
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.name").value(r.name()))
                .andExpect(jsonPath("$.description").value(r.description()))
                .andExpect(jsonPath("$.displayStatistic").value(r.displayStatistic()))
                .andExpect(jsonPath("$.price").value(r.price()))
                .andExpect(jsonPath("$.category").value(r.category().toString()))
                .andExpect(jsonPath("$.imageName").value(r.imageName()));

        verify(productAdapter).updateProduct(any());
    }

    private void injectPutSaveCalls(ProductDTO r) {
        doAnswer(i -> {
            var product = i.getArgument(0, Product.class);
            assertAll(
                    () -> assertEquals(r.name(), product.getName()),
                    () -> assertEquals(r.description(), product.getDescription()),
                    () -> assertEquals(r.imageName(), product.getImageName()),
                    () -> assertEquals(r.category(), product.getCategory()),
                    () -> assertEquals(r.price(), product.getPrice()),
                    () -> assertEquals(r.displayStatistic(), product.getDisplayStatistic()),
                    () -> assertEquals(r.enabled(), product.getEnabled())
            );
            return null;
        }).when(productAdapter).updateProduct(any());
    }

    @Test
    @WithOperator
    void putProductAsOperator() throws Exception {
        var r = createProductResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/products/{id}", team1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void putProductAsUser() throws Exception {
        var r = createProductResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/products/{id}", team1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void putProductAsAnonymous() throws Exception {
        var r = createProductResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/products/{id}", team1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void putProductNotFound() throws Exception {
        var r = createProductResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/products/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    @Test
    @WithAdmin
    void putProductInvalid() throws Exception {
        var r = createProductResource(null);

        var actions = mockMvc.perform(
                put("/api/products/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void deleteProductAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/products/{id}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(productAdapter).removeProduct(product1.getId());
    }

    @Test
    @WithOperator
    void deleteProductAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/products/{id}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteProductAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/products/{id}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteProductAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/products/{id}", product1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    private ProductDTO createProductResource(String name) {
        return new ProductDTO(
                5L,
                name,
                integerSequence++,
                randomStringGenerator(),
                ProductCategory.FOOD,
                true,
                Instant.now(),
                Instant.now(),
                randomStringGenerator(),
                true
        );
    }
}
