package de.utally.webservice.rest;

import de.utally.webservice.rest.exception.UtallyRestException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class RestImageHandlerTest {
    private static final String STORAGE_PATH = "src/test/resources";
    private static final String TEST_FILE_NAME = "testfile.txt";
    private static final String TEST_FILE_PATH = STORAGE_PATH + File.separatorChar + TEST_FILE_NAME;
    private static final File TEST_FILE = new File(TEST_FILE_PATH);
    private static final String TEST_FILE_CONTENT = "test file content";

    RestImageHandler restImageHandler;

    @BeforeEach
    void setUp() {
        restImageHandler = new RestImageHandler(STORAGE_PATH);
    }

    @AfterEach
    void tearDown() {
        if (TEST_FILE.exists() && !TEST_FILE.delete()) {
            fail("Cannot delete file");
        }
    }

    @Test
    void getAllFilenames() {
        List<String> filenames = null;
        try {
            filenames = restImageHandler.getAllFilenames();
        } catch (IOException e) {
            fail("IOException occured", e);
        }

        assertThat(filenames)
                .isNotNull()
                .contains("dummyimage.txt", "emptyfile.txt")
                .doesNotContain(TEST_FILE_NAME);
    }

    @Test
    void getFileResource() {
        try (PrintWriter out = new PrintWriter(TEST_FILE_PATH)) {
            out.println(TEST_FILE_CONTENT);
        } catch (IOException e) {
            fail("IOException occured", e);
        }

        assertThat(TEST_FILE).exists();

        var resource = restImageHandler.getFileResource(TEST_FILE_NAME);

        assertThat(resource.getFilename()).isEqualTo(TEST_FILE_NAME);
    }

    @Test
    void getFileResourceMissing() {
        assertThat(TEST_FILE).doesNotExist();
        assertThatExceptionOfType(UtallyRestException.class).isThrownBy(() -> restImageHandler.getFileResource(TEST_FILE_NAME));
        assertThat(TEST_FILE).doesNotExist();
    }

    @Test
    void storeImage() {
        MultipartFile file = mock(MultipartFile.class);
        when(file.isEmpty()).thenReturn(false);
        when(file.getSize()).thenReturn(5000000L);
        when(file.getOriginalFilename()).thenReturn(TEST_FILE_NAME);
        try {
            restImageHandler.storeImage(file);
        } catch (IOException e) {
            fail("IOException occured", e);
        }
        try {
            verify(file).transferTo(Path.of(TEST_FILE_PATH));
        } catch (IOException e) {
            fail("IOException occured", e);
        }
    }

    @Test
    void storeImageNull() {
        assertThatNullPointerException().isThrownBy(() -> restImageHandler.storeImage(null));
    }

    @Test
    void storeImageEmpty() {
        MultipartFile file = mock(MultipartFile.class);
        when(file.isEmpty()).thenReturn(true);
        assertThatExceptionOfType(UtallyRestException.class).isThrownBy(() -> restImageHandler.storeImage(file));
    }

    @Test
    void storeImageTooLarge() {
        MultipartFile file = mock(MultipartFile.class);
        when(file.isEmpty()).thenReturn(false);
        when(file.getSize()).thenReturn(5000001L);
        assertThatExceptionOfType(UtallyRestException.class).isThrownBy(() -> restImageHandler.storeImage(file));
    }

    @Test
    void deleteImage() {
        try (PrintWriter out = new PrintWriter(TEST_FILE_PATH)) {
            out.println(TEST_FILE_CONTENT);
        } catch (IOException e) {
            fail("IOException occured", e);
        }

        assertThat(TEST_FILE).exists();

        try {
            restImageHandler.deleteImage(TEST_FILE_NAME);
        } catch (IOException e) {
            fail("IOException occured", e);
        }

        assertThat(TEST_FILE).doesNotExist();
    }

    @Test
    void deleteImageNotExisting() {
        assertThat(TEST_FILE).doesNotExist();

        try {
            restImageHandler.deleteImage(TEST_FILE_NAME);
        } catch (IOException e) {
            fail("IOException occured", e);
        }

        assertThat(TEST_FILE).doesNotExist();
    }
}
