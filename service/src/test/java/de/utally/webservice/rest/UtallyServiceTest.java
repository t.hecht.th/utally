package de.utally.webservice.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.utally.data.exception.*;
import de.utally.data.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@TestPropertySource(properties = {
        "operator.username=operatorUN",
        "operator.password=operatorPW",
        "admin.username=adminUN",
        "admin.password=adminPW",
        "user.username=userUN",
        "user.password=userPW"
})
public abstract class UtallyServiceTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    LocationAdapter locationAdapter;

    @Autowired
    EventAdapter eventAdapter;

    @Autowired
    ProductAdapter productAdapter;

    @Autowired
    PlayerAdapter playerAdapter;

    @Autowired
    TallyAdapter tallyAdapter;

    @Autowired
    TimeSlotAdapter timeSlotAdapter;

    @Autowired
    TeamAdapter teamAdapter;

    @Autowired
    protected ObjectMapper objectMapper;

    Event event1;
    Event event2;

    Location location1;
    Location location2;
    Location location3;
    Location location4;

    Product product1;
    Product product2;
    Product product3;
    Product product4;

    Team team1;
    Team team2;
    Team team3;
    Team team4;

    TimeSlot timeSlot1;
    TimeSlot timeSlot2;
    TimeSlot timeSlot3;
    TimeSlot timeSlot4;

    Player player1;
    Player player2;
    Player player3;
    Player player4;

    Tally tally1;
    Tally tally2;
    Tally tally3;
    Tally tally4;
    Tally tally5;
    Tally tally6;
    Tally tally7;
    Tally tally8;

    long idSequence = 5L;
    Random r = new Random(120);
    Instant instant = Instant.ofEpochMilli(1610098386175L);
    int integerSequence = 1005;

    void setUp() {
        createEnvironment();
    }

    void tearDown() {
        reset(
                locationAdapter,
                eventAdapter,
                playerAdapter,
                timeSlotAdapter,
                tallyAdapter,
                productAdapter,
                teamAdapter
        );
    }

    private void createEnvironment() {
        createProducts();
        createLocations();
        createTeams();
        createEvents();
        createPlayer();
        createTallies();
        createTimeSlots();
    }

    private void createProducts() {
        product1 = new Product();
        product1.setId(idSequence++);
        product1.setName(randomStringGenerator());
        product1.setEnabled(true);
        product1.setPrice(integerSequence++);
        product1.setImageName(randomStringGenerator());
        product1.setDisplayStatistic(true);
        product1.setCreationTime(nextInstant());
        product1.setModificationTime(nextInstant());
        product1.setDescription(randomStringGenerator());
        product1.setCategory(ProductCategory.FOOD);

        product2 = new Product();
        product2.setId(idSequence++);
        product2.setName(randomStringGenerator());
        product2.setEnabled(true);
        product2.setPrice(integerSequence++);
        product2.setImageName(randomStringGenerator());
        product2.setDisplayStatistic(true);
        product2.setCreationTime(nextInstant());
        product2.setModificationTime(nextInstant());
        product2.setDescription(randomStringGenerator());
        product2.setCategory(ProductCategory.DRINK);

        product3 = new Product();
        product3.setId(idSequence++);
        product3.setName(randomStringGenerator());
        product3.setEnabled(true);
        product3.setPrice(integerSequence++);
        product3.setImageName(randomStringGenerator());
        product3.setDisplayStatistic(true);
        product3.setCreationTime(nextInstant());
        product3.setModificationTime(nextInstant());
        product3.setDescription(randomStringGenerator());
        product3.setCategory(ProductCategory.MDSE);

        product4 = new Product();
        product4.setId(idSequence++);
        product4.setName(randomStringGenerator());
        product4.setEnabled(true);
        product4.setPrice(integerSequence++);
        product4.setImageName(randomStringGenerator());
        product4.setDisplayStatistic(true);
        product4.setCreationTime(nextInstant());
        product4.setModificationTime(nextInstant());
        product4.setDescription(randomStringGenerator());
        product4.setCategory(ProductCategory.MISC);

        when(productAdapter.getProducts()).thenReturn(List.of(product1, product2, product3, product4));
        when(productAdapter.getProductById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (product1.getId().equals(id)) {
                return product1;
            }
            if (product2.getId().equals(id)) {
                return product2;
            }
            if (product3.getId().equals(id)) {
                return product3;
            }
            if (product4.getId().equals(id)) {
                return product4;
            }

            throw new ProductNotFoundException();
        });
        when(productAdapter.existsById(anyLong())).thenReturn(Boolean.FALSE);
        when(productAdapter.existsById(product1.getId())).thenReturn(Boolean.TRUE);
        when(productAdapter.existsById(product2.getId())).thenReturn(Boolean.TRUE);
        when(productAdapter.existsById(product3.getId())).thenReturn(Boolean.TRUE);
        when(productAdapter.existsById(product4.getId())).thenReturn(Boolean.TRUE);
    }

    private void createLocations() {
        location1 = new Location();
        location1.setId(idSequence++);
        location1.setDescription(randomStringGenerator());
        location1.setName(randomStringGenerator());
        location1.setEnabled(true);
        location1.setCreationTime(nextInstant());
        location1.setModificationTime(nextInstant());
        location1.setProducts(Set.of(product1, product2));

        location2 = new Location();
        location2.setId(idSequence++);
        location2.setDescription(randomStringGenerator());
        location2.setName(randomStringGenerator());
        location2.setEnabled(true);
        location2.setCreationTime(nextInstant());
        location2.setModificationTime(nextInstant());
        location2.setProducts(Set.of(product3, product4));

        location3 = new Location();
        location3.setId(idSequence++);
        location3.setDescription(randomStringGenerator());
        location3.setName(randomStringGenerator());
        location3.setEnabled(true);
        location3.setCreationTime(nextInstant());
        location3.setModificationTime(nextInstant());
        location3.setProducts(Set.of(product1, product3));

        location4 = new Location();
        location4.setId(idSequence++);
        location4.setDescription(randomStringGenerator());
        location4.setName(randomStringGenerator());
        location4.setEnabled(true);
        location4.setCreationTime(nextInstant());
        location4.setModificationTime(nextInstant());
        location4.setProducts(Collections.emptySet());

        when(locationAdapter.getLocations()).thenReturn(List.of(location1, location2, location3, location4));
        when(locationAdapter.getLocation(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (location1.getId().equals(id)) {
                return location1;
            }
            if (location2.getId().equals(id)) {
                return location2;
            }
            if (location3.getId().equals(id)) {
                return location3;
            }
            if (location4.getId().equals(id)) {
                return location4;
            }

            throw new LocationNotFoundException();
        });
        when(locationAdapter.existsById(anyLong())).thenReturn(Boolean.FALSE);
        when(locationAdapter.existsById(location1.getId())).thenReturn(Boolean.TRUE);
        when(locationAdapter.existsById(location2.getId())).thenReturn(Boolean.TRUE);
        when(locationAdapter.existsById(location3.getId())).thenReturn(Boolean.TRUE);
        when(locationAdapter.existsById(location4.getId())).thenReturn(Boolean.TRUE);

        when(locationAdapter.getProducts(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (location1.getId().equals(id)) {
                return List.copyOf(location1.getProducts());
            }
            if (location2.getId().equals(id)) {
                return List.copyOf(location2.getProducts());
            }
            if (location3.getId().equals(id)) {
                return List.copyOf(location3.getProducts());
            }
            if (location4.getId().equals(id)) {
                return List.copyOf(location4.getProducts());
            }

            throw new LocationNotFoundException();
        });
    }

    private void createTeams() {
        team1 = new Team();
        team1.setId(idSequence++);
        team1.setShortName(randomStringGenerator());
        team1.setDescription(randomStringGenerator());
        team1.setFullName(randomStringGenerator());
        team1.setEnabled(true);
        team1.setCity(randomStringGenerator());
        team1.setCreationTime(nextInstant());
        team1.setModificationTime(nextInstant());

        team2 = new Team();
        team2.setId(idSequence++);
        team2.setShortName(randomStringGenerator());
        team2.setDescription(randomStringGenerator());
        team2.setFullName(randomStringGenerator());
        team2.setEnabled(true);
        team2.setCity(randomStringGenerator());
        team2.setCreationTime(nextInstant());
        team2.setModificationTime(nextInstant());

        team3 = new Team();
        team3.setId(idSequence++);
        team3.setShortName(randomStringGenerator());
        team3.setDescription(randomStringGenerator());
        team3.setFullName(randomStringGenerator());
        team3.setEnabled(true);
        team3.setCity(randomStringGenerator());
        team3.setCreationTime(nextInstant());
        team3.setModificationTime(nextInstant());

        team4 = new Team();
        team4.setId(idSequence++);
        team4.setShortName(randomStringGenerator());
        team4.setDescription(randomStringGenerator());
        team4.setFullName(randomStringGenerator());
        team4.setEnabled(true);
        team4.setCity(randomStringGenerator());
        team4.setCreationTime(nextInstant());
        team4.setModificationTime(nextInstant());

        when(teamAdapter.getTeams()).thenReturn(List.of(team1, team2, team3, team4));
        when(teamAdapter.getTeamById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (team1.getId().equals(id)) {
                return team1;
            }
            if (team2.getId().equals(id)) {
                return team2;
            }
            if (team3.getId().equals(id)) {
                return team3;
            }
            if (team4.getId().equals(id)) {
                return team4;
            }

            throw new TeamNotFoundException();
        });
    }

    private void createTimeSlots() {
        timeSlot1 = new TimeSlot();
        timeSlot1.setId(idSequence++);
        timeSlot1.setCreationTime(nextInstant());
        timeSlot1.setModificationTime(nextInstant());
        timeSlot1.setEnabled(true);
        timeSlot1.setDescription(randomStringGenerator());
        timeSlot1.setStart(tally1.getTime().minus(5L, ChronoUnit.HOURS));
        timeSlot1.setEnd(tally1.getTime().plus(5L, ChronoUnit.HOURS));
        timeSlot1.setLabel(randomStringGenerator());
        timeSlot1.setEvent(event1);

        timeSlot2 = new TimeSlot();
        timeSlot2.setId(idSequence++);
        timeSlot2.setCreationTime(nextInstant());
        timeSlot2.setModificationTime(nextInstant());
        timeSlot2.setEnabled(true);
        timeSlot2.setDescription(randomStringGenerator());
        timeSlot2.setStart(tally1.getTime().minus(5L, ChronoUnit.HOURS));
        timeSlot2.setEnd(tally1.getTime().plus(5L, ChronoUnit.HOURS));
        timeSlot2.setLabel(randomStringGenerator());
        timeSlot2.setEvent(event1);

        timeSlot3 = new TimeSlot();
        timeSlot3.setId(idSequence++);
        timeSlot3.setCreationTime(nextInstant());
        timeSlot3.setModificationTime(nextInstant());
        timeSlot3.setEnabled(true);
        timeSlot3.setDescription(randomStringGenerator());
        timeSlot3.setStart(tally1.getTime().minus(5L, ChronoUnit.HOURS));
        timeSlot3.setEnd(tally1.getTime().plus(5L, ChronoUnit.HOURS));
        timeSlot3.setLabel(randomStringGenerator());
        timeSlot3.setEvent(event2);

        timeSlot4 = new TimeSlot();
        timeSlot4.setId(idSequence++);
        timeSlot4.setCreationTime(nextInstant());
        timeSlot4.setModificationTime(nextInstant());
        timeSlot4.setEnabled(true);
        timeSlot4.setDescription(randomStringGenerator());
        timeSlot4.setStart(tally1.getTime().minus(5L, ChronoUnit.HOURS));
        timeSlot4.setEnd(tally1.getTime().plus(5L, ChronoUnit.HOURS));
        timeSlot4.setLabel(randomStringGenerator());
        timeSlot4.setEvent(event2);

        when(timeSlotAdapter.getTimeSlots()).thenReturn(List.of(timeSlot1, timeSlot2, timeSlot3, timeSlot4));
        when(timeSlotAdapter.getTimeSlotById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (timeSlot1.getId().equals(id)) {
                return timeSlot1;
            }
            if (timeSlot2.getId().equals(id)) {
                return timeSlot2;
            }
            if (timeSlot3.getId().equals(id)) {
                return timeSlot3;
            }
            if (timeSlot4.getId().equals(id)) {
                return timeSlot4;
            }

            throw new TimeSlotNotFoundException();
        });

        when(timeSlotAdapter.getTimeSlotsByEvent(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (event1.getId().equals(id)) {
                return List.of(timeSlot1, timeSlot2);
            }
            if (event2.getId().equals(id)) {
                return List.of(timeSlot3, timeSlot4);
            }

            throw new EventNotFoundException();
        });

        when(eventAdapter.getEventByTimeSlot(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (timeSlot1.getId().equals(id)) {
                return event1;
            }
            if (timeSlot2.getId().equals(id)) {
                return event1;
            }
            if (timeSlot3.getId().equals(id)) {
                return event2;
            }
            if (timeSlot4.getId().equals(id)) {
                return event2;
            }

            throw new TimeSlotNotFoundException();
        });
    }

    private void createEvents() {
        event1 = new Event();
        event1.setId(idSequence++);
        event1.setEnabled(true);
        event1.setDescription(randomStringGenerator());
        event1.setCreationTime(nextInstant());
        event1.setModificationTime(nextInstant());
        event1.setTitle(randomStringGenerator());
        event1.setPlayersFee(integerSequence++);
        event1.setLocations(Set.of(location1, location2, location3));
        event1.setTeams(Set.of(team1, team2, team3));

        event2 = new Event();
        event2.setId(idSequence++);
        event2.setEnabled(true);
        event2.setDescription(randomStringGenerator());
        event2.setCreationTime(nextInstant());
        event2.setModificationTime(nextInstant());
        event2.setTitle(randomStringGenerator());
        event2.setPlayersFee(integerSequence++);
        event2.setLocations(Set.of(location3, location4));
        event2.setTeams(Set.of(team3, team4));

        when(eventAdapter.getEvents()).thenReturn(List.of(event1, event2));
        when(eventAdapter.getEventById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (event1.getId().equals(id)) {
                return event1;
            }
            if (event2.getId().equals(id)) {
                return event2;
            }

            throw new EventNotFoundException();
        });

        when(eventAdapter.getLocations(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (event1.getId().equals(id)) {
                return List.of(location1, location2, location3);
            }
            if (event2.getId().equals(id)) {
                return List.of(location3, location4);
            }

            throw new EventNotFoundException();
        });

        when(eventAdapter.getTeams(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (event1.getId().equals(id)) {
                return List.of(team1, team2, team3);
            }
            if (event2.getId().equals(id)) {
                return List.of(team3, team4);
            }

            throw new EventNotFoundException();
        });

        when(eventAdapter.getEventsByLocation(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (location1.getId().equals(id)) {
                return List.of(event1);
            }
            if (location2.getId().equals(id)) {
                return List.of(event1);
            }
            if (location3.getId().equals(id)) {
                return List.of(event1, event2);
            }
            if (location4.getId().equals(id)) {
                return List.of(event2);
            }

            throw new LocationNotFoundException();
        });

        when(eventAdapter.getEventsByTeam(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (team1.getId().equals(id)) {
                return List.of(event1);
            }
            if (team2.getId().equals(id)) {
                return List.of(event1);
            }
            if (team3.getId().equals(id)) {
                return List.of(event1, event2);
            }
            if (team4.getId().equals(id)) {
                return List.of(event2);
            }

            throw new TeamNotFoundException();
        });

        when(eventAdapter.getEventsByProduct(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (product1.getId().equals(id)) {
                return List.of(event1, event2);
            }
            if (product2.getId().equals(id)) {
                return List.of(event1);
            }
            if (product3.getId().equals(id)) {
                return List.of(event1, event2);
            }
            if (product4.getId().equals(id)) {
                return List.of(event1);
            }

            throw new ProductNotFoundException();
        });

        when(productAdapter.getProductsByEvent(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (event1.getId().equals(id)) {
                return Set.of(product1, product2, product3, product4);
            }
            if (event2.getId().equals(id)) {
                return Set.of(product3, product4);
            }

            throw new EventNotFoundException();
        });
    }

    private void createPlayer() {
        player1 = new Player();
        player1.setId(idSequence++);
        player1.setName(randomStringGenerator());
        player1.setCreationTime(nextInstant());
        player1.setModificationTime(nextInstant());
        player1.setEnabled(true);
        player1.setPayTime(nextInstant());
        player1.setPaid(false);
        player1.setWithPlayersFee(false);
        player1.setComment("");
        player1.setPaidPlayersFee(0);
        player1.setTeam(team1);
        player1.setEvent(event1);
        player1.setPreferences(Map.of(randomStringGenerator(), randomStringGenerator()));

        player2 = new Player();
        player2.setId(idSequence++);
        player2.setName(randomStringGenerator());
        player2.setCreationTime(nextInstant());
        player2.setModificationTime(nextInstant());
        player2.setEnabled(true);
        player2.setPayTime(nextInstant());
        player2.setPaid(true);
        player2.setWithPlayersFee(true);
        player2.setComment(randomStringGenerator());
        player2.setPaidPlayersFee(integerSequence);
        player2.setTeam(team1);
        player2.setEvent(event1);
        player2.setPreferences(Map.of(randomStringGenerator(), randomStringGenerator()));

        player3 = new Player();
        player3.setId(idSequence++);
        player3.setName(randomStringGenerator());
        player3.setCreationTime(nextInstant());
        player3.setModificationTime(nextInstant());
        player3.setEnabled(true);
        player3.setPayTime(nextInstant());
        player3.setPaid(false);
        player3.setWithPlayersFee(false);
        player3.setComment("");
        player3.setPaidPlayersFee(0);
        player3.setTeam(team3);
        player3.setEvent(event1);
        player3.setPreferences(Map.of(randomStringGenerator(), randomStringGenerator()));

        player4 = new Player();
        player4.setId(idSequence++);
        player4.setName(randomStringGenerator());
        player4.setCreationTime(nextInstant());
        player4.setModificationTime(nextInstant());
        player4.setEnabled(true);
        player4.setPayTime(nextInstant());
        player4.setPaid(false);
        player4.setWithPlayersFee(false);
        player4.setComment("");
        player4.setPaidPlayersFee(0);
        player4.setTeam(team4);
        player4.setEvent(event2);
        player4.setPreferences(Map.of(randomStringGenerator(), randomStringGenerator()));

        when(playerAdapter.getPlayers()).thenReturn(List.of(player1, player2, player3, player4));
        when(playerAdapter.getPlayerById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (player1.getId().equals(id)) {
                return player1;
            }
            if (player2.getId().equals(id)) {
                return player2;
            }
            if (player3.getId().equals(id)) {
                return player3;
            }
            if (player4.getId().equals(id)) {
                return player4;
            }

            throw new PlayerNotFoundException();
        });

        when(playerAdapter.existsById(anyLong())).thenReturn(Boolean.FALSE);
        when(playerAdapter.existsById(player1.getId())).thenReturn(Boolean.TRUE);
        when(playerAdapter.existsById(player2.getId())).thenReturn(Boolean.TRUE);
        when(playerAdapter.existsById(player3.getId())).thenReturn(Boolean.TRUE);
        when(playerAdapter.existsById(player4.getId())).thenReturn(Boolean.TRUE);

        when(playerAdapter.getPlayersByEvent(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (event1.getId().equals(id)) {
                return List.of(player1, player2, player3);
            }
            if (event2.getId().equals(id)) {
                return List.of(player4);
            }

            throw new EventNotFoundException();
        });

        when(playerAdapter.getPlayersByTeam(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (team1.getId().equals(id)) {
                return List.of(player1, player2);
            }
            if (team3.getId().equals(id)) {
                return List.of(player3);
            }
            if (team4.getId().equals(id)) {
                return List.of(player4);
            }

            throw new TeamNotFoundException();
        });

        when(playerAdapter.getPlayersByTeamAndEvent(anyLong(), anyLong())).thenAnswer(i -> {
            var teamId = i.getArgument(0, Long.class);
            var eventId = i.getArgument(1, Long.class);
            if (team1.getId().equals(teamId) && event1.getId().equals(eventId)) {
                return List.of(player1, player2);
            }
            if (team3.getId().equals(teamId) && event1.getId().equals(eventId)) {
                return List.of(player3);
            }
            if (team4.getId().equals(teamId) && event2.getId().equals(eventId)) {
                return List.of(player4);
            }

            throwIfTeamNotExists(teamId);

            throwIfEventNotExists(eventId);

            return Collections.emptyList();
        });

        when(playerAdapter.getPlayersByTeamsAndEvent(anyList(), anyLong())).thenAnswer(i -> {
            List<Long> teams = i.getArgument(0, List.class);
            var eventId = i.getArgument(1, Long.class);
            if (teams.equals(List.of(team1.getId(), team3.getId())) && event1.getId().equals(eventId)) {
                return List.of(player1, player2, player3);
            }
            if (teams.equals(List.of(team1.getId())) && event1.getId().equals(eventId)) {
                return List.of(player1, player2);
            }
            if (teams.equals(List.of(team3.getId())) && event1.getId().equals(eventId)) {
                return List.of(player3);
            }
            if (teams.equals(List.of(team4.getId())) && event2.getId().equals(eventId)) {
                return List.of(player4);
            }

            throwIfEventNotExists(eventId);

            return Collections.emptyList();
        });
    }

    private void createTallies() {
        tally1 = new Tally();
        tally1.setId(idSequence++);
        tally1.setTime(nextInstant());
        tally1.setCancelled(false);
        tally1.setPlayer(player1);
        tally1.setProduct(product1);
        tally1.setLocation(location1);

        tally2 = new Tally();
        tally2.setId(idSequence++);
        tally2.setTime(nextInstant());
        tally2.setCancelled(false);
        tally2.setPlayer(player1);
        tally2.setProduct(product2);
        tally2.setLocation(location1);

        tally3 = new Tally();
        tally3.setId(idSequence++);
        tally3.setTime(nextInstant());
        tally3.setCancelled(false);
        tally3.setPlayer(player2);
        tally3.setProduct(product3);
        tally3.setLocation(location2);

        tally4 = new Tally();
        tally4.setId(idSequence++);
        tally4.setTime(nextInstant());
        tally4.setCancelled(false);
        tally4.setPlayer(player2);
        tally4.setProduct(product4);
        tally4.setLocation(location2);

        tally5 = new Tally();
        tally5.setId(idSequence++);
        tally5.setTime(nextInstant());
        tally5.setCancelled(false);
        tally5.setPlayer(player3);
        tally5.setProduct(product3);
        tally5.setLocation(location2);

        tally6 = new Tally();
        tally6.setId(idSequence++);
        tally6.setTime(nextInstant());
        tally6.setCancelled(false);
        tally6.setPlayer(player3);
        tally6.setProduct(product3);
        tally6.setLocation(location3);

        tally7 = new Tally();
        tally7.setId(idSequence++);
        tally7.setTime(nextInstant());
        tally7.setCancelled(false);
        tally7.setPlayer(player4);
        tally7.setProduct(product1);
        tally7.setLocation(location3);

        tally8 = new Tally();
        tally8.setId(idSequence++);
        tally8.setTime(nextInstant());
        tally8.setCancelled(false);
        tally8.setPlayer(player4);
        tally8.setProduct(product3);
        tally8.setLocation(location3);

        when(tallyAdapter.getTallies()).thenReturn(List.of(tally1, tally2, tally3, tally4, tally5, tally6, tally7, tally8));
        when(tallyAdapter.getTallyById(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (tally1.getId().equals(id)) {
                return tally1;
            }
            if (tally2.getId().equals(id)) {
                return tally2;
            }
            if (tally3.getId().equals(id)) {
                return tally3;
            }
            if (tally4.getId().equals(id)) {
                return tally4;
            }
            if (tally5.getId().equals(id)) {
                return tally5;
            }
            if (tally6.getId().equals(id)) {
                return tally6;
            }
            if (tally7.getId().equals(id)) {
                return tally7;
            }
            if (tally8.getId().equals(id)) {
                return tally8;
            }

            throw new TallyNotFoundException();
        });

        when(tallyAdapter.getTalliesByEvent(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (event1.getId().equals(id)) {
                return List.of(tally1, tally2, tally3, tally4, tally5, tally6);
            }
            if (event2.getId().equals(id)) {
                return List.of(tally7, tally8);
            }

            throw new EventNotFoundException();
        });

        when(tallyAdapter.getTalliesByTimeSlot(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (timeSlot1.getId().equals(id) || timeSlot2.getId().equals(id)) {
                return List.of(tally1, tally2, tally3, tally4, tally5, tally6);
            }
            if (timeSlot3.getId().equals(id) || timeSlot4.getId().equals(id)) {
                return List.of(tally7, tally8);
            }

            throw new TimeSlotNotFoundException();
        });

        when(tallyAdapter.getTalliesByLocation(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (location1.getId().equals(id)) {
                return List.of(tally1, tally2);
            }
            if (location2.getId().equals(id)) {
                return List.of(tally3, tally4, tally5);
            }
            if (location3.getId().equals(id)) {
                return List.of(tally6, tally7, tally8);
            }
            if (location4.getId().equals(id)) {
                return Collections.emptyList();
            }

            throw new LocationNotFoundException();
        });

        when(tallyAdapter.getTalliesByProduct(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (product1.getId().equals(id)) {
                return List.of(tally1, tally7);
            }
            if (product2.getId().equals(id)) {
                return List.of(tally2);
            }
            if (product3.getId().equals(id)) {
                return List.of(tally3, tally5, tally6, tally8);
            }
            if (product4.getId().equals(id)) {
                return List.of(tally4);
            }

            throw new ProductNotFoundException();
        });

        when(tallyAdapter.getTalliesByTeam(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (team1.getId().equals(id)) {
                return List.of(tally1, tally2, tally3, tally4);
            }
            if (team2.getId().equals(id)) {
                return Collections.emptyList();
            }
            if (team3.getId().equals(id)) {
                return List.of(tally5, tally6);
            }
            if (team4.getId().equals(id)) {
                return List.of(tally7, tally8);
            }

            throw new TeamNotFoundException();
        });

        when(tallyAdapter.getTalliesByPlayer(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (player1.getId().equals(id)) {
                return List.of(tally1, tally2);
            }
            if (player2.getId().equals(id)) {
                return List.of(tally3, tally4);
            }
            if (player3.getId().equals(id)) {
                return List.of(tally5, tally6);
            }
            if (player4.getId().equals(id)) {
                return List.of(tally7, tally8);
            }

            throw new PlayerNotFoundException();
        });

        when(tallyAdapter.getTalliesByLocationAndEvent(anyLong(), anyLong())).thenAnswer(i -> {
            var locationId = i.getArgument(0, Long.class);
            var eventId = i.getArgument(1, Long.class);
            if (location1.getId().equals(locationId) && event1.getId().equals(eventId)) {
                return List.of(tally1, tally2);
            }
            if (location2.getId().equals(locationId) && event1.getId().equals(eventId)) {
                return List.of(tally3, tally4, tally5);
            }
            if (location3.getId().equals(locationId) && event1.getId().equals(eventId)) {
                return List.of(tally6);
            }
            if (location3.getId().equals(locationId) && event2.getId().equals(eventId)) {
                return List.of(tally7, tally8);
            }

            throwIfLocationNotExists(locationId);

            throwIfEventNotExists(eventId);

            return Collections.emptyList();
        });

        when(tallyAdapter.getTalliesByLocationAndPlayer(anyLong(), anyLong())).thenAnswer(i -> {
            var locationId = i.getArgument(0, Long.class);
            var playerId = i.getArgument(1, Long.class);
            if (location1.getId().equals(locationId) && player1.getId().equals(playerId)) {
                return List.of(tally1, tally2);
            }
            if (location2.getId().equals(locationId) && player2.getId().equals(playerId)) {
                return List.of(tally3, tally4);
            }
            if (location2.getId().equals(locationId) && player3.getId().equals(playerId)) {
                return List.of(tally5);
            }
            if (location3.getId().equals(locationId) && player3.getId().equals(playerId)) {
                return List.of(tally6);
            }
            if (location3.getId().equals(locationId) && player4.getId().equals(playerId)) {
                return List.of(tally7, tally8);
            }

            throwIfLocationNotExists(locationId);

            throwIfPlayerNotExists(playerId);

            return Collections.emptyList();
        });

        when(tallyAdapter.getTalliesByProductAndEvent(anyLong(), anyLong())).thenAnswer(i -> {
            var productId = i.getArgument(0, Long.class);
            var eventId = i.getArgument(1, Long.class);
            if (product1.getId().equals(productId) && event1.getId().equals(eventId)) {
                return List.of(tally1);
            }
            if (product2.getId().equals(productId) && event2.getId().equals(eventId)) {
                return List.of(tally7);
            }
            if (product2.getId().equals(productId) && event1.getId().equals(eventId)) {
                return List.of(tally2);
            }
            if (product3.getId().equals(productId) && event1.getId().equals(eventId)) {
                return List.of(tally3, tally5, tally6);
            }
            if (product3.getId().equals(productId) && event2.getId().equals(eventId)) {
                return List.of(tally8);
            }
            if (product4.getId().equals(productId) && event1.getId().equals(eventId)) {
                return List.of(tally4);
            }

            throwIfProductNotExists(productId);

            throwIfEventNotExists(eventId);

            return Collections.emptyList();
        });

        when(tallyAdapter.getTalliesByProductAndPlayer(anyLong(), anyLong())).thenAnswer(i -> {
            var productId = i.getArgument(0, Long.class);
            var playerId = i.getArgument(1, Long.class);
            if (product1.getId().equals(productId) && player1.getId().equals(playerId)) {
                return List.of(tally1);
            }
            if (product2.getId().equals(productId) && player1.getId().equals(playerId)) {
                return List.of(tally2);
            }
            if (product3.getId().equals(productId) && player2.getId().equals(playerId)) {
                return List.of(tally3);
            }
            if (product4.getId().equals(productId) && player2.getId().equals(playerId)) {
                return List.of(tally4);
            }
            if (product3.getId().equals(productId) && player3.getId().equals(playerId)) {
                return List.of(tally5, tally6);
            }
            if (product3.getId().equals(productId) && player4.getId().equals(playerId)) {
                return List.of(tally8);
            }

            throwIfProductNotExists(productId);

            throwIfPlayerNotExists(playerId);

            return Collections.emptyList();
        });

        when(tallyAdapter.getTalliesByTeamAndEvent(anyLong(), anyLong())).thenAnswer(i -> {
            var teamId = i.getArgument(0, Long.class);
            var eventId = i.getArgument(1, Long.class);
            if (team1.getId().equals(teamId) && event1.getId().equals(eventId)) {
                return List.of(tally1, tally2, tally3, tally4);
            }
            if (team3.getId().equals(teamId) && event1.getId().equals(eventId)) {
                return List.of(tally5, tally6);
            }
            if (team4.getId().equals(teamId) && event2.getId().equals(eventId)) {
                return List.of(tally7, tally8);
            }

            throwIfTeamNotExists(teamId);

            throwIfEventNotExists(eventId);

            return Collections.emptyList();
        });

        when(eventAdapter.getNumberOfTalliesByTeamInEventForStatistics(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (event1.getId().equals(id)) {
                return Map.of(product1.getId(), Map.of(
                        team1.getId(), 1L,
                        team2.getId(), 0L,
                        team3.getId(), 0L
                ), product2.getId(), Map.of(
                        team1.getId(), 1L,
                        team2.getId(), 0L,
                        team3.getId(), 0L
                ), product3.getId(), Map.of(
                        team1.getId(), 1L,
                        team2.getId(), 0L,
                        team3.getId(), 2L
                ), product4.getId(), Map.of(
                        team1.getId(), 1L,
                        team2.getId(), 0L,
                        team3.getId(), 0L
                ));
            }
            if (event2.getId().equals(id)) {
                return Map.of(product1.getId(), Map.of(
                        team3.getId(), 0L,
                        team4.getId(), 1L
                ), product3.getId(), Map.of(
                        team3.getId(), 0L,
                        team4.getId(), 1L
                ));
            }

            throw new EventNotFoundException();
        });

        when(timeSlotAdapter.getNumberOfTalliesByTeamInTimeSlotForStatistics(anyLong())).thenAnswer(i -> {
            var id = i.getArgument(0, Long.class);
            if (timeSlot1.getId().equals(id) || timeSlot2.getId().equals(id)) {
                return Map.of(product1.getId(), Map.of(
                        team1.getId(), 1L,
                        team2.getId(), 0L,
                        team3.getId(), 0L
                ), product2.getId(), Map.of(
                        team1.getId(), 1L,
                        team2.getId(), 0L,
                        team3.getId(), 0L
                ), product3.getId(), Map.of(
                        team1.getId(), 1L,
                        team2.getId(), 0L,
                        team3.getId(), 2L
                ), product4.getId(), Map.of(
                        team1.getId(), 1L,
                        team2.getId(), 0L,
                        team3.getId(), 0L
                ));
            }
            if (timeSlot3.getId().equals(id) || timeSlot4.getId().equals(id)) {
                return Map.of(product1.getId(), Map.of(
                        team3.getId(), 0L,
                        team4.getId(), 1L
                ), product3.getId(), Map.of(
                        team3.getId(), 0L,
                        team4.getId(), 1L
                ));
            }

            throw new TimeSlotNotFoundException();
        });
    }

    private void throwIfLocationNotExists(Long teamId) {
        if (!location1.getId().equals(teamId) && !location2.getId().equals(teamId) && !location3.getId().equals(teamId) && !location4.getId().equals(teamId)) {
            throw new LocationNotFoundException();
        }
    }

    private void throwIfPlayerNotExists(Long teamId) {
        if (!player1.getId().equals(teamId) && !player2.getId().equals(teamId) && !player3.getId().equals(teamId) && !player4.getId().equals(teamId)) {
            throw new PlayerNotFoundException();
        }
    }

    private void throwIfProductNotExists(Long teamId) {
        if (!product1.getId().equals(teamId) && !product2.getId().equals(teamId) && !product3.getId().equals(teamId) && !product4.getId().equals(teamId)) {
            throw new ProductNotFoundException();
        }
    }

    private void throwIfTeamNotExists(Long teamId) {
        if (!team1.getId().equals(teamId) && !team2.getId().equals(teamId) && !team3.getId().equals(teamId) && !team4.getId().equals(teamId)) {
            throw new TeamNotFoundException();
        }
    }

    private void throwIfEventNotExists(Long eventId) {
        if (!event1.getId().equals(eventId) && !event2.getId().equals(eventId)) {
            throw new EventNotFoundException();
        }
    }

    String randomStringGenerator() {
        String ALPHANUMERIC_VALUEPOOL = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < 10; ++i) {
            int index = r.nextInt(ALPHANUMERIC_VALUEPOOL.length());
            sb.append(ALPHANUMERIC_VALUEPOOL.charAt(index));
        }

        return sb.toString();
    }

    Instant nextInstant() {
        instant = instant.plus(5L, ChronoUnit.SECONDS);
        return instant;
    }
}
