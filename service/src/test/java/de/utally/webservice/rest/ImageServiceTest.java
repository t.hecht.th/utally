package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.exception.UtallyRestException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.List;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import({TestImageHandler.class, WebSecurityConfiguration.class, TestMapperConfig.class})
@WebMvcTest(value = ImageService.class)
class ImageServiceTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    RestImageHandler restImageHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() {
        reset(restImageHandler);
    }

    @Test
    @WithUser
    void getImageFilenamesAsUser() throws Exception {
        performGetAllFilenames();
    }

    @Test
    @WithOperator
    void getImageFilenamesAsOperator() throws Exception {
        performGetAllFilenames();
    }

    @Test
    @WithAdmin
    void getImageFilenamesAsAdmin() throws Exception {
        performGetAllFilenames();
    }

    private void performGetAllFilenames() throws Exception {
        when(restImageHandler.getAllFilenames()).thenReturn(List.of("filename1.txt", "filename2.txt"));

        mockMvc.perform(
                        get("/api/images/").accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").value("filename1.txt"))
                .andExpect(jsonPath("$[1]").value("filename2.txt"));
    }

    @Test
    void getImageFilenamesAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/images/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getImageFilenamesIOException() throws Exception {
        when(restImageHandler.getAllFilenames()).thenThrow(new IOException());

        var actions = mockMvc.perform(
                get("/api/images/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.INTE0001);
    }

    @Test
    @WithAdmin
    void getImageAsAdmin() throws Exception {
        performGetImage();
    }

    @Test
    @WithOperator
    void getImageAsOperator() throws Exception {
        performGetImage();
    }

    @Test
    @WithUser
    void getImageAsUser() throws Exception {
        performGetImage();
    }

    private void performGetImage() throws Exception {
        String filename = "dummyimage.txt";
        when(restImageHandler.getFileResource(any()))
                .thenReturn(new FileSystemResource("src/test/resources/dummyimage.txt"));

        mockMvc.perform(
                        get("/api/images/" + filename)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("This is the content of a dummy image."))
                .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\""));

        verify(restImageHandler).getFileResource(filename);
    }

    @Test
    void getImageAsAnonymous() throws Exception {
        String filename = "dummyimage.txt";
        var actions = mockMvc.perform(
                get("/api/images/" + filename)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithUser
    void getImageNotFound() throws Exception {
        String filename = "dummyimage.txt";
        when(restImageHandler.getFileResource(any())).thenThrow(new UtallyRestException(ErrorCode.NOTF0008));
        var actions = mockMvc.perform(
                get("/api/images/" + filename)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0008);
    }

    @Test
    @WithAdmin
    void handleFileUploadAsAdmin() throws Exception {
        MockMultipartFile multipartFile = createMockMultipartFile();
        mockMvc.perform(
                        multipart("/api/images/").file(multipartFile)
                ).andDo(print())
                .andExpect(status().isNoContent());

        verify(restImageHandler).storeImage(any());
    }

    @Test
    @WithOperator
    void handleFileUploadAsOperator() throws Exception {
        MockMultipartFile multipartFile = createMockMultipartFile();
        var actions = mockMvc.perform(
                        multipart("/api/images/").file(multipartFile)
                ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithOperator
    void handleFileUploadAsUser() throws Exception {
        MockMultipartFile multipartFile = createMockMultipartFile();
        var actions = mockMvc.perform(
                multipart("/api/images/").file(multipartFile)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void handleFileUploadAsAnonymous() throws Exception {
        MockMultipartFile multipartFile = createMockMultipartFile();
        var actions = mockMvc.perform(
                multipart("/api/images/").file(multipartFile)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void handleFileUploadIOException() throws Exception {
        doThrow(new IOException()).when(restImageHandler).storeImage(any());
        MockMultipartFile multipartFile = createMockMultipartFile();
        var actions = mockMvc.perform(
                multipart("/api/images/").file(multipartFile)
        ).andDo(print());

        expectError(actions, ErrorCode.INTE0001);
    }

    private MockMultipartFile createMockMultipartFile() {
        return new MockMultipartFile("file", "test.txt",
                "text/plain", "Image Service Test".getBytes());
    }

    @Test
    @WithAdmin
    void deleteImageAsAdmin() throws Exception {
        String filename = "filename";
        mockMvc.perform(
                        delete("/api/images/" + filename).accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isNoContent());

        verify(restImageHandler).deleteImage(filename);
    }

    @Test
    @WithUser
    void deleteImageAsUser() throws Exception {
        String filename = "filename";

        var actions = mockMvc.perform(
                delete("/api/images/" + filename)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithOperator
    void deleteImageAsOperator() throws Exception {
        String filename = "filename";

        var actions = mockMvc.perform(
                delete("/api/images/" + filename)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteImageAsAnonymous() throws Exception {
        String filename = "filename";

        var actions = mockMvc.perform(
                delete("/api/images/" + filename)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void deleteImageIOException() throws Exception {
        String filename = "filename";
        doThrow(new IOException()).when(restImageHandler).deleteImage(anyString());

        var actions = mockMvc.perform(
                delete("/api/images/" + filename)
        ).andDo(print());

        expectError(actions, ErrorCode.INTE0001);

        verify(restImageHandler).deleteImage(filename);
    }
}
