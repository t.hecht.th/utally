package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import de.utally.data.exception.EventNotFoundException;
import de.utally.data.exception.LocationNotFoundException;
import de.utally.data.exception.TeamNotFoundException;
import de.utally.data.model.Event;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.model.EventDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectError;
import static de.utally.webservice.rest.UtallyServiceErrorHandler.expectInvalidArgumentError;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Import({TestAdapters.class, WebSecurityConfiguration.class, TestMapperConfig.class})
@WebMvcTest(value = EventService.class)
class EventServiceTest extends UtallyServiceTest {

    @BeforeEach
    void setUp() {
        super.setUp();
    }

    @AfterEach
    void tearDown() {
        super.tearDown();
    }

    @Test
    @WithAdmin
    void getEventsAsAdmin() throws Exception {
        performGetAllEvents();
    }

    @Test
    @WithOperator
    void getEventsAsOperator() throws Exception {
        performGetAllEvents();
    }

    @Test
    @WithUser
    void getEventsAsUser() throws Exception {
        performGetAllEvents();
    }

    private void performGetAllEvents() throws Exception {
        mockMvc.perform(
                get("/api/events/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(event1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$[0].title").value(event1.getTitle()))
                .andExpect(jsonPath("$[0].description").value(event1.getDescription()))
                .andExpect(jsonPath("$[0].playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$[0].locationIds").exists())
                .andExpect(jsonPath("$[0].locationIds").isArray())
                .andExpect(jsonPath("$[0].teamIds").exists())
                .andExpect(jsonPath("$[0].teamIds").isArray())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value(event2.getId()))
                .andExpect(jsonPath("$[1].creationTime").value(event2.getCreationTime().toString()))
                .andExpect(jsonPath("$[1].modificationTime").value(event2.getModificationTime().toString()))
                .andExpect(jsonPath("$[1].enabled").value(event2.getEnabled()))
                .andExpect(jsonPath("$[1].title").value(event2.getTitle()))
                .andExpect(jsonPath("$[1].description").value(event2.getDescription()))
                .andExpect(jsonPath("$[1].playersFee").value(event2.getPlayersFee()))
                .andExpect(jsonPath("$[1].locationIds").exists())
                .andExpect(jsonPath("$[1].locationIds").isArray())
                .andExpect(jsonPath("$[1].teamIds").exists())
                .andExpect(jsonPath("$[1].teamIds").isArray());
    }

    @Test
    void getEventsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/").accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getEventsByTeamAsAdmin() throws Exception {
        performGetAllEventsByTeam();
    }

    @Test
    @WithOperator
    void getEventsByTeamAsOperator() throws Exception {
        performGetAllEventsByTeam();
    }

    @Test
    @WithUser
    void getEventsByTeamAsUser() throws Exception {
        performGetAllEventsByTeam();
    }

    private void performGetAllEventsByTeam() throws Exception {
        mockMvc.perform(
                get("/api/events/team/{id}", team1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(event1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$[0].title").value(event1.getTitle()))
                .andExpect(jsonPath("$[0].description").value(event1.getDescription()))
                .andExpect(jsonPath("$[0].playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$[0].locationIds").exists())
                .andExpect(jsonPath("$[0].locationIds").isArray())
                .andExpect(jsonPath("$[0].teamIds").exists())
                .andExpect(jsonPath("$[0].teamIds").isArray());
    }

    @Test
    void getEventsByTeamAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/team/{id}", team1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getEventsByTeamNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/team/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);
    }

    @Test
    @WithAdmin
    void getEventsByLocationAsAdmin() throws Exception {
        performGetAllEventsByLocation();
    }

    @Test
    @WithOperator
    void getEventsByLocationAsOperator() throws Exception {
        performGetAllEventsByLocation();
    }

    @Test
    @WithUser
    void getEventsByLocationAsUser() throws Exception {
        performGetAllEventsByLocation();
    }

    private void performGetAllEventsByLocation() throws Exception {
        mockMvc.perform(
                get("/api/events/location/{id}", location1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(event1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$[0].title").value(event1.getTitle()))
                .andExpect(jsonPath("$[0].description").value(event1.getDescription()))
                .andExpect(jsonPath("$[0].playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$[0].locationIds").exists())
                .andExpect(jsonPath("$[0].locationIds").isArray())
                .andExpect(jsonPath("$[0].teamIds").exists())
                .andExpect(jsonPath("$[0].teamIds").isArray());
    }

    @Test
    void getEventsByLocationAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/location/{id}", location1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getEventsByLocationNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/location/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void getEventsByProductAsAdmin() throws Exception {
        performGetAllEventsByProduct();
    }

    @Test
    @WithOperator
    void getEventsByProductAsOperator() throws Exception {
        performGetAllEventsByProduct();
    }

    @Test
    @WithUser
    void getEventsByProductAsUser() throws Exception {
        performGetAllEventsByProduct();
    }

    private void performGetAllEventsByProduct() throws Exception {
        mockMvc.perform(
                get("/api/events/product/{id}", product1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value(event1.getId()))
                .andExpect(jsonPath("$[0].creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$[0].modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$[0].enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$[0].title").value(event1.getTitle()))
                .andExpect(jsonPath("$[0].description").value(event1.getDescription()))
                .andExpect(jsonPath("$[0].playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$[0].locationIds").exists())
                .andExpect(jsonPath("$[0].locationIds").isArray())
                .andExpect(jsonPath("$[0].teamIds").exists())
                .andExpect(jsonPath("$[0].teamIds").isArray());
    }

    @Test
    void getEventsByProductAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/product/{id}", product1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getEventsByProductNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/product/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0003);
    }

    @Test
    @WithAdmin
    void getEventAsAdmin() throws Exception {
        performGetEvent();
    }

    @Test
    @WithOperator
    void getEventAsOperator() throws Exception {
        performGetEvent();
    }

    @Test
    @WithUser
    void getEventAsUser() throws Exception {
        performGetEvent();
    }

    private void performGetEvent() throws Exception {
        mockMvc.perform(
                get("/api/events/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(event1.getId()))
                .andExpect(jsonPath("$.creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$.title").value(event1.getTitle()))
                .andExpect(jsonPath("$.description").value(event1.getDescription()))
                .andExpect(jsonPath("$.playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$.locationIds").exists())
                .andExpect(jsonPath("$.locationIds").isArray())
                .andExpect(jsonPath("$.teamIds").exists())
                .andExpect(jsonPath("$.teamIds").isArray());
    }

    @Test
    void getEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/{id}", event1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getEventNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void getEventByTimeSlotAsAdmin() throws Exception {
        performGetEventByTimeSlot();
    }

    @Test
    @WithOperator
    void getEventByTimeSlotAsOperator() throws Exception {
        performGetEventByTimeSlot();
    }

    @Test
    @WithUser
    void getEventByTimeSlotAsUser() throws Exception {
        performGetEventByTimeSlot();
    }

    private void performGetEventByTimeSlot() throws Exception {
        mockMvc.perform(
                get("/api/events/timeslot/{id}", timeSlot1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(event1.getId()))
                .andExpect(jsonPath("$.creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$.title").value(event1.getTitle()))
                .andExpect(jsonPath("$.description").value(event1.getDescription()))
                .andExpect(jsonPath("$.playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$.locationIds").exists())
                .andExpect(jsonPath("$.locationIds").isArray())
                .andExpect(jsonPath("$.teamIds").exists())
                .andExpect(jsonPath("$.teamIds").isArray());
    }

    @Test
    void getEventByTimeSlotAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/timeslot/{id}", timeSlot1.getId()).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getEventByTimeSlotNotFound() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/timeslot/{id}", idSequence++).accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0006);
    }

    @Test
    @WithAdmin
    void postEventAsAdmin() throws Exception {
        var r = createEventResource(randomStringGenerator());

        injectPostSaveCalls(r);

        performPostEvent(r);
    }

    private void performPostEvent(EventDTO r) throws Exception {
        mockMvc.perform(
                post("/api/events/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.creationTime").exists())
                .andExpect(jsonPath("$.modificationTime").exists())
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.title").value(r.title()))
                .andExpect(jsonPath("$.description").value(r.description()))
                .andExpect(jsonPath("$.playersFee").value(r.playersFee()));

        verify(eventAdapter).insertEvent(anyString(), anyString(), anyBoolean(), anyInt());
    }

    private void injectPostSaveCalls(EventDTO r) {
        doAnswer(i -> {
            var event = new Event();
            event.setId(idSequence++);
            event.setTitle(i.getArgument(0, String.class));
            event.setDescription(i.getArgument(1, String.class));
            event.setEnabled(i.getArgument(2, Boolean.class));
            event.setPlayersFee(i.getArgument(3, Integer.class));
            event.setCreationTime(nextInstant());
            event.setModificationTime(nextInstant());
            assertAll(
                    () -> assertEquals(r.description(), event.getDescription()),
                    () -> assertEquals(r.title(), event.getTitle()),
                    () -> assertEquals(r.enabled(), event.getEnabled()),
                    () -> assertEquals(r.playersFee(), event.getPlayersFee())
            );
            return event;
        }).when(eventAdapter).insertEvent(anyString(), anyString(), anyBoolean(), anyInt());
    }

    @Test
    @WithOperator
    void postEventAsOperator() throws Exception {
        var r = createEventResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/events/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void postEventAsUser() throws Exception {
        var r = createEventResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/events/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void postEventAsAnonymous() throws Exception {
        var r = createEventResource(randomStringGenerator());

        var actions = mockMvc.perform(
                post("/api/events/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void postEventInvalid() throws Exception {
        var r = createEventResource(null);

        var actions = mockMvc.perform(
                post("/api/events/")
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void putEventAsAdmin() throws Exception {
        var r = createEventResource(randomStringGenerator());

        injectPutSaveCalls(r);

        performPutEvent(r);
    }

    private void performPutEvent(EventDTO r) throws Exception {
        mockMvc.perform(
                put("/api/events/{id}", event1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(event1.getId()))
                .andExpect(jsonPath("$.creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(r.enabled()))
                .andExpect(jsonPath("$.title").value(r.title()))
                .andExpect(jsonPath("$.description").value(r.description()))
                .andExpect(jsonPath("$.playersFee").value(r.playersFee()));

        verify(eventAdapter).updateEvent(any());
    }

    private void injectPutSaveCalls(EventDTO r) {
        doAnswer(i -> {
            var event = i.getArgument(0, Event.class);
            assertAll(
                    () -> assertEquals(r.description(), event.getDescription()),
                    () -> assertEquals(r.title(), event.getTitle()),
                    () -> assertEquals(r.enabled(), event.getEnabled()),
                    () -> assertEquals(r.playersFee(), event.getPlayersFee())
            );
            return null;
        }).when(eventAdapter).updateEvent(any());
    }

    @Test
    @WithOperator
    void putEventAsOperator() throws Exception {
        var r = createEventResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/events/{id}", event1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void putEventAsUser() throws Exception {
        var r = createEventResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/events/{id}", event1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void putEventAsAnonymous() throws Exception {
        var r = createEventResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/events/{id}", event1.getId())
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void putEventNotFound() throws Exception {
        var r = createEventResource(randomStringGenerator());

        var actions = mockMvc.perform(
                put("/api/events/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    @Test
    @WithAdmin
    void putEventInvalid() throws Exception {
        var r = createEventResource(null);

        var actions = mockMvc.perform(
                put("/api/events/{id}", idSequence++)
                        .content(objectMapper.writeValueAsString(r))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectInvalidArgumentError(actions, r);
    }

    @Test
    @WithAdmin
    void deleteEventAsAdmin() throws Exception {
        mockMvc.perform(
                delete("/api/events/{id}", event1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(eventAdapter).removeEvent(event1.getId());
    }

    @Test
    @WithOperator
    void deleteEventAsOperator() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/events/{id}", event1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void deleteEventAsUser() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/events/{id}", event1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void deleteEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                delete("/api/events/{id}", event1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void addLocationToEventAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/events/{id}/addlocation/{locationId}", event1.getId(), location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(event1.getId()))
                .andExpect(jsonPath("$.creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$.title").value(event1.getTitle()))
                .andExpect(jsonPath("$.description").value(event1.getDescription()))
                .andExpect(jsonPath("$.playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$.locationIds").exists())
                .andExpect(jsonPath("$.locationIds").isArray())
                .andExpect(jsonPath("$.teamIds").exists())
                .andExpect(jsonPath("$.teamIds").isArray());

        verify(eventAdapter).addLocation(event1.getId(), location1.getId());
    }

    @Test
    @WithOperator
    void addLocationToEventAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addlocation/{locationId}", event1.getId(), location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void addLocationToEventAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addlocation/{locationId}", event1.getId(), location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void addLocationToEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addlocation/{locationId}", event1.getId(), location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void addLocationToEventEventNotFound() throws Exception {
        doThrow(EventNotFoundException.class).when(eventAdapter).addLocation(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addlocation/{locationId}", idSequence++, location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);

        verify(eventAdapter).addLocation(anyLong(), eq(location1.getId()));
    }

    @Test
    @WithAdmin
    void addLocationToEventLocationNotFound() throws Exception {
        doThrow(LocationNotFoundException.class).when(eventAdapter).addLocation(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addlocation/{locationId}", event1.getId(), idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);

        verify(eventAdapter).addLocation(eq(event1.getId()), anyLong());
    }

    @Test
    @WithAdmin
    void removeLocationFromEventAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/events/{id}/removelocation/{locationId}", event1.getId(), location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(event1.getId()))
                .andExpect(jsonPath("$.creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$.title").value(event1.getTitle()))
                .andExpect(jsonPath("$.description").value(event1.getDescription()))
                .andExpect(jsonPath("$.playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$.locationIds").exists())
                .andExpect(jsonPath("$.locationIds").isArray())
                .andExpect(jsonPath("$.teamIds").exists())
                .andExpect(jsonPath("$.teamIds").isArray());

        verify(eventAdapter).removeLocation(event1.getId(), location1.getId());
    }

    @Test
    @WithOperator
    void removeLocationFromEventAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removelocation/{locationId}", event1.getId(), location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void removeLocationFromEventAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removelocation/{locationId}", event1.getId(), location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void removeLocationFromEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removelocation/{locationId}", event1.getId(), location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void removeLocationFromEventEventNotFound() throws Exception {
        doThrow(EventNotFoundException.class).when(eventAdapter).removeLocation(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removelocation/{locationId}", idSequence++, location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);

        verify(eventAdapter).removeLocation(anyLong(), eq(location1.getId()));
    }

    @Test
    @WithAdmin
    void removeLocationFromEventLocationNotFound() throws Exception {
        doThrow(LocationNotFoundException.class).when(eventAdapter).removeLocation(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removelocation/{locationId}", event1.getId(), idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);

        verify(eventAdapter).removeLocation(eq(event1.getId()), anyLong());
    }

    @Test
    @WithAdmin
    void removeLocationFromEventsAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/events/removelocation/{locationId}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(eventAdapter).removeLocationFromAllEvents(location1.getId());
    }

    @Test
    @WithOperator
    void removeLocationFromEventsAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/removelocation/{locationId}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void removeLocationFromEventsAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/removelocation/{locationId}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void removeLocationFromEventsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/removelocation/{locationId}", location1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void removeLocationFromEventsNotFound() throws Exception {
        doThrow(LocationNotFoundException.class).when(eventAdapter).removeLocationFromAllEvents(anyLong());
        var actions = mockMvc.perform(
                        patch("/api/events/removelocation/{locationId}", idSequence++)
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print());

        expectError(actions, ErrorCode.NOTF0002);
    }

    @Test
    @WithAdmin
    void addTeamToEventAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/events/{id}/addteam/{teamId}", event1.getId(), team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(event1.getId()))
                .andExpect(jsonPath("$.creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$.title").value(event1.getTitle()))
                .andExpect(jsonPath("$.description").value(event1.getDescription()))
                .andExpect(jsonPath("$.playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$.locationIds").exists())
                .andExpect(jsonPath("$.locationIds").isArray())
                .andExpect(jsonPath("$.teamIds").exists())
                .andExpect(jsonPath("$.teamIds").isArray());

        verify(eventAdapter).addTeam(event1.getId(), team1.getId());
    }

    @Test
    @WithOperator
    void addTeamToEventAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addteam/{teamId}", event1.getId(), team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void addTeamToEventAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addteam/{teamId}", event1.getId(), team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void addTeamToEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addteam/{teamId}", event1.getId(), team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void addTeamToEventEventNotFound() throws Exception {
        doThrow(EventNotFoundException.class).when(eventAdapter).addTeam(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addteam/{teamId}", idSequence++, team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);

        verify(eventAdapter).addTeam(anyLong(), eq(team1.getId()));
    }

    @Test
    @WithAdmin
    void addTeamToEventTeamNotFound() throws Exception {
        doThrow(TeamNotFoundException.class).when(eventAdapter).addTeam(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/{id}/addteam/{teamId}", event1.getId(), idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);

        verify(eventAdapter).addTeam(eq(event1.getId()), anyLong());
    }

    @Test
    @WithAdmin
    void removeTeamFromEventAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/events/{id}/removeteam/{teamId}", event1.getId(), team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(event1.getId()))
                .andExpect(jsonPath("$.creationTime").value(event1.getCreationTime().toString()))
                .andExpect(jsonPath("$.modificationTime").value(event1.getModificationTime().toString()))
                .andExpect(jsonPath("$.enabled").value(event1.getEnabled()))
                .andExpect(jsonPath("$.title").value(event1.getTitle()))
                .andExpect(jsonPath("$.description").value(event1.getDescription()))
                .andExpect(jsonPath("$.playersFee").value(event1.getPlayersFee()))
                .andExpect(jsonPath("$.locationIds").exists())
                .andExpect(jsonPath("$.locationIds").isArray())
                .andExpect(jsonPath("$.teamIds").exists())
                .andExpect(jsonPath("$.teamIds").isArray());

        verify(eventAdapter).removeTeam(event1.getId(), team1.getId());
    }

    @Test
    @WithOperator
    void removeTeamFromEventAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removeteam/{teamId}", event1.getId(), team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void removeTeamFromEventAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removeteam/{teamId}", event1.getId(), team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void removeTeamFromEventAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removeteam/{teamId}", event1.getId(), team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void removeTeamFromEventEventNotFound() throws Exception {
        doThrow(EventNotFoundException.class).when(eventAdapter).removeTeam(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removeteam/{teamId}", idSequence++, team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);

        verify(eventAdapter).removeTeam(anyLong(), eq(team1.getId()));
    }

    @Test
    @WithAdmin
    void removeTeamFromEventTeamNotFound() throws Exception {
        doThrow(TeamNotFoundException.class).when(eventAdapter).removeTeam(anyLong(), anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/{id}/removeteam/{teamId}", event1.getId(), idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);

        verify(eventAdapter).removeTeam(eq(event1.getId()), anyLong());
    }

    @Test
    @WithAdmin
    void removeTeamFromEventsAsAdmin() throws Exception {
        mockMvc.perform(
                patch("/api/events/removeteam/{teamId}", team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print())
                .andExpect(status().isNoContent());

        verify(eventAdapter).removeTeamFromAllEvents(team1.getId());
    }

    @Test
    @WithOperator
    void removeTeamFromEventsAsOperator() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/removeteam/{teamId}", team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    @WithUser
    void removeTeamFromEventsAsUser() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/removeteam/{teamId}", team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0002);
    }

    @Test
    void removeTeamFromEventsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                patch("/api/events/removeteam/{teamId}", team1.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void removeTeamFromEventsNotFound() throws Exception {
        doThrow(TeamNotFoundException.class).when(eventAdapter).removeTeamFromAllEvents(anyLong());
        var actions = mockMvc.perform(
                patch("/api/events/removeteam/{teamId}", idSequence++)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0004);
    }

    @Test
    @WithAdmin
    void getStatisticsAsAdmin() throws Exception {
        performGetStatistics();
    }

    @Test
    @WithOperator
    void getStatisticsAsOperator() throws Exception {
        performGetStatistics();
    }

    @Test
    @WithUser
    void getStatisticsAsUser() throws Exception {
        performGetStatistics();
    }

    private void performGetStatistics() throws Exception {
        mockMvc.perform(
                        get("/api/events/statistics/{id}", event2.getId())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())

                .andExpect(jsonPath("$[?(@.productId == " + product1.getId() + " && @.teamId == " + team3.getId() + ")].count").value(0))
                .andExpect(jsonPath("$[?(@.productId == " + product1.getId() + " && @.teamId == " + team4.getId() + ")].count").value(1))
                .andExpect(jsonPath("$[?(@.productId == " + product3.getId() + " && @.teamId == " + team3.getId() + ")].count").value(0))
                .andExpect(jsonPath("$[?(@.productId == " + product3.getId() + " && @.teamId == " + team4.getId() + ")].count").value(1));

        verify(eventAdapter).getNumberOfTalliesByTeamInEventForStatistics(event2.getId());
    }

    @Test
    void getStatisticsAsAnonymous() throws Exception {
        var actions = mockMvc.perform(
                get("/api/events/statistics/{Id}", event1.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.AUTH0001);
    }

    @Test
    @WithAdmin
    void getStatisticsNotFound() throws Exception {
        doThrow(EventNotFoundException.class).when(eventAdapter).getNumberOfTalliesByTeamInEventForStatistics(anyLong());
        var actions = mockMvc.perform(
                get("/api/events/statistics/{Id}", idSequence++)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andDo(print());

        expectError(actions, ErrorCode.NOTF0001);
    }

    private EventDTO createEventResource(String title) {
        return new EventDTO(
                null,
                title,
                140,
                true,
                null,
                null,
                randomStringGenerator(),
                null,
                null
        );
    }
}
