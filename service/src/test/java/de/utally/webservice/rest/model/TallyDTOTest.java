package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class TallyDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    @Test
    void validatePlayerIdNull() {
        var cut = new TallyDTO(5L, null, 6L, Instant.now(), 7L, true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateLocationIdNull() {
        var cut = new TallyDTO(5L, 8L, null, Instant.now(), 7L, true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateProductIdNull() {
        var cut = new TallyDTO(5L, 8L, 6L, Instant.now(), null, true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validate() {
        var cut = new TallyDTO(5L, 8L, 6L, Instant.now(), 7L, true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }
}
