package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PatchPlayerAsAdminDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    @Test
    void validateNameMinValid() {
        var cut = new PatchPlayerAsAdminDTO("a", true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMinInvalid() {
        var cut = new PatchPlayerAsAdminDTO("", true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameMaxValid() {
        var cut = new PatchPlayerAsAdminDTO(StringUtils.repeat('a', 64), true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMaxInvalid() {
        var cut = new PatchPlayerAsAdminDTO(StringUtils.repeat('a', 65), true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameNull() {
        var cut = new PatchPlayerAsAdminDTO(null, true);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEnabledNull() {
        var cut = new PatchPlayerAsAdminDTO("name", null);

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
