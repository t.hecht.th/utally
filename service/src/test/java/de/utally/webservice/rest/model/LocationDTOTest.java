package de.utally.webservice.rest.model;

import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import jakarta.validation.Validation;
import java.time.Instant;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class LocationDTOTest {

    private final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    // Validation Tests
    @Test
    void validateNameMinValid() {
        var cut = new LocationDTO(
                5L,
                "a",
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMinInvalid() {
        var cut = new LocationDTO(
                5L,
                "",
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameMaxValid() {
        var cut = new LocationDTO(
                5L,
                StringUtils.repeat('a', 64),
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateNameMaxInvalid() {
        var cut = new LocationDTO(
                5L,
                StringUtils.repeat('a', 65),
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateNameNull() {
        var cut = new LocationDTO(
                5L,
                null,
                true,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateEnabledNull() {
        var cut = new LocationDTO(
                5L,
                "name",
                null,
                Instant.now(),
                Instant.now(),
                "description",
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxValid() {
        var cut = new LocationDTO(
                5L,
                "name",
                true,
                Instant.now(),
                Instant.now(),
                StringUtils.repeat('a', 2048),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertTrue(result.isEmpty());
    }

    @Test
    void validateDescriptionMaxInvalid() {
        var cut = new LocationDTO(
                5L,
                "name",
                true,
                Instant.now(),
                Instant.now(),
                StringUtils.repeat('a', 2049),
                Set.of(6L)
        );

        var validator = validatorFactory.getValidator();
        var result = validator.validate(cut);

        assertFalse(result.isEmpty());
    }
}
