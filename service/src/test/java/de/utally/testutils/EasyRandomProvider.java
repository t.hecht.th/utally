package de.utally.testutils;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;

import java.util.Currency;

public class EasyRandomProvider {
    public static EasyRandom easyRandom() {
        return new EasyRandom(
                new EasyRandomParameters().randomize(Currency.class, () -> Currency.getInstance("EUR"))
        );
    }
}
