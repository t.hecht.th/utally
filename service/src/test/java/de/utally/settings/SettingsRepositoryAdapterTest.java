package de.utally.settings;

import de.utally.settings.internal.SettingsConstants;
import de.utally.settings.internal.SettingsEntity;
import de.utally.settings.internal.SettingsRepository;
import de.utally.testutils.EasyRandomProvider;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SettingsRepositoryAdapterTest {

	private static final EasyRandom r = EasyRandomProvider.easyRandom();
	
	@Mock
	SettingsRepository repository;
	
	SettingsRepositoryAdapter adapter;

	@BeforeEach
	void setUp() {
		this.adapter = new SettingsRepositoryAdapter(repository);
	}

	@Test
	void initSettings() {
		when(repository.findById(anyString())).thenReturn(Optional.empty());
		
		initAdapter();
		
		assertAll("Initial settings",
				() -> assertEquals(Currency.getInstance("EUR"), adapter.getCurrency()),
				() -> assertEquals("yyyy-MM-dd HH:mm", adapter.getDateTimeFormat()),
				() -> assertEquals(Integer.valueOf(5000), adapter.getGrowlDelay()),
				() -> assertEquals(ZoneId.systemDefault(), adapter.getTimeZone()),
				() -> assertEquals(Integer.valueOf(300000), adapter.getKeepAliveTimeout()),
				() -> assertTrue(adapter.getSelfRegistration()),
				() -> assertEquals(Integer.valueOf(30), adapter.getDataPoints()),
				() -> assertEquals("2018-11-13 22:28", adapter.convertInstantToString(ZonedDateTime.of(2018, 11, 13, 22, 28, 30, 500, ZoneOffset.systemDefault()).toInstant())),
				() -> assertEquals(Integer.valueOf(10000), adapter.getLiveStatisticsInterval())
			);
	}
	
	@Test
	void preConfiguredSettings() {
		Currency currency = Currency.getInstance("USD");
		String format = "yyyy-MM-dd";
		Integer delay = 7000;
		ZoneId timezone = ZoneId.of("UTC");
		Integer keepAliveTimeout = 700000;
		Integer dataPoints = 200;
		Integer liveStatisticsInterval = 2000;
		
		SettingsEntity currencySetting = createSetting(SettingsConstants.CURRENCY, currency.getCurrencyCode());
		SettingsEntity formatSetting = createSetting(SettingsConstants.DATE_TIME_FORMAT, format);
		SettingsEntity delaySetting = createSetting(SettingsConstants.GROWL_DELAY, delay.toString());
		SettingsEntity timezoneSetting = createSetting(SettingsConstants.TIME_ZONE, timezone.getId());
		SettingsEntity keepAliveTimeoutSetting = createSetting(SettingsConstants.KEEP_ALIVE_TIMEOUT, keepAliveTimeout.toString());
		SettingsEntity selfRegistrationSetting = createSetting(SettingsConstants.SELF_REGISTRATION, Boolean.FALSE.toString());
		SettingsEntity dataPointsSetting = createSetting(SettingsConstants.DATA_POINTS, dataPoints.toString());
		SettingsEntity liveStatisticsIntervalSetting = createSetting(SettingsConstants.LIVE_STATISTICS_INTERVAL, liveStatisticsInterval.toString());
		
		when(repository.findById(SettingsConstants.CURRENCY)).thenReturn(Optional.of(currencySetting));
		when(repository.findById(SettingsConstants.DATE_TIME_FORMAT)).thenReturn(Optional.of(formatSetting));
		when(repository.findById(SettingsConstants.GROWL_DELAY)).thenReturn(Optional.of(delaySetting));
		when(repository.findById(SettingsConstants.TIME_ZONE)).thenReturn(Optional.of(timezoneSetting));
		when(repository.findById(SettingsConstants.KEEP_ALIVE_TIMEOUT)).thenReturn(Optional.of(keepAliveTimeoutSetting));
		when(repository.findById(SettingsConstants.SELF_REGISTRATION)).thenReturn(Optional.of(selfRegistrationSetting));
		when(repository.findById(SettingsConstants.DATA_POINTS)).thenReturn(Optional.of(dataPointsSetting));
		when(repository.findById(SettingsConstants.LIVE_STATISTICS_INTERVAL)).thenReturn(Optional.of(liveStatisticsIntervalSetting));
		
		initAdapter();
		
		assertAll("Initial settings",
				() -> assertEquals(currency, adapter.getCurrency()),
				() -> assertEquals(format, adapter.getDateTimeFormat()),
				() -> assertEquals(delay, adapter.getGrowlDelay()),
				() -> assertEquals(timezone, adapter.getTimeZone()),
				() -> assertEquals(keepAliveTimeout, adapter.getKeepAliveTimeout()),
				() -> assertFalse(adapter.getSelfRegistration()),
				() -> assertEquals(dataPoints, adapter.getDataPoints()),
				() -> assertEquals(liveStatisticsInterval, adapter.getLiveStatisticsInterval())
			);
	}
	
	@ParameterizedTest
	@ValueSource(ints= {2000, 3000, 4000})
	void setGrowlDelay(Integer delay) {
		when(repository.findById(anyString())).thenReturn(Optional.empty());
		
		doAnswer((Answer<SettingsEntity>) invocation -> {
			SettingsEntity setting = invocation.getArgument(0);
			assertAll(
					() -> assertEquals(SettingsConstants.GROWL_DELAY, setting.getKey()),
					() -> assertEquals(delay.toString(), setting.getValue())
				);
			return setting;
		}).when(repository).save(any());
		
		initAdapter();
		adapter.setGrowlDelay(delay);
		verify(repository).save(any());
	}
	
	@ParameterizedTest
	@ValueSource(strings= {"yyyy-MM-dd", "yyyy MM dd", "dd MM yyyy"})
	void setDateTimeFormat(String format) {
		doAnswer((Answer<SettingsEntity>) invocation -> {
			SettingsEntity setting = invocation.getArgument(0);
			assertAll(
					() -> assertEquals(SettingsConstants.DATE_TIME_FORMAT, setting.getKey()),
					() -> assertEquals(format, setting.getValue())
				);
			return setting;
		}).when(repository).save(any());
		
		initAdapter();
		when(repository.findById(anyString())).thenReturn(Optional.of(createSetting(SettingsConstants.DATE_TIME_FORMAT, "yyyy-MM-dd HH:mm.ss")));
		adapter.setDateTimeFormat(format);
		verify(repository).save(any());
		Instant instant = ZonedDateTime.of(2018, 11, 13, 21, 28, 30, 500, ZoneOffset.UTC).toInstant();
		assertEquals(DateTimeFormatter.ofPattern(format).format(instant.atZone(ZoneOffset.UTC)), adapter.convertInstantToString(instant));
	}
	
	@ParameterizedTest
	@ValueSource(strings= {"USD", "EUR", "GBP"})
	void setCurrency(String currencyId) {
		Currency currency = Currency.getInstance(currencyId);
		
		when(repository.findById(anyString())).thenReturn(Optional.empty());
		
		doAnswer((Answer<SettingsEntity>) invocation -> {
			SettingsEntity setting = invocation.getArgument(0);
			assertAll(
					() -> assertEquals(SettingsConstants.CURRENCY, setting.getKey()),
					() -> assertEquals(currency.getCurrencyCode(), setting.getValue())
				);
			return setting;
		}).when(repository).save(any());
		
		initAdapter();
		adapter.setCurrency(currency);
		verify(repository).save(any());
	}
	
	@ParameterizedTest
	@ValueSource(strings= {"UTC", "Europe/Berlin", "MET"})
	void setTimeZone(String zoneId) {
		ZoneId timezone = ZoneId.of(zoneId);
		
		when(repository.findById(anyString())).thenReturn(Optional.empty());
		
		doAnswer((Answer<SettingsEntity>) invocation -> {
			SettingsEntity setting = invocation.getArgument(0);
			assertAll(
					() -> assertEquals(SettingsConstants.TIME_ZONE, setting.getKey()),
					() -> assertEquals(timezone.getId(), setting.getValue())
				);
			return setting;
		}).when(repository).save(any());
		
		initAdapter();
		adapter.setTimeZone(timezone);
		verify(repository).save(any());
	}
	
	@ParameterizedTest
	@ValueSource(ints= {2000, 3000, 4000})
	void setKeepAliveTimeout(Integer keepAliveTimeout) {
		
		when(repository.findById(anyString())).thenReturn(Optional.empty());
		
		doAnswer((Answer<SettingsEntity>) invocation -> {
			SettingsEntity setting = invocation.getArgument(0);
			assertAll(
					() -> assertEquals(SettingsConstants.KEEP_ALIVE_TIMEOUT, setting.getKey()),
					() -> assertEquals(keepAliveTimeout.toString(), setting.getValue())
				);
			return setting;
		}).when(repository).save(any());
		
		initAdapter();
		adapter.setKeepAliveTimeout(keepAliveTimeout);
		verify(repository).save(any());
	}
	
	@Test
	void setSelfRegistration() {
		when(repository.findById(anyString())).thenReturn(Optional.empty());
		
		doAnswer((Answer<SettingsEntity>) invocation -> {
			SettingsEntity setting = invocation.getArgument(0);
			assertAll(
					() -> assertEquals(SettingsConstants.SELF_REGISTRATION, setting.getKey()),
					() -> assertNotNull(setting.getValue()),
					() -> assertFalse(Boolean.parseBoolean(setting.getValue()))
				);
			return setting;
		}).when(repository).save(any());
		
		initAdapter();
		adapter.setSelfRegistration(false);
		verify(repository).save(any());
	}
	
	@ParameterizedTest
	@ValueSource(ints= {20, 30, 40})
	void setDataPoints(Integer dataPoints) {
		when(repository.findById(anyString())).thenReturn(Optional.empty());
		
		doAnswer((Answer<SettingsEntity>) invocation -> {
			SettingsEntity setting = invocation.getArgument(0);
			assertAll(
					() -> assertEquals(SettingsConstants.DATA_POINTS, setting.getKey()),
					() -> assertEquals(dataPoints.toString(), setting.getValue())
				);
			return setting;
		}).when(repository).save(any());
		
		initAdapter();
		adapter.setDataPoints(dataPoints);
		verify(repository).save(any());
	}
	
	@ParameterizedTest
	@ValueSource(ints= {50, 60, 70})
	void setLiveStatisticsInterval(Integer liveStatisticsInterval) {
		when(repository.findById(anyString())).thenReturn(Optional.empty());
		
		doAnswer((Answer<SettingsEntity>) invocation -> {
			SettingsEntity setting = invocation.getArgument(0);
			assertAll(
					() -> assertEquals(SettingsConstants.LIVE_STATISTICS_INTERVAL, setting.getKey()),
					() -> assertEquals(liveStatisticsInterval.toString(), setting.getValue())
				);
			return setting;
		}).when(repository).save(any());
		
		initAdapter();
		adapter.setLiveStatisticsInterval(liveStatisticsInterval);
		verify(repository).save(any());
	}

	@Test
	void updateSettings() {
		var settings = r.nextObject(Settings.class);
		adapter.updateSettings(settings);
		var result = adapter.getSettings();

		assertThat(result).usingRecursiveComparison().isEqualTo(settings);
	}

	@Test
	void updateSettingsWithNullValues() {
		var oldSettings = adapter.getSettings();
		var settings = new Settings();
		settings.setCurrency(null);
		settings.setSelfRegistration(null);
		settings.setTimeZone(null);
		settings.setDateTimeFormat(null);
		settings.setGrowlDelay(null);
		settings.setDataPoints(null);
		settings.setLiveStatisticsInterval(null);
		settings.setKeepAliveTimeout(null);
		adapter.updateSettings(settings);
		var result = adapter.getSettings();

		assertThat(result).usingRecursiveComparison().isEqualTo(oldSettings);
		assertThat(result).usingRecursiveComparison().isNotEqualTo(settings);
	}

	private void initAdapter() {
		adapter.initSettings();
	}

	private SettingsEntity createSetting(String key, String value) {
		SettingsEntity setting = new SettingsEntity();
		setting.setKey(key);
		setting.setValue(value);
		return setting;
	}
}
