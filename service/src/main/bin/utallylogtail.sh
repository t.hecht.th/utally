#!/bin/bash

if [ -z ${UTALLY_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UTALLY_HOME="`cd "$DIRNAME/..";pwd`"
fi;

if [ -z ${UTALLY_LOG_PATH} ]
then
  UTALLY_LOG_PATH=$UTALLY_HOME/log
fi;

UTALLY_LOG_FILE=${UTALLY_LOG_PATH}/utally.log

echo "tail -F ${UTALLY_LOG_FILE}"
echo
exec tail -F ${UTALLY_LOG_FILE}