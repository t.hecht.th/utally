#!/bin/bash

if [ -z ${JAVA_HOME} ]
then
  JAVA_PATH=`which java`
else
  JAVA_PATH=$JAVA_HOME/bin/java
fi;

if [ -z ${UTALLY_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UTALLY_HOME="`cd "$DIRNAME/..";pwd`"
fi;

if [ ! -f /utally/config/utally-docker.properties ]; then
    cp /utally/utally-2.0.0/etc/utally-docker.properties /utally/config/utally-docker.properties
fi

if [ ! -f /utally/config/logback-spring.xml ]; then
    cp /utally/utally-2.0.0/etc/logback-spring.xml /utally/config/logback-spring.xml
fi

if [ ! -f /utally/config/privacy.txt.template ]; then
    cp /utally/utally-2.0.0/etc/privacy.txt.template /utally/config/privacy.txt.template
fi

if [ ! -f /utally/config/privacy-ssl.txt.template ]; then
    cp /utally/utally-2.0.0/etc/privacy-ssl.txt.template /utally/config/privacy-ssl.txt.template
fi

if [ ! -f /utally/config/service.grafana ]; then
    cp /utally/utally-2.0.0/etc/service.grafana /utally/config/service.grafana
fi

if [ ! -f /utally/config/tallies.grafana ]; then
    cp /utally/utally-2.0.0/etc/tallies.grafana /utally/config/tallies.grafana
fi

if [ ! -f /utally/config/volume.grafana ]; then
    cp /utally/utally-2.0.0/etc/volume.grafana /utally/config/volume.grafana
fi

if [ ! -f /utally/images/demo-bag.png ]; then
    cp /utally/utally-2.0.0/images/demo-bag.png /utally/images/demo-bag.png
fi

if [ ! -f /utally/images/demo-bratwurst.png ]; then
    cp /utally/utally-2.0.0/images/demo-bratwurst.png /utally/images/demo-bratwurst.png
fi

if [ ! -f /utally/images/demo-longdrink.png ]; then
    cp /utally/utally-2.0.0/images/demo-longdrink.png /utally/images/demo-longdrink.png
fi

if [ ! -f /utally/images/demo-salad.png ]; then
    cp /utally/utally-2.0.0/images/demo-salad.png /utally/images/demo-salad.png
fi

if [ ! -f /utally/images/demo-softdrink.png ]; then
    cp /utally/utally-2.0.0/images/demo-softdrink.png /utally/images/demo-softdrink.png
fi

if [ -z ${UTALLY_CONFIG_FILE} ]
then
  UTALLY_CONFIG_FILE=/utally/config/utally-docker.properties
fi;

if [ -z ${UTALLY_LOG_PATH} ]
then
  UTALLY_LOG_PATH=/utally/log
fi;

if [ -z ${UTALLY_DEBUG} ]
then
  UTALLY_JAVAFLAGS="$UTALLY_JAVAFLAGS -Xdebug -Xrunjdwp:transport=dt_socket,address=9912,server=y,suspend=n"
fi

echo "========================================================================="
echo ""
echo "  Starting Utally"
echo ""
echo "  UTALLY_HOME: $UTALLY_HOME"
echo ""
echo "  JAVA_PATH: $JAVA_PATH"
echo ""
echo "  UTALLY_CONFIG_FILE: $UTALLY_CONFIG_FILE"
echo ""
echo "  UTALLY_LOG_PATH: $UTALLY_LOG_PATH"
echo ""
echo "========================================================================="
echo ""

$JAVA_PATH $UTALLY_JAVAFLAGS --add-opens java.base/java.lang=ALL-UNNAMED -Dloader.main=de.utally.Utally -Dloader.path="lib/,etc/,$UTALLY_HOME/lib/*,$UTALLY_HOME/etc/" -Dspring.config.location=$UTALLY_CONFIG_FILE -jar $UTALLY_HOME/lib/Utally.jar --utally.path=$UTALLY_HOME > $UTALLY_LOG_PATH/utally_console.log 2>&1 &
echo "Finished starting Utally"

while true; do sleep 1; done
