#!/bin/bash

if [ -z ${JAVA_HOME} ]
then
  JAVA_PATH=`which java`
else
  JAVA_PATH=$JAVA_HOME/bin/java
fi;

if [ -z ${UTALLY_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UTALLY_HOME="`cd "$DIRNAME/..";pwd`"
fi;

if [ -z ${UTALLY_CONFIG_FILE} ]
then
  UTALLY_CONFIG_FILE=$UTALLY_HOME/etc/utally.properties
fi;

if [ -z ${UTALLY_LOG_PATH} ]
then
  UTALLY_LOG_PATH=$UTALLY_HOME/log
fi;

if [ ! -z ${UTALLY_DEBUG} ]
then
  UTALLY_JAVAFLAGS="$UTALLY_JAVAFLAGS -Xdebug -Xrunjdwp:transport=dt_socket,address=9912,server=y,suspend=n"
fi

echo "========================================================================="
echo ""
echo "  Starting Utally"
echo ""
echo "  UTALLY_HOME: $UTALLY_HOME"
echo ""
echo "  JAVA_PATH: $JAVA_PATH"
echo ""
echo "  UTALLY_CONFIG_FILE: $UTALLY_CONFIG_FILE"
echo ""
echo "  UTALLY_LOG_PATH: $UTALLY_LOG_PATH"
echo ""
echo "========================================================================="
echo ""

$JAVA_PATH $UTALLY_JAVAFLAGS --add-opens java.base/java.lang=ALL-UNNAMED -Dloader.main=de.utally.Utally -Dloader.path="lib/,etc/,$UTALLY_HOME/lib/*,$UTALLY_HOME/etc/" -Dspring.config.location=$UTALLY_CONFIG_FILE -jar $UTALLY_HOME/lib/Utally.jar --utally.path=$UTALLY_HOME > $UTALLY_LOG_PATH/utally_console.log 2>&1 &
echo "Finished starting Utally"
echo `$UTALLY_HOME/bin/utallyps.sh`
