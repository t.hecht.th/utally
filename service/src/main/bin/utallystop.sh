#!/bin/bash

if [ -z ${UTALLY_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UTALLY_HOME="`cd "$DIRNAME/..";pwd`"
fi;

PID=`$UTALLY_HOME/bin/utallyps.sh | awk '{print $2}'`

if [ -z "${PID}" ]
then
        echo "Utally process is not running. No shutdown will be performed."
        exit 0
fi

echo "Shutting down Utally."

kill $PID;

echo "Shutdown done."