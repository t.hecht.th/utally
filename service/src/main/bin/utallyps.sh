#!/bin/bash

if [ -z ${UTALLY_HOME} ]
then
  DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
  UTALLY_HOME="`cd "$DIRNAME/..";pwd`"
fi;

ps auxww | grep $UTALLY_HOME/lib/Utally.jar | grep -v grep