package de.utally;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.scheduling.annotation.EnableScheduling;

@EntityScan(basePackageClasses = { Utally.class, Jsr310JpaConverters.class })
@SpringBootApplication
@EnableScheduling
public class Utally implements ApplicationListener<ApplicationEvent> {
	private static final Logger logger = LoggerFactory.getLogger(Utally.class);

	@Value("${server.servlet.context-path}")
	private String servletPath;

	public static void main(String[] args) {
		SpringApplication.run(Utally.class, args);
	}

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		if(event instanceof ApplicationReadyEvent) {
			logger.info("Utally started.");
			if (logger.isInfoEnabled()) {
				logger.info("Visit <HOST>{}/index.xhtml", servletPath);
			}
		}
		
		if(event instanceof ContextClosedEvent) {
			logger.info("Closing Utally.");
		}
	}
}
