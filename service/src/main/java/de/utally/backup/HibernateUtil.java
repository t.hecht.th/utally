package de.utally.backup;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

/**
 * Utility class to create {@link SessionFactory}s from database url, user and
 * password.
 * 
 * @author Tobias Hecht
 *
 */
final class HibernateUtil {
	private static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);
	private static StandardServiceRegistry registry;
	private static final Map<String, SessionFactory> sessionFactories = new HashMap<>();

	/**
	 * Private constructor to prevent instantiation.
	 */
	private HibernateUtil() {

	}

	/**
	 * Constructs a {@link SessionFactory} for the database connection data.
	 * 
	 * @param url
	 *            is the database url
	 * @param user
	 *            is the database user
	 * @param password
	 *            is the database password
	 * @return {@link Optional} containing a {@link SessionFactory} if creation
	 *         succeeded
	 */
	public static Optional<SessionFactory> getSessionFactory(String url, String user, String password) {
		if (!sessionFactories.containsKey(url)) {
			try {
				StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

				Properties settings = new Properties();
				settings.put(AvailableSettings.URL, url);
				settings.put(AvailableSettings.USER, user);
				settings.put(AvailableSettings.PASS, password);

				// Apply settings
				registryBuilder.applySettings(settings);

				// Create registry
				registry = registryBuilder.build();

				// Create MetadataSources
				MetadataSources sources = new MetadataSources(registry);

				// Create Metadata
				Metadata metadata = sources.getMetadataBuilder().build();

				// Create SessionFactory
				SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

				sessionFactories.put(url, sessionFactory);

			} catch (Exception e) {
				logger.error("Failed to create session factory.", e);
				shutdown();
			}
		}
		return Optional.ofNullable(sessionFactories.get(url));
	}

	/**
	 * Destroys the {@link StandardServiceRegistry}
	 */
	public static void shutdown() {
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}
