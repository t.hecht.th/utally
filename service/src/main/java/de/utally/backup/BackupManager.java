package de.utally.backup;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Manager class to trigger the database backup creation. The trigger is
 * scheduled by a Spring cron scheduler.
 * 
 * @author Tobias Hecht
 *
 */
@Component
public class BackupManager {
	private static final Logger logger = LoggerFactory.getLogger(BackupManager.class);

	private final boolean backupEnabled;

	private final String backupPath;

	private final String url;

	private final String user;

	private final String password;

	/**
	 * Initializes the {@link BackupManager} with the necessary backup parameter.
	 *
	 * @param backupEnabled a boolean indicating whether backups shall be created
	 * @param backupPath the path where the backups shall be stored
	 * @param url the datasource url
	 * @param user the datasource user
	 * @param password the datasource password
	 */
	@Autowired
	public BackupManager(
			@Value("${backup.enabled:false}") boolean backupEnabled,
			@Value("${backup.path:.}") String backupPath,
			@Value("${spring.datasource.url}") String url,
			@Value("${spring.datasource.username}") String user,
			@Value("${spring.datasource.password}") String password
	) {
		this.backupEnabled = backupEnabled;
		this.backupPath = backupPath;
		this.url = url;
		this.user = user;
		this.password = password;
	}

	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");

	/**
	 * If backup management is enabled by backup.enabled property, this method
	 * executes the HSQLDB backup query and stores the backup to the configured
	 * path. This method is scheduled by a Spring cron scheduler using a cron
	 * {@link String} configured by the backup.cronString property.
	 */
	@Scheduled(cron = "${backup.cronString}")
	public void execute() {
		if (backupEnabled) {
			logger.info("Creating database backup.");
			var optSessionFactory = HibernateUtil.getSessionFactory(url, user, password);
			if (optSessionFactory.isPresent()) {
				String path;
				try (Session session = optSessionFactory.get().openSession()) {
					Transaction transaction = session.beginTransaction();

					LocalDateTime now = LocalDateTime.now();
					path = new File(backupPath + File.separator + "backup_" + formatter.format(now) + ".tar.gz")
							.getCanonicalPath();

					logger.info("Storing backup to {}", path);

					@SuppressWarnings("rawtypes")
					NativeQuery query = session.createNativeQuery("BACKUP DATABASE TO '" + path + "' BLOCKING", Void.class);
					query.executeUpdate();
					transaction.commit();
				} catch (IOException e) {
					logger.error("Failed to determine backup path. Skipping database export.", e);
				}
			} else {
				logger.warn("No session factory. Skipping database export.");
			}
		} else {
			logger.info("Backup management disabled. Skipping database export.");
		}
	}
}
