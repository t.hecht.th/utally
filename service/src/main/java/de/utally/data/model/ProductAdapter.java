package de.utally.data.model;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Interface to provide methods for {@link Product} management.
 * 
 * @author Tobias Hecht
 *
 */
public interface ProductAdapter {

	Product insertProduct(String name, String description, Integer price, String imageName,
			ProductCategory productCategory, Boolean enabled, Boolean displayStatistic);

	boolean existsById(Long productId);

	List<Product> getProducts();

	Set<Product> getProductsByEvent(Long eventId);

	List<Product> getEnabledProducts();

	Product getProductById(Long productId);

	List<Product> getProductByIds(Collection<Long> productIds);

	void updateProduct(Product product);

	void removeProduct(Long productId);
}
