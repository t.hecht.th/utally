package de.utally.data.model;

import java.time.Instant;

/**
 * Data class for Utally Time Slots
 * 
 * @author Tobias Hecht
 *
 */
public class TimeSlot implements Identifiable, Auditable, HasEnabledState, Describable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4188002786619411341L;

	private Long id;

	private String label;

	private Instant start;

	private Instant end;

	private Boolean enabled;

	private Instant creationTime;

	private Instant modificationTime;
	
	private String description;

	private Event event;

	/*
	 * (non-Javadoc)
	 * @see de.utally.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Identifiable#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the label of the time slot.
	 * 
	 * @return the label of the time slot
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Sets a new label of the time slot.
	 * 
	 * @param label is the new label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Returns the start time of the time slot.
	 * 
	 * @return the start time of the time slot
	 */
	public Instant getStart() {
		return start;
	}

	/**
	 * Sets a new start time of the time slot.
	 * 
	 * @param start is the new start time
	 */
	public void setStart(Instant start) {
		this.start = start;
	}

	/**
	 * Returns the end time of the time slot.
	 * 
	 * @return the end time of the time slot
	 */
	public Instant getEnd() {
		return end;
	}

	/**
	 * Sets a new end time of the time slot.
	 * 
	 * @param end is the new end time
	 */
	public void setEnd(Instant end) {
		this.end = end;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#getEnabled()
	 */
	@Override
	public Boolean getEnabled() {
		return enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#setEnabled(java.lang.Boolean)
	 */
	@Override
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getCreationTime()
	 */
	@Override
	public Instant getCreationTime() {
		return creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setCreationTime(java.time.Instant)
	 */
	@Override
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getModificationTime()
	 */
	@Override
	public Instant getModificationTime() {
		return modificationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setModificationTime(java.time.Instant)
	 */
	@Override
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the associated {@link Event}.
	 *
	 * @return the associated {@link Event}
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * Sets a new {@link Event} associated to time slot.
	 *
	 * @param event is the new {@link Event}
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * Indicates whether the given {@link Instant} is between the start and end time of this time slot.
	 * 
	 * @param time is the {@link Instant} to check
	 * @return true, if the {@link Instant} is between the start and end time
	 */
	public boolean contains(Instant time) {
		if (time == null) {
			return false;
		} else {
			return start.isBefore(time) && end.isAfter(time);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"(%s@%s [id=%d, label=%s, start=%s, end=%s, enabled=%b, creationTime=%s, modificationTime=%s])",
				this.getClass().getSimpleName(), Integer.toHexString(hashCode()), this.getId(), this.getLabel(),
				this.getStart().toString(), this.getEnd().toString(), this.getEnabled(),
				this.getCreationTime(), this.getModificationTime());
	}
}
