package de.utally.data.model;

import java.io.Serializable;

/**
 * Common methods for all Utally data classes.
 * 
 * @author Tobias Hecht
 *
 */
public interface Identifiable extends Serializable {
	/**
	 * Returns the model id.
	 * 
	 * @return the id
	 */
	Long getId();

	/**
	 * Sets a new id.
	 * 
	 * @param id
	 *            is the new id
	 */
	void setId(Long id);
	
	/**
	 * Idicates whether some other object is equal to this one by class and id.
	 * 
	 * @param that
	 *            is the reference object with which to compare
	 * @return if this object is an instance of the same class and has the same id
	 */
	default boolean equalsById(Object that) {
		if (that == null) {
			return false;
		}

		if (!this.getClass().equals(that.getClass())) {
			return false;
		}

		Identifiable thatModel = (Identifiable) that;
		return this.getId().equals(thatModel.getId());
	}
}
