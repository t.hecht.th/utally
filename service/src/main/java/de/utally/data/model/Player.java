package de.utally.data.model;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Data class for Utally Players
 * 
 * @author Tobias Hecht
 *
 */
public class Player implements Identifiable, Auditable, HasEnabledState {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918440296749342025L;

	private Long id;

	private String name;

	private Instant creationTime;

	private Instant modificationTime;

	private Team team;

	private Event event;

	private Boolean paid = Boolean.FALSE;

	private Boolean withPlayersFee = Boolean.FALSE;

	private String comment;

	private Instant payTime;

	private Boolean enabled;

	private Integer paidPlayersFee;

	private Map<String, String> preferences = new HashMap<>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Identifiable#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the player name.
	 * 
	 * @return the player name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets a new player name.
	 * 
	 * @param name
	 *            is the new player name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getCreationTime()
	 */
	@Override
	public Instant getCreationTime() {
		return creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setCreationTime(java.time.Instant)
	 */
	@Override
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getModificationTime()
	 */
	@Override
	public Instant getModificationTime() {
		return modificationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setModificationTime(java.time.Instant)
	 */
	@Override
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/**
	 * Returns the {@link Team} of the player.
	 * 
	 * @return the {@link Team}
	 */
	public Team getTeam() {
		return team;
	}

	/**
	 * Sets a new {@link Team} for the player.
	 * 
	 * @param team
	 *            is the new {@link Team}
	 */
	public void setTeam(Team team) {
		this.team = team;
	}

	/**
	 * Returns the {@link Event} the player is associated to.
	 * 
	 * @return the {@link Event}
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * Sets a new {@link Event} association to the player.
	 * 
	 * @param event
	 *            is the new {@link Event}
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * Returns true, if the player has paid.
	 * 
	 * @return true, if the player has paid
	 */
	public Boolean getPaid() {
		return paid;
	}

	/**
	 * Sets a new paid state.
	 * 
	 * @param paid
	 *            is the new paid state
	 */
	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	/**
	 * Returns true, if the player has paid with players fee.
	 * 
	 * @return true, if the player has paid with players fee
	 */
	public Boolean getWithPlayersFee() {
		return withPlayersFee;
	}

	/**
	 * Sets a new paid with players fee state.
	 * 
	 * @param withPlayersFee
	 *            is the new paid with players fee state
	 */
	public void setWithPlayersFee(Boolean withPlayersFee) {
		this.withPlayersFee = withPlayersFee;
	}

	/**
	 * Returns the comment added to the player.
	 * 
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets a new comment to the player
	 * 
	 * @param comment
	 *            is the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Returns the time when the player has paid.
	 * 
	 * @return the time when the player has paid
	 */
	public Instant getPayTime() {
		return payTime;
	}

	/**
	 * Sets a new time when the player has paid.
	 * 
	 * @param payTime
	 *            is the new time when the player has paid
	 */
	public void setPayTime(Instant payTime) {
		this.payTime = payTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#getEnabled()
	 */
	@Override
	public Boolean getEnabled() {
		return enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#setEnabled(java.lang.Boolean)
	 */
	@Override
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the amount of players fee the player has paid.
	 * 
	 * @return the amount of players fee the player has paid
	 */
	public Integer getPaidPlayersFee() {
		return paidPlayersFee;
	}

	/**
	 * Sets a new amount of players fee the player has paid.
	 * 
	 * @param paidPlayersFee
	 *            is the new amount of players fee the player has paid
	 */
	public void setPaidPlayersFee(Integer paidPlayersFee) {
		this.paidPlayersFee = paidPlayersFee;
	}

	/**
	 * Returns a KV-{@link Map} of player preferences.
	 * 
	 * @return the preferences
	 */
	public Map<String, String> getPreferences() {
		return preferences;
	}

	/**
	 * Sets a new {@link Map} of player preferences.
	 * 
	 * @param preferences
	 *            are the new preferences
	 */
	public void setPreferences(Map<String, String> preferences) {
		this.preferences = preferences;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"(%s@%s [id=%d, name=%s, team=%s, event=%s, paid=%b, withPlayersFee=%b, comment=%s, payTime=%s, enabled=%b, paidPlayersFee=%d, creationTime=%s, modificationTime=%s, preferences=%s])",
				this.getClass().getSimpleName(), Integer.toHexString(hashCode()), this.getId(), this.getName(),
				this.getTeam(), this.getEvent(), this.getPaid(), this.getWithPlayersFee(), this.getComment(),
				this.getPayTime(), this.getEnabled(), this.getPaidPlayersFee(), this.getCreationTime(),
				this.getModificationTime(), this.getPreferences());
	}
}
