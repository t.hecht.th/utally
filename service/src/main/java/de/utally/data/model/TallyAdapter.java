package de.utally.data.model;

import java.util.List;

/**
 * Interface to provide methods for {@link Tally} management.
 * 
 * @author Tobias Hecht
 *
 */
public interface TallyAdapter {

	Tally insertTally(Long playerId, Long productId, Long locationId);

	Tally getTallyById(Long id);

	List<Tally> getTallies();

	List<Tally> getTalliesByPlayer(Long playerId);

	List<Tally> getTalliesByProductAndPlayer(Long productId, Long playerId);

	List<Tally> getTalliesByLocationAndPlayer(Long locationId, Long playerId);

	List<Tally> getTalliesByProduct(Long productId);

	List<Tally> getTalliesByLocation(Long locationId);

	List<Tally> getTalliesByEvent(Long eventId);

	List<Tally> getTalliesByTimeSlot(Long timeSlotId);

	List<Tally> getTalliesByTeam(Long teamId);

	List<Tally> getTalliesByLocationAndEvent(Long locationId, Long eventId);

	List<Tally> getTalliesByProductAndEvent(Long productId, Long eventId);

	List<Tally> getTalliesByTeamAndEvent(Long teamId, Long eventId);

	void removeTally(Long tallyId);

	void removeTalliesByProduct(Long productId);

	void removeTalliesByPlayer(Long playerId);

	void removeTalliesByLocation(Long locationId);

	void cancelTally(Long tallyid);

	void revokeCancellation(Long tallyId);
}
