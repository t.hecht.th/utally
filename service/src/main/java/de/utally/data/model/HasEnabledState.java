package de.utally.data.model;

/**
 * Common methods for Utally data classes with enabled state.
 * 
 * @author Tobias Hecht
 *
 */
public interface HasEnabledState {
	/**
	 * Returns true if the object is enabled.
	 * 
	 * @return true if the object is enabled
	 */
	public Boolean getEnabled();

	/**
	 * Sets a new enabled state.
	 * 
	 * @param enabled
	 *            is the new enabled state
	 */
	public void setEnabled(Boolean enabled);
}
