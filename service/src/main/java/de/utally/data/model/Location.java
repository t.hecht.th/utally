package de.utally.data.model;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Data class for Utally locations
 * 
 * @author Tobias Hecht
 *
 */
public class Location implements Identifiable, Auditable, HasEnabledState, Describable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5819686261392831847L;

	private Long id;
	
	private String name;
	
	private Boolean enabled;
	
	private Instant creationTime;
	
	private Instant modificationTime;
	
	private Set<Product> products = new HashSet<>();
	
	private String description;

	/*
	 * (non-Javadoc)
	 * @see de.utally.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Identifiable#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the location name.
	 * 
	 * @return the location name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets a new location name.
	 * 
	 * @param name is the new location name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#getEnabled()
	 */
	@Override
	public Boolean getEnabled() {
		return enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#setEnabled(java.lang.Boolean)
	 */
	@Override
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the {@link Product}s associated to the location.
	 * 
	 * @return the {@link Product}s associated to the location
	 */
	public Set<Product> getProducts() {
		return new HashSet<>(products);
	}

	/**
	 * Sets new {@link Product}s associated to the location.
	 * 
	 * @param products
	 *            are the new {@link Product}s associated to the location
	 */
	public void setProducts(Set<Product> products) {
		this.products = new HashSet<>(products);
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getCreationTime()
	 */
	@Override
	public Instant getCreationTime() {
		return creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setCreationTime(java.time.Instant)
	 */
	@Override
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getModificationTime()
	 */
	@Override
	public Instant getModificationTime() {
		return modificationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setModificationTime(java.time.Instant)
	 */
	@Override
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("(%s@%s [id=%d, name=%s, enabled=%b, products=%s, creationTime=%s, modificationTime=%s])",
				this.getClass().getSimpleName(), Integer.toHexString(hashCode()), this.getId(), this.getName(),
				this.getEnabled(), this.getProducts(), this.getCreationTime(),
				this.getModificationTime());
	}
}
