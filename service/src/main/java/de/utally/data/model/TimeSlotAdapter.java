package de.utally.data.model;

import java.time.Instant;
import java.util.List;
import java.util.Map;

/**
 * Interface to provide methods for {@link TimeSlot} management.
 * 
 * @author Tobias Hecht
 *
 */
public interface TimeSlotAdapter {

	TimeSlot insertTimeSlot(String label, String description, Instant start, Instant end, Boolean enabled, Long eventId);

	List<TimeSlot> getTimeSlots();

	List<TimeSlot> getTimeSlotsByEvent(Long eventId);

	List<TimeSlot> getEnabledTimeSlots();

	TimeSlot getTimeSlotById(Long id);

	void updateTimeSlot(TimeSlot timeSlot);

	void removeTimeSlot(Long id);

	Map<Long, Map<Long, Long>> getNumberOfTalliesByTeamInTimeSlotForStatistics(Long id);
}
