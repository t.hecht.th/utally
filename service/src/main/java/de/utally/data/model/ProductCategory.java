package de.utally.data.model;

/**
 * Product Category enumeration
 * 
 * @author Tobias Hecht
 *
 */
public enum ProductCategory {
	FOOD, // Category for oof products
	DRINK, // Category for drinks
	MDSE, // Category for merchandise products
	MISC // Category for the rest
}
