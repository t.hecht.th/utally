package de.utally.data.model;

import java.util.List;

public interface PlayerAdapter {

	Player insertPlayer(String name, Long teamId, Long eventId, Boolean enabled);

	Player getPlayerById(Long id);

	boolean existsById(Long playerId);

	List<Player> getPlayersByTeamAndEvent(Long teamId, Long eventId);

	List<Player> getPlayersByEvent(Long eventId);

	List<Player> getPlayersByTeam(Long teamId);

	List<Player> getPlayersByTeamsAndEvent(List<Long> teamIds, Long eventId);

	List<Player> getPlayers();

	List<Player> getEnabledPlayers();

	void updatePlayer(Player player);

	void mergePlayerIntoPlayer(Long sourceId, Long destinationId);

	void removePlayer(Long id);

	void pay(Long playerId, String comment, Boolean withPlayersFee, Integer paidPlayersFee);

	void revertPayment(Long playerId);
}
