package de.utally.data.model;

import java.time.Instant;

/**
 * Data class for Utally Tallies
 * 
 * @author Tobias Hecht
 *
 */
public class Tally implements Identifiable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6479225109599371524L;

	private Long id;
	
	private Player player;
	
	private Location location;
	
	private Instant time;
	
	private Product product;
	
	private Boolean cancelled;

	/*
	 * (non-Javadoc)
	 * @see de.utally.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Identifiable#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the tally insertion time.
	 * 
	 * @return the tally insertion time
	 */
	public Instant getTime() {
		return time;
	}

	/**
	 * Sets a new tally insertion time.
	 * 
	 * @param time
	 *            is the new tally insertion time
	 */
	public void setTime(Instant time) {
		this.time = time;
	}

	/**
	 * Returns true if the tally is cancelled.
	 * 
	 * @return true if the tally is cancelled
	 */
	public Boolean getCancelled() {
		return cancelled;
	}

	/**
	 * Sets a new cancelled state.
	 * 
	 * @param cancelled is the new cancelled state
	 */
	public void setCancelled(Boolean cancelled) {
		this.cancelled = cancelled;
	}

	/**
	 * Returns the {@link Player} the tally is associated to.
	 * 
	 * @return the {@link Player} the tally is associated to
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Sets a new {@link Player} association.
	 * 
	 * @param player is the new {@link Player}
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * Returns the {@link Location} the tally is associated to.
	 * 
	 * @return the {@link Location} the tally is associated to
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * Sets a new {@link Location} association.
	 * 
	 * @param location is the new {@link Location}
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * Returns the {@link Product} the tally is associated to.
	 * 
	 * @return the {@link Product} the tally is associated to
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * Sets a new {@link Product} association.
	 * 
	 * @param product is the new {@link Product}
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"(%s@%s [id=%d, player=%s, location=%s, time=%s, product=%s, cancelled=%b])",
				this.getClass().getSimpleName(), Integer.toHexString(hashCode()), this.getId(), this.getPlayer(),
				this.getLocation(), this.getTime(), this.getProduct(),
				this.getCancelled());
	}
	
	/**
	 * Returns true if the tally is not cancelled.
	 * 
	 * @return true if the tally is not cancelled
	 */
	public boolean isValid() {
		return !this.getCancelled();
	}
}
