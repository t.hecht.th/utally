package de.utally.data.model;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Data class for Utally Events
 * 
 * @author Tobias Hecht
 *
 */
public class Event implements Identifiable, Auditable, HasEnabledState, Describable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5276559440710500876L;

	private Long id;

	private String title;

	private Integer playersFee;

	private Boolean enabled;

	private Instant creationTime;

	private Instant modificationTime;

	private Set<Location> locations = new HashSet<>();

	private Set<Team> teams = new HashSet<>();

	private String description;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Identifiable#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the event title.
	 * 
	 * @return the event title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets a new event title.
	 * 
	 * @param title
	 *            is the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the event players fee.
	 * 
	 * @return the players dee
	 */
	public Integer getPlayersFee() {
		return playersFee;
	}

	/**
	 * Sets a new event players fee.
	 * 
	 * @param playersFee
	 *            is the new players fee
	 */
	public void setPlayersFee(Integer playersFee) {
		this.playersFee = playersFee;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#getEnabled()
	 */
	@Override
	public Boolean getEnabled() {
		return enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#setEnabled(java.lang.Boolean)
	 */
	@Override
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getCreationTime()
	 */
	@Override
	public Instant getCreationTime() {
		return creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setCreationTime(java.time.Instant)
	 */
	@Override
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getModificationTime()
	 */
	@Override
	public Instant getModificationTime() {
		return modificationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setModificationTime(java.time.Instant)
	 */
	@Override
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/**
	 * Returns the {@link Location}s associated to the event.
	 * 
	 * @return the {@link Location}s associated to the event
	 */
	public Set<Location> getLocations() {
		return new HashSet<>(locations);
	}

	/**
	 * Sets new {@link Location}s associated to the event.
	 * 
	 * @param locations
	 *            are the new {@link Location}s associated to the event
	 */
	public void setLocations(Set<Location> locations) {
		this.locations = new HashSet<>(locations);
	}

	/**
	 * Returns the {@link Team}s associated to the event.
	 * 
	 * @return the {@link Team}s associated to the event
	 */
	public Set<Team> getTeams() {
		return new HashSet<>(teams);
	}

	/**
	 * Sets new {@link Team}s associated to the event.
	 * 
	 * @param teams
	 *            are the new {@link Team}s associated to the event
	 */
	public void setTeams(Set<Team> teams) {
		this.teams = new HashSet<>(teams);
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"(%s@%s [id=%d, title=%s, playersFee=%d, enabled=%b, locations=%s, teams=%s, creationTime=%s, modificationTime=%s])",
				this.getClass().getSimpleName(), Integer.toHexString(hashCode()), this.getId(), this.getTitle(),
				this.getPlayersFee(), this.getEnabled(), this.getLocations(), this.getTeams(), this.getCreationTime(), this.getModificationTime());
	}
}
