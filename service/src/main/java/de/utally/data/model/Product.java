package de.utally.data.model;

import java.time.Instant;

public class Product implements Identifiable, Auditable, HasEnabledState, Describable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3399878960309239334L;

	private Long id;

	private String name;

	private Integer price;

	private String imageName;

	private ProductCategory category;

	private Boolean enabled;

	private Instant creationTime;

	private Instant modificationTime;

	private String description;

	private Boolean displayStatistic;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Identifiable#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the product name.
	 * 
	 * @return the product name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets a new product name.
	 * 
	 * @param name
	 *            is the new product name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the product price.
	 * 
	 * @return the product price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * Sets a new product price
	 * 
	 * @param price
	 *            is the new product price
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	/**
	 * Returns the name of the product image
	 * 
	 * @return the name of the product image
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * Sets a new name of the product image
	 * 
	 * @param imageName
	 *            is the new name of the product image
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * Returns the {@link ProductCategory}.
	 * 
	 * @return the {@link ProductCategory}
	 */
	public ProductCategory getCategory() {
		return category;
	}

	/**
	 * Sets a new {@link ProductCategory}.
	 * 
	 * @param category
	 *            is the new {@link ProductCategory}
	 */
	public void setCategory(ProductCategory category) {
		this.category = category;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#getEnabled()
	 */
	@Override
	public Boolean getEnabled() {
		return enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#setEnabled(java.lang.Boolean)
	 */
	@Override
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getCreationTime()
	 */
	@Override
	public Instant getCreationTime() {
		return creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setCreationTime(java.time.Instant)
	 */
	@Override
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getModificationTime()
	 */
	@Override
	public Instant getModificationTime() {
		return modificationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setModificationTime(java.time.Instant)
	 */
	@Override
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Indicates whether this product shall be displayed in special statistic views.
	 * 
	 * @return whether this product shall be displayed in special statistic views
	 */
	public Boolean getDisplayStatistic() {
		return displayStatistic;
	}

	/**
	 * Sets whether this product shall be displayed in special statistic views.
	 * 
	 * @param displayStatistic
	 *            true, if this product shall be displayed in special statistic
	 *            views
	 */
	public void setDisplayStatistic(Boolean displayStatistic) {
		this.displayStatistic = displayStatistic;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"(%s@%s [id=%d, name=%s, price=%d, imageName=%s, category=%s, enabled=%b, creationTime=%s, modificationTime=%s, displayStatistic=%b])",
				this.getClass().getSimpleName(), Integer.toHexString(hashCode()), this.getId(), this.getName(),
				this.getPrice(), this.getImageName(), this.getCategory(), this.getEnabled(), this.getCreationTime(),
				this.getModificationTime(), this.getDisplayStatistic());
	}
}
