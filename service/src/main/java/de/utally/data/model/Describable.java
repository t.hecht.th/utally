package de.utally.data.model;

/**
 * Common methods for Utally data classes with description.
 * 
 * @author Tobias Hecht
 *
 */
public interface Describable {
	/**
	 * Returns the description
	 * 
	 * @return the description
	 */
	String getDescription();
	
	/**
	 * Sets a new description
	 * 
	 * @param description
	 *            is the new description
	 */
	void setDescription(String description);
}
