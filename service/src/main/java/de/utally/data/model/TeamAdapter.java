package de.utally.data.model;

import java.util.List;

/**
 * Interface to provide methods for {@link Team} management.
 * 
 * @author Tobias Hecht
 *
 */
public interface TeamAdapter {

	Team insertTeam(String fullName, String description, String city, String shortName, Boolean enabled);

	List<Team> getTeams();

	List<Team> getEnabledTeams();

	Team getTeamById(Long id);

	void updateTeam(Team team);

	void removeTeam(Long id);
}
