package de.utally.data.model;

import java.time.Instant;

/**
 * Data class for Utally Teams
 * 
 * @author Tobias Hecht
 *
 */
public class Team implements Identifiable, Auditable, HasEnabledState, Describable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1677621354762805557L;

	private Long id;
	
	private String fullName;
	
	private String city;
	
	private String shortName;
	
	private Boolean enabled;
	
	private Instant creationTime;
	
	private Instant modificationTime;
	
	private String description;
	
	/*
	 * (non-Javadoc)
	 * @see de.utally.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Identifiable#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Returns the full name of the team.
	 * 
	 * @return the full name of the team
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Sets a new full name of the team.
	 * 
	 * @param fullName is the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * Returns the city of the team.
	 * 
	 * @return the city of the team
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * Sets a new city of the team.
	 * 
	 * @param city is the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Returns the short name of the team.
	 * 
	 * @return the short name of the team
	 */
	public String getShortName() {
		return shortName;
	}
	
	/**
	 * Sets a new short name of the team.
	 * 
	 * @param shortName is the new short name
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#getEnabled()
	 */
	@Override
	public Boolean getEnabled() {
		return enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.HasEnabledState#setEnabled(java.lang.Boolean)
	 */
	@Override
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getCreationTime()
	 */
	@Override
	public Instant getCreationTime() {
		return creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setCreationTime(java.time.Instant)
	 */
	@Override
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#getModificationTime()
	 */
	@Override
	public Instant getModificationTime() {
		return modificationTime;
	}

	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Auditable#setModificationTime(java.time.Instant)
	 */
	@Override
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.utally.data.model.Describable#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"(%s@%s [id=%d, fullName=%s, shortName=%s, city=%s, enabled=%b, creationTime=%s, modificationTime=%s])",
				this.getClass().getSimpleName(), Integer.toHexString(hashCode()), this.getId(), this.getFullName(),
				this.getShortName(), this.getCity(), this.getEnabled(),
				this.getCreationTime(), this.getModificationTime());
	}
	
	/**
	 * Indeicates whether the given {@link Player} is in the team.
	 * 
	 * @param player is the {@link Player}
	 * @return true, if the {@link Player} is in the team
	 */
	public boolean contains(Player player) {
		if(player == null) {
			return false;
		}
		
		return this.equalsById(player.getTeam());
	}
}
