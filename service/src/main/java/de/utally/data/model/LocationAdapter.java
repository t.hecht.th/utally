package de.utally.data.model;

import java.util.List;

/**
 * Interface to provide methods for {@link Location} management.
 * 
 * @author Tobias Hecht
 *
 */
public interface LocationAdapter {

	Location insertLocation(String name, String description, Boolean enabled);

	boolean existsById(Long locationId);

	Location getLocation(Long id);

	List<Location> getLocations();

	List<Location> getEnabledLocations();

	void updateLocation(Location location);

	void removeLocation(Long id);

	void addProduct(Long id, Long productId);

	List<Product> getProducts(Long id);

	void removeProduct(Long id, Long productId);

	void removeProductFromAllLocations(Long productId);
}
