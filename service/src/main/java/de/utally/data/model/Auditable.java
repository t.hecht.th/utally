package de.utally.data.model;

import java.time.Instant;

/**
 * Common methods for Utally data classes with creation and modification times.
 * 
 * @author Tobias Hecht
 *
 */
public interface Auditable {
	/**
	 * Returns the creation time.
	 * 
	 * @return the creation time
	 */
	Instant getCreationTime();

	/**
	 * Sets a new creation time.
	 * 
	 * @param creationTime
	 *            is the new creation time
	 */
	void setCreationTime(Instant creationTime);

	/**
	 * Returns the last modification time.
	 * 
	 * @return the last modification time
	 */
	Instant getModificationTime();

	/**
	 * Sets a new modification time.
	 * 
	 * @param modificationTime
	 *            is the new modification time
	 */
	void setModificationTime(Instant modificationTime);
	
	/**
	 * Sets the modification time to {@link Instant}.now().
	 */
	default void updateModificationTime() {
		this.setModificationTime(Instant.now());
	}
}
