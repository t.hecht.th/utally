package de.utally.data.model;

import java.util.List;
import java.util.Map;

/**
 * Interface to provide methods for {@link Event} management.
 * 
 * @author Tobias Hecht
 *
 */
public interface EventAdapter {

	Event insertEvent(String title, String description, Boolean enabled, Integer playersFee);

	Event getEventById(Long id);

	Event getEventByTimeSlot(Long timeSlotId);

	List<Event> getEventsByLocation(Long locationId);

	List<Event> getEventsByProduct(Long productId);

	List<Event> getEventsByTeam(Long teamId);

	List<Event> getEvents();

	List<Event> getEnabledEvents();

	void updateEvent(Event event);

	void removeEvent(Long id);

	void addLocation(Long id, Long locationId);

	List<Location> getLocations(Long id);

	void removeLocation(Long id, Long locationId);

	void removeLocationFromAllEvents(Long locationId);

	void addTeam(Long id, Long teamId);

	List<Team> getTeams(Long id);

	void removeTeam(Long id, Long teamId);

	void removeTeamFromAllEvents(Long teamId);

	Map<Long, Map<Long, Long>> getNumberOfTalliesByTeamInEventForStatistics(Long id);
}
