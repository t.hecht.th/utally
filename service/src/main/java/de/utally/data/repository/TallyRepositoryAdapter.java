package de.utally.data.repository;

import de.utally.data.exception.*;
import de.utally.data.model.Tally;
import de.utally.data.model.TallyAdapter;
import de.utally.data.repository.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * {@link TallyAdapter} implementation to wrap a {@link TallyRepository}.
 * 
 * @author Tobias Hecht
 *
 */
@Transactional
@Component
public class TallyRepositoryAdapter implements TallyAdapter {
	/**
	 * 
	 */
	private static final String TALLY_ID_MUST_NOT_BE_NULL = "Tally id must not be null.";

	/**
	 * 
	 */
	private static final String TEAM_ID_MUST_NOT_BE_NULL = "Team id must not be null.";

	/**
	 * 
	 */
	private static final String EVENT_ID_MUST_NOT_BE_NULL = "Event id must not be null.";

	/**
	 * 
	 */
	private static final String LOCATION_ID_MUST_NOT_BE_NULL = "Location id must not be null.";

	/**
	 * 
	 */
	private static final String PRODUCT_ID_MUST_NOT_BE_NULL = "Product id must not be null.";

	/**
	 * 
	 */
	private static final String PLAYER_ID_MUST_NOT_BE_NULL = "Player id must not be null.";

	private static final Logger logger = LoggerFactory.getLogger(TallyRepositoryAdapter.class);

	private final TallyRepository tallyRepository;

	private final PlayerRepository playerRepository;

	private final ProductRepository productRepository;

	private final LocationRepository locationRepository;

	private final EventRepository eventRepository;

	private final TeamRepository teamRepository;

	private final TimeSlotRepository timeSlotRepository;

	public TallyRepositoryAdapter(TallyRepository tallyRepository, PlayerRepository playerRepository,
								  ProductRepository productRepository, LocationRepository locationRepository, EventRepository eventRepository,
								  TeamRepository teamRepository, TimeSlotRepository timeSlotRepository) {
		this.tallyRepository = tallyRepository;
		this.playerRepository = playerRepository;
		this.productRepository = productRepository;
		this.locationRepository = locationRepository;
		this.eventRepository = eventRepository;
		this.teamRepository = teamRepository;
		this.timeSlotRepository = timeSlotRepository;
	}

	@Override
	public Tally insertTally(Long playerId, Long productId, Long locationId) {
		Objects.requireNonNull(playerId, PLAYER_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(productId, PRODUCT_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		var player = playerRepository.findById(playerId).orElseThrow(PlayerNotFoundException::new);
		var product = productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
		var location = locationRepository.findById(locationId).orElseThrow(LocationNotFoundException::new);
		TallyEntity tallyEntity = new TallyEntity();
		tallyEntity.setLocation(location);
		tallyEntity.setPlayer(player);
		tallyEntity.setProduct(product);
		tallyEntity.setTime(Instant.now());
		logger.info("Inserting tally for player {} from team {} with product {} and location {}",
				player.getName(), player.getTeam().getFullName(), product.getName(), location.getName());
		return tallyRepository.save(tallyEntity).toTally();
	}

	@Override
	public Tally getTallyById(Long id) {
		Objects.requireNonNull(id, TALLY_ID_MUST_NOT_BE_NULL);
		return tallyRepository.findById(id).map(TallyEntity::toTally).orElseThrow(TallyNotFoundException::new);
	}

	@Override
	public List<Tally> getTalliesByPlayer(Long playerId) {
		Objects.requireNonNull(playerId, PLAYER_ID_MUST_NOT_BE_NULL);
		logger.debug("Returning tallies for player {}", playerId);
		if (!playerRepository.existsById(playerId)) {
			throw new PlayerNotFoundException();
		}
		return tallyRepository.findByPlayerId(playerId).stream().map(TallyEntity::toTally).toList();
	}

	@Override
	public List<Tally> getTalliesByProductAndPlayer(Long productId, Long playerId) {
		Objects.requireNonNull(productId, PRODUCT_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(playerId, PLAYER_ID_MUST_NOT_BE_NULL);
		if (!productRepository.existsById(productId)) {
			throw new ProductNotFoundException();
		}

		if (!playerRepository.existsById(playerId)) {
			throw new PlayerNotFoundException();
		}
		logger.debug("Returning tallies for product {} and player {}", productId, playerId);
		return tallyRepository.findByProductIdAndPlayerId(productId, playerId).stream().map(TallyEntity::toTally).toList();
	}

	@Override
	public List<Tally> getTalliesByProduct(Long productId) {
		Objects.requireNonNull(productId, PRODUCT_ID_MUST_NOT_BE_NULL);
		if (!productRepository.existsById(productId)) {
			throw new ProductNotFoundException();
		}

		return tallyRepository.findByProductId(productId).stream().map(TallyEntity::toTally).toList();
	}

	@Override
	public List<Tally> getTalliesByLocation(Long locationId) {
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		if (!locationRepository.existsById(locationId)) {
			throw new LocationNotFoundException();
		}
		return tallyRepository.findByLocationId(locationId).stream().map(TallyEntity::toTally).toList();
	}

	@Override
	public List<Tally> getTalliesByEvent(Long eventId) {
		Objects.requireNonNull(eventId, EVENT_ID_MUST_NOT_BE_NULL);
		var event = eventRepository.findById(eventId).orElseThrow(EventNotFoundException::new);
		List<Long> locationIds = event.getLocations().stream().map(LocationEntity::getId)
				.toList();
		return tallyRepository.findByLocationIdIn(locationIds).stream().map(TallyEntity::toTally).toList();
	}

	@Override
	public List<Tally> getTalliesByTimeSlot(Long timeSlotId) {
		Objects.requireNonNull(timeSlotId, "Timeslot id must not be null.");
		var timeSlot = timeSlotRepository.findById(timeSlotId).orElseThrow(TimeSlotNotFoundException::new);
		return tallyRepository.findByTimeBetween(timeSlot.getStart(), timeSlot.getEnd()).stream().map(TallyEntity::toTally).toList();
	}

	@Override
	public List<Tally> getTalliesByTeam(Long teamId) {
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		if (!teamRepository.existsById(teamId)) {
			throw new TeamNotFoundException();
		}
		return tallyRepository
				.findByPlayerTeamId(teamId)
				.stream()
				.map(TallyEntity::toTally)
				.toList();
	}

	@Override
	public List<Tally> getTalliesByLocationAndEvent(Long locationId, Long eventId) {
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(eventId, EVENT_ID_MUST_NOT_BE_NULL);
		if (!locationRepository.existsById(locationId)) {
			throw new LocationNotFoundException();
		}
		if (!eventRepository.existsById(eventId)) {
			throw new EventNotFoundException();
		}
		var talliesByLocation = this.getTalliesByLocation(locationId);
		return talliesByLocation.stream().filter(t -> t.getPlayer().getEvent().getId().equals(eventId))
				.toList();
	}

	@Override
	public List<Tally> getTalliesByProductAndEvent(Long productId, Long eventId) {
		Objects.requireNonNull(productId, PRODUCT_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(eventId, EVENT_ID_MUST_NOT_BE_NULL);
		if (!productRepository.existsById(productId)) {
			throw new ProductNotFoundException();
		}

		if (!eventRepository.existsById(eventId)) {
			throw new EventNotFoundException();
		}
		var talliesByProduct = this.getTalliesByProduct(productId);
		return talliesByProduct.stream().filter(t -> t.getPlayer().getEvent().getId().equals(eventId))
				.toList();
	}

	@Override
	public List<Tally> getTalliesByTeamAndEvent(Long teamId, Long eventId) {
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(eventId, EVENT_ID_MUST_NOT_BE_NULL);
		if (!teamRepository.existsById(teamId)) {
			throw new TeamNotFoundException();
		}

		if (!eventRepository.existsById(eventId)) {
			throw new EventNotFoundException();
		}
		var talliesByEvent = this.getTalliesByEvent(eventId);
		return talliesByEvent.stream().filter(t -> t.getPlayer().getTeam().getId().equals(teamId))
				.toList();
	}

	@Override
	public void cancelTally(Long tallyId) {
		logger.info("Cancelling tally {}", tallyId);
		setCancelled(tallyId, Boolean.TRUE);
	}

	@Override
	public void revokeCancellation(Long tallyId) {
		logger.debug("Revoke cancellation for tally {}", tallyId);
		setCancelled(tallyId, Boolean.FALSE);
	}

	private void setCancelled(Long tallyId, Boolean cancelled) {
		Objects.requireNonNull(tallyId, TALLY_ID_MUST_NOT_BE_NULL);
		var tally = tallyRepository.findById(tallyId).orElseThrow(TallyNotFoundException::new);
		tally.setCancelled(cancelled);
		tallyRepository.save(tally);
	}

	@Override
	public void removeTally(Long tallyId) {
		Objects.requireNonNull(tallyId, TALLY_ID_MUST_NOT_BE_NULL);
		logger.info("Deleting  tally {}", tallyId);
		tallyRepository.deleteById(tallyId);
	}

	@Override
	public List<Tally> getTallies() {
		List<Tally> tallies = new ArrayList<>();
		for (TallyEntity tallyEntity : tallyRepository.findAll()) {
			tallies.add(tallyEntity.toTally());
		}
		logger.debug("Returning all tallies.");
		return tallies;
	}

	@Override
	public List<Tally> getTalliesByLocationAndPlayer(Long locationId, Long playerId) {
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(playerId, PLAYER_ID_MUST_NOT_BE_NULL);
		if (!locationRepository.existsById(locationId)) {
			throw new LocationNotFoundException();
		}

		if (!playerRepository.existsById(playerId)) {
			throw new PlayerNotFoundException();
		}
		logger.debug("Returning tallies for location {} and player {}", locationId, playerId);
		return tallyRepository.findByLocationIdAndPlayerId(locationId, playerId).stream().map(TallyEntity::toTally)
				.toList();
	}

	@Override
	public void removeTalliesByProduct(Long productId) {
		Objects.requireNonNull(productId, PRODUCT_ID_MUST_NOT_BE_NULL);
		if (!productRepository.existsById(productId)) {
			throw new ProductNotFoundException();
		}
		tallyRepository.deleteByProductId(productId);
	}

	@Override
	public void removeTalliesByPlayer(Long playerId) {
		Objects.requireNonNull(playerId, PLAYER_ID_MUST_NOT_BE_NULL);
		if (!playerRepository.existsById(playerId)) {
			throw new PlayerNotFoundException();
		}
		tallyRepository.deleteByPlayerId(playerId);
	}

	@Override
	public void removeTalliesByLocation(Long locationId) {
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		if (!locationRepository.existsById(locationId)) {
			throw new LocationNotFoundException();
		}
		tallyRepository.deleteByLocationId(locationId);
	}
}
