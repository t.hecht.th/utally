package de.utally.data.repository.internal;

import de.utally.data.model.Team;

import jakarta.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity class for Utally Teams
 * 
 * @author Tobias Hecht
 *
 */
@Entity
@Table(name="utally_team")
public class TeamEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
	@SequenceGenerator(name = "id_generator", sequenceName = "HIBERNATE_SEQUENCE", allocationSize = 1)
	private Long id;
	
	@Column(name="full_name", length=128, nullable = false)
	private String fullName;
	
	@Column(name="city", length=64, nullable = false)
	private String city;
	
	@Column(name="short_name", length=64, nullable = false)
	private String shortName;
	
	@Column(name="enabled", nullable = false)
	private Boolean enabled;
	
	@Column(name="creation_time", nullable = false)
	private Instant creationTime;
	
	@Column(name="modification_time", nullable = false)
	private Instant modificationTime;
	
	@Column(name="description", length=2048)
	private String description;
	
	/**
	 * Returns the entity id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets a new id.
	 * 
	 * @param id
	 *            is the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Returns the full name of the team.
	 * 
	 * @return the full name of the team
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Sets a new full name of the team.
	 * 
	 * @param fullName is the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * Returns the city of the team.
	 * 
	 * @return the city of the team
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * Sets a new city of the team.
	 * 
	 * @param city is the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Returns the short name of the team.
	 * 
	 * @return the short name of the team
	 */
	public String getShortName() {
		return shortName;
	}
	
	/**
	 * Sets a new short name of the team.
	 * 
	 * @param shortName is the new short name
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * Returns true if the entity is enabled.
	 * 
	 * @return true if the entity is enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * Sets a new enabled state.
	 * 
	 * @param enabled
	 *            is the new enabled state
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the creation time.
	 * 
	 * @return the creation time
	 */
	public Instant getCreationTime() {
		return creationTime;
	}

	/**
	 * Sets a new creation time.
	 * 
	 * @param creationTime
	 *            is the new creation time
	 */
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * Returns the last modification time.
	 * 
	 * @return the last modification time
	 */
	public Instant getModificationTime() {
		return modificationTime;
	}

	/**
	 * Sets a new modification time.
	 * 
	 * @param modificationTime
	 *            is the new modification time
	 */
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/**
	 * Returns the description
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets a new description
	 * 
	 * @param description
	 *            is the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public static Set<Team> toTeams(Collection<TeamEntity> entities) {
		return entities.stream()
				.map(TeamEntity::toTeam)
				.collect(Collectors.toSet());
	}

	@Transient
	public Team toTeam() {
		var object = new Team();
		object.setCity(this.getCity());
		object.setFullName(this.getFullName());
		object.setId(this.getId());
		object.setShortName(this.getShortName());
		object.setCreationTime(this.getCreationTime());
		object.setEnabled(this.getEnabled());
		object.setModificationTime(this.getModificationTime());
		object.setDescription(this.getDescription());
		return object;
	}

	public static Set<TeamEntity> fromTeams(Collection<Team> objects) {
		return objects.stream().map(TeamEntity::fromTeam)
				.collect(Collectors.toSet());
	}

	public static TeamEntity fromTeam(Team object) {
		var entity = new TeamEntity();
		entity.setCity(object.getCity());
		entity.setFullName(object.getFullName());
		entity.setId(object.getId());
		entity.setShortName(object.getShortName());
		entity.setCreationTime(object.getCreationTime());
		entity.setEnabled(object.getEnabled());
		entity.setModificationTime(object.getModificationTime());
		entity.setDescription(object.getDescription());
		return entity;
	}
}
