package de.utally.data.repository;

import de.utally.data.exception.*;
import de.utally.data.model.Event;
import de.utally.data.model.EventAdapter;
import de.utally.data.model.Location;
import de.utally.data.model.Team;
import de.utally.data.repository.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * {@link EventAdapter} implementation to wrap an {@link EventRepository}.
 * 
 * @author Tobias Hecht
 *
 */
@Transactional
@Component
public class EventRepositoryAdapter implements EventAdapter {

	/**
	 * 
	 */
	private static final String LOCATION_ID_MUST_NOT_BE_NULL = "Location id must not be null.";

	/**
	 * 
	 */
	private static final String TIME_SLOT_ID_MUST_NOT_BE_NULL = "Time slot id must not be null.";

	/**
	 * 
	 */
	private static final String TEAM_ID_MUST_NOT_BE_NULL = "Team id must not be null.";

	/**
	 * 
	 */
	private static final String EVENT_ID_MUST_NOT_BE_NULL = "Event id must not be null.";

	private static final Logger logger = LoggerFactory.getLogger(EventRepositoryAdapter.class);

	private final EventRepository eventRepository;

	private final LocationRepository locationRepository;

	private final TimeSlotRepository timeSlotRepository;

	private final TeamRepository teamRepository;

	private final ProductRepository productRepository;

	private final PlayerRepository playerRepository;

	private final TallyRepository tallyRepository;

	public EventRepositoryAdapter(EventRepository eventRepository, LocationRepository locationRepository,
			TimeSlotRepository timeSlotRepository, TeamRepository teamRepository, ProductRepository productRepository,
			PlayerRepository playerRepository, TallyRepository tallyRepository) {
		this.eventRepository = eventRepository;
		this.locationRepository = locationRepository;
		this.timeSlotRepository = timeSlotRepository;
		this.teamRepository = teamRepository;
		this.productRepository = productRepository;
		this.playerRepository = playerRepository;
		this.tallyRepository = tallyRepository;
	}

	@Override
	public Event insertEvent(String title, String description, Boolean enabled, Integer playersFee) {
		Objects.requireNonNull(title, "Event title must not be null.");
		Objects.requireNonNull(enabled, "Event enabled state must not be null.");
		Objects.requireNonNull(playersFee, "Event players fee must not be null.");
		var eventEntity = new EventEntity();
		eventEntity.setTitle(title);
		eventEntity.setDescription(description);
		eventEntity.setCreationTime(Instant.now());
		eventEntity.setEnabled(enabled);
		eventEntity.setModificationTime(Instant.now());
		eventEntity.setPlayersFee(playersFee);
		logger.info("Inserting event with title {}", title);
		return eventRepository.save(eventEntity).toEvent();
	}

	@Override
	public void updateEvent(Event event) {
		Objects.requireNonNull(event, "Event must not be null.");
		var eventEntity = eventRepository.findById(event.getId()).orElseThrow(EventNotFoundException::new);
		eventEntity.setTitle(event.getTitle());
		eventEntity.setDescription(event.getDescription());
		eventEntity.setEnabled(event.getEnabled());
		eventEntity.setModificationTime(Instant.now());
		eventEntity.setPlayersFee(event.getPlayersFee());
		eventRepository.save(eventEntity);
	}

	@Override
	public Event getEventById(Long id) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		logger.debug("Returning event {}", id);
		return eventRepository.findById(id).map(EventEntity::toEvent).orElseThrow(EventNotFoundException::new);
	}

	@Override
	public Event getEventByTimeSlot(Long timeSlotId) {
		Objects.requireNonNull(timeSlotId, TIME_SLOT_ID_MUST_NOT_BE_NULL);
		var timeSlot = timeSlotRepository.findById(timeSlotId).orElseThrow(TimeSlotNotFoundException::new);
		return timeSlot.getEvent().toEvent();
	}

	@Override
	public List<Event> getEventsByLocation(Long locationId) {
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		var location = locationRepository.findById(locationId).orElseThrow(LocationNotFoundException::new);
		return eventRepository.findAllByLocation(location).stream().map(EventEntity::toEvent).toList();
	}

	@Override
	public List<Event> getEventsByProduct(Long productId) {
		Objects.requireNonNull(productId, "Product id must not be null.");
		var product = productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
		return eventRepository.findAllByProduct(product).stream().map(EventEntity::toEvent).toList();
	}

	@Override
	public List<Event> getEventsByTeam(Long teamId) {
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		var team = teamRepository.findById(teamId).orElseThrow(TeamNotFoundException::new);
		return eventRepository.findAllByTeam(team).stream().map(EventEntity::toEvent).toList();
	}

	@Override
	public List<Event> getEvents() {
		List<Event> events = new ArrayList<>();
		for (EventEntity eventEntity : eventRepository.findAll()) {
			events.add(eventEntity.toEvent());
		}
		logger.debug("Returning all events.");
		return events;
	}

	@Override
	public List<Event> getEnabledEvents() {
		logger.debug("Returning all enabled events.");
		return eventRepository.findByEnabled(true).stream().map(EventEntity::toEvent).toList();
	}

	@Override
	public void removeEvent(Long id) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		logger.info("Deleting event {}", id);
		timeSlotRepository.deleteAllByEventId(id);
		List<PlayerEntity> players = playerRepository.findByEventId(id);
		for (PlayerEntity player : players) {
			tallyRepository.deleteByPlayerId(player.getId());
		}
		playerRepository.deleteAll(players);

		eventRepository.deleteById(id);
	}

	@Override
	public void addLocation(Long id, Long locationId) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		var event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
		var location = locationRepository.findById(locationId).orElseThrow(LocationNotFoundException::new);
		logger.info("Inserting location {} for event {}", location.getName(), event.getTitle());
		var locations = new HashSet<>(event.getLocations());
		locations.add(location);
		event.setLocations(locations);
		event.setModificationTime(Instant.now());
		eventRepository.save(event);
	}

	@Override
	public List<Location> getLocations(Long id) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		var event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
		logger.debug("Returning locations for event {}", id);
		return event.getLocations().stream().map(LocationEntity::toLocation).toList();
	}

	@Override
	public void removeLocation(Long id, Long locationId) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		var event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
		var location = locationRepository.findById(locationId).orElseThrow(LocationNotFoundException::new);
		logger.info("Removing location {} from event {}", location.getName(), event.getTitle());
		var locations = new HashSet<>(event.getLocations());
		locations.remove(location);
		event.setLocations(locations);
		event.setModificationTime(Instant.now());
		eventRepository.save(event);
	}

	@Override
	public void addTeam(Long id, Long teamId) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		var event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
		var team = teamRepository.findById(teamId).orElseThrow(TeamNotFoundException::new);
		logger.info("Inserting team {} for event {}", team.getFullName(), event.getTitle());
		var teams = new HashSet<>(event.getTeams());
		teams.add(team);
		event.setTeams(teams);
		event.setModificationTime(Instant.now());
		eventRepository.save(event);
	}

	@Override
	public List<Team> getTeams(Long id) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		var event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
		logger.debug("Returning teams for event {}", id);
		return event.getTeams().stream().map(TeamEntity::toTeam).toList();
	}

	@Override
	public void removeTeam(Long id, Long teamId) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		var event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
		var team = teamRepository.findById(teamId).orElseThrow(TeamNotFoundException::new);
		logger.info("Removing team {} from event {}", team.getFullName(), event.getTitle());
		var teams = new HashSet<>(event.getTeams());
		teams.remove(team);
		event.setTeams(teams);
		event.setModificationTime(Instant.now());
		eventRepository.save(event);
	}

	@Override
	public void removeLocationFromAllEvents(Long locationId) {
		Objects.requireNonNull(locationId, LOCATION_ID_MUST_NOT_BE_NULL);
		if (!locationRepository.existsById(locationId)) {
			throw new LocationNotFoundException();
		}
		logger.info("Removing location {} from all events.", locationId);
		for (EventEntity eventEntity : eventRepository.findAll()) {
			var locations = new HashSet<>(eventEntity.getLocations());
			if (locations.removeIf(l -> l.getId().equals(locationId))) {
				eventEntity.setLocations(locations);
				eventEntity.setModificationTime(Instant.now());
				eventRepository.save(eventEntity);
			}
		}
	}

	@Override
	public void removeTeamFromAllEvents(Long teamId) {
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		if (!teamRepository.existsById(teamId)) {
			throw new TeamNotFoundException();
		}
		logger.info("Removing team {} from all events.", teamId);
		for (EventEntity eventEntity : eventRepository.findAll()) {
			var teams = new HashSet<>(eventEntity.getTeams());
			if (teams.removeIf(t -> t.getId().equals(teamId))) {
				eventEntity.setTeams(teams);
				eventEntity.setModificationTime(Instant.now());
				eventRepository.save(eventEntity);
			}
		}
	}

	@Override
	public Map<Long, Map<Long, Long>> getNumberOfTalliesByTeamInEventForStatistics(Long id) {
		Objects.requireNonNull(id, EVENT_ID_MUST_NOT_BE_NULL);
		var event = eventRepository.findById(id).orElseThrow(EventNotFoundException::new);
		var productIds = event
				.getLocations()
				.stream()
				.map(LocationEntity::getProducts)
				.flatMap(Collection::stream)
				.filter(ProductEntity::getDisplayStatistic)
				.map(ProductEntity::getId)
				.collect(Collectors.toSet());
		Map<Long, Map<Long, Long>> result = new HashMap<>();
		for (var productId : productIds) {
			Map<Long, Long> countByTeam = new HashMap<>();
			for (var team : event.getTeams()) {
				countByTeam.put(team.getId(), tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalse(team.getId(), productId));
			}
			result.put(productId, countByTeam);
		}

		return result;
	}
}
