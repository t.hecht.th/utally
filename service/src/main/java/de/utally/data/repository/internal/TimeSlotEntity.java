package de.utally.data.repository.internal;

import de.utally.data.model.TimeSlot;

import jakarta.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "utally_timeslot")
public class TimeSlotEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
	@SequenceGenerator(name = "id_generator", sequenceName = "HIBERNATE_SEQUENCE", allocationSize = 1)
	private Long id;
	
	@Column(name="slot_label", length=64, nullable = false)
	private String label;
	
	@Column(name="slot_start", nullable = false)
	private Instant start;
	
	@Column(name="slot_end", nullable = false)
	private Instant end;
	
	@Column(name="enabled", nullable = false)
	private Boolean enabled;
	
	@Column(name="creation_time", nullable = false)
	private Instant creationTime;
	
	@Column(name="modification_time", nullable = false)
	private Instant modificationTime;
	
	@Column(name="description", length=2048)
	private String description;

	@ManyToOne
	@JoinColumn(name = "event_id", referencedColumnName = "id")
	private EventEntity event;

	/**
	 * Returns the entity id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets a new id.
	 * 
	 * @param id
	 *            is the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the label of the time slot.
	 * 
	 * @return the label of the time slot
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Sets a new label of the time slot.
	 * 
	 * @param label is the new label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Returns the start time of the time slot.
	 * 
	 * @return the start time of the time slot
	 */
	public Instant getStart() {
		return start;
	}

	/**
	 * Sets a new start time of the time slot.
	 * 
	 * @param start is the new start time
	 */
	public void setStart(Instant start) {
		this.start = start;
	}

	/**
	 * Returns the end time of the time slot.
	 * 
	 * @return the end time of the time slot
	 */
	public Instant getEnd() {
		return end;
	}

	/**
	 * Sets a new end time of the time slot.
	 * 
	 * @param end is the new end time
	 */
	public void setEnd(Instant end) {
		this.end = end;
	}

	/**
	 * Returns true if the entity is enabled.
	 * 
	 * @return true if the entity is enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * Sets a new enabled state.
	 * 
	 * @param enabled
	 *            is the new enabled state
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the creation time.
	 * 
	 * @return the creation time
	 */
	public Instant getCreationTime() {
		return creationTime;
	}

	/**
	 * Sets a new creation time.
	 * 
	 * @param creationTime
	 *            is the new creation time
	 */
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * Returns the last modification time.
	 * 
	 * @return the last modification time
	 */
	public Instant getModificationTime() {
		return modificationTime;
	}

	/**
	 * Sets a new modification time.
	 * 
	 * @param modificationTime
	 *            is the new modification time
	 */
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/**
	 * Returns the description
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets a new description
	 * 
	 * @param description
	 *            is the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the {@link EventEntity}
	 *
	 * @return the {@link EventEntity}
	 */
	public EventEntity getEvent() {
		return event;
	}

	/**
	 * Sets a new {@link EventEntity}
	 *
	 * @param event
	 *            is the new {@link EventEntity}
	 */
	public void setEvent(EventEntity event) {
		this.event = event;
	}

	public static Set<TimeSlot> toTimeSlots(Collection<TimeSlotEntity> entities) {
		return entities.stream().map(TimeSlotEntity::toTimeSlot)
				.collect(Collectors.toSet());
	}

	@Transient
	public TimeSlot toTimeSlot() {
		var o = new TimeSlot();
		o.setEnd(this.getEnd());
		o.setId(this.getId());
		o.setLabel(this.getLabel());
		o.setStart(this.getStart());
		o.setCreationTime(this.getCreationTime());
		o.setEnabled(this.getEnabled());
		o.setModificationTime(this.getModificationTime());
		o.setDescription(this.getDescription());
		o.setEvent(this.getEvent().toEvent());
		return o;
	}

	public static Set<TimeSlotEntity> fromTimeSlots(Collection<TimeSlot> objects) {
		return objects.stream().map(TimeSlotEntity::fromTimeSlot)
				.collect(Collectors.toSet());
	}

	public static TimeSlotEntity fromTimeSlot(TimeSlot object) {
		var entity = new TimeSlotEntity();
		entity.setEnd(object.getEnd());
		entity.setId(object.getId());
		entity.setLabel(object.getLabel());
		entity.setStart(object.getStart());
		entity.setCreationTime(object.getCreationTime());
		entity.setEnabled(object.getEnabled());
		entity.setModificationTime(object.getModificationTime());
		entity.setDescription(object.getDescription());
		entity.setEvent(EventEntity.fromEvent(object.getEvent()));
		return entity;
	}
}
