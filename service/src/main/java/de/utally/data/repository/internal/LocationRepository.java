package de.utally.data.repository.internal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link CrudRepository} extension to manage {@link LocationEntity}s
 * 
 * @author Tobias Hecht
 *
 */
@Repository
public interface LocationRepository extends CrudRepository<LocationEntity, Long> {

	/**
	 * Returns all {@link LocationEntity}s with the given enabled state.
	 * 
	 * @param enabled
	 *            is the enabled state
	 * @return all matching {@link LocationEntity}s
	 */
	List<LocationEntity> findByEnabled(boolean enabled);
}
