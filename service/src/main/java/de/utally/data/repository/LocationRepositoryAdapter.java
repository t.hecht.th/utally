package de.utally.data.repository;

import de.utally.data.exception.LocationNotFoundException;
import de.utally.data.exception.ProductNotFoundException;
import de.utally.data.model.Location;
import de.utally.data.model.LocationAdapter;
import de.utally.data.model.Product;
import de.utally.data.repository.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

/**
 * {@link LocationAdapter} implementation to wrap a {@link LocationRepository}.
 * 
 * @author Tobias Hecht
 *
 */
@Transactional
@Component
public class LocationRepositoryAdapter implements LocationAdapter {
	/**
	 * 
	 */
	private static final String PRODUCT_ID_MUST_NOT_BE_NULL = "Product id must not be null.";

	/**
	 * 
	 */
	private static final String LOCATION_ID_MUST_NOT_BE_NULL = "Location id must not be null.";

	/**
	 * 
	 */
	private static final String LOCATION_MUST_NOT_BE_NULL = "Location must not be null.";

	/**
	 * 
	 */
	private static final String LOCATION_NAME_MUST_NOT_BE_NULL = "Location name must not be null.";

	private static final Logger logger = LoggerFactory.getLogger(LocationRepositoryAdapter.class);

	private final LocationRepository locationRepository;

	private final ProductRepository productRepository;

	private final TallyRepository tallyRepository;

	public LocationRepositoryAdapter(LocationRepository locationRepository, ProductRepository productRepository,
			TallyRepository tallyRepository) {
		this.locationRepository = locationRepository;
		this.productRepository = productRepository;
		this.tallyRepository = tallyRepository;
	}

	@Override
	public Location insertLocation(String name, String description, Boolean enabled) {
		Objects.requireNonNull(name, LOCATION_NAME_MUST_NOT_BE_NULL);
		Objects.requireNonNull(enabled, "Enabled state must not be null");
		var locationEntity = new LocationEntity();
		locationEntity.setName(name);
		locationEntity.setDescription(description);
		locationEntity.setCreationTime(Instant.now());
		locationEntity.setEnabled(enabled);
		locationEntity.setModificationTime(Instant.now());
		logger.info("Inserting location with name {}", name);
		return locationRepository.save(locationEntity).toLocation();
	}

	@Override
	public boolean existsById(Long locationId) {
		return locationRepository.existsById(locationId);
	}

	@Override
	public void updateLocation(Location location) {
		Objects.requireNonNull(location, LOCATION_MUST_NOT_BE_NULL);
		var locationEntity = locationRepository.findById(location.getId()).orElseThrow(LocationNotFoundException::new);
		locationEntity.setName(location.getName());
		locationEntity.setDescription(location.getDescription());
		locationEntity.setModificationTime(Instant.now());
		locationEntity.setEnabled(location.getEnabled());
		locationRepository.save(locationEntity);
	}

	@Override
	public Location getLocation(Long id) {
		Objects.requireNonNull(id, LOCATION_ID_MUST_NOT_BE_NULL);
		logger.debug("Returning location {}", id);
		return locationRepository.findById(id).map(LocationEntity::toLocation).orElseThrow(LocationNotFoundException::new);
	}

	@Override
	public void removeLocation(Long id) {
		Objects.requireNonNull(id, LOCATION_ID_MUST_NOT_BE_NULL);
		logger.info("Deleting location {}", id);
		tallyRepository.deleteByLocationId(id);
		locationRepository.deleteById(id);
	}

	@Override
	public void addProduct(Long id, Long productId) {
		Objects.requireNonNull(id, LOCATION_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(productId, PRODUCT_ID_MUST_NOT_BE_NULL);
		var location = locationRepository.findById(id).orElseThrow(LocationNotFoundException::new);
		var product = productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
		logger.info("Inserting product {} for location {}", product.getName(), location.getName());
		var productEntities = new HashSet<>(location.getProducts());
		productEntities.add(product);
		location.setProducts(productEntities);
		location.setModificationTime(Instant.now());
		locationRepository.save(location);
	}

	@Override
	public List<Product> getProducts(Long id) {
		Objects.requireNonNull(id, LOCATION_ID_MUST_NOT_BE_NULL);
		logger.debug("Returning products for location {}", id);
		return new ArrayList<>(locationRepository.findById(id).map(LocationEntity::getProducts)
				.map(ProductEntity::toProducts).orElseThrow(LocationNotFoundException::new));
	}

	@Override
	public void removeProduct(Long id, Long productId) {
		Objects.requireNonNull(id, LOCATION_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(productId, PRODUCT_ID_MUST_NOT_BE_NULL);
		var location = locationRepository.findById(id).orElseThrow(LocationNotFoundException::new);
		var product = productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
		logger.info("Removing product {} from location {}", product.getName(), location.getName());
		var productEntities = new HashSet<>(location.getProducts());
		productEntities.remove(product);
		location.setProducts(productEntities);
		location.setModificationTime(Instant.now());
		locationRepository.save(location);
	}

	@Override
	public List<Location> getLocations() {
		List<Location> locations = new ArrayList<>();
		for (LocationEntity locationEntity : locationRepository.findAll()) {
			locations.add(locationEntity.toLocation());
		}
		logger.debug("Returning all locations.");
		return locations;
	}

	@Override
	public List<Location> getEnabledLocations() {
		logger.debug("Returning all enabled locations.");
		return locationRepository.findByEnabled(true).stream().map(LocationEntity::toLocation).toList();
	}

	@Override
	public void removeProductFromAllLocations(Long productId) {
		Objects.requireNonNull(productId, PRODUCT_ID_MUST_NOT_BE_NULL);
		if (!productRepository.existsById(productId)) {
			throw new ProductNotFoundException();
		}
		logger.info("Removing product {} from all locations.", productId);
		for (LocationEntity locationEntity : locationRepository.findAll()) {
			var products = new HashSet<>(locationEntity.getProducts());
			if (products.removeIf(p -> p.getId().equals(productId))) {
				locationEntity.setProducts(products);
				locationRepository.save(locationEntity);
			}
		}
	}

}
