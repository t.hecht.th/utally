package de.utally.data.repository.internal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link CrudRepository} extension to manage {@link PlayerEntity}s
 * 
 * @author Tobias Hecht
 *
 */
@Repository
public interface PlayerRepository extends CrudRepository<PlayerEntity, Long> {

	/**
	 * Returns all {@link PlayerEntity}s associated to the given {@link EventEntity}
	 * id.
	 * 
	 * @param eventId
	 *            is the {@link EventEntity} id
	 * @return all {@link PlayerEntity}s associated to the given {@link EventEntity}
	 *         id
	 */
	List<PlayerEntity> findByEventId(Long eventId);

	/**
	 * Returns all {@link PlayerEntity}s associated to the given {@link EventEntity}
	 * and {@link TeamEntity} id.
	 * 
	 * @param teamId
	 *            is the {@link TeamEntity} id
	 * @param eventId
	 *            is the {@link EventEntity} id
	 * @return all {@link PlayerEntity}s associated to the given {@link EventEntity}
	 *         and {@link TeamEntity} id
	 */
	List<PlayerEntity> findByTeamIdAndEventId(Long teamId, Long eventId);

	/**
	 * Returns all {@link PlayerEntity}s with the given enabled state.
	 * 
	 * @param enabled
	 *            is the enabled state
	 * @return all matching {@link PlayerEntity}s
	 */
	List<PlayerEntity> findByEnabled(boolean enabled);

	/**
	 * Returns all {@link PlayerEntity}s associated to the given {@link TeamEntity}
	 * id.
	 * 
	 * @param teamId
	 *            is the {@link TeamEntity} id
	 * @return all {@link PlayerEntity}s associated to the given {@link TeamEntity}
	 *         id
	 */
	List<PlayerEntity> findByTeamId(Long teamId);

	/**
	 * Returns all {@link PlayerEntity}s associated to the given {@link EventEntity}
	 * id and {@link TeamEntity} ids.
	 * 
	 * @param teamIds
	 *            are the {@link TeamEntity} ids
	 * @param eventId
	 *            is the {@link EventEntity} id
	 * @return all {@link PlayerEntity}s associated to the given {@link EventEntity}
	 *         id and {@link TeamEntity} ids
	 */
	List<PlayerEntity> findByTeamIdInAndEventId(List<Long> teamIds, Long eventId);
}
