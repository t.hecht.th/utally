package de.utally.data.repository.internal;

import de.utally.data.model.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link CrudRepository} extension to manage {@link TimeSlotEntity}s
 * 
 * @author Tobias Hecht
 *
 */
@Repository
public interface TimeSlotRepository extends CrudRepository<TimeSlotEntity, Long> {
	
	/**
	 * Returns all {@link TimeSlotEntity}s with the given enabled state.
	 * 
	 * @param enabled
	 *            is the enabled state
	 * @return all matching {@link TimeSlotEntity}s
	 */
	List<TimeSlotEntity> findByEnabled(boolean enabled);

	/**
	 * Returns all {@link TimeSlotEntity}s associated to the {@link Event} with the given id.
	 *
	 * @param eventId
	 *            is the {@link Event} id
	 * @return all matching {@link TimeSlotEntity}s
	 */
	List<TimeSlotEntity> findByEventIdOrderByStart(long eventId);

	/**
	 * Deletes all {@link TimeSlotEntity} time slots entites associated to the {@link Event} with the given id.
	 *
	 * @param eventId is the {@link Event} id
	 */
	void deleteAllByEventId(long eventId);
}
