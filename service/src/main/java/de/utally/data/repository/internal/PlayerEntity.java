package de.utally.data.repository.internal;

import de.utally.data.model.Player;

import jakarta.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity class for Utally Players
 * 
 * @author Tobias Hecht
 *
 */
@Entity
@Table(name = "utally_player")
public class PlayerEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
	@SequenceGenerator(name = "id_generator", sequenceName = "HIBERNATE_SEQUENCE", allocationSize = 1)
	private Long id;

	@Column(name = "player_name", length = 64, nullable = false)
	private String name;

	@Column(name = "creation_time", nullable = false)
	private Instant creationTime;

	@Column(name = "modification_time", nullable = false)
	private Instant modificationTime;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "team_id", referencedColumnName = "id")
	private TeamEntity team;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_id", referencedColumnName = "id")
	private EventEntity event;

	@Column(name = "paid", nullable = false)
	private Boolean paid = Boolean.FALSE;

	@Column(name = "paid_with_playersfee", nullable = false)
	private Boolean withPlayersFee = Boolean.FALSE;

	@Column(name = "comment", length = 64, nullable = false)
	private String comment = "";

	@Column(name = "payed_at")
	private Instant payTime;

	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

	@Column(name = "paid_players_fee", nullable = false)
	private Integer paidPlayersFee;

	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name = "utally_preferences", joinColumns = @JoinColumn(name = "player_id"))
	@MapKeyColumn(name="key")
    @Column(name="value", length=1024)
	private Map<String, String> preferences = new HashMap<>();

	/**
	 * Returns the entity id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets a new id.
	 * 
	 * @param id
	 *            is the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the player name.
	 * 
	 * @return the player name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets a new player name.
	 * 
	 * @param name
	 *            is the new player name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the {@link TeamEntity} of the player.
	 * 
	 * @return the {@link TeamEntity}
	 */
	public TeamEntity getTeam() {
		return team;
	}

	/**
	 * Sets a new {@link TeamEntity} for the player.
	 * 
	 * @param team
	 *            is the new {@link TeamEntity}
	 */
	public void setTeam(TeamEntity team) {
		this.team = team;
	}

	/**
	 * Returns the {@link EventEntity} the player is associated to.
	 * 
	 * @return the {@link EventEntity}
	 */
	public EventEntity getEvent() {
		return event;
	}

	/**
	 * Sets a new {@link EventEntity} association to the player.
	 * 
	 * @param event
	 *            is the new {@link EventEntity}
	 */
	public void setEvent(EventEntity event) {
		this.event = event;
	}

	/**
	 * Returns true, if the player has paid.
	 * 
	 * @return true, if the player has paid
	 */
	public Boolean getPaid() {
		return paid;
	}

	/**
	 * Sets a new paid state.
	 * 
	 * @param paid
	 *            is the new paid state
	 */
	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	/**
	 * Returns true, if the player has paid with players fee.
	 * 
	 * @return true, if the player has paid with players fee
	 */
	public Boolean getWithPlayersFee() {
		return withPlayersFee;
	}

	/**
	 * Sets a new paid with players fee state.
	 * 
	 * @param withPlayersFee
	 *            is the new paid with players fee state
	 */
	public void setWithPlayersFee(Boolean withPlayersFee) {
		this.withPlayersFee = withPlayersFee;
	}

	/**
	 * Returns the comment added to the player.
	 * 
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets a new comment to the player
	 * 
	 * @param comment
	 *            is the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Returns the time when the player has paid.
	 * 
	 * @return the time when the player has paid
	 */
	public Instant getPayTime() {
		return payTime;
	}

	/**
	 * Sets a new time when the player has paid.
	 * 
	 * @param payTime
	 *            is the new time when the player has paid
	 */
	public void setPayTime(Instant payTime) {
		this.payTime = payTime;
	}

	/**
	 * Returns the creation time.
	 * 
	 * @return the creation time
	 */
	public Instant getCreationTime() {
		return creationTime;
	}

	/**
	 * Sets a new creation time.
	 * 
	 * @param creationTime
	 *            is the new creation time
	 */
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * Returns the last modification time.
	 * 
	 * @return the last modification time
	 */
	public Instant getModificationTime() {
		return modificationTime;
	}

	/**
	 * Sets a new modification time.
	 * 
	 * @param modificationTime
	 *            is the new modification time
	 */
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/**
	 * Returns true if the entity is enabled.
	 * 
	 * @return true if the entity is enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * Sets a new enabled state.
	 * 
	 * @param enabled
	 *            is the new enabled state
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the amount of players fee the player has paid.
	 * 
	 * @return the amount of players fee the player has paid
	 */
	public Integer getPaidPlayersFee() {
		return paidPlayersFee;
	}

	/**
	 * Sets a new amount of players fee the player has paid.
	 * 
	 * @param paidPlayersFee
	 *            is the new amount of players fee the player has paid
	 */
	public void setPaidPlayersFee(Integer paidPlayersFee) {
		this.paidPlayersFee = paidPlayersFee;
	}

	/**
	 * Returns a KV-{@link Map} of player preferences.
	 * 
	 * @return the preferences
	 */
	public Map<String, String> getPreferences() {
		return preferences;
	}

	/**
	 * Sets a new {@link Map} of player preferences.
	 * 
	 * @param preferences
	 *            are the new preferences
	 */
	public void setPreferences(Map<String, String> preferences) {
		this.preferences = preferences;
	}

	public static Set<Player> toPlayers(Collection<PlayerEntity> entities) {
		return entities.stream().map(PlayerEntity::toPlayer)
				.collect(Collectors.toSet());
	}

	@Transient
	public Player toPlayer() {
		var object = new Player();
		object.setComment(this.getComment());
		object.setId(this.getId());
		object.setName(this.getName());
		object.setPaid(this.getPaid());
		object.setPayTime(this.getPayTime());
		object.setCreationTime(this.getCreationTime());
		object.setModificationTime(this.getModificationTime());
		object.setPaidPlayersFee(this.getPaidPlayersFee());
		object.setPayTime(this.getPayTime());
		object.setEnabled(this.getEnabled());
		object.setTeam(this.getTeam().toTeam());
		object.setEvent(this.getEvent().toEvent());
		object.setWithPlayersFee(this.getWithPlayersFee());
		object.setPreferences(new HashMap<>(this.getPreferences()));
		return object;
	}

	public static Set<PlayerEntity> fromPlayers(Collection<Player> objects) {
		return objects.stream().map(PlayerEntity::fromPlayer)
				.collect(Collectors.toSet());
	}

	public static PlayerEntity fromPlayer(Player object) {
		var entity = new PlayerEntity();
		entity.setComment(object.getComment());
		entity.setId(object.getId());
		entity.setName(object.getName());
		entity.setPaid(object.getPaid());
		entity.setPayTime(object.getPayTime());
		entity.setCreationTime(object.getCreationTime());
		entity.setModificationTime(object.getModificationTime());
		entity.setPaidPlayersFee(object.getPaidPlayersFee());
		entity.setPayTime(object.getPayTime());
		entity.setEnabled(object.getEnabled());
		entity.setTeam(TeamEntity.fromTeam(object.getTeam()));
		entity.setEvent(EventEntity.fromEvent(object.getEvent()));
		entity.setWithPlayersFee(object.getWithPlayersFee());
		entity.setPreferences(new HashMap<>(object.getPreferences()));
		return entity;
	}
}
