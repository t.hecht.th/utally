package de.utally.data.repository;

import de.utally.data.exception.EventNotFoundException;
import de.utally.data.exception.TimeSlotNotFoundException;
import de.utally.data.model.TimeSlot;
import de.utally.data.model.TimeSlotAdapter;
import de.utally.data.repository.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * {@link TimeSlotAdapter} implementation to wrap a {@link TimeSlotRepository}.
 * 
 * @author Tobias Hecht
 *
 */
@Transactional
@Component
public class TimeSlotRepositoryAdapter implements TimeSlotAdapter {
	private static final Logger logger = LoggerFactory.getLogger(TimeSlotRepositoryAdapter.class);
	private static final String TIME_SLOT_ID_MUST_NOT_BE_NULL = "Time slot id must not be null.";

	private final TimeSlotRepository repository;

	private final EventRepository eventRepository;

	private final TallyRepository tallyRepository;

	public TimeSlotRepositoryAdapter(TimeSlotRepository repository, EventRepository eventRepository, TallyRepository tallyRepository) {
		this.repository = repository;
		this.eventRepository = eventRepository;
		this.tallyRepository = tallyRepository;
	}

	@Override
	public TimeSlot insertTimeSlot(String label, String description, Instant start, Instant end,
			Boolean enabled, Long eventId) {
		Objects.requireNonNull(eventId, "Event id must not be null.");
		var event = eventRepository.findById(eventId).orElseThrow(EventNotFoundException::new);

		TimeSlotEntity timeSlotEntity = new TimeSlotEntity();
		timeSlotEntity.setDescription(description);
		timeSlotEntity.setEnd(end);
		timeSlotEntity.setLabel(label);
		timeSlotEntity.setStart(start);
		timeSlotEntity.setCreationTime(Instant.now());
		timeSlotEntity.setEnabled(enabled);
		timeSlotEntity.setModificationTime(Instant.now());
		timeSlotEntity.setEvent(event);
		logger.info("Inserting time slot {} from {} to {}", label, start, end);
		return repository.save(timeSlotEntity).toTimeSlot();
	}

	@Override
	public void updateTimeSlot(TimeSlot timeSlot) {
		Objects.requireNonNull(timeSlot, "Time slot must not be null.");
		var timeSlotEntity = repository.findById(timeSlot.getId()).orElseThrow(TimeSlotNotFoundException::new);
		timeSlotEntity.setDescription(timeSlot.getDescription());
		timeSlotEntity.setEnd(timeSlot.getEnd());
		timeSlotEntity.setLabel(timeSlot.getLabel());
		timeSlotEntity.setStart(timeSlot.getStart());
		timeSlotEntity.setEnabled(timeSlot.getEnabled());
		timeSlotEntity.setModificationTime(Instant.now());
		repository.save(timeSlotEntity);
	}

	@Override
	public List<TimeSlot> getTimeSlots() {
		List<TimeSlot> timeSlots = new ArrayList<>();
		for (TimeSlotEntity timeSlotEntity : repository.findAll()) {
			timeSlots.add(timeSlotEntity.toTimeSlot());
		}
		logger.debug("Returning all time slots.");
		return timeSlots;
	}

	@Override
	public List<TimeSlot> getTimeSlotsByEvent(Long eventId) {
		Objects.requireNonNull(eventId, "Event id must not be null.");
		if (!eventRepository.existsById(eventId)) {
			throw new EventNotFoundException();
		}
		return repository
				.findByEventIdOrderByStart(eventId)
				.stream()
				.map(TimeSlotEntity::toTimeSlot)
				.toList();
	}

	@Override
	public List<TimeSlot> getEnabledTimeSlots() {
		logger.debug("Returning all enabled time slots.");
		return repository.findByEnabled(true).stream().map(TimeSlotEntity::toTimeSlot).toList();
	}

	@Override
	public TimeSlot getTimeSlotById(Long id) {
		Objects.requireNonNull(id, TIME_SLOT_ID_MUST_NOT_BE_NULL);
		logger.debug("Returning time slot {}", id);
		return repository.findById(id).map(TimeSlotEntity::toTimeSlot).orElseThrow(TimeSlotNotFoundException::new);
	}

	@Override
	public void removeTimeSlot(Long id) {
		Objects.requireNonNull(id, TIME_SLOT_ID_MUST_NOT_BE_NULL);
		logger.info("Deleting time slot {}", id);
		repository.deleteById(id);
	}

	@Override
	public Map<Long, Map<Long, Long>> getNumberOfTalliesByTeamInTimeSlotForStatistics(Long id) {
		Objects.requireNonNull(id, TIME_SLOT_ID_MUST_NOT_BE_NULL);
		var timeSlot = repository.findById(id).orElseThrow(TimeSlotNotFoundException::new);
		var event = timeSlot.getEvent();
		var productIds = event
				.getLocations()
				.stream()
				.map(LocationEntity::getProducts)
				.flatMap(Collection::stream)
				.filter(ProductEntity::getDisplayStatistic)
				.map(ProductEntity::getId)
				.collect(Collectors.toSet());
		Map<Long, Map<Long, Long>> result = new HashMap<>();
		for (var productId : productIds) {
			Map<Long, Long> countByTeam = new HashMap<>();
			for (var team : event.getTeams()) {
				countByTeam.put(team.getId(), tallyRepository.countAllByPlayerTeamIdAndProductIdAndCancelledFalseAndTimeBetween(team.getId(), productId, timeSlot.getStart(), timeSlot.getEnd()));
			}
			result.put(productId, countByTeam);
		}

		return result;
	}
}
