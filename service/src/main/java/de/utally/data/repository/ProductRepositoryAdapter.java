package de.utally.data.repository;

import de.utally.data.exception.EventNotFoundException;
import de.utally.data.exception.ProductNotFoundException;
import de.utally.data.model.Product;
import de.utally.data.model.ProductAdapter;
import de.utally.data.model.ProductCategory;
import de.utally.data.repository.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * {@link ProductAdapter} implementation to wrap a {@link ProductRepository}.
 * 
 * @author Tobias Hecht
 *
 */
@Transactional
@Component
public class ProductRepositoryAdapter implements ProductAdapter {
	private static final Logger logger = LoggerFactory.getLogger(ProductRepositoryAdapter.class);

	private final ProductRepository repository;

	private final TallyRepository tallyRepository;

	private final EventRepository eventRepository;

	public ProductRepositoryAdapter(ProductRepository productRepository, TallyRepository tallyRepository, EventRepository eventRepository) {
		this.repository = productRepository;
		this.tallyRepository = tallyRepository;
		this.eventRepository = eventRepository;
	}

	@Override
	public Product insertProduct(String name, String description, Integer price, String imageName,
			ProductCategory productCategory, Boolean enabled, Boolean displayStatistic) {
		Objects.requireNonNull(name, "Product name must not be null.");
		Objects.requireNonNull(price, "Price must not be null.");
		Objects.requireNonNull(imageName, "Image name must not be null.");
		Objects.requireNonNull(productCategory, "Category must not be null.");
		ProductEntity productEntity = new ProductEntity();
		productEntity.setCategory(productCategory);
		productEntity.setDescription(description);
		productEntity.setImageName(imageName);
		productEntity.setName(name);
		productEntity.setPrice(price);
		productEntity.setCreationTime(Instant.now());
		productEntity.setEnabled(enabled);
		productEntity.setDisplayStatistic(displayStatistic);
		productEntity.setModificationTime(Instant.now());
		logger.info("Inserting product {} with price {}, image {} and category {}", name, price, imageName,
				productCategory);
		return repository.save(productEntity).toProduct();
	}

	@Override
	public boolean existsById(Long productId) {
		return repository.existsById(productId);
	}

	@Override
	public void updateProduct(Product product) {
		Objects.requireNonNull(product, "Product must not be null");
		var productEntity = repository.findById(product.getId()).orElseThrow(ProductNotFoundException::new);
		productEntity.setCategory(product.getCategory());
		productEntity.setDescription(product.getDescription());
		productEntity.setImageName(product.getImageName());
		productEntity.setName(product.getName());
		productEntity.setPrice(product.getPrice());
		productEntity.setEnabled(product.getEnabled());
		productEntity.setDisplayStatistic(product.getDisplayStatistic());
		productEntity.setModificationTime(Instant.now());
		repository.save(productEntity);
	}

	@Override
	public List<Product> getProducts() {
		List<Product> products = new ArrayList<>();
		for (ProductEntity productEntity : repository.findAll()) {
			products.add(productEntity.toProduct());
		}
		logger.debug("Returning all products.");
		return products;
	}

	@Override
	public Set<Product> getProductsByEvent(Long eventId) {
		Objects.requireNonNull(eventId, "Event id must not be null");
		var event = eventRepository.findById(eventId);
		if (event.isEmpty()) {
			throw new EventNotFoundException();
		}
		var products = event
				.get()
				.getLocations()
				.stream()
				.map(LocationEntity::getProducts)
				.flatMap(Collection::stream)
				.collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ProductEntity::getId))), ArrayList::new));

		return ProductEntity.toProducts(products);
	}

	@Override
	public List<Product> getEnabledProducts() {
		logger.debug("Returning all enabled products.");
		return repository.findByEnabled(true).stream().map(ProductEntity::toProduct).toList();
	}

	@Override
	public List<Product> getProductByIds(Collection<Long> productIds) {
		Objects.requireNonNull(productIds, "Product ids must not be null");
		List<Product> products = new ArrayList<>();
		for (ProductEntity productEntity : repository.findAllById(productIds)) {
			products.add(productEntity.toProduct());
		}
		return products;
	}

	@Override
	public void removeProduct(Long productId) {
		Objects.requireNonNull(productId, "Product id must not be null.");
		logger.info("Deleting product {}", productId);
		tallyRepository.deleteByProductId(productId);
		repository.deleteById(productId);
	}

	@Override
	public Product getProductById(Long productId) {
		Objects.requireNonNull(productId, "Product id must not be null.");
		logger.debug("Returning product {}", productId);
		return repository.findById(productId).map(ProductEntity::toProduct).orElseThrow(ProductNotFoundException::new);
	}

}
