package de.utally.data.repository;

import de.utally.data.exception.EventNotFoundException;
import de.utally.data.exception.PlayerNotFoundException;
import de.utally.data.exception.PlayersNotOfSameEventException;
import de.utally.data.exception.TeamNotFoundException;
import de.utally.data.model.Player;
import de.utally.data.model.PlayerAdapter;
import de.utally.data.repository.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * {@link PlayerAdapter} implementation to wrap a {@link PlayerRepository}.
 * 
 * @author Tobias Hecht
 *
 */
@Transactional
@Component
public class PlayerRepositoryAdapter implements PlayerAdapter {
	/**
	 * 
	 */
	private static final String PLAYER_ID_MUST_NOT_BE_NULL = "Player id must not be null.";

	/**
	 * 
	 */
	private static final String TEAM_IDS_MUST_NOT_BE_NULL = "Team ids must not be null.";

	/**
	 * 
	 */
	private static final String PLAYER_MUST_NOT_BE_NULL = "Player must not be null.";

	/**
	 * 
	 */
	private static final String EVENT_ID_MUST_NOT_BE_NULL = "Event id must not be null.";

	/**
	 * 
	 */
	private static final String TEAM_ID_MUST_NOT_BE_NULL = "Team id must not be null.";

	/**
	 * 
	 */
	private static final String PLAYER_NAME_MUST_NOT_BE_NULL = "Player name must not be null.";

	private static final Logger logger = LoggerFactory.getLogger(PlayerRepositoryAdapter.class);

	private final PlayerRepository playerRepository;

	private final TeamRepository teamRepository;

	private final EventRepository eventRepository;

	private final TallyRepository tallyRepository;

	public PlayerRepositoryAdapter(PlayerRepository playerRepository, TeamRepository teamRepository,
			EventRepository eventRepository, TallyRepository tallyRepository) {
		this.playerRepository = playerRepository;
		this.teamRepository = teamRepository;
		this.eventRepository = eventRepository;
		this.tallyRepository = tallyRepository;
	}

	@Override
	public Player insertPlayer(String name, Long teamId, Long eventId, Boolean enabled) {
		Objects.requireNonNull(name, PLAYER_NAME_MUST_NOT_BE_NULL);
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(eventId, EVENT_ID_MUST_NOT_BE_NULL);
		var teamEntity = teamRepository.findById(teamId).orElseThrow(TeamNotFoundException::new);
		var eventEntity = eventRepository.findById(eventId).orElseThrow(EventNotFoundException::new);
		PlayerEntity playerEntity = new PlayerEntity();
		playerEntity.setName(name);
		playerEntity.setCreationTime(Instant.now());
		playerEntity.setModificationTime(Instant.now());
		playerEntity.setEnabled(enabled);
		playerEntity.setPaidPlayersFee(0);
		playerEntity.setTeam(teamEntity);
		playerEntity.setEvent(eventEntity);
		logger.info("Inserting player {} for team {}", name, teamEntity.getFullName());
		return playerRepository.save(playerEntity).toPlayer();
	}

	@Override
	public boolean existsById(Long playerId) {
		return playerRepository.existsById(playerId);
	}

	@Override
	public void updatePlayer(Player player) {
		Objects.requireNonNull(player, PLAYER_MUST_NOT_BE_NULL);
		var playerEntity = playerRepository.findById(player.getId()).orElseThrow(PlayerNotFoundException::new);
		playerEntity.setName(player.getName());
		playerEntity.setEnabled(player.getEnabled());
		playerEntity.setPreferences(new HashMap<>(player.getPreferences()));
		playerEntity.setModificationTime(Instant.now());
		playerEntity.setComment(player.getComment());
		playerRepository.save(playerEntity);
	}

	@Override
	public List<Player> getPlayersByEvent(Long eventId) {
		Objects.requireNonNull(eventId, EVENT_ID_MUST_NOT_BE_NULL);
		logger.debug("Returning players for event {}", eventId);
		if (!eventRepository.existsById(eventId)) {
			throw new EventNotFoundException();
		}
		return playerRepository.findByEventId(eventId).stream().map(PlayerEntity::toPlayer).toList();
	}

	@Override
	public List<Player> getPlayersByTeamAndEvent(Long teamId, Long eventId) {
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(eventId, EVENT_ID_MUST_NOT_BE_NULL);
		if (!teamRepository.existsById(teamId)) {
			throw new TeamNotFoundException();
		}
		if (!eventRepository.existsById(eventId)) {
			throw new EventNotFoundException();
		}
		logger.debug("Returning players of team {} for event {}", teamId, eventId);
		return playerRepository.findByTeamIdAndEventId(teamId, eventId).stream().map(PlayerEntity::toPlayer).toList();
	}

	@Override
	public List<Player> getPlayersByTeam(Long teamId) {
		Objects.requireNonNull(teamId, TEAM_ID_MUST_NOT_BE_NULL);
		if (!teamRepository.existsById(teamId)) {
			throw new TeamNotFoundException();
		}
		logger.debug("Returning players of team {}", teamId);
		return playerRepository.findByTeamId(teamId).stream().map(PlayerEntity::toPlayer).toList();
	}

	@Override
	public List<Player> getPlayersByTeamsAndEvent(List<Long> teamIds, Long eventId) {
		Objects.requireNonNull(teamIds, TEAM_IDS_MUST_NOT_BE_NULL);
		Objects.requireNonNull(eventId, EVENT_ID_MUST_NOT_BE_NULL);
		if (!eventRepository.existsById(eventId)) {
			throw new EventNotFoundException();
		}
		logger.debug("Returning players of teams {} and event {}.", teamIds, eventId);
		return playerRepository.findByTeamIdInAndEventId(teamIds, eventId).stream().map(PlayerEntity::toPlayer).toList();
	}

	@Override
	public List<Player> getPlayers() {
		List<Player> players = new ArrayList<>();
		for (PlayerEntity playerEntity : playerRepository.findAll()) {
			players.add(playerEntity.toPlayer());
		}
		logger.debug("Returning all players.");
		return players;
	}

	@Override
	public List<Player> getEnabledPlayers() {
		logger.debug("Returning all enabled players.");
		return playerRepository.findByEnabled(true).stream().map(PlayerEntity::toPlayer).toList();
	}

	@Override
	public void pay(Long playerId, String comment, Boolean withPlayersFee, Integer paidPlayersFee) {
		Objects.requireNonNull(playerId, PLAYER_ID_MUST_NOT_BE_NULL);
		Objects.requireNonNull(comment, "Comment must not be null.");
		Objects.requireNonNull(withPlayersFee, "With players fee flag must not be null.");
		Objects.requireNonNull(paidPlayersFee, "Paid players fee must not be null.");
		var playerEntity = playerRepository.findById(playerId).orElseThrow(PlayerNotFoundException::new);
		logger.info("Player {} pays with players fee {} and comment {}", playerEntity.getName(), withPlayersFee, comment);
		playerEntity.setPaid(Boolean.TRUE);
		playerEntity.setWithPlayersFee(withPlayersFee);
		playerEntity.setComment(comment);
		playerEntity.setPaidPlayersFee(paidPlayersFee);
		var now = Instant.now();
		playerEntity.setModificationTime(now);
		playerEntity.setPayTime(now);
		playerRepository.save(playerEntity);
	}

	@Override
	public void revertPayment(Long playerId) {
		Objects.requireNonNull(playerId, PLAYER_ID_MUST_NOT_BE_NULL);
		var playerEntity = playerRepository.findById(playerId).orElseThrow(PlayerNotFoundException::new);
		logger.info("Player {} reverts the payment.", playerEntity.getName());
		playerEntity.setPaid(Boolean.FALSE);
		playerEntity.setWithPlayersFee(Boolean.FALSE);
		playerEntity.setModificationTime(Instant.now());
		playerEntity.setPaidPlayersFee(0);
		playerEntity.setPayTime(null);
		playerRepository.save(playerEntity);
	}

	@Override
	public void removePlayer(Long id) {
		Objects.requireNonNull(id, PLAYER_ID_MUST_NOT_BE_NULL);
		logger.info("Deleting player {}", id);
		tallyRepository.deleteByPlayerId(id);
		playerRepository.deleteById(id);
	}

	@Override
	public Player getPlayerById(Long id) {
		Objects.requireNonNull(id, PLAYER_ID_MUST_NOT_BE_NULL);
		logger.debug("Returning player {}", id);
		return playerRepository.findById(id).map(PlayerEntity::toPlayer).orElseThrow(PlayerNotFoundException::new);
	}

	@Override
	public void mergePlayerIntoPlayer(Long sourceId, Long destinationId) {
		Objects.requireNonNull(sourceId, "Source player id must not be null.");
		Objects.requireNonNull(destinationId, "Destination player id must not be null.");

		var source = playerRepository.findById(sourceId).orElseThrow(PlayerNotFoundException::new);
		var destination = playerRepository.findById(destinationId).orElseThrow(PlayerNotFoundException::new);
		if (!source.getEvent().getId().equals(destination.getEvent().getId())) {
			throw new PlayersNotOfSameEventException();
		}
		logger.info("Merging player {} into player {}", source.getName(), destination.getName());
		var tallyEntities = tallyRepository.findByPlayerId(sourceId);
		tallyEntities.forEach(t -> t.setPlayer(destination));
		tallyRepository.saveAll(tallyEntities);
		logger.info("Deleting player {}", source.getName());
		playerRepository.delete(source);
	}
}
