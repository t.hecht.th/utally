package de.utally.data.repository.internal;

import de.utally.data.model.Location;

import jakarta.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity class for Utally locations
 * 
 * @author Tobias Hecht
 *
 */
@Entity
@Table(name="utally_location")
public class LocationEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
	@SequenceGenerator(name = "id_generator", sequenceName = "HIBERNATE_SEQUENCE", allocationSize = 1)
	private Long id;
	
	@Column(name="location_name", length=64, nullable = false)
	private String name;
	
	@Column(name="enabled", nullable = false)
	private Boolean enabled;
	
	@Column(name="creation_time", nullable = false)
	private Instant creationTime;
	
	@Column(name="modification_time", nullable = false)
	private Instant modificationTime;
	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "utally_location_product_mapping", joinColumns = @JoinColumn(name = "location_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"))
	private Set<ProductEntity> products = new HashSet<>();
	
	@Column(name="description", length=2048)
	private String description;

	/**
	 * Returns the entity id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets a new id.
	 * 
	 * @param id
	 *            is the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the location name.
	 * 
	 * @return the location name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets a new location name.
	 * 
	 * @param name is the new location name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns true if the entity is enabled.
	 * 
	 * @return true if the entity is enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * Sets a new enabled state.
	 * 
	 * @param enabled
	 *            is the new enabled state
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the creation time.
	 * 
	 * @return the creation time
	 */
	public Instant getCreationTime() {
		return creationTime;
	}

	/**
	 * Sets a new creation time.
	 * 
	 * @param creationTime
	 *            is the new creation time
	 */
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * Returns the last modification time.
	 * 
	 * @return the last modification time
	 */
	public Instant getModificationTime() {
		return modificationTime;
	}

	/**
	 * Sets a new modification time.
	 * 
	 * @param modificationTime
	 *            is the new modification time
	 */
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/**
	 * Returns the {@link ProductEntity}s associated to the location.
	 * 
	 * @return the {@link ProductEntity}s associated to the location
	 */
	public Set<ProductEntity> getProducts() {
		return new HashSet<>(products);
	}

	/**
	 * Sets new {@link ProductEntity}s associated to the location.
	 * 
	 * @param products
	 *            are the new {@link ProductEntity}s associated to the location
	 */
	public void setProducts(Set<ProductEntity> products) {
		this.products = new HashSet<>(products);
	}

	/**
	 * Returns the description
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets a new description
	 * 
	 * @param description
	 *            is the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public static Set<Location> toLocations(Collection<LocationEntity> entities) {
		return entities.stream().map(LocationEntity::toLocation)
				.collect(Collectors.toSet());
	}

	@Transient
	public Location toLocation() {
		var object = new Location();
		object.setId(this.getId());
		object.setName(this.getName());
		object.setProducts(ProductEntity.toProducts(this.getProducts()));
		object.setCreationTime(this.getCreationTime());
		object.setModificationTime(this.getModificationTime());
		object.setEnabled(this.getEnabled());
		object.setDescription(this.getDescription());
		return object;
	}

	public static Set<LocationEntity> fromLocations(Collection<Location> objects) {
		return objects.stream().map(LocationEntity::fromLocation)
				.collect(Collectors.toSet());
	}

	public static LocationEntity fromLocation(Location object) {
		var e = new LocationEntity();
		e.setId(object.getId());
		e.setName(object.getName());
		e.setProducts(ProductEntity.fromProducts(object.getProducts()));
		e.setCreationTime(object.getCreationTime());
		e.setModificationTime(object.getModificationTime());
		e.setEnabled(object.getEnabled());
		e.setDescription(object.getDescription());
		return e;
	}
}
