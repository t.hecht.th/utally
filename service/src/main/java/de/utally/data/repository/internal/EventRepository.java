package de.utally.data.repository.internal;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link CrudRepository} extension to manage {@link EventEntity}s
 * 
 * @author Tobias Hecht
 *
 */
@Repository
public interface EventRepository extends CrudRepository<EventEntity, Long> {

	/**
	 * Returns all {@link EventEntity}s with the given enabled state.
	 * 
	 * @param enabled
	 *            is the enabled state
	 * @return all matching {@link EventEntity}s
	 */
	List<EventEntity> findByEnabled(boolean enabled);

	/**
	 * Returns all {@link EventEntity}s which are associated to the given
	 * {@link LocationEntity}.
	 * 
	 * @param locationEntity
	 *            is the {@link LocationEntity}
	 * @return all {@link EventEntity}s which are associated to the given
	 *         {@link LocationEntity}
	 */
	@Query("SELECT DISTINCT e FROM EventEntity AS e WHERE :locationEntity MEMBER OF e.locations")
	List<EventEntity> findAllByLocation(@Param("locationEntity") LocationEntity locationEntity);

	/**
	 * Returns all {@link EventEntity}s which are associated to the given
	 * {@link TeamEntity}.
	 * 
	 * @param teamEntity
	 *            is the {@link TeamEntity}
	 * @return all {@link EventEntity}s which are associated to the given
	 *         {@link TeamEntity}
	 */
	@Query("SELECT DISTINCT e FROM EventEntity AS e WHERE :teamEntity MEMBER OF e.teams")
	List<EventEntity> findAllByTeam(@Param("teamEntity") TeamEntity teamEntity);

	/**
	 * Returns all {@link EventEntity}s which are associated to the given
	 * {@link ProductEntity}.
	 * 
	 * @param productEntity
	 *            is the {@link ProductEntity}
	 * @return all {@link EventEntity}s which are associated to the given
	 *         {@link ProductEntity}
	 */
	@Query("SELECT DISTINCT e FROM EventEntity e LEFT JOIN e.locations l WHERE :productEntity MEMBER OF l.products")
	List<EventEntity> findAllByProduct(@Param("productEntity") ProductEntity productEntity);
}
