package de.utally.data.repository.internal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * {@link CrudRepository} extension to manage {@link TeamEntity}s
 * 
 * @author Tobias Hecht
 *
 */
@Repository
public interface TeamRepository extends CrudRepository<TeamEntity, Long> {
	
	/**
	 * Returns all {@link TeamEntity}s with the given enabled state.
	 * 
	 * @param enabled
	 *            is the enabled state
	 * @return all matching {@link TeamEntity}s
	 */
	List<TeamEntity> findByEnabled(boolean enabled);
}
