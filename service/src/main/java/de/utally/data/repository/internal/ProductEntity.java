package de.utally.data.repository.internal;

import de.utally.data.model.Product;
import de.utally.data.model.ProductCategory;
import jakarta.persistence.*;

import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity class for Utally Products
 * 
 * @author Tobias Hecht
 *
 */
@Entity
@Table(name="utally_product")
public class ProductEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
	@SequenceGenerator(name = "id_generator", sequenceName = "HIBERNATE_SEQUENCE", allocationSize = 1)
	private Long id;
	
	@Column(name="name", length=64, nullable = false)
	private String name;
	
	@Column(name="price", nullable = false)
	private Integer price;
	
	@Column(name="image", length=128, nullable = false)
	private String imageName;
	
	@Column(name="category", nullable = false)
	@Enumerated(EnumType.STRING)
	private ProductCategory category;
	
	@Column(name="enabled", nullable = false)
	private Boolean enabled;
	
	@Column(name="creation_time", nullable = false)
	private Instant creationTime;
	
	@Column(name="modification_time", nullable = false)
	private Instant modificationTime;
	
	@Column(name="description", length=2048)
	private String description;
	
	@Column(name="display_statistic", nullable = false)
	private Boolean displayStatistic;

	/**
	 * Returns the entity id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets a new id.
	 * 
	 * @param id
	 *            is the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the product name.
	 * 
	 * @return the product name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets a new product name.
	 * 
	 * @param name
	 *            is the new product name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the product price.
	 * 
	 * @return the product price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * Sets a new product price
	 * 
	 * @param price
	 *            is the new product price
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	/**
	 * Returns the name of the product image
	 * 
	 * @return the name of the product image
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * Sets a new name of the product image
	 * 
	 * @param imageName
	 *            is the new name of the product image
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * Returns the {@link ProductCategory}.
	 * 
	 * @return the {@link ProductCategory}
	 */
	public ProductCategory getCategory() {
		return category;
	}

	/**
	 * Sets a new {@link ProductCategory}.
	 * 
	 * @param category
	 *            is the new {@link ProductCategory}
	 */
	public void setCategory(ProductCategory category) {
		this.category = category;
	}

	/**
	 * Returns true if the entity is enabled.
	 * 
	 * @return true if the entity is enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * Sets a new enabled state.
	 * 
	 * @param enabled
	 *            is the new enabled state
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the creation time.
	 * 
	 * @return the creation time
	 */
	public Instant getCreationTime() {
		return creationTime;
	}

	/**
	 * Sets a new creation time.
	 * 
	 * @param creationTime
	 *            is the new creation time
	 */
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * Returns the last modification time.
	 * 
	 * @return the last modification time
	 */
	public Instant getModificationTime() {
		return modificationTime;
	}

	/**
	 * Sets a new modification time.
	 * 
	 * @param modificationTime
	 *            is the new modification time
	 */
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/**
	 * Returns the description
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets a new description
	 * 
	 * @param description
	 *            is the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Indicates whether this product shall be displayed in special statistic views.
	 * 
	 * @return whether this product shall be displayed in special statistic views
	 */
	public Boolean getDisplayStatistic() {
		return displayStatistic;
	}

	/**
	 * Sets whether this product shall be displayed in special statistic views.
	 * 
	 * @param displayStatistic
	 *            true, if this product shall be displayed in special statistic
	 *            views
	 */
	public void setDisplayStatistic(Boolean displayStatistic) {
		this.displayStatistic = displayStatistic;
	}

	public static Set<Product> toProducts(Collection<ProductEntity> entities) {
		return entities.stream().map(ProductEntity::toProduct)
				.collect(Collectors.toSet());
	}

	@Transient
	public Product toProduct() {
		var object = new Product();
		object.setCategory(this.getCategory());
		object.setId(this.getId());
		object.setImageName(this.getImageName());
		object.setName(this.getName());
		object.setPrice(this.getPrice());
		object.setCreationTime(this.getCreationTime());
		object.setEnabled(this.getEnabled());
		object.setModificationTime(this.getModificationTime());
		object.setDescription(this.getDescription());
		object.setDisplayStatistic(this.getDisplayStatistic());
		return object;
	}

	public static Set<ProductEntity> fromProducts(Collection<Product> objects) {
		return objects.stream().map(ProductEntity::fromProduct)
				.collect(Collectors.toSet());
	}

	public static ProductEntity fromProduct(Product object) {
		var entity = new ProductEntity();
		entity.setCategory(object.getCategory());
		entity.setId(object.getId());
		entity.setImageName(object.getImageName());
		entity.setName(object.getName());
		entity.setPrice(object.getPrice());
		entity.setCreationTime(object.getCreationTime());
		entity.setEnabled(object.getEnabled());
		entity.setModificationTime(object.getModificationTime());
		entity.setDescription(object.getDescription());
		entity.setDisplayStatistic(object.getDisplayStatistic());
		return entity;
	}
}
