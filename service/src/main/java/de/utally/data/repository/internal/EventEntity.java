package de.utally.data.repository.internal;

import de.utally.data.model.Event;

import jakarta.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity class for Utally Events
 * 
 * @author Tobias Hecht
 *
 */
@Entity
@Table(name = "utally_event")
public class EventEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
	@SequenceGenerator(name = "id_generator", sequenceName = "HIBERNATE_SEQUENCE", allocationSize = 1)
	private Long id;

	@Column(name = "event_title", length = 64, nullable = false)
	private String title;

	@Column(name = "players_fee", nullable = false)
	private Integer playersFee;

	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

	@Column(name = "creation_time", nullable = false)
	private Instant creationTime;

	@Column(name = "modification_time", nullable = false)
	private Instant modificationTime;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "utally_event_location_mapping", joinColumns = @JoinColumn(name = "location_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "event_id", referencedColumnName = "id"))
	private Set<LocationEntity> locations = new HashSet<>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "utally_event_team_mapping", joinColumns = @JoinColumn(name = "team_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "event_id", referencedColumnName = "id"))
	private Set<TeamEntity> teams = new HashSet<>();

	@Column(name = "description", length = 2048)
	private String description;

	/**
	 * Returns the entity id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets a new id.
	 * 
	 * @param id
	 *            is the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the event title.
	 * 
	 * @return the event title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets a new event title.
	 * 
	 * @param title
	 *            is the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the event players fee.
	 * 
	 * @return the players dee
	 */
	public Integer getPlayersFee() {
		return playersFee;
	}

	/**
	 * Sets a new event players fee.
	 * 
	 * @param playersFee
	 *            is the new players fee
	 */
	public void setPlayersFee(Integer playersFee) {
		this.playersFee = playersFee;
	}

	/**
	 * Returns true if the entity is enabled.
	 * 
	 * @return true if the entity is enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * Sets a new enabled state.
	 * 
	 * @param enabled
	 *            is the new enabled state
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Returns the creation time.
	 * 
	 * @return the creation time
	 */
	public Instant getCreationTime() {
		return creationTime;
	}

	/**
	 * Sets a new creation time.
	 * 
	 * @param creationTime
	 *            is the new creation time
	 */
	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * Returns the last modification time.
	 * 
	 * @return the last modification time
	 */
	public Instant getModificationTime() {
		return modificationTime;
	}

	/**
	 * Sets a new modification time.
	 * 
	 * @param modificationTime
	 *            is the new modification time
	 */
	public void setModificationTime(Instant modificationTime) {
		this.modificationTime = modificationTime;
	}

	/**
	 * Returns the {@link LocationEntity}s associated to the event.
	 * 
	 * @return the {@link LocationEntity}s associated to the event
	 */
	public Set<LocationEntity> getLocations() {
		return locations;
	}

	/**
	 * Sets new {@link LocationEntity}s associated to the event.
	 * 
	 * @param locations
	 *            are the new {@link LocationEntity}s associated to the event
	 */
	public void setLocations(Set<LocationEntity> locations) {
		this.locations = new HashSet<>(locations);
	}

	/**
	 * Returns the {@link TeamEntity}s associated to the event.
	 * 
	 * @return the {@link TeamEntity}s associated to the event
	 */
	public Set<TeamEntity> getTeams() {
		return new HashSet<>(teams);
	}

	/**
	 * Sets new {@link TeamEntity}s associated to the event.
	 * 
	 * @param teams
	 *            are the new {@link TeamEntity}s associated to the event
	 */
	public void setTeams(Set<TeamEntity> teams) {
		this.teams = new HashSet<>(teams);
	}

	/**
	 * Returns the description
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets a new description
	 * 
	 * @param description
	 *            is the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public static Set<Event> toEvents(Collection<EventEntity> entities) {
		return entities.stream().map(EventEntity::toEvent)
				.collect(Collectors.toSet());
	}

	@Transient
	public Event toEvent() {
		var o = new Event();
		o.setId(this.getId());
		o.setLocations(LocationEntity.toLocations(this.getLocations()));
		o.setTeams(TeamEntity.toTeams(this.getTeams()));
		o.setTitle(this.getTitle());
		o.setCreationTime(this.getCreationTime());
		o.setEnabled(this.getEnabled());
		o.setModificationTime(this.getModificationTime());
		o.setPlayersFee(this.getPlayersFee());
		o.setDescription(this.getDescription());
		return o;
	}

	public static Set<EventEntity> fromEvents(Collection<Event> objects) {
		return objects.stream().map(EventEntity::fromEvent)
				.collect(Collectors.toSet());
	}

	public static EventEntity fromEvent(Event object) {
		var e = new EventEntity();
		e.setId(object.getId());
		e.setLocations(LocationEntity.fromLocations(object.getLocations()));
		e.setTeams(TeamEntity.fromTeams(object.getTeams()));
		e.setTitle(object.getTitle());
		e.setCreationTime(object.getCreationTime());
		e.setEnabled(object.getEnabled());
		e.setModificationTime(object.getModificationTime());
		e.setPlayersFee(object.getPlayersFee());
		e.setDescription(object.getDescription());
		return e;
	}
}
