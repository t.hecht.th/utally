package de.utally.data.repository.internal;

import de.utally.data.model.Tally;

import jakarta.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity class for Utally Tallies
 * 
 * @author Tobias Hecht
 *
 */
@Entity
@Table(name="utally_tally")
public class TallyEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
	@SequenceGenerator(name = "id_generator", sequenceName = "HIBERNATE_SEQUENCE", allocationSize = 1)
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="player_id", referencedColumnName = "id")
	private PlayerEntity player;
	
	@Column(name="time", nullable = false)
	private Instant time = Instant.now();
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private ProductEntity product;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private LocationEntity location;
	
	@Column(name="cancelled", nullable = false)
	private Boolean cancelled = Boolean.FALSE;

	/**
	 * Returns the entity id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets a new id.
	 * 
	 * @param id
	 *            is the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the tally insertion time.
	 * 
	 * @return the tally insertion time
	 */
	public Instant getTime() {
		return time;
	}

	/**
	 * Sets a new tally insertion time.
	 * 
	 * @param time
	 *            is the new tally insertion time
	 */
	public void setTime(Instant time) {
		this.time = time;
	}

	/**
	 * Returns true if the tally is cancelled.
	 * 
	 * @return true if the tally is cancelled
	 */
	public Boolean getCancelled() {
		return cancelled;
	}

	/**
	 * Sets a new cancelled state.
	 * 
	 * @param cancelled is the new cancelled state
	 */
	public void setCancelled(Boolean cancelled) {
		this.cancelled = cancelled;
	}

	/**
	 * Returns the {@link PlayerEntity} the tally is associated to.
	 * 
	 * @return the {@link PlayerEntity} the tally is associated to
	 */
	public PlayerEntity getPlayer() {
		return player;
	}

	/**
	 * Sets a new {@link PlayerEntity} association.
	 * 
	 * @param player is the new {@link PlayerEntity}
	 */
	public void setPlayer(PlayerEntity player) {
		this.player = player;
	}

	/**
	 * Returns the {@link ProductEntity} the tally is associated to.
	 * 
	 * @return the {@link ProductEntity} the tally is associated to
	 */
	public ProductEntity getProduct() {
		return product;
	}

	/**
	 * Sets a new {@link ProductEntity} association.
	 * 
	 * @param product is the new {@link ProductEntity}
	 */
	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	/**
	 * Returns the {@link LocationEntity} the tally is associated to.
	 * 
	 * @return the {@link LocationEntity} the tally is associated to
	 */
	public LocationEntity getLocation() {
		return location;
	}

	/**
	 * Sets a new {@link LocationEntity} association.
	 * 
	 * @param location is the new {@link LocationEntity}
	 */
	public void setLocation(LocationEntity location) {
		this.location = location;
	}

	public static Set<Tally> toTallies(Collection<TallyEntity> entities) {
		return entities.stream().map(TallyEntity::toTally)
				.collect(Collectors.toSet());
	}

	@Transient
	public Tally toTally() {
		var object = new Tally();
		object.setCancelled(this.getCancelled());
		object.setId(this.getId());
		object.setPlayer(this.getPlayer().toPlayer());
		object.setProduct(this.getProduct().toProduct());
		object.setLocation(this.getLocation().toLocation());
		object.setTime(this.getTime());
		return object;
	}

	public static Set<TallyEntity> fromTallies(Collection<Tally> objects) {
		return objects.stream().map(TallyEntity::fromTally)
				.collect(Collectors.toSet());
	}

	public static TallyEntity fromTally(Tally object) {
		var e = new TallyEntity();
		e.setCancelled(object.getCancelled());
		e.setId(object.getId());
		e.setPlayer(PlayerEntity.fromPlayer(object.getPlayer()));
		e.setProduct(ProductEntity.fromProduct(object.getProduct()));
		e.setLocation(LocationEntity.fromLocation(object.getLocation()));
		e.setTime(object.getTime());
		return e;
	}
}
