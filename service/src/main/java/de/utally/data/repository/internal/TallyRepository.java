package de.utally.data.repository.internal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface TallyRepository extends CrudRepository<TallyEntity, Long> {

	List<TallyEntity> findByProductIdAndPlayerId(Long productId, Long playerId);

	List<TallyEntity> findByPlayerId(Long playerId);

	List<TallyEntity> findByLocationIdAndPlayerId(Long locationId, Long playerId);

	List<TallyEntity> findByProductId(Long productId);

	List<TallyEntity> findByLocationId(Long locationId);

	List<TallyEntity> findByPlayerTeamId(Long teamId);

	List<TallyEntity> findByTimeBetween(Instant start, Instant end);

	long countAllByPlayerTeamIdAndProductIdAndCancelledFalse(Long teamId, Long productId);

	long countAllByPlayerTeamIdAndProductIdAndCancelledFalseAndTimeBetween(Long teamId, Long productId, Instant start, Instant end);

	List<TallyEntity> findByLocationIdIn(List<Long> locationIds);

	void deleteByProductId(Long productId);

	void deleteByPlayerId(Long playerId);

	void deleteByLocationId(Long locationId);
}
