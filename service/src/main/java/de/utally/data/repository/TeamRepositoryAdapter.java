package de.utally.data.repository;

import de.utally.data.exception.TeamNotFoundException;
import de.utally.data.model.Team;
import de.utally.data.model.TeamAdapter;
import de.utally.data.repository.internal.TeamEntity;
import de.utally.data.repository.internal.TeamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * {@link TeamAdapter} implementation to wrap a {@link TeamRepository}.
 * 
 * @author Tobias Hecht
 *
 */
@Transactional
@Component
public class TeamRepositoryAdapter implements TeamAdapter {
	private static final Logger logger = LoggerFactory.getLogger(TeamRepositoryAdapter.class);

	private final TeamRepository repository;

	public TeamRepositoryAdapter(TeamRepository repository) {
		this.repository = repository;
	}

	@Override
	public Team insertTeam(String fullName, String description, String city, String shortName,
			Boolean enabled) {
		Objects.requireNonNull(fullName, "Team full name must not be null.");
		Objects.requireNonNull(city, "Team city name must not be null.");
		Objects.requireNonNull(shortName, "Team short name must not be null.");
		TeamEntity teamEntity = new TeamEntity();
		teamEntity.setCity(city);
		teamEntity.setDescription(description);
		teamEntity.setFullName(fullName);
		teamEntity.setShortName(shortName);
		teamEntity.setCreationTime(Instant.now());
		teamEntity.setEnabled(enabled);
		teamEntity.setModificationTime(Instant.now());
		logger.info("Inserting team {} with short name {} from city {}", fullName, shortName, city);
		return repository.save(teamEntity).toTeam();
	}

	@Override
	public void updateTeam(Team team) {
		Objects.requireNonNull(team, "Team must not be null.");
		var teamEntity = repository.findById(team.getId()).orElseThrow(TeamNotFoundException::new);
		teamEntity.setCity(team.getCity());
		teamEntity.setDescription(team.getDescription());
		teamEntity.setFullName(team.getFullName());
		teamEntity.setShortName(team.getShortName());
		teamEntity.setEnabled(team.getEnabled());
		teamEntity.setModificationTime(Instant.now());
		repository.save(teamEntity);
	}

	@Override
	public List<Team> getTeams() {
		List<Team> teams = new ArrayList<>();
		for (TeamEntity teamEntity : repository.findAll()) {
			teams.add(teamEntity.toTeam());
		}
		logger.debug("Returning all teams.");
		return teams;
	}

	@Override
	public List<Team> getEnabledTeams() {
		logger.debug("Returning all enabled teams.");
		return repository.findByEnabled(true).stream().map(TeamEntity::toTeam).toList();
	}

	@Override
	public void removeTeam(Long id) {
		Objects.requireNonNull(id, "Team id must not be null.");
		logger.info("Deleting team {}", id);
		repository.deleteById(id);
	}

	@Override
	public Team getTeamById(Long id) {
		Objects.requireNonNull(id, "Team id must not be null.");
		logger.debug("Returning team {}", id);
		return repository.findById(id).map(TeamEntity::toTeam).orElseThrow(TeamNotFoundException::new);
	}
}
