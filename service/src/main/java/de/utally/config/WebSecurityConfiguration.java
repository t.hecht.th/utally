package de.utally.config;

import de.utally.webservice.rest.UtallyAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class WebSecurityConfiguration {

	public static final String ADMIN_ROLE = "ADMIN";
	public static final String OPERATOR_ROLE = "OPERATOR";
	public static final String USER_ROLE = "USER";

	@Bean
	public UserDetailsService userDetailsService(
			PasswordEncoder passwordEncoder,
			@Value("${operator.username}") String operatorUN,
			@Value("${operator.password}") String operatorPW,
			@Value("${admin.username}") String adminUN,
			@Value("${admin.password}") String adminPW,
			@Value("${user.username}") String userUN,
			@Value("${user.password}") String userPW
	) {
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(User.withUsername(operatorUN)
				.password(passwordEncoder.encode(operatorPW)).roles(OPERATOR_ROLE).build());
		manager.createUser(User.withUsername(adminUN)
				.password(passwordEncoder.encode(adminPW)).roles(ADMIN_ROLE).build());
		manager.createUser(User.withUsername(userUN)
				.password(passwordEncoder.encode(userPW)).roles(USER_ROLE).build());

		return manager;
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		var exceptionHandler = new UtallyAuthenticationEntryPoint();
		http.cors(Customizer.withDefaults()).headers(h -> h.frameOptions(HeadersConfigurer.FrameOptionsConfig::sameOrigin)
				).csrf(AbstractHttpConfigurer::disable)
				.authorizeHttpRequests(
						a -> a.requestMatchers("/api/**").fullyAuthenticated()
								.requestMatchers("/**").permitAll())
				.httpBasic(Customizer.withDefaults())
				.exceptionHandling(
						e -> e.authenticationEntryPoint(exceptionHandler).accessDeniedHandler(exceptionHandler)
				);
		return http.build();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
