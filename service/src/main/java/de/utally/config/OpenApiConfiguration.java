package de.utally.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

@OpenAPIDefinition(
        info = @Info(
                title = "Utally",
                description = "Open Source Digital Tally List for Ultimate Tournaments",
                contact = @Contact(
                        name = "Tobias Hecht",
                        url = "https://twitter.com/Tbs_Hcht",
                        email = "tobi3000@gmx.net"
                ),
                license = @License(
                        name = "Apache License, Version 2.0",
                        url = "https://gitlab.com/t.hecht.th/utally/-/blob/master/LICENSE"
                ),
                version = "2.0.0"
        )
)
@SecurityScheme(
        name = "basicAuth",
        type = SecuritySchemeType.HTTP,
        scheme = "basic"
)
public class OpenApiConfiguration {
}
