package de.utally.config;

import de.utally.data.model.*;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@Configuration
@ConditionalOnProperty(value = "demo.active", havingValue = "true")
public class DemoConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(DemoConfiguration.class);

    private final EventAdapter eventAdapter;
    private final TeamAdapter teamAdapter;
    private final LocationAdapter locationAdapter;
    private final ProductAdapter productAdapter;
    private final TimeSlotAdapter timeSlotAdapter;
    private final PlayerAdapter playerAdapter;

    public DemoConfiguration(
            EventAdapter eventAdapter,
            TeamAdapter teamAdapter,
            LocationAdapter locationAdapter,
            ProductAdapter productAdapter,
            TimeSlotAdapter timeSlotAdapter,
            PlayerAdapter playerAdapter
    ) {
        this.eventAdapter = eventAdapter;
        this.teamAdapter = teamAdapter;
        this.locationAdapter = locationAdapter;
        this.productAdapter = productAdapter;
        this.timeSlotAdapter = timeSlotAdapter;
        this.playerAdapter = playerAdapter;
    }

    @PostConstruct
    public void createDemoData() {
        if (!eventAdapter.getEvents().isEmpty()) {
            logger.info("Database is not empty. Inserting demo data skipped.");
            return;
        }
        logger.info("Loading demo data");
        var event = eventAdapter.insertEvent("Farmvillecup 2018", "Description of Farmvillecup 2018", true, 1200);
        var event2 = eventAdapter.insertEvent("Farmvillecup 2019", "Description of Farmvillecup 2019", true, 1200);

        timeSlotAdapter.insertTimeSlot(
                "Fr",
                "Friday",
                ZonedDateTime.of(2018, 6, 8, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                ZonedDateTime.of(2018, 6, 9, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                true,
                event.getId()
        );
        timeSlotAdapter.insertTimeSlot(
                "Sa",
                "Saturday",
                ZonedDateTime.of(2018, 6, 9, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                ZonedDateTime.of(2018, 6, 9, 22, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                true,
                event.getId()
        );
        timeSlotAdapter.insertTimeSlot(
                "Pa",
                "Party",
                ZonedDateTime.of(2018, 6, 9, 22, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                ZonedDateTime.of(2018, 6, 10, 6, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                true,
                event.getId()
        );
        timeSlotAdapter.insertTimeSlot(
                "Su",
                "Sunday",
                ZonedDateTime.of(2018, 6, 10, 6, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                ZonedDateTime.of(2018, 6, 11, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                true,
                event.getId()
        );

        timeSlotAdapter.insertTimeSlot(
                "Fr",
                "Friday",
                ZonedDateTime.of(2019, 5, 31, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                ZonedDateTime.of(2019, 6, 1, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                true,
                event2.getId()
        );
        timeSlotAdapter.insertTimeSlot(
                "Sa",
                "Saturday",
                ZonedDateTime.of(2019, 6, 1, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                ZonedDateTime.of(2019, 6, 1, 22, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                true,
                event2.getId()
        );
        timeSlotAdapter.insertTimeSlot(
                "Pa",
                "Party",
                ZonedDateTime.of(2019, 6, 1, 22, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                ZonedDateTime.of(2019, 6, 2, 6, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                true,
                event2.getId()
        );
        timeSlotAdapter.insertTimeSlot(
                "Su",
                "Sunday",
                ZonedDateTime.of(2019, 6, 2, 6, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                ZonedDateTime.of(2019, 6, 2, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant(),
                true,
                event2.getId()
        );

        teamAdapter.insertTeam("LBV Phoenix Baltimate", "Description of Baltimate", "Lübeck", "Baltimate", true);
        teamAdapter.insertTeam("Bonnsai", "Description of Bonnsai", "Bonn", "Bonnsai", true);
        teamAdapter.insertTeam("Carapedo", "Description of Carapedo", "Wuppertal", "Carapedo", true);
        teamAdapter.insertTeam("Deine Mudder Bremen", "Description of Deine Mudder Bremen", "Bremen", "Deine Mudder", true);
        teamAdapter.insertTeam("DiscUrs - Ultimate Berlin", "Description of DiscUrs", "Berlin", "DiscUrs", true);
        teamAdapter.insertTeam("DU bist Frisbee", "Description of DU bist Frisbee", "Duisburg", "DU bist Frisbee", true);
        teamAdapter.insertTeam("Endzonies", "Description of Endzonies", "Rostock", "Endzonies", true);
        teamAdapter.insertTeam("Fischkutter", "Description of Fischkutter", "Hamburg", "Fischkutter", true);
        teamAdapter.insertTeam("Friesengeister", "Description of Friesengeister", "Emden", "Friesengeister", true);
        teamAdapter.insertTeam("Göttinger 7", "Description of Göttinger 7", "Göttingen", "Göttinger 7", true);
        teamAdapter.insertTeam("Griffin's Lehre", "Description of Griffin's Lehre", "Greifswald", "Griffin's Lehre", true);
        teamAdapter.insertTeam("Hallunken", "Description of Hallunken", "Halle", "Hallunken", true);
        teamAdapter.insertTeam("Hamburg Alsters", "Description of Hamburg Alster", "Hamburg", "Alsters", true);
        teamAdapter.insertTeam("Hundflachwerfen", "Description of Hundflachwerfen", "Berlin", "Hundflachwerfen", true);
        teamAdapter.insertTeam("Indiscutabel", "Description of Indiscutabel", "Münster", "Indiscutabel", true);
        teamAdapter.insertTeam("Murmeltiere", "Description of Murmeltiere", "Osnabrück", "Murmeltiere", true);
        teamAdapter.insertTeam("Parkscheibe", "Description of Parkscheibe", "Berlin", "Parkscheibe", true);
        teamAdapter.insertTeam("PrinzHessInnen", "Description of PrinzHessInnen", "Frankfurt", "PrinzHessInnen", true);
        teamAdapter.insertTeam("Red Eagles", "Description of Red Eagles", "Berlin", "Red Eagles", true);
        teamAdapter.insertTeam("RoToR", "Description of RoToR", "Berlin", "RoToR", true);
        teamAdapter.insertTeam("Studentenfutter", "Description of Studentenfutter", "Lüneburg", "Studentenfutter", true);
        teamAdapter.insertTeam("Sunblocker", "Description of Sunblocker", "Oldenburg", "Sunblocker", true);
        teamAdapter.insertTeam("TeKielas", "Description of TeKielas", "Kiel", "TeKielas", true);
        teamAdapter.insertTeam("Ultimate Farmers Lüneburg", "Description of Ultimate Farmers Lüneurg", "Lüneburg", "Farmers", true);

        for (var team : teamAdapter.getTeams()) {
            eventAdapter.addTeam(event.getId(), team.getId());
            eventAdapter.addTeam(event2.getId(), team.getId());

            for (int i = 0; i < 10; i++) {
                playerAdapter.insertPlayer("P" + i + "T" + team.getId() + "E" + event.getId(), team.getId(), event.getId(), true);
                playerAdapter.insertPlayer("P" + i + "T" + team.getId() + "E" + event2.getId(), team.getId(), event2.getId(), true);
            }
        }

        var l1 = locationAdapter.insertLocation("Buffet", "Description of Buffet", true);
        var l2 = locationAdapter.insertLocation("Party", "Description of Party", true);
        var l3 = locationAdapter.insertLocation("BBQ", "Description of BBQ", true);

        for (var location : locationAdapter.getLocations()) {
            eventAdapter.addLocation(event.getId(), location.getId());
            eventAdapter.addLocation(event2.getId(), location.getId());
        }

        var p1 = productAdapter.insertProduct(
                "Bratwurst",
                "Description of Bratwurst",
                250,
                "demo-bratwurst.png",
                ProductCategory.FOOD,
                true,
                false
        );

        var p2 = productAdapter.insertProduct(
                "Softdrink",
                "Description of Softdrink",
                100,
                "demo-softdrink.png",
                ProductCategory.DRINK,
                true,
                true
        );

        var p3 = productAdapter.insertProduct(
                "Longdrink",
                "Description of Longdrink",
                350,
                "demo-longdrink.png",
                ProductCategory.DRINK,
                true,
                true
        );

        var p4 = productAdapter.insertProduct(
                "Salad",
                "Description of Salad",
                150,
                "demo-salad.png",
                ProductCategory.FOOD,
                true,
                false
        );

        var p5 = productAdapter.insertProduct(
                "Bag",
                "Description of Bag",
                800,
                "demo-bag.png",
                ProductCategory.MDSE,
                true,
                false
        );

        locationAdapter.addProduct(l1.getId(), p2.getId());
        locationAdapter.addProduct(l1.getId(), p4.getId());
        locationAdapter.addProduct(l1.getId(), p5.getId());
        locationAdapter.addProduct(l2.getId(), p2.getId());
        locationAdapter.addProduct(l2.getId(), p3.getId());
        locationAdapter.addProduct(l3.getId(), p1.getId());
    }
}
