package de.utally.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "payment")
public record PaymentConfiguration(Bank bank, Paypal paypal) {

    public record Bank(String recipient, String name, String iban, String bic) { }

    public record Paypal(String me, String account) { }
}
