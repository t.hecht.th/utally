package de.utally.webservice.rest.exception;

public class UtallyRestException extends RuntimeException {
    private final ErrorCode errorCode;

    public UtallyRestException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
