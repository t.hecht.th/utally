package de.utally.webservice.rest.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Currency;

public class CurrencyCodeValidator implements ConstraintValidator<CurrencyCodeConstraint, String> {
    @Override
    public void initialize(CurrencyCodeConstraint constraintAnnotation) {
        // Not needed
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return Currency.getAvailableCurrencies().stream().map(Currency::getCurrencyCode).anyMatch(value::equals);
    }
}
