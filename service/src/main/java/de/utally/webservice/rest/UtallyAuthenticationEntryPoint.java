package de.utally.webservice.rest;

import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class UtallyAuthenticationEntryPoint implements AuthenticationEntryPoint, AccessDeniedHandler {
    private static final Logger logger = LoggerFactory.getLogger(UtallyAuthenticationEntryPoint.class);

    @Override
    public void commence(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authException
    ) throws IOException {
        logger.debug("Authentication failed", authException);
        response.addHeader("WWW-Authenticate", "Basic realm=\"Realm\"");
        createServletResponse(response, ErrorCode.AUTH0001);
    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    public ResponseEntity<ErrorCodeDTO> handleException(UsernameNotFoundException ex) {
        logger.debug("Username not found", ex);
        return createErrorResponse(ErrorCode.AUTH0001);
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
        logger.debug("Access denied", accessDeniedException);
        createServletResponse(response, ErrorCode.AUTH0002);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<ErrorCodeDTO> handleException(AccessDeniedException ex) {
        logger.debug("Access denied", ex);
        return createErrorResponse(ErrorCode.AUTH0002);
    }

    private ResponseEntity<ErrorCodeDTO> createErrorResponse(ErrorCode errorCode) {
        return ResponseEntity.status(errorCode.getStatus()).body(new ErrorCodeDTO(errorCode));
    }

    private void createServletResponse(HttpServletResponse response, ErrorCode errorCode) throws IOException {
        response.setStatus(errorCode.getStatus().value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(new ErrorCodeDTO(errorCode).toJson());
        response.getWriter().flush();
    }
}
