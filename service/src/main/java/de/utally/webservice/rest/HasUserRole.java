package de.utally.webservice.rest;

import de.utally.config.WebSecurityConfiguration;
import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAnyRole('" +
        WebSecurityConfiguration.ADMIN_ROLE + "', '" +
        WebSecurityConfiguration.OPERATOR_ROLE + "', '" +
        WebSecurityConfiguration.USER_ROLE + "')")
public @interface HasUserRole {
}
