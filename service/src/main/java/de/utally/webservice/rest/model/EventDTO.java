package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import java.time.Instant;
import java.util.Set;

@Schema(description = "DTO which represents a Event")
public record EventDTO(
        @Schema(
                description = "The id of the event",
                example = "5",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Long id,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The title of the event",
                example = "Farmvillecup 2022",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 64,
                minLength = 1
        )
        @Size(min = 1, max = 64, message = "Length of title must be between 1 and 64")
        @NotNull(message = "Title must not be null")
        String title,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The amount of players fee in cent",
                example = "1500",
                accessMode = Schema.AccessMode.READ_WRITE,
                minimum = "0"
        )
        @PositiveOrZero(message = "Players fee must be at least 0")
        @NotNull(message = "Players fee must not be null")
        Integer playersFee,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Determines if the event shall be shown in lists",
                example = "true",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Enabled state must not be null")
        Boolean enabled,

        @Schema(
                description = "The creation time of the event",
                example = "2021-06-19T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant creationTime,

        @Schema(
                description = "The last modification time of the event",
                example = "2021-06-20T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant modificationTime,

        @Schema(
                requiredMode = Schema.RequiredMode.NOT_REQUIRED,
                description = "The description of the event",
                example = "The beloved Farmvillecup in 2022",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 2048
        )
        @Size(max = 2048, message = "The maximum length of the description is 2048")
        String description,

        @Schema(
                description = "The associated locations of the event",
                example = "[7, 9]",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Set<Long> locationIds,

        @Schema(
                description = "The associated teams of the event",
                example = "[12, 14]",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Set<Long> teamIds
) {
}
