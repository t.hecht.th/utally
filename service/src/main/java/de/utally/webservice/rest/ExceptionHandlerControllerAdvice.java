package de.utally.webservice.rest;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import de.utally.data.exception.*;
import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.exception.UtallyRestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlerControllerAdvice {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlerControllerAdvice.class);
    private static final String EXCEPTION_OCCURRED = "Exception occurred: ";

    @ExceptionHandler(value = EventNotFoundException.class)
    public ResponseEntity<ErrorCodeDTO> handleEventNotFoundException(EventNotFoundException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.NOTF0001);
    }

    @ExceptionHandler(value = LocationNotFoundException.class)
    public ResponseEntity<ErrorCodeDTO> handleLocationNotFoundException(LocationNotFoundException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.NOTF0002);
    }

    @ExceptionHandler(value = ProductNotFoundException.class)
    public ResponseEntity<ErrorCodeDTO> handleProductNotFoundException(ProductNotFoundException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.NOTF0003);
    }

    @ExceptionHandler(value = TeamNotFoundException.class)
    public ResponseEntity<ErrorCodeDTO> handleTeamNotFoundException(TeamNotFoundException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.NOTF0004);
    }

    @ExceptionHandler(value = PlayerNotFoundException.class)
    public ResponseEntity<ErrorCodeDTO> handlePlayerNotFoundException(PlayerNotFoundException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.NOTF0005);
    }

    @ExceptionHandler(value = TimeSlotNotFoundException.class)
    public ResponseEntity<ErrorCodeDTO> handleTimeSlotNotFoundException(TimeSlotNotFoundException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.NOTF0006);
    }

    @ExceptionHandler(value = TallyNotFoundException.class)
    public ResponseEntity<ErrorCodeDTO> handleTallyNotFoundException(TallyNotFoundException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.NOTF0007);
    }

    @ExceptionHandler(value = PlayersNotOfSameEventException.class)
    public ResponseEntity<ErrorCodeDTO> handlePlayersNotOfSameEventException(PlayersNotOfSameEventException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.BADR0001);
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorCodeDTO> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.BADR0002);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorCodeDTO> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        String detailedMessageAppendix = ex.getFieldErrors().stream().map(s -> "- " + s.getDefaultMessage()).collect(Collectors.joining("\n"));
        return createErrorResponse(ErrorCode.BADR0003, detailedMessageAppendix);
    }

    @ExceptionHandler(value = InvalidFormatException.class)
    public ResponseEntity<ErrorCodeDTO> handleInvalidFormatException(InvalidFormatException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        String detailedMessageAppendix = ex.getMessage();
        return createErrorResponse(ErrorCode.BADR0003, detailedMessageAppendix);
    }

    @ExceptionHandler(value = MismatchedInputException.class)
    public ResponseEntity<ErrorCodeDTO> handleMismatchedInputException(MismatchedInputException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.BADR0006);
    }

    @ExceptionHandler(value = IOException.class)
    public ResponseEntity<ErrorCodeDTO> handleIOException(IOException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.INTE0001);
    }

    @ExceptionHandler(value = UtallyRestException.class)
    public ResponseEntity<ErrorCodeDTO> handleUtallyRestException(UtallyRestException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ex.getErrorCode());
    }

    @ExceptionHandler(value = NoSuchFileException.class)
    public ResponseEntity<ErrorCodeDTO> handleNoSuchFileException(NoSuchFileException ex) {
        logger.error(EXCEPTION_OCCURRED, ex);
        return createErrorResponse(ErrorCode.NOTF0008);
    }

    private ResponseEntity<ErrorCodeDTO> createErrorResponse(ErrorCode errorCode) {
        return ResponseEntity.status(errorCode.getStatus()).body(new ErrorCodeDTO(errorCode));
    }

    private ResponseEntity<ErrorCodeDTO> createErrorResponse(ErrorCode errorCode, String detailedMessageAppendix) {
        return ResponseEntity.status(errorCode.getStatus()).body(new ErrorCodeDTO(errorCode, detailedMessageAppendix));
    }
}
