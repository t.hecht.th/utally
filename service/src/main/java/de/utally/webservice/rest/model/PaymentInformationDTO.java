package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "DTO which represents the payment information")
public record PaymentInformationDTO(
        @Schema(
                description = "The account recipient information",
                example = "Ultimate Frisbee Lüneburg e.V.",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String bankRecipient,

        @Schema(
                description = "The bank name information",
                example = "Volksbank Lüneburger Heide",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String bankName,

        @Schema(
                description = "The account iban information",
                example = "DE28240603008537433400",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String bankIban,

        @Schema(
                description = "The bank bic information",
                example = "GENODEF1NBU",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String bankBic,

        @Schema(
                description = "The Paypal me account name",
                example = "TbsHcht",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String paypalMe,

        @Schema(
                description = "The Paypal account",
                example = "tobi3000@gmx.net",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String paypalAccount
) {
}
