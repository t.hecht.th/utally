package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.time.Instant;
import java.util.Map;

@Schema(description = "DTO which represents a Player")
public record PlayerDTO(
        @Schema(
                description = "The id of the player",
                example = "5",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Long id,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The name of the player",
                example = "Tobias",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 64,
                minLength = 1
        )
        @NotNull(message = "Name must not be null")
        @Size(min = 1, max = 64, message = "Name length must be between 1 and 64")
        String name,

        @Schema(
                description = "The creation time of the player",
                example = "2021-06-19T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant creationTime,

        @Schema(
                description = "The last modification time of the player",
                example = "2021-06-20T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant modificationTime,

        @Schema(
                description = "Indicates whether the player has paid",
                example = "true",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Boolean paid,

        @Schema(
                description = "Indicates whether the player has paid with players fee",
                example = "true",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Boolean withPlayersFee,

        @Schema(
                description = "Comment assigned to the player",
                example = "Tobias has paid yesterday",
                accessMode = Schema.AccessMode.READ_ONLY,
                maxLength = 64,
                minLength = 1
        )
        String comment,

        @Schema(
                description = "The pay time of the player",
                example = "2021-06-19T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant payTime,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Determines if the player shall be shown in lists",
                example = "true",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Enabled Flag must not be null")
        Boolean enabled,

        @Schema(
                description = "The amount of paid players fee in cent",
                example = "1000",
                accessMode = Schema.AccessMode.READ_ONLY,
                minimum = "0"
        )
        Integer paidPlayersFee,

        @Schema(
                description = "Key value map of user specific settings",
                example = "{\"key1\" : \"value1\", \"key2\" : \"value2\"}",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Map<String, String> preferences,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The id of the team to associate the player",
                example = "7",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Team id must not be null")
        Long teamId,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The id of the event to associate the player",
                example = "9",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Event id must not be null")
        Long eventId
) {
}
