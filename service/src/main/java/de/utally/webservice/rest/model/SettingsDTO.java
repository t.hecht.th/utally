package de.utally.webservice.rest.model;

import de.utally.webservice.rest.validation.CurrencyCodeConstraint;
import de.utally.webservice.rest.validation.ZoneIdConstraint;
import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.PositiveOrZero;

@Schema(description = "DTO which represents the Settings")
public record SettingsDTO(
        @Schema(
                description = "The growl delay in milliseconds",
                example = "5000",
                accessMode = Schema.AccessMode.READ_WRITE,
                minimum = "0"
        )
        @PositiveOrZero(message = "Growl delay must be at least 0")
        Integer growlDelay,

                @Schema(
                description = "The currency code in ISO 4217",
        example = "EUR",
        accessMode = Schema.AccessMode.READ_WRITE
)
        @CurrencyCodeConstraint
        String currencyCode,

        @Schema(
                description = "The time zone compatible with java.time.ZoneId",
                example = "UTC",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @ZoneIdConstraint
        TimeZoneDTO timeZone,

        @Schema(
                description = "The date time format in ISO 8601",
                example = "HH:mm dd-MM-yyyy",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        String dateTimeFormat,

        @Schema(
                description = "The keep alive timeout in milliseconds used to hold the session. (deprecated)",
                example = "30000",
                accessMode = Schema.AccessMode.READ_WRITE,
                minimum = "0"
        )
        @PositiveOrZero(message = "Keep alive timeout must be at least 0")
        Integer keepAliveTimeout,

        @Schema(
                description = "True, if the user can register without an operator",
                example = "true",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        Boolean selfRegistration,

        @Schema(
                description = "Determines the binning size in statistics. The higher the data point numer the shorter the bin.",
                example = "30",
                accessMode = Schema.AccessMode.READ_WRITE,
                minimum = "0"
        )
        @PositiveOrZero(message = "Number of data points must be at least 0")
        Integer dataPoints,

        @Schema(
                description = "The interval in which the party statistic page refreshes automatically.",
                example = "10000",
                accessMode = Schema.AccessMode.READ_WRITE,
                minimum = "0"
        )
        @PositiveOrZero(message = "Live statistics interval must be at least 0")
        Integer liveStatisticsInterval
) {
}
