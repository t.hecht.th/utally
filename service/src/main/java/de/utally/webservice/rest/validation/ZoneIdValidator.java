package de.utally.webservice.rest.validation;

import de.utally.webservice.rest.model.TimeZoneDTO;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.time.ZoneId;

public class ZoneIdValidator implements ConstraintValidator<ZoneIdConstraint, TimeZoneDTO> {
    @Override
    public void initialize(ZoneIdConstraint constraintAnnotation) {
        // Not needed
    }

    @Override
    public boolean isValid(TimeZoneDTO value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        if (value.zoneId() == null) {
            return false;
        }

        return ZoneId.getAvailableZoneIds().contains(value.zoneId());
    }
}
