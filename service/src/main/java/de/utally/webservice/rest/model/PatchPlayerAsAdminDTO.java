package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record PatchPlayerAsAdminDTO(
        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The name of the player",
                example = "Tobias",
                accessMode = Schema.AccessMode.WRITE_ONLY,
                maxLength = 64,
                minLength = 1
        )
        @NotNull(message = "Name must not be null")
        @Size(min = 1, max = 64, message = "Name length must be between 1 and 64")
        String name,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Determines if the player shall be shown in lists",
                example = "true",
                accessMode = Schema.AccessMode.WRITE_ONLY
        )
        @NotNull(message = "Enabled Flag must not be null")
        Boolean enabled
) {
}
