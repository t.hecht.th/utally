package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.time.Instant;

@Schema(description = "DTO which represents a TimeSlot")
public record TimeSlotDTO(
        @Schema(
                description = "The id of the time slot",
                example = "5",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Long id,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The label of the time slot",
                example = "Sa",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 64,
                minLength = 1
        )
        @NotNull(message = "Label must not be null")
        @Size(min = 1, max = 64, message = "Label length must be between 1 and 64")
        String label,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The start time of the time slot",
                example = "2022-06-22T06:00:00.000Z",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Start must not be null")
        Instant start,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The end time of the time slot",
                example = "2022-06-22T18:00:00.000Z",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "End must not be null")
        Instant end,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Determines if the time slot shall be shown in lists",
                example = "true",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Enabled Flag must not be null")
        Boolean enabled,

        @Schema(
                description = "The creation time of the time slot",
                example = "2021-06-19T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant creationTime,

        @Schema(
                description = "The last modification time of the time slot",
                example = "2021-06-20T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant modificationTime,

        @Schema(
                requiredMode = Schema.RequiredMode.NOT_REQUIRED,
                description = "The description of the time slot",
                example = "Saturday at Farmvillecup 2022",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 2048
        )
        @Size(max = 2048, message = "Length of description must be lower than or equal 2048")
        String description,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The id of the event to associate the time slot",
                example = "9",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Event id must not be null")
        Long eventId
) {
}
