package de.utally.webservice.rest;

import de.utally.data.model.TimeSlot;
import de.utally.data.model.TimeSlotAdapter;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.model.DtoMapper;
import de.utally.webservice.rest.model.StatisticDTO;
import de.utally.webservice.rest.model.TimeSlotDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/api/timeslots")
@CrossOrigin(origins = "*")
@Tag(
        name = "Time Slot",
        description = "REST Interface handling time slot resources"
)
public class TimeSlotService {
    private final TimeSlotAdapter timeSlotAdapter;

    private final DtoMapper mapper;

    public TimeSlotService(TimeSlotAdapter timeSlotAdapter, DtoMapper mapper) {
        this.timeSlotAdapter = timeSlotAdapter;
        this.mapper = mapper;
    }

    @Operation(
            summary = "Get all time slots",
            description = "Returns all configured time slots",
            tags = {"Time Slot"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TimeSlotDTO>> getTimeSlots() {
        var timeSlots = timeSlotAdapter.getTimeSlots();
        return ResponseEntity.ok(convertModelToList(timeSlots));
    }

    @Operation(
            summary = "Get all time slot by event",
            description = "Returns all configured time slots associated to the event",
            tags = {"Time Slot"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/event/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TimeSlotDTO>> getTimeSlotsByEvent(
            @Parameter(description = "Event id") @PathVariable long id
    ) {
        return ResponseEntity.ok(convertModelToList(timeSlotAdapter.getTimeSlotsByEvent(id)));
    }

    @Operation(
            summary = "Get time slot by id",
            description = "Returns the time slot identified by id",
            tags = {"Time Slot"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Time slot not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TimeSlotDTO> getTimeSlot(
            @Parameter(description = "Time slot id") @PathVariable long id
    ) {
        return ResponseEntity.ok(mapper.toDto(timeSlotAdapter.getTimeSlotById(id)));
    }

    @Operation(
            summary = "Creates a new time slot",
            description = "Creates a new time slot using the label, description, enabled state, start time and end time from request",
            tags = {"Time Slot"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TimeSlotDTO> postTimeSlot(@Valid @RequestBody TimeSlotDTO r) {
        var timeSlot = timeSlotAdapter.insertTimeSlot(
                r.label(),
                r.description(),
                r.start(),
                r.end(),
                r.enabled(),
                r.eventId()
        );
        return ResponseEntity.ok(mapper.toDto(timeSlot));
    }

    @Operation(
            summary = "Update time slot",
            description = "Updates a time slot using the label, description, enabled state, start time and end time from request",
            tags = {"Time Slot"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Time slot not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TimeSlotDTO> putTimeSlot(
            @Parameter(description = "Time slot id") @PathVariable long id,
            @Valid @RequestBody TimeSlotDTO r
    ) {
        var timeSlot = timeSlotAdapter.getTimeSlotById(id);

        timeSlot.setLabel(r.label());
        timeSlot.setDescription(r.description());
        timeSlot.setEnd(r.end());
        timeSlot.setStart(r.start());
        timeSlot.setEnabled(r.enabled());
        timeSlotAdapter.updateTimeSlot(timeSlot);

        return ResponseEntity.ok(mapper.toDto(timeSlotAdapter.getTimeSlotById(id)));
    }

    @Operation(
            summary = "Delete time slot",
            description = "Deletes the specified time slot",
            tags = {"Time Slot"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteTimeSlot(
            @Parameter(description = "Time slot id") @PathVariable long id
    ) {
        timeSlotAdapter.removeTimeSlot(id);
        return ResponseEntity.noContent().build();
    }

    private List<TimeSlotDTO> convertModelToList(Collection<TimeSlot> model) {
        return model.stream().map(mapper::toDto).toList();
    }

    @Operation(
            summary = "Get the number of tallies by timeslot grouped by team and product",
            description = "The numbers are grouped by product. Each group contains a map of all teams to the number of tallies of that product.",
            tags = {"Time Slot"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/statistics/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<StatisticDTO>> getStatistics(@Parameter(description = "Event id") @PathVariable long id) {
        var statisticEntries = timeSlotAdapter.getNumberOfTalliesByTeamInTimeSlotForStatistics(id);
        List<StatisticDTO> statisticDTOS = new ArrayList<>();
        for (var productEntry : statisticEntries.entrySet()) {
            for (var teamEntry : productEntry.getValue().entrySet()) {
                var statisticDTO = new StatisticDTO(productEntry.getKey(), teamEntry.getKey(), teamEntry.getValue());
                statisticDTOS.add(statisticDTO);
            }
        }
        return ResponseEntity.ok(statisticDTOS);
    }
}
