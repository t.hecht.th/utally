package de.utally.webservice.rest;

import de.utally.webservice.rest.exception.ErrorCodeDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(path = "/api/images")
@CrossOrigin(origins = "*")
@Tag(
        name = "Image",
        description = "REST Interface handling image resources"
)
public class ImageService {
    private final RestImageHandler restImageHandler;

    public ImageService(RestImageHandler restImageHandler) {
        this.restImageHandler = restImageHandler;
    }

    @Operation(
            summary = "Get all image filenames",
            description = "Returns all filenames of uploaded images",
            tags = {"Image"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "An IO error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> getImageFilenames() throws IOException {
        return ResponseEntity.ok(restImageHandler.getAllFilenames());
    }

    @Operation(
            summary = "Get image file",
            description = "Returns the specified image file",
            tags = {"Image"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Image not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @GetMapping(value = "/{filename:.+}",
            produces = {MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_JPEG_VALUE}
    )
    @HasUserRole
    @ResponseBody
    public ResponseEntity<Resource> getImage(
            @Parameter(description = "Image filename") @PathVariable String filename
    ) {
        Resource resource = restImageHandler.getFileResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
    }

    @Operation(
            summary = "Upload image file",
            description = "Uploads the image resource",
            tags = {"Image"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "An IO error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PostMapping(value = "/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Void> handleFileUpload(
            @Parameter(description = "Image resource") @RequestParam("file") MultipartFile file
    ) throws IOException {
        restImageHandler.storeImage(file);

        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Delete image file",
            description = "Deletes the specified image file",
            tags = {"Image"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "An IO error occurred",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping("/{filename:.+}")
    public ResponseEntity<Void> deleteImage(
            @Parameter(description = "Image filename") @PathVariable String filename
    ) throws IOException {
        restImageHandler.deleteImage(filename);

        return ResponseEntity.noContent().build();
    }
}
