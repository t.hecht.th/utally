package de.utally.webservice.rest;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@Hidden
@RestController
public class AngularService {

    @GetMapping(path = {"/operator/**", "/user/**", "/admin/**", "/login/**", "/party/**"})
    public RedirectView forwardToIndex() {
        var view = new RedirectView("/", true);
        view.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
        return view;
    }
}
