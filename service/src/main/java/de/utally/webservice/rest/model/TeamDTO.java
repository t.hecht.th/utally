package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.Instant;

@Schema(description = "DTO which represents a Team")
public record TeamDTO(
        @Schema(
                description = "The id of the team",
                example = "5",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Long id,

                @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
        description = "The description of the team",
        example = "Ultimate Farmers Lüneburg",
        accessMode = Schema.AccessMode.READ_WRITE,
        maxLength = 128,
        minLength = 1
)
        @NotNull(message = "Full Name must not be null")
        @Size(min = 1, max = 128, message = "Full Name length must be between 1 and 128")
        String fullName,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The full name of the team",
                example = "Lüneburg",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 64,
                minLength = 1
        )
        @NotNull(message = "City must not be null")
        @Size(min = 1, max = 64, message = "City length must be between 1 and 64")
        String city,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The description of the team",
                example = "Farmers",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 64,
                minLength = 1
        )
        @NotNull(message = "Short Name must not be null")
        @Size(min = 1, max = 64, message = "Short Name length must be between 1 and 64")
        String shortName,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Determines if the team shall be shown in lists",
                example = "true",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Enabled Flag must not be null")
        Boolean enabled,

        @Schema(
                description = "The creation time of the team",
                example = "2021-06-19T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant creationTime,

        @Schema(
                description = "The last modification time of the team",
                example = "2021-06-20T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant modificationTime,

        @Schema(
                requiredMode = Schema.RequiredMode.NOT_REQUIRED,
                description = "The description of the team",
                example = "Lüneburgs greatest frisbee team",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 2048
        )
        @Size(max = 2048, message = "Length of description must be lower than or equal 2048")
        String description
) {
}
