package de.utally.webservice.rest.model;

import de.utally.data.model.ProductCategory;
import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import java.time.Instant;

@Schema(description = "DTO which represents a Product")
public record ProductDTO(
        @Schema(
                description = "The id of the product",
                example = "5",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Long id,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The name of the product",
                example = "Soup",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 64,
                minLength = 1
        )
        @NotNull(message = "Name must not be null")
        @Size(min = 1, max = 64, message = "Name length must be between 1 and 64")
        String name,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The price of product in cent",
                example = "120",
                accessMode = Schema.AccessMode.READ_WRITE,
                minimum = "0"
        )
        @NotNull(message = "Price must not be null")
        @PositiveOrZero(message = "Price must be at least 0")
        Integer price,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The name of the image file uploaded before",
                example = "img123456789.png",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 128,
                minLength = 1
        )
        @NotNull(message = "Image name must not be null")
        @Size(min = 1, max = 128, message = "Image Name length must be between 1 and 128")
        String imageName,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Categorizes the product",
                example = "FOOD",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Category must not be null")
        ProductCategory category,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Determines if the product shall be shown in lists",
                example = "true",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Enabled Flag must not be null")
        Boolean enabled,

        @Schema(
                description = "The creation time of the product",
                example = "2021-06-19T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant creationTime,

        @Schema(
                description = "The last modification time of the product",
                example = "2021-06-20T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant modificationTime,

        @Schema(
                requiredMode = Schema.RequiredMode.NOT_REQUIRED,
                description = "The description of the product",
                example = "Soup at Farmvillecup 2022",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 2048
        )
        @Size(max = 2048, message = "Length of description must be lower than or equal 2048")
        String description,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Determines if the product shall be shown in statistics",
                example = "false",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Display Statistics Flag must not be null")
        Boolean displayStatistic
) {
}
