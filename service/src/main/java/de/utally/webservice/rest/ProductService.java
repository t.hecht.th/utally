package de.utally.webservice.rest;

import de.utally.data.model.LocationAdapter;
import de.utally.data.model.Product;
import de.utally.data.model.ProductAdapter;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.model.DtoMapper;
import de.utally.webservice.rest.model.ProductDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/api/products")
@CrossOrigin(origins = "*")
@Tag(
        name = "Product",
        description = "REST Interface handling product resources"
)
public class ProductService {
    private final ProductAdapter productAdapter;

    private final LocationAdapter locationAdapter;

    private final DtoMapper mapper;

    public ProductService(ProductAdapter productAdapter, LocationAdapter locationAdapter, DtoMapper mapper) {
        this.productAdapter = productAdapter;
        this.locationAdapter = locationAdapter;
        this.mapper = mapper;
    }

    @Operation(
            summary = "Get all products",
            description = "Returns all configured products",
            tags = {"Product"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductDTO>> getProducts() {
        var products = productAdapter.getProducts();
        return ResponseEntity.ok(convertModelToList(products));
    }

    @Operation(
            summary = "Get all products by event",
            description = "Returns all configured products associated to the event",
            tags = {"Product"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/event/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductDTO>> getProductsByEvent(
            @Parameter(description = "Event id") @PathVariable long id
    ) {
        var products = productAdapter.getProductsByEvent(id);
        return ResponseEntity.ok(convertModelToList(products));
    }

    @Operation(
            summary = "Get all products by location",
            description = "Returns all configured products associated to the location",
            tags = {"Product"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/location/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductDTO>> getProductsByLocation(
            @Parameter(description = "Location id") @PathVariable long id
    ) {
        var products = locationAdapter.getProducts(id);
        return ResponseEntity.ok(convertModelToList(products));
    }

    @Operation(
            summary = "Get product by id",
            description = "Returns the product identified by id",
            tags = {"Product"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> getProduct(
            @Parameter(description = "Product id") @PathVariable long id
    ) {
        return ResponseEntity.ok(mapper.toDto(productAdapter.getProductById(id)));
    }

    @Operation(
            summary = "Create new product",
            description = "Creates a new product using the name description, price, image name, category, enabled state and display statistics flag from request",
            tags = {"Product"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> postProduct(@Valid @RequestBody ProductDTO r) {
        var product = productAdapter.insertProduct(
                r.name(),
                r.description(),
                r.price(),
                r.imageName(),
                r.category(),
                r.enabled(),
                r.displayStatistic()
        );

        return ResponseEntity.ok(mapper.toDto(product));
    }

    @Operation(
            summary = "Updates a product",
            description = "Updates a product using the name description, price, image name, category, enabled state and display statistics flag from request",
            tags = {"Product"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> putProduct(
            @Parameter(description = "Product id") @PathVariable long id,
            @Valid @RequestBody ProductDTO r
    ) {
        var product = productAdapter.getProductById(id);

        product.setDescription(r.description());
        product.setName(r.name());
        product.setCategory(r.category());
        product.setDisplayStatistic(r.displayStatistic());
        product.setImageName(r.imageName());
        product.setPrice(r.price());
        product.setEnabled(r.enabled());
        productAdapter.updateProduct(product);

        return ResponseEntity.ok(mapper.toDto(productAdapter.getProductById(id)));
    }

    @Operation(
            summary = "Delete product",
            description = "Deletes the specified product",
            tags = {"Product"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteProduct(
            @Parameter(description = "Product id") @PathVariable long id
    ) {
        productAdapter.removeProduct(id);
        return ResponseEntity.noContent().build();
    }

    private List<ProductDTO> convertModelToList(Collection<Product> model) {
        return model.stream().map(mapper::toDto).toList();
    }
}
