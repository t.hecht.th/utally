package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.Instant;
import java.util.Set;

@Schema(description = "DTO which represents a Location")
public record LocationDTO(
        @Schema(
                description = "The id of the location",
                example = "5",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Long id,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The name of the location",
                example = "Buffet",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 64,
                minLength = 1
        )
        @NotNull(message = "Name must not be null")
        @Size(min = 1, max = 64, message = "Name length must be between 1 and 64")
        String name,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Determines if the location shall be shown in lists",
                example = "true",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Enabled Flag must not be null")
        Boolean enabled,

        @Schema(
                description = "The creation time of the location",
                example = "2021-06-19T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant creationTime,

        @Schema(
                description = "The last modification time of the location",
                example = "2021-06-20T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant modificationTime,

        @Schema(
                requiredMode = Schema.RequiredMode.NOT_REQUIRED,
                description = "The description of the location",
                example = "Buffet inside the Stall at Farmvillecup 2022",
                accessMode = Schema.AccessMode.READ_WRITE,
                maxLength = 2048
        )
        @Size(max = 2048, message = "Length of description must be lower than or equal 2048")
        String description,

        @Schema(
                description = "The associated products of the location",
                example = "[21, 24]",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Set<Long> productIds
) {
}
