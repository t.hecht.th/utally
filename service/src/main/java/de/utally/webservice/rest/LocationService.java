package de.utally.webservice.rest;

import de.utally.data.model.EventAdapter;
import de.utally.data.model.Location;
import de.utally.data.model.LocationAdapter;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.model.DtoMapper;
import de.utally.webservice.rest.model.LocationDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/api/locations")
@CrossOrigin(origins = "*")
@Tag(
        name = "Location",
        description = "REST Interface handling location resources"
)
public class LocationService {
    private final LocationAdapter locationAdapter;

    private final EventAdapter eventAdapter;

    private final DtoMapper mapper;

    public LocationService(LocationAdapter locationAdapter, EventAdapter eventAdapter, DtoMapper mapper) {
        this.locationAdapter = locationAdapter;
        this.eventAdapter = eventAdapter;
        this.mapper = mapper;
    }

    @Operation(
            summary = "Get all locations",
            description = "Returns all configured locations",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LocationDTO>> getLocations() {
        var locations = locationAdapter.getLocations();
        return ResponseEntity.ok(convertModelToList(locations));
    }

    @Operation(
            summary = "Get all locations by event",
            description = "Returns all configured locations associated to the event",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/event/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LocationDTO>> getLocationsByEvent(
            @Parameter(description = "Event id") @PathVariable long id
    ) {
        return ResponseEntity.ok(convertModelToList(eventAdapter.getLocations(id)));
    }

    @Operation(
            summary = "Get location by id",
            description = "Returns the location identified by id",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LocationDTO> getLocation(
            @Parameter(description = "Location id") @PathVariable long id
    ) {
        return ResponseEntity.ok(mapper.toDto(locationAdapter.getLocation(id)));
    }

    @Operation(
            summary = "Create new location",
            description = "Creates a new location using the enabled, name and description from request",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LocationDTO> postLocation(@Valid @RequestBody LocationDTO r) {
        var location = locationAdapter.insertLocation(r.name(), r.description(), r.enabled());
        return ResponseEntity.ok(mapper.toDto(location));
    }

    @Operation(
            summary = "Update location",
            description = "Updates a location using the enabled, name and description from request",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LocationDTO> putLocation(
            @Parameter(description = "Location id") @PathVariable long id,
            @Valid @RequestBody LocationDTO r
    ) {
        var location = locationAdapter.getLocation(id);
        location.setName(r.name());
        location.setDescription(r.description());
        location.setEnabled(r.enabled());
        locationAdapter.updateLocation(location);

        return ResponseEntity.ok(mapper.toDto(locationAdapter.getLocation(id)));
    }

    @Operation(
            summary = "Delete location",
            description = "Deletes the specified location",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteLocation(
            @Parameter(description = "Location id") @PathVariable long id
    ) {
        locationAdapter.removeLocation(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Add a product to location",
            description = "Adds a product to a specific location",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location or product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/{id}/addproduct/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LocationDTO> addProductToLocation(
            @Parameter(description = "Location id") @PathVariable long id,
            @Parameter(description = "Product id") @PathVariable long productId
    ) {
        locationAdapter.addProduct(id, productId);
        return getLocationAfterPatch(id);
    }

    @Operation(
            summary = "Remove a product from location",
            description = "Removes a product from a specific location",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location or product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/{id}/removeproduct/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LocationDTO> removeProductFromLocation(
            @Parameter(description = "Location id") @PathVariable long id,
            @Parameter(description = "Product id") @PathVariable long productId
    ) {
        locationAdapter.removeProduct(id, productId);
        return getLocationAfterPatch(id);
    }

    private ResponseEntity<LocationDTO> getLocationAfterPatch(long id) {
        return ResponseEntity.ok(mapper.toDto(locationAdapter.getLocation(id)));
    }

    @Operation(
            summary = "Remove a product from all locations",
            description = "Removes a product from all associated locations",
            tags = {"Location"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping("/removeproduct/{productId}")
    public ResponseEntity<Void> removeProductFromLocations(
            @Parameter(description = "Product id") @PathVariable long productId
    ) {
        locationAdapter.removeProductFromAllLocations(productId);
        return ResponseEntity.noContent().build();
    }

    private List<LocationDTO> convertModelToList(List<Location> model) {
        return model.stream().map(mapper::toDto).toList();
    }
}
