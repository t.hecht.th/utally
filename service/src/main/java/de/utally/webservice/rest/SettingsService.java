package de.utally.webservice.rest;

import de.utally.config.PaymentConfiguration;
import de.utally.settings.SettingsAdapter;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.model.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

import java.time.ZoneId;
import java.util.Currency;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/settings")
@CrossOrigin(origins = "*")
@Tag(
        name = "Settings",
        description = "REST Interface handling setting resources"
)
public class SettingsService {
    private final SettingsAdapter settingsAdapter;

    private final DtoMapper mapper;

    private final PaymentConfiguration paymentConfiguration;

    public SettingsService(SettingsAdapter settingsAdapter, DtoMapper mapper, PaymentConfiguration paymentConfiguration) {
        this.settingsAdapter = settingsAdapter;
        this.mapper = mapper;
        this.paymentConfiguration = paymentConfiguration;
    }

    @Operation(
            summary = "Get settings",
            description = "Returns the configured settings",
            tags = {"Settings"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SettingsDTO> getSettings() {
        return ResponseEntity.ok(mapper.toDto(settingsAdapter.getSettings()));
    }

    @Operation(
            summary = "Get available time zones",
            description = "Returns the zone ids available in the service",
            tags = {"Settings"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @GetMapping(path = "/timezones", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<TimeZoneDTO>> getTimeZones() {
        return ResponseEntity.ok(
                ZoneId
                        .getAvailableZoneIds()
                        .stream()
                        .map(ZoneId::of)
                        .map(mapper::toDto)
                        .collect(Collectors.toSet())
        );
    }

    @Operation(
            summary = "Get available currencies",
            description = "Returns the currencies available in the service",
            tags = {"Settings"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @GetMapping(path = "/currencies", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<CurrencyDTO>> getCurrencies() {
        return ResponseEntity.ok(Currency.getAvailableCurrencies().stream().map(mapper::toDto).collect(Collectors.toSet()));
    }

    @Operation(
            summary = "Update settings",
            description = "Updates the settings",
            tags = {"Settings"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SettingsDTO> putSettings(@Valid @RequestBody SettingsDTO r) {
        settingsAdapter.updateSettings(mapper.toInternal(r));
        return ResponseEntity.ok(mapper.toDto(settingsAdapter.getSettings()));
    }

    @Operation(
            summary = "Get configured payment information",
            description = "Returns the configured payment information, e.g. bank information",
            tags = {"Settings"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasOperatorRole
    @GetMapping(path = "/payment", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentInformationDTO> getPaymentInformation() {
        String bankRecipient = Optional.ofNullable(paymentConfiguration.bank()).map(PaymentConfiguration.Bank::recipient).orElse(null);
        String bankName = Optional.ofNullable(paymentConfiguration.bank()).map(PaymentConfiguration.Bank::name).orElse(null);
        String bankIban = Optional.ofNullable(paymentConfiguration.bank()).map(PaymentConfiguration.Bank::iban).orElse(null);
        String bankBic = Optional.ofNullable(paymentConfiguration.bank()).map(PaymentConfiguration.Bank::bic).orElse(null);
        String paypalMe = Optional.ofNullable(paymentConfiguration.paypal()).map(PaymentConfiguration.Paypal::me).orElse(null);
        String paypalAccount = Optional.ofNullable(paymentConfiguration.paypal()).map(PaymentConfiguration.Paypal::account).orElse(null);

        return ResponseEntity.ok(
                new PaymentInformationDTO(bankRecipient, bankName, bankIban, bankBic, paypalMe, paypalAccount));
    }
}
