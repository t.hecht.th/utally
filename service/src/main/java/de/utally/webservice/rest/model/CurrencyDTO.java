package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "DTO which represents a Currency")
public record CurrencyDTO(
        @Schema(
                description = "The code of the currency according to ISO 4217",
                example = "EUR",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String currencyCode,

        @Schema(
                description = "The symbol of the currency",
                example = "€",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String symbol,

        @Schema(
                description = "A human readable name of the currency for usage in the frontend",
                example = "€",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String displayName
) {
}
