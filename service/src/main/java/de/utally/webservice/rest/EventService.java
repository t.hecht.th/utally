package de.utally.webservice.rest;

import de.utally.data.model.Event;
import de.utally.data.model.EventAdapter;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.model.DtoMapper;
import de.utally.webservice.rest.model.EventDTO;
import de.utally.webservice.rest.model.StatisticDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/api/events")
@CrossOrigin(origins = "*")
@Tag(
        name = "Event",
        description = "REST Interface handling event resources"
)
public class EventService {
    private final EventAdapter eventAdapter;

    private final DtoMapper mapper;

    public EventService(EventAdapter eventAdapter, DtoMapper mapper) {
        this.eventAdapter = eventAdapter;
        this.mapper = mapper;
    }

    @Operation(
            summary = "Get all events",
            description = "Returns all configured events",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EventDTO>> getEvents() {
        return ResponseEntity.ok(convertModelToList(eventAdapter.getEvents()));
    }

    @Operation(
            summary = "Get all events by team",
            description = "Returns all configured events associated to the team",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/team/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EventDTO>> getEventsByTeam(
            @Parameter(description = "Team id") @PathVariable long id
    ) {
        return ResponseEntity.ok(convertModelToList(eventAdapter.getEventsByTeam(id)));
    }

    @Operation(
            summary = "Get all events by location",
            description = "Returns all configured events associated to the location",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/location/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EventDTO>> getEventsByLocation(
            @Parameter(description = "Location id") @PathVariable long id
    ) {
        return ResponseEntity.ok(convertModelToList(eventAdapter.getEventsByLocation(id)));
    }

    @Operation(
            summary = "Get all events by product",
            description = "Returns all configured events associated to the product",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/product/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EventDTO>> getEventsByProduct(
            @Parameter(description = "Product id") @PathVariable long id
    ) {
        return ResponseEntity.ok(convertModelToList(eventAdapter.getEventsByProduct(id)));
    }

    @Operation(
            summary = "Get event by id",
            description = "Returns the event identified by id",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> getEvent(
            @Parameter(description = "Event id") @PathVariable long id
    ) {
        return ResponseEntity.ok(mapper.toDto(eventAdapter.getEventById(id)));
    }

    @Operation(
            summary = "Get event by time slot",
            description = "Returns the configured event associated to the time slot",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Time slot not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/timeslot/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> getEventByTimeSlot(
            @Parameter(description = "Time slot id") @PathVariable long id
    ) {
        return ResponseEntity.ok(mapper.toDto(eventAdapter.getEventByTimeSlot(id)));
    }

    @Operation(
            summary = "Create new event",
            description = "Creates a new event using the enabled, title, description and playersFee from request",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> postEvent(@Valid @RequestBody EventDTO r) {
        var event = eventAdapter.insertEvent(r.title(), r.description(), r.enabled(), r.playersFee());
        return ResponseEntity.ok(mapper.toDto(event));
    }

    @Operation(
            summary = "Update event",
            description = "Updates an event using the enabled, title, description and playersFee from request",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> putEvent(
            @Parameter(description = "Event id") @PathVariable long id,
            @Valid @RequestBody EventDTO r
    ) {
        var event = eventAdapter.getEventById(id);

        event.setEnabled(r.enabled());
        event.setTitle(r.title());
        event.setDescription(r.description());
        event.setPlayersFee(r.playersFee());
        eventAdapter.updateEvent(event);

        return ResponseEntity.ok(mapper.toDto(eventAdapter.getEventById(id)));
    }

    @Operation(
            summary = "Delete event",
            description = "Deletes the specified event",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteEvent(
            @Parameter(description = "Event id") @PathVariable long id
    ) {
        eventAdapter.removeEvent(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Add a location to event",
            description = "Adds a location to a specific event",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event or location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/{id}/addlocation/{locationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> addLocationToEvent(
            @Parameter(description = "Event id") @PathVariable long id,
            @Parameter(description = "Location id") @PathVariable long locationId
    ) {
        eventAdapter.addLocation(id, locationId);
        return ResponseEntity.ok(mapper.toDto(eventAdapter.getEventById(id)));
    }

    @Operation(
            summary = "Remove a location from event",
            description = "Removes a location from a specific event",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event or location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/{id}/removelocation/{locationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> removeLocationFromEvent(
            @Parameter(description = "Event id") @PathVariable long id,
            @Parameter(description = "Location id") @PathVariable long locationId
    ) {
        eventAdapter.removeLocation(id, locationId);
        return ResponseEntity.ok(mapper.toDto(eventAdapter.getEventById(id)));
    }

    @Operation(
            summary = "Remove a location from all events",
            description = "Removes a location from all associated events",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(value = "/removelocation/{locationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> removeLocationFromEvents(
            @Parameter(description = "Location id") @PathVariable long locationId
    ) {
        eventAdapter.removeLocationFromAllEvents(locationId);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Add a team to event",
            description = "Adds a team to a specific event",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event or team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/{id}/addteam/{teamId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> addTeamToEvent(
            @Parameter(description = "Event id") @PathVariable long id,
            @Parameter(description = "Team id") @PathVariable long teamId
    ) {
        eventAdapter.addTeam(id, teamId);
        return ResponseEntity.ok(mapper.toDto(eventAdapter.getEventById(id)));
    }

    @Operation(
            summary = "Remove a team from event",
            description = "Removes a team from a specific event",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event or team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/{id}/removeteam/{teamId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> removeTeamFromEvent(
            @Parameter(description = "Event id") @PathVariable long id,
            @Parameter(description = "Team id") @PathVariable long teamId
    ) {
        eventAdapter.removeTeam(id, teamId);
        return ResponseEntity.ok(mapper.toDto(eventAdapter.getEventById(id)));
    }

    @Operation(
            summary = "Remove a team from all events",
            description = "Removes a team from all associated events",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/removeteam/{teamId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> removeTeamFromEvents(
            @Parameter(description = "Team id") @PathVariable long teamId
    ) {
        eventAdapter.removeTeamFromAllEvents(teamId);
        return ResponseEntity.noContent().build();
    }

    private List<EventDTO> convertModelToList(Collection<Event> model) {
        return model.stream().map(mapper::toDto).toList();
    }

    @Operation(
            summary = "Get the number of tallies by event grouped by team and product",
            description = "The numbers are grouped by product. Each group contains a map of all teams to the number of tallies of that product.",
            tags = {"Event"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/statistics/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<StatisticDTO>> getStatistics(@Parameter(description = "Event id") @PathVariable long id) {
        var statisticEntries = eventAdapter.getNumberOfTalliesByTeamInEventForStatistics(id);
        List<StatisticDTO> statisticDTOS = new ArrayList<>();
        for (var productEntry : statisticEntries.entrySet()) {
            for (var teamEntry : productEntry.getValue().entrySet()) {
                var statisticDTO = new StatisticDTO(productEntry.getKey(), teamEntry.getKey(), teamEntry.getValue());
                statisticDTOS.add(statisticDTO);
            }
        }
        return ResponseEntity.ok(statisticDTOS);
    }
}
