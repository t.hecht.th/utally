package de.utally.webservice.rest;

import de.utally.data.model.EventAdapter;
import de.utally.data.model.Team;
import de.utally.data.model.TeamAdapter;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.model.DtoMapper;
import de.utally.webservice.rest.model.TeamDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/api/teams")
@CrossOrigin(origins = "*")
@Tag(
        name = "Team",
        description = "REST Interface handling team resources"
)
public class TeamService {
    private final TeamAdapter teamAdapter;

    private final EventAdapter eventAdapter;

    private final DtoMapper mapper;

    public TeamService(TeamAdapter teamAdapter, EventAdapter eventAdapter, DtoMapper mapper) {
        this.teamAdapter = teamAdapter;
        this.eventAdapter = eventAdapter;
        this.mapper = mapper;
    }

    @Operation(
            summary = "Get all teams",
            description = "Returns all configured teams",
            tags = {"Team"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TeamDTO>> getTeams() {
        var teams = teamAdapter.getTeams();
        return ResponseEntity.ok(convertModelToList(teams));
    }

    @Operation(
            summary = "Get all teams by event",
            description = "Returns all configured teams associated to the event",
            tags = {"Team"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/event/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TeamDTO>> getTeamsByEvent(
            @Parameter(description = "Event id") @PathVariable long id
    ) {
        return ResponseEntity.ok(convertModelToList(eventAdapter.getTeams(id)));
    }

    @Operation(
            summary = "Get team by id",
            description = "Returns the team identified by id",
            tags = {"Team"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDTO> getTeam(
            @Parameter(description = "Team id") @PathVariable long id
    ) {
        return ResponseEntity.ok(mapper.toDto(teamAdapter.getTeamById(id)));
    }

    @Operation(
            summary = "Create new team",
            description = "Creates a new team using the enabled, fullName, shortName, city and description from request",
            tags = {"Team"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDTO> postTeam(@Valid @RequestBody TeamDTO r) {
        var team = teamAdapter.insertTeam(r.fullName(), r.description(), r.city(), r.shortName(), r.enabled());
        return ResponseEntity.ok(mapper.toDto(team));
    }

    @Operation(
            summary = "Update team",
            description = "Updates a team using the enabled, fullName, shortName, city and description from request",
            tags = {"Team"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDTO> putTeam(
            @Parameter(description = "Team id") @PathVariable long id,
            @Valid @RequestBody TeamDTO r
    ) {
        var team = teamAdapter.getTeamById(id);
        team.setEnabled(r.enabled());
        team.setDescription(r.description());
        team.setCity(r.city());
        team.setFullName(r.fullName());
        team.setShortName(r.shortName());
        teamAdapter.updateTeam(team);

        return ResponseEntity.ok(mapper.toDto(teamAdapter.getTeamById(id)));
    }

    @Operation(
            summary = "Delete team",
            description = "Deletes the specified team",
            tags = {"Team"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteTeam(
            @Parameter(description = "Team id") @PathVariable long id
    ) {
        teamAdapter.removeTeam(id);
        return ResponseEntity.noContent().build();
    }

    private List<TeamDTO> convertModelToList(Collection<Team> model) {
        return model.stream().map(mapper::toDto).toList();
    }
}
