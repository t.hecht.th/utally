package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.Map;

public record PatchPlayerAsUserDTO(
        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The name of the player",
                example = "Tobias",
                accessMode = Schema.AccessMode.WRITE_ONLY,
                maxLength = 64
        )
        @NotNull(message = "Name must not be null")
        @Size(min = 1, max = 64, message = "Name length must be between 1 and 64")
        String name,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Key value map of user specific settings",
                example = "{\"key1\" : \"value1\", \"key2\" : \"value2\"}",
                accessMode = Schema.AccessMode.WRITE_ONLY
        )
        @NotNull(message = "Preferences must not be null. Provide an empty map instead.")
        Map<String, String> preferences
) {
}
