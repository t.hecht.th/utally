package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import java.time.Instant;

@Schema(description = "DTO which represents a Tally")
public record TallyDTO(
        @Schema(
                description = "The id of the tally",
                example = "5",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Long id,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The id of the player to associate the tally",
                example = "9",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Player id must not be null")
        Long playerId,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The id of the location to associate the tally",
                example = "10",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Location id must not be null")
        Long locationId,

        @Schema(
                description = "The time of the tally",
                example = "2021-06-19T13:10:00.843Z",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Instant time,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The id of the product to associate the tally",
                example = "11",
                accessMode = Schema.AccessMode.READ_WRITE
        )
        @NotNull(message = "Product id must not be null")
        Long productId,

        @Schema(
                description = "Indicates whether the tally is cancelled",
                example = "false",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        Boolean cancelled
) {
}
