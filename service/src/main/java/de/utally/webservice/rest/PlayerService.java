package de.utally.webservice.rest;

import de.utally.data.model.Player;
import de.utally.data.model.PlayerAdapter;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.model.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

/**
 * REST Controller handling player resources
 *
 * @author Tobias Hecht
 */
@RestController
@RequestMapping(path = "/api/players")
@CrossOrigin(origins = "*")
@Tag(
        name = "Player",
        description = "REST Interface handling player resources"
)
public class PlayerService {
    private final PlayerAdapter playerAdapter;

    private final DtoMapper mapper;

    public PlayerService(PlayerAdapter playerAdapter, DtoMapper mapper) {
        this.playerAdapter = playerAdapter;
        this.mapper = mapper;
    }

    @Operation(
            summary = "Get all players",
            description = "Returns all configured players",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PlayerDTO>> getPlayers() {
        var players = playerAdapter.getPlayers();
        return ResponseEntity.ok(convertModelToList(players));
    }

    @Operation(
            summary = "Get all players by event",
            description = "Returns all configured players associated to the event",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/event/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PlayerDTO>> getPlayersByEvent(@Parameter(description = "Event id") @PathVariable long id) {
        var players = playerAdapter.getPlayersByEvent(id);
        return ResponseEntity.ok(convertModelToList(players));
    }

    @Operation(
            summary = "Get all players by team",
            description = "Returns all configured players associated to the team",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/team/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PlayerDTO>> getPlayersByTeam(@Parameter(description = "Team id") @PathVariable long id) {
        var players = playerAdapter.getPlayersByTeam(id);
        return ResponseEntity.ok(convertModelToList(players));
    }

    @Operation(
            summary = "Get all players by team and event",
            description = "Returns all configured players associated to the team and event",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Team or event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/team/{teamId}/event/{eventId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PlayerDTO>> getPlayersByTeamAndEvent(
            @Parameter(description = "Team id") @PathVariable long teamId,
            @Parameter(description = "Event id") @PathVariable long eventId
    ) {
        var players = playerAdapter.getPlayersByTeamAndEvent(teamId, eventId);
        return ResponseEntity.ok(convertModelToList(players));
    }

    @Operation(
            summary = "Get player by id",
            description = "Returns the player identified by id",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDTO> getPlayer(@Parameter(description = "Player id") @PathVariable long id) {
        return getPlayerRepresentationModel(id);
    }

    @Operation(
            summary = "Create new player",
            description = "Creates a new player using the name, team id, event id and enabled state from request",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDTO> postPlayer(@Valid @RequestBody PlayerDTO r) {
        var player = playerAdapter.insertPlayer(r.name(), r.teamId(), r.eventId(), r.enabled());
        return ResponseEntity.ok(mapper.toDto(player));
    }

    @Operation(
            summary = "Update player with new name and enabled state",
            description = "Updates a player with the given id using the name and enabled state from request",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @PatchMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDTO> patchPlayerAsAdmin(
            @Parameter(description = "Player id") @PathVariable long id,
            @Valid @RequestBody PatchPlayerAsAdminDTO r
    ) {
        var player = playerAdapter.getPlayerById(id);

        player.setEnabled(r.enabled());
        player.setName(r.name());
        playerAdapter.updatePlayer(player);

        return getPlayerRepresentationModel(id);
    }

    @Operation(
            summary = "Update player with new name and preferences",
            description = "Updates a player with the given id using the name and preferences from request",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @PatchMapping(path = "/{id}/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDTO> patchPlayerAsUser(
            @Parameter(description = "Player id") @PathVariable long id,
            @Valid @RequestBody PatchPlayerAsUserDTO r
    ) {
        var player = playerAdapter.getPlayerById(id);

        player.setName(r.name());
        player.setPreferences(r.preferences());
        playerAdapter.updatePlayer(player);

        return getPlayerRepresentationModel(id);
    }

    @Operation(
            summary = "Update player with new comment",
            description = "Updates a player with the given id using the comment from request",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasOperatorRole
    @PatchMapping(path = "/{id}/comment", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDTO> patchPlayerComment(
            @Parameter(description = "Player id") @PathVariable long id,
            @Valid @RequestBody PatchPlayerCommentDTO r
    ) {
        var player = playerAdapter.getPlayerById(id);

        player.setComment(r.comment());
        playerAdapter.updatePlayer(player);

        return getPlayerRepresentationModel(id);
    }

    @Operation(
            summary = "Update player with new comment, players fee pay state and players fee pay amount",
            description = "Updates a player with the given id using the comment, players fee pay state and players fee pay amount from request",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasOperatorRole
    @PatchMapping(path = "/{id}/pay", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDTO> patchPlayerAsOperator(
            @Parameter(description = "Player id") @PathVariable long id,
            @Valid @RequestBody PatchPlayerAsOperatorDTO r
    ) {
        playerAdapter.pay(id, r.comment(), r.withPlayersFee(), r.paidPlayersFee());
        return getPlayerRepresentationModel(id);
    }

    @Operation(
            summary = "Reverts the payment of the specified player",
            description = "Reverts the payment of the specified player and deletes the comment, pay time and sets the paid flag to false",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasOperatorRole
    @PatchMapping(path = "/{id}/revert", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDTO> revertPayment(
            @Parameter(description = "Player id") @PathVariable long id
    ) {
        playerAdapter.revertPayment(id);
        return getPlayerRepresentationModel(id);
    }

    @Operation(
            summary = "Merge two players into one",
            description = "Moves all tallies from the source player to the destination player and deletes the source",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Players are from different events",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Source or destination not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasOperatorRole
    @PatchMapping(path = "/merge/{source}/{destination}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerDTO> mergePlayers(
            @Parameter(description = "Source id") @PathVariable long source,
            @Parameter(description = "Destination id") @PathVariable long destination
    ) {
        playerAdapter.mergePlayerIntoPlayer(source, destination);
        return getPlayerRepresentationModel(destination);
    }

    @Operation(
            summary = "Delete player",
            description = "Deletes the specified player",
            tags = {"Player"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deletePlayer(
            @Parameter(description = "Player id") @PathVariable long id
    ) {
        playerAdapter.removePlayer(id);
        return ResponseEntity.noContent().build();
    }

    private ResponseEntity<PlayerDTO> getPlayerRepresentationModel(long id) {
        return ResponseEntity.ok(mapper.toDto(playerAdapter.getPlayerById(id)));
    }

    private List<PlayerDTO> convertModelToList(List<Player> model) {
        return model.stream().map(mapper::toDto).toList();
    }
}
