package de.utally.webservice.rest.model;

import de.utally.data.model.*;
import de.utally.settings.Settings;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.time.ZoneId;
import java.util.Currency;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DtoMapper {

    @Mapping(source = "team.id", target = "teamId")
    @Mapping(source = "event.id", target = "eventId")
    PlayerDTO toDto(Player player);

    ProductDTO toDto(Product product);

    CurrencyDTO toDto(Currency currency);

    @Mapping(target = "locationIds", expression = "java(event.getLocations().stream().map(de.utally.data.model.Location::getId).collect(java.util.stream.Collectors.toSet()))")
    @Mapping(target = "teamIds", expression = "java(event.getTeams().stream().map(de.utally.data.model.Team::getId).collect(java.util.stream.Collectors.toSet()))")
    EventDTO toDto(Event event);

    @Mapping(target = "productIds", expression = "java(location.getProducts().stream().map(de.utally.data.model.Product::getId).collect(java.util.stream.Collectors.toSet()))")
    LocationDTO toDto(Location location);

    @Mapping(source = "id", target = "zoneId")
    @Mapping(target = "abbreviation", expression = "java(zoneId.getDisplayName(java.time.format.TextStyle.SHORT_STANDALONE, java.util.Locale.ENGLISH))")
    @Mapping(target = "offset", expression = "java(zoneId.getRules().getOffset(java.time.Instant.now()).getId())")
    TimeZoneDTO toDto(ZoneId zoneId);

    @Mapping(source = "currency.currencyCode", target = "currencyCode")
    SettingsDTO toDto(Settings settings);

    @Mapping(target = "currency", expression = "java(settingsDTO.currencyCode() == null ? null : java.util.Currency.getInstance(settingsDTO.currencyCode()))")
    @Mapping(target = "timeZone", expression = "java(settingsDTO.timeZone() == null ? null : java.time.ZoneId.of(settingsDTO.timeZone().zoneId()))")
    Settings toInternal(SettingsDTO settingsDTO);

    @Mapping(source = "event.id", target = "eventId")
    TimeSlotDTO toDto(TimeSlot timeSlot);

    @Mapping(source = "location.id", target = "locationId")
    @Mapping(source = "player.id", target = "playerId")
    @Mapping(source = "product.id", target = "productId")
    TallyDTO toDto(Tally tally);

    TeamDTO toDto(Team team);
}
