package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

public record TimeZoneDTO(
        @Schema(
                description = "The time zone compatible with java.time.ZoneId",
                example = "Europe/Berlin",
                accessMode = Schema.AccessMode.READ_WRITE,
                requiredMode = Schema.RequiredMode.REQUIRED
        )
        String zoneId,

                @Schema(
                description = "The abbreviation of the timezone",
        example = "MST",
        accessMode = Schema.AccessMode.READ_ONLY
)
        String abbreviation,

        @Schema(
                description = "The offset of today of the timezone",
                example = "+02:00",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        String offset
) {
}
