package de.utally.webservice.rest;

import de.utally.webservice.rest.exception.ErrorCode;
import de.utally.webservice.rest.exception.UtallyRestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class RestImageHandler {
    private static final Logger logger = LoggerFactory.getLogger(RestImageHandler.class);

    private final String imageStoragePath;

    public RestImageHandler(@Value("${images.root}") String imageStoragePat) {
        this.imageStoragePath = imageStoragePat;
    }

    public List<String> getAllFilenames() throws IOException {
        List<String> filenames = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Path.of(imageStoragePath))) {
            for (var path : directoryStream) {
                if (!Files.isDirectory(path)) {
                    var filename = path.getFileName().toString();
                    logger.debug("Found file {}", filename);
                    filenames.add(filename);
                }
            }
        }
        return filenames;
    }

    public Resource getFileResource(String filename) {
        var file = Path.of(imageStoragePath, filename).toFile();
        if (!file.exists()) {
            logger.error("File {} does not exist.", filename);
            throw new UtallyRestException(ErrorCode.NOTF0008);
        }
        return new FileSystemResource(file);
    }

    public void storeImage(MultipartFile file) throws IOException {
        Objects.requireNonNull(file, "file must not be null.");
        if (file.isEmpty()) {
            throw new UtallyRestException(ErrorCode.BADR0004);
        }

        if (file.getSize() > 5000000) {
            logger.error("File with size {} is too large", file.getSize());
            throw new UtallyRestException(ErrorCode.BADR0005);
        }

        logger.info("Storing file {} into directory {}", file.getOriginalFilename(), imageStoragePath);
        file.transferTo(Path.of(imageStoragePath, file.getOriginalFilename()));
    }

    public void deleteImage(String filename) throws IOException {
        var imagePath = Path.of(imageStoragePath, filename);
        Files.deleteIfExists(imagePath);
        logger.info("Deleted {}", filename);
    }
}
