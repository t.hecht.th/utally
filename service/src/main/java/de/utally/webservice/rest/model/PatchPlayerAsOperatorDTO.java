package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;

public record PatchPlayerAsOperatorDTO(
        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Indicates whether the player has paid with players fee",
                example = "true",
                accessMode = Schema.AccessMode.WRITE_ONLY
        )
        @NotNull(message = "With Players Fee Flag must not be null")
        Boolean withPlayersFee,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Comment assigned to the player",
                example = "Tobias has paid yesterday",
                accessMode = Schema.AccessMode.WRITE_ONLY,
                maxLength = 64
        )
        @NotNull(message = "Comment must not be null")
        @Size(max = 64, message = "Comment length must be between 0 and 64")
        String comment,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The amount of paid players fee in cent",
                example = "1000",
                accessMode = Schema.AccessMode.WRITE_ONLY,
                minimum = "0"
        )
        @NotNull(message = "Paid Players Fee must not be null")
        @PositiveOrZero(message = "Paid Players Fee must be at least 0")
        Integer paidPlayersFee
) {
}
