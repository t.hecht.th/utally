package de.utally.webservice.rest.exception;

import org.springframework.http.HttpStatus;

public enum ErrorCode {
    NOTF0001(HttpStatus.NOT_FOUND, "Event not found", "Event with the specified id does not exist"),
    NOTF0002(HttpStatus.NOT_FOUND, "Location not found", "Location with the specified id does not exist"),
    NOTF0003(HttpStatus.NOT_FOUND, "Product not found", "Product with the specified id does not exist"),
    NOTF0004(HttpStatus.NOT_FOUND, "Team not found", "Team with the specified id does not exist"),
    NOTF0005(HttpStatus.NOT_FOUND, "Player not found", "Player with the specified id does not exist"),
    NOTF0006(HttpStatus.NOT_FOUND, "Time slot not found", "Time slot with the specified id does not exist"),
    NOTF0007(HttpStatus.NOT_FOUND, "Tally not found", "Tally with the specified id does not exist"),
    NOTF0008(HttpStatus.NOT_FOUND, "Image not found", "Image with the filename id does not exist"),
    INTE0001(HttpStatus.INTERNAL_SERVER_ERROR, "An internal server error occurred.", "A generic error occurred on the server"),
    AUTH0001(HttpStatus.UNAUTHORIZED, "Unauthorized", "You must login to perform this operation"),
    AUTH0002(HttpStatus.FORBIDDEN, "Forbidden", "You have no authorization to perform this operation"),
    BADR0001(HttpStatus.BAD_REQUEST, "Players of different events", "Cannot merge players of different events"),
    BADR0002(HttpStatus.BAD_REQUEST, "Argument type mismatch", "The type of argument used to perform request does not match the required type"),
    BADR0003(HttpStatus.BAD_REQUEST, "Invalid Argument", "The argument used to perform request does not match the requirements."),
    BADR0004(HttpStatus.BAD_REQUEST, "No image uploaded", "No image found in your request"),
    BADR0005(HttpStatus.BAD_REQUEST, "Image too large", "Image must be smaller than 5 MB"),
    BADR0006(HttpStatus.BAD_REQUEST, "Mismatched Input", "The request does not match the specification.");

    private final HttpStatus status;
    private final String message;
    private final String detail;

    /**
     * Constuctor to initialize the status, message and detailed description.
     *
     * @param status the {@link HttpStatus}
     * @param message the error message
     * @param detail the detailed description
     */
    ErrorCode(HttpStatus status, String message, String detail) {
        this.status = status;
        this.message = message;
        this.detail = detail;
    }

    /**
     * Returns the {@link HttpStatus}
     *
     * @return the {@link HttpStatus}
     */
    public HttpStatus getStatus() {
        return status;
    }

    /**
     * Returns the error message
     *
     * @return the error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Returns the detailed description of the error
     *
     * @return the detailed description
     */
    public String getDetail() {
        return detail;
    }
}
