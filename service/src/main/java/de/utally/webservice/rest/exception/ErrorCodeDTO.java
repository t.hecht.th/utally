package de.utally.webservice.rest.exception;

import io.swagger.v3.oas.annotations.media.Schema;
import org.json.JSONObject;

/**
 * DTO which represents the error case determined by an {@link ErrorCode}
 *
 * @author Tobias Hecht
 */
public class ErrorCodeDTO {
    @Schema(
            description = "The code of the error",
            example = "NOTF0001",
            accessMode = Schema.AccessMode.READ_ONLY
    )
    private final String error;

    @Schema(
            description = "A short description of the error",
            example = "Event not found",
            accessMode = Schema.AccessMode.READ_ONLY
    )
    private final String message;

    @Schema(
            description = "A detailed description of the error",
            example = "Event with the specified id does not exist",
            accessMode = Schema.AccessMode.READ_ONLY
    )
    private final String detail;

    public ErrorCodeDTO(ErrorCode errorCode) {
        this.error = errorCode.name();
        this.message = errorCode.getMessage();
        this.detail = errorCode.getDetail();
    }

    public ErrorCodeDTO(ErrorCode errorCode, String detailedMessageAppendix) {
        this.error = errorCode.name();
        this.message = errorCode.getMessage();
        this.detail = errorCode.getDetail() + "\n" + detailedMessageAppendix;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getDetail() {
        return detail;
    }

    /**
     * Returns the error code content as JSON object.
     *
     * Example NOTF0001:
     *
     * <pre>{
     *     "error" : "NOTF0001",
     *     "message" : "Event not found",
     *     "detail" : "Event with the specified id does not exist"
     * }</pre>
     *
     * @return the error code content as JSON object
     */
    public String toJson() {
        var json = new JSONObject();
        json.put("error", error);
        json.put("message", message);
        json.put("detail", detail);
        return json.toString();
    }
}
