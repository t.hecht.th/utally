package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

@Schema(description = "DTO which represents a statistics entry")
public record StatisticDTO(
        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The id of the product to associate the entry",
                example = "11",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        @NotNull(message = "Product id must not be null")
        Long productId,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The id of the team to associate the entry",
                example = "11",
                accessMode = Schema.AccessMode.READ_ONLY
        )
        @NotNull(message = "Team id must not be null")
        Long teamId,

        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "The number of tallies of the product consumed by the team",
                example = "120",
                accessMode = Schema.AccessMode.READ_ONLY,
                minimum = "0"
        )
        @NotNull(message = "Count must not be null")
        @PositiveOrZero(message = "Count must be at least 0")
        Long count
) {
}
