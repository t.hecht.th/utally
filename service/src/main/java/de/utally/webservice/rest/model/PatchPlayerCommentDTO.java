package de.utally.webservice.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record PatchPlayerCommentDTO(
        @Schema(
                requiredMode = Schema.RequiredMode.REQUIRED,
                description = "Comment assigned to the player",
                example = "Tobias has paid yesterday",
                accessMode = Schema.AccessMode.WRITE_ONLY,
                maxLength = 64
        )
        @NotNull(message = "Comment must not be null")
        @Size(max = 64, message = "Comment length must be between 0 and 64")
        String comment
) {
}
