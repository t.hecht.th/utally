package de.utally.webservice.rest;

import de.utally.data.model.Tally;
import de.utally.data.model.TallyAdapter;
import de.utally.webservice.rest.exception.ErrorCodeDTO;
import de.utally.webservice.rest.model.DtoMapper;
import de.utally.webservice.rest.model.TallyDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/api/tallies")
@CrossOrigin(origins = "*")
@Tag(
        name = "Tally",
        description = "REST Interface handling tally resources"
)
public class TallyService {
    private final TallyAdapter tallyAdapter;

    private final DtoMapper mapper;

    public TallyService(TallyAdapter tallyAdapter, DtoMapper mapper) {
        this.tallyAdapter = tallyAdapter;
        this.mapper = mapper;
    }

    @Operation(
            summary = "Get all tallies",
            description = "Returns all tallies",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTallies() {
        var tallies = tallyAdapter.getTallies();
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by event",
            description = "Returns all tallies associated to the event",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/event/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByEvent(
            @Parameter(description = "Event id") @PathVariable long id
    ) {
        var tallies = tallyAdapter.getTalliesByEvent(id);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by location",
            description = "Returns all tallies associated to the location",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/location/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByLocation(
            @Parameter(description = "Location id") @PathVariable long id
    ) {
        var tallies = tallyAdapter.getTalliesByLocation(id);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by player",
            description = "Returns all tallies associated to the player",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/player/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByPlayer(
            @Parameter(description = "Player id") @PathVariable long id
    ) {
        var tallies = tallyAdapter.getTalliesByPlayer(id);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by product",
            description = "Returns all tallies associated to the product",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/product/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByProduct(
            @Parameter(description = "Product id") @PathVariable long id
    ) {
        var tallies = tallyAdapter.getTalliesByProduct(id);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by team",
            description = "Returns all tallies associated to the team",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/team/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByTeam(
            @Parameter(description = "Team id") @PathVariable long id
    ) {
        var tallies = tallyAdapter.getTalliesByTeam(id);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by timeslot",
            description = "Returns all tallies associated to the timeslot",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/timeslot/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByTimeSlot(
            @Parameter(description = "Timeslot id") @PathVariable long id
    ) {
        var tallies = tallyAdapter.getTalliesByTimeSlot(id);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by product and player",
            description = "Returns all tallies associated to the product and player",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Product or player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/product/{productId}/player/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByProductAndPlayer(
            @Parameter(description = "Product id") @PathVariable long productId,
            @Parameter(description = "Player id") @PathVariable long playerId
    ) {
        var tallies = tallyAdapter.getTalliesByProductAndPlayer(productId, playerId);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by location and player",
            description = "Returns all tallies associated to the location and player",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location or player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/location/{locationId}/player/{playerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByLocationAndPlayer(
            @Parameter(description = "Location id") @PathVariable long locationId,
            @Parameter(description = "Player id") @PathVariable long playerId
    ) {
        var tallies = tallyAdapter.getTalliesByLocationAndPlayer(locationId, playerId);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by location and event",
            description = "Returns all tallies associated to the location and event",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event or location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/location/{locationId}/event/{eventId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByLocationAndEvent(
            @Parameter(description = "Location id") @PathVariable long locationId,
            @Parameter(description = "Event id") @PathVariable long eventId
    ) {
        var tallies = tallyAdapter.getTalliesByLocationAndEvent(locationId, eventId);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by product and event",
            description = "Returns all tallies associated to the product and event",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event or product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/product/{productId}/event/{eventId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByProductAndEvent(
            @Parameter(description = "Product id") @PathVariable long productId,
            @Parameter(description = "Event id") @PathVariable long eventId
    ) {
        var tallies = tallyAdapter.getTalliesByProductAndEvent(productId, eventId);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get all tallies by team and event",
            description = "Returns all tallies associated to the team and event",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Event or team not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/team/{teamId}/event/{eventId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TallyDTO>> getTalliesByTeamAndEvent(
            @Parameter(description = "Team id") @PathVariable long teamId,
            @Parameter(description = "Event id") @PathVariable long eventId
    ) {
        var tallies = tallyAdapter.getTalliesByTeamAndEvent(teamId, eventId);
        return ResponseEntity.ok(convertModelToList(tallies));
    }

    @Operation(
            summary = "Get tally by id",
            description = "Returns the tally identified by id",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Tally not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TallyDTO> getTally(
            @Parameter(description = "Tally id") @PathVariable long id
    ) {
        return getTallyDTO(id);
    }

    @Operation(
            summary = "Create new tally",
            description = "Creates a new tally using the player id, product id and location id from request",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location, product or player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TallyDTO> postTally(@Valid @RequestBody TallyDTO r) {
        var tally = tallyAdapter.insertTally(r.playerId(), r.productId(), r.locationId());
        return ResponseEntity.ok(mapper.toDto(tally));
    }

    @Operation(
            summary = "Set cancelled state of tally",
            description = "Sets the cancelled state of specified tally",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Tally not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasUserRole
    @PatchMapping(path = "/{id}/cancelled/{cancelled}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TallyDTO> patchTally(
            @Parameter(description = "Tally id") @PathVariable long id,
            @Parameter(description = "Cancelled state") @PathVariable boolean cancelled
    ) {
        if (cancelled) {
            tallyAdapter.cancelTally(id);
        } else {
            tallyAdapter.revokeCancellation(id);
        }
        return getTallyDTO(id);
    }

    private ResponseEntity<TallyDTO> getTallyDTO(long id) {
        var tally = tallyAdapter.getTallyById(id);
        return ResponseEntity.ok(mapper.toDto(tally));
    }

    @Operation(
            summary = "Delete tally",
            description = "Deletes the specified tally",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteTally(
            @Parameter(description = "Tally id") @PathVariable long id
    ) {
        tallyAdapter.removeTally(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Delete tallies by product",
            description = "Deletes the tallies associated to the specified product",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Product not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/product/{id}")
    public ResponseEntity<Void> deleteTalliesByProduct(
            @Parameter(description = "Product id") @PathVariable long id
    ) {
        tallyAdapter.removeTalliesByProduct(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Delete tallies by player",
            description = "Deletes the tallies associated to the specified player",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Player not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/player/{id}")
    public ResponseEntity<Void> deleteTalliesByPlayer(
            @Parameter(description = "Player id") @PathVariable long id
    ) {
        tallyAdapter.removeTalliesByPlayer(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(
            summary = "Delete tallies by location",
            description = "Deletes the tallies associated to the specified location",
            tags = {"Tally"},
            security = @SecurityRequirement(name = "basicAuth")
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "You must login to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "You are not authorized to perform this operation",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Location not found",
                            content = @Content(schema = @Schema(implementation = ErrorCodeDTO.class))
                    )
            }
    )
    @HasAdminRole
    @DeleteMapping(path = "/location/{id}")
    public ResponseEntity<Void> deleteTalliesByLocation(
            @Parameter(description = "Location id") @PathVariable long id
    ) {
        tallyAdapter.removeTalliesByLocation(id);
        return ResponseEntity.noContent().build();
    }

    private List<TallyDTO> convertModelToList(Collection<Tally> model) {
        return model.stream().map(mapper::toDto).toList();
    }
}
