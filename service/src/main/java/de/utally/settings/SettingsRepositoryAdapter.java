package de.utally.settings;

import de.utally.settings.internal.SettingsConstants;
import de.utally.settings.internal.SettingsEntity;
import de.utally.settings.internal.SettingsRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.Optional;

/**
 * {@link SettingsAdapter} implementation to wrap a {@link SettingsRepository}
 * 
 * @author Tobias Hecht
 *
 */
@Component
public class SettingsRepositoryAdapter implements SettingsAdapter {

	private final SettingsRepository settingsRepository;

	private final Settings settings = new Settings();

	private DateTimeFormatter formatter;

	/**
	 * Constructor to initialize the repository attribute and the initial settings.
	 * 
	 * @param settingsRepository is the {@link SettingsRepository}
	 */
	public SettingsRepositoryAdapter(SettingsRepository settingsRepository) {
		this.settingsRepository = settingsRepository;
	}

	/**
	 * Fetches the settings from the data source or initializes the settings with
	 * default values.
	 */
	@PostConstruct
	public void initSettings() {
		Optional<SettingsEntity> setting = settingsRepository.findById(SettingsConstants.CURRENCY);
		setting.ifPresent(settingsEntity -> settings.setCurrency(Currency.getInstance(settingsEntity.getValue())));

		setting = settingsRepository.findById(SettingsConstants.DATE_TIME_FORMAT);
		setting.ifPresent(settingsEntity -> settings.setDateTimeFormat(settingsEntity.getValue()));

		setting = settingsRepository.findById(SettingsConstants.GROWL_DELAY);
		setting.ifPresent(settingsEntity -> settings.setGrowlDelay(Integer.parseInt(settingsEntity.getValue())));

		setting = settingsRepository.findById(SettingsConstants.TIME_ZONE);
		setting.ifPresent(settingsEntity -> settings.setTimeZone(ZoneId.of(settingsEntity.getValue())));

		setting = settingsRepository.findById(SettingsConstants.KEEP_ALIVE_TIMEOUT);
		setting.ifPresent(settingsEntity -> settings.setKeepAliveTimeout(Integer.parseInt(settingsEntity.getValue())));

		setting = settingsRepository.findById(SettingsConstants.SELF_REGISTRATION);
		setting.ifPresent(settingsEntity -> settings.setSelfRegistration(Boolean.parseBoolean(settingsEntity.getValue())));

		setting = settingsRepository.findById(SettingsConstants.DATA_POINTS);
		setting.ifPresent(settingsEntity -> settings.setDataPoints(Integer.parseInt(settingsEntity.getValue())));

		setting = settingsRepository.findById(SettingsConstants.LIVE_STATISTICS_INTERVAL);
		setting.ifPresent(settingsEntity -> settings.setLiveStatisticsInterval(Integer.parseInt(settingsEntity.getValue())));

		updateFormatter();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#getGrowlDelay()
	 */
	@Override
	public Integer getGrowlDelay() {
		return settings.getGrowlDelay();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#setGrowlDelay(java.lang.Integer)
	 */
	@Override
	public void setGrowlDelay(Integer delay) {
		this.settings.setGrowlDelay(delay);

		SettingsEntity setting = getSetting(SettingsConstants.GROWL_DELAY);
		setting.setValue(this.settings.getGrowlDelay().toString());
		settingsRepository.save(setting);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#getCurrency()
	 */
	@Override
	public Currency getCurrency() {
		return settings.getCurrency();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#setCurrency(java.util.Currency)
	 */
	@Override
	public void setCurrency(Currency currency) {
		this.settings.setCurrency(currency);

		SettingsEntity setting = getSetting(SettingsConstants.CURRENCY);
		setting.setValue(this.settings.getCurrency().getCurrencyCode());
		settingsRepository.save(setting);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#getTimeZone()
	 */
	@Override
	public ZoneId getTimeZone() {
		return settings.getTimeZone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#setTimeZone(java.time.ZoneId)
	 */
	@Override
	public void setTimeZone(ZoneId zoneId) {
		this.settings.setTimeZone(zoneId);

		SettingsEntity setting = getSetting(SettingsConstants.TIME_ZONE);
		setting.setValue(this.settings.getTimeZone().getId());
		settingsRepository.save(setting);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#getDateTimeFormat()
	 */
	@Override
	public String getDateTimeFormat() {
		return settings.getDateTimeFormat();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#setDateTimeFormat(java.lang.String)
	 */
	@Override
	public void setDateTimeFormat(String format) {
		this.settings.setDateTimeFormat(format);

		SettingsEntity setting = getSetting(SettingsConstants.DATE_TIME_FORMAT);
		setting.setValue(this.getDateTimeFormat());
		settingsRepository.save(setting);
		updateFormatter();
	}

	/**
	 * Returns the {@link SettingsEntity} from the repository if existing or creates
	 * a new Entity.
	 * 
	 * @param key the {@link SettingsEntity} key
	 * @return the {@link SettingsEntity}
	 */
	private SettingsEntity getSetting(String key) {
		SettingsEntity setting;
		Optional<SettingsEntity> optSetting = settingsRepository.findById(key);
		if (optSetting.isPresent()) {
			setting = optSetting.get();
		} else {
			setting = new SettingsEntity();
			setting.setKey(key);
		}
		return setting;
	}

	/**
	 * Updates the formatter.
	 */
	private void updateFormatter() {
		formatter = DateTimeFormatter.ofPattern(settings.getDateTimeFormat());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.utally.settings.SettingsAdapter#convertInstantToString(java.time.Instant)
	 */
	@Override
	public String convertInstantToString(Instant instant) {
		return formatter.format(instant.atZone(settings.getTimeZone()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#getKeepAliveTimeout()
	 */
	@Override
	public Integer getKeepAliveTimeout() {
		return settings.getKeepAliveTimeout();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.utally.settings.SettingsAdapter#setKeepAliveTimeout(java.lang.Integer)
	 */
	@Override
	public void setKeepAliveTimeout(Integer timeout) {
		this.settings.setKeepAliveTimeout(timeout);

		SettingsEntity setting = getSetting(SettingsConstants.KEEP_ALIVE_TIMEOUT);
		setting.setValue(this.settings.getKeepAliveTimeout().toString());
		settingsRepository.save(setting);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#getSelfRegistration()
	 */
	@Override
	public Boolean getSelfRegistration() {
		return this.settings.getSelfRegistration();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.utally.settings.SettingsAdapter#setSelfRegistration(java.lang.Boolean)
	 */
	@Override
	public void setSelfRegistration(Boolean selfRegistration) {
		this.settings.setSelfRegistration(selfRegistration);

		SettingsEntity setting = getSetting(SettingsConstants.SELF_REGISTRATION);
		setting.setValue(this.settings.getSelfRegistration().toString());
		settingsRepository.save(setting);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#getDataPoints()
	 */
	@Override
	public Integer getDataPoints() {
		return this.settings.getDataPoints();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#setDataPoints(java.lang.Integer)
	 */
	@Override
	public void setDataPoints(Integer dataPoints) {
		this.settings.setDataPoints(dataPoints);

		SettingsEntity setting = getSetting(SettingsConstants.DATA_POINTS);
		setting.setValue(this.settings.getDataPoints().toString());
		settingsRepository.save(setting);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#getLiveStatisticsInterval()
	 */
	@Override
	public Integer getLiveStatisticsInterval() {
		return this.settings.getLiveStatisticsInterval();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.utally.settings.SettingsAdapter#setLiveStatisticsInterval(java.lang.
	 * Integer)
	 */
	@Override
	public void setLiveStatisticsInterval(Integer liveStatisticsInterval) {
		this.settings.setLiveStatisticsInterval(liveStatisticsInterval);

		SettingsEntity setting = getSetting(SettingsConstants.LIVE_STATISTICS_INTERVAL);
		setting.setValue(this.settings.getLiveStatisticsInterval().toString());
		settingsRepository.save(setting);
	}

	@Override
	public Settings getSettings() {
		return settings;
	}

	@Override
	public void updateSettings(Settings settings) {
		if (settings.getCurrency() != null) {
			setCurrency(settings.getCurrency());
		}

		if (settings.getDataPoints() != null) {
			setDataPoints(settings.getDataPoints());
		}

		if (settings.getGrowlDelay() != null) {
			setGrowlDelay(settings.getGrowlDelay());
		}

		if (settings.getDateTimeFormat() != null) {
			setDateTimeFormat(settings.getDateTimeFormat());
		}

		if (settings.getKeepAliveTimeout() != null) {
			setKeepAliveTimeout(settings.getKeepAliveTimeout());
		}

		if (settings.getLiveStatisticsInterval() != null) {
			setLiveStatisticsInterval(settings.getLiveStatisticsInterval());
		}

		if (settings.getSelfRegistration() != null) {
			setSelfRegistration(settings.getSelfRegistration());
		}

		if (settings.getTimeZone() != null) {
			setTimeZone(settings.getTimeZone());
		}
	}
}
