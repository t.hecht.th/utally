package de.utally.settings;

import java.time.ZoneId;
import java.util.Currency;

public class Settings {
    private Integer growlDelay = 5000;
    private Currency currency = Currency.getInstance("EUR");
    private ZoneId timeZone = ZoneId.systemDefault();
    private String dateTimeFormat = "yyyy-MM-dd HH:mm";
    private Integer keepAliveTimeout = 300000;
    private Boolean selfRegistration = Boolean.TRUE;
    private Integer dataPoints = 30;
    private Integer liveStatisticsInterval = 10000;

    public Integer getGrowlDelay() {
        return growlDelay;
    }

    public void setGrowlDelay(Integer growlDelay) {
        this.growlDelay = growlDelay;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public ZoneId getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(ZoneId timeZone) {
        this.timeZone = timeZone;
    }

    public String getDateTimeFormat() {
        return dateTimeFormat;
    }

    public void setDateTimeFormat(String dateTimeFormat) {
        this.dateTimeFormat = dateTimeFormat;
    }

    public Integer getKeepAliveTimeout() {
        return keepAliveTimeout;
    }

    public void setKeepAliveTimeout(Integer keepAliveTimeout) {
        this.keepAliveTimeout = keepAliveTimeout;
    }

    public Boolean getSelfRegistration() {
        return selfRegistration;
    }

    public void setSelfRegistration(Boolean selfRegistration) {
        this.selfRegistration = selfRegistration;
    }

    public Integer getDataPoints() {
        return dataPoints;
    }

    public void setDataPoints(Integer dataPoints) {
        this.dataPoints = dataPoints;
    }

    public Integer getLiveStatisticsInterval() {
        return liveStatisticsInterval;
    }

    public void setLiveStatisticsInterval(Integer liveStatisticsInterval) {
        this.liveStatisticsInterval = liveStatisticsInterval;
    }
}
