package de.utally.settings.internal;

/**
 * Utility class to define settings constants
 * 
 * @author Tobias Hecht
 *
 */
public final class SettingsConstants {
	
	public static final String GROWL_DELAY = "growl.delay";
	public static final String TIME_ZONE = "timezone";
	public static final String CURRENCY = "currency";
	public static final String DATE_TIME_FORMAT = "datetime.format";
	public static final String KEEP_ALIVE_TIMEOUT = "keepalive.timeout";
	public static final String SELF_REGISTRATION = "selfregistration";
	public static final String DATA_POINTS = "data_points";
	public static final String LIVE_STATISTICS_INTERVAL = "live_statistics_interval";
	
	/**
	 * Private constructor to prevent instantiation.
	 */
	private SettingsConstants() {
		
	}
}
