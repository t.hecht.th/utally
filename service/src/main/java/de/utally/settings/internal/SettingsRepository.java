package de.utally.settings.internal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link CrudRepository} extension to manage {@link SettingsEntity}s
 * 
 * @author Tobias Hecht
 *
 */
@Repository
public interface SettingsRepository extends CrudRepository<SettingsEntity, String> {

}
