package de.utally.settings.internal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * Entity class for Utally Settings
 * 
 * @author Tobias Hecht
 *
 */
@Entity
@Table(name = "utally_setting")
public class SettingsEntity {
	@Id
	@Column(name = "setting_key")
	private String key;

	@Column(name = "setting_value", length = 1024)
	private String value;

	/**
	 * Returns the settings key.
	 * 
	 * @return the settings key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets a new settings key.
	 * 
	 * @param key
	 *            is the new settings key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Returns the settings value.
	 * 
	 * @return the settings value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets a new settings value.
	 * 
	 * @param value
	 *            is the new settings value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
