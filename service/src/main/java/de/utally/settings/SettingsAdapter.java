package de.utally.settings;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Currency;

public interface SettingsAdapter {

	Integer getGrowlDelay();

	void setGrowlDelay(Integer delay);

	Currency getCurrency();

	void setCurrency(Currency currency);

	ZoneId getTimeZone();

	void setTimeZone(ZoneId zoneId);

	String getDateTimeFormat();

	void setDateTimeFormat(String format);

	Integer getKeepAliveTimeout();

	void setKeepAliveTimeout(Integer timeout);

	Boolean getSelfRegistration();

	void setSelfRegistration(Boolean selfRegistration);

	Integer getDataPoints();

	void setDataPoints(Integer dataPoints);

	Integer getLiveStatisticsInterval();

	void setLiveStatisticsInterval(Integer liveStatisticsInterval);

	String convertInstantToString(Instant instant);

	Settings getSettings();

	void updateSettings(Settings settings);
}
