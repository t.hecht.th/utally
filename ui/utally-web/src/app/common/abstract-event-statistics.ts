import {Tally} from "../model/tally";
import {Product} from "../model/product";
import {Settings} from "../model/settings";
import {SettingsHandlerService} from "../service/settings-handler.service";
import {Colors} from "./colors";
import {ChartConfiguration, ChartOptions} from "chart.js";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {AfterViewInit, Injectable, OnInit} from "@angular/core";

@Injectable()
export abstract class AbstractEventStatistics<T> implements OnInit, AfterViewInit {

  protected dataSource: MatTableDataSource<T>;

  lineChartOptions: ChartOptions<'line'> = {
    responsive: true,
  };
  chartLegend = true;

  talliesChartData: ChartConfiguration<'line'>['data'] = {
    labels: [],
    datasets: []
  };

  cancelledTalliesChartData: ChartConfiguration<'line'>['data'] = {
    labels: [],
    datasets: []
  };

  talliesVolumeChartData: ChartConfiguration<'line'>['data'] = {
    labels: [],
    datasets: []
  };

  cancelledTalliesVolumeChartData: ChartConfiguration<'line'>['data'] = {
    labels: [],
    datasets: []
  };

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.getItems());
    this.updateLineCharts();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.getPaginator();
  }

  updateLineCharts() {
    let sortedTallies = this.getTallies().sort((a, b) => new Date(a.time).getTime() - new Date(b.time).getTime());
    if (sortedTallies.length < 2) {
      return;
    }

    let talliesChartData = this.talliesChartData;
    let cancelledTalliesChartData = this.cancelledTalliesChartData;
    let talliesVolumeChartData = this.talliesVolumeChartData;
    let cancelledTalliesVolumeChartData = this.cancelledTalliesVolumeChartData;

    let numberOfDataPoints = this.getSettings().dataPoints;

    let startTime = new Date(sortedTallies[0].time);
    let endTime = new Date(sortedTallies[sortedTallies.length-1].time);
    let stepSize = Math.ceil((endTime.getTime() - startTime.getTime())/numberOfDataPoints);

    let labels: string[] = [this.getSettingsHandlerService().getTimestampFormatted(startTime)];
    let talliesData: number[][] = this.getItems().map(_ => [0]);
    let volumeData: number[][] = this.getItems().map(_ => [0]);
    let cancelledTalliesData: number[][] = this.getItems().map(_ => [0]);
    let cancelledVolumeData: number[][] = this.getItems().map(_ => [0]);

    for (let i = 0; i < numberOfDataPoints; i++) {
      let lowerBoundary = startTime.getTime() + i * stepSize;
      let upperBoundary = lowerBoundary + stepSize;
      labels.push(this.getSettingsHandlerService().getTimestampFormatted(new Date(upperBoundary)));

      for (let j = 0; j < this.getItems().length; j++) {
        let talliesOld = talliesData[j][i];
        let volumeOld = volumeData[j][i];
        let cancelledTalliesOld = cancelledTalliesData[j][i];
        let cancelledVolumeOld = cancelledVolumeData[j][i];
        let item = this.getItems()[j];
        let validTallies = this.getTalliesByItem(item)
          .filter(t => new Date(t.time).getTime() >= lowerBoundary && new Date(t.time).getTime() < upperBoundary);
        let cancelledTallies = this.getCancelledTallies(item)
          .filter(t => new Date(t.time).getTime() >= lowerBoundary && new Date(t.time).getTime() < upperBoundary);
        talliesData[j].push(talliesOld + validTallies.length);
        volumeData[j].push(volumeOld + validTallies.map(t => this.getTallyPrice(t)).reduce((a,b) => a+b, 0));
        cancelledTalliesData[j].push(cancelledTalliesOld + cancelledTallies.length);
        cancelledVolumeData[j].push(cancelledVolumeOld + cancelledTallies.map(t => this.getTallyPrice(t)).reduce((a,b) => a+b, 0));
      }
    }

    let talliesDataSets = [];
    let cancelledTalliesDataSets = [];
    let talliesVolumeDataSets = [];
    let cancelledTalliesVolumeDataSets = [];
    for (let i = 0; i < this.getItems().length; i++) {
      talliesDataSets.push({
        data: talliesData[i],
        label: this.getLabel(this.getItems()[i]),
        fill: true,
        tension: 0.5,
        borderColor: Colors.BORDER_COLORS[i],
        backgroundColor: Colors.BACKGROUND_COLORS[i],
        pointBorderColor: Colors.BORDER_COLORS[i],
        pointBackgroundColor: Colors.BACKGROUND_COLORS[i]
      });

      cancelledTalliesDataSets.push({
        data: cancelledTalliesData[i],
        label: this.getLabel(this.getItems()[i]),
        fill: true,
        tension: 0.5,
        borderColor: Colors.BORDER_COLORS[i],
        backgroundColor: Colors.BACKGROUND_COLORS[i],
        pointBorderColor: Colors.BORDER_COLORS[i],
        pointBackgroundColor: Colors.BACKGROUND_COLORS[i]
      });

      talliesVolumeDataSets.push({
        data: volumeData[i].map(v => this.getSettingsHandlerService().getFractionDigits(v)),
        label: this.getLabel(this.getItems()[i]),
        fill: true,
        tension: 0.5,
        borderColor: Colors.BORDER_COLORS[i],
        backgroundColor: Colors.BACKGROUND_COLORS[i],
        pointBorderColor: Colors.BORDER_COLORS[i],
        pointBackgroundColor: Colors.BACKGROUND_COLORS[i]
      });

      cancelledTalliesVolumeDataSets.push({
        data: cancelledVolumeData[i].map(v => this.getSettingsHandlerService().getFractionDigits(v)),
        label: this.getLabel(this.getItems()[i]),
        fill: true,
        tension: 0.5,
        borderColor: Colors.BORDER_COLORS[i],
        backgroundColor: Colors.BACKGROUND_COLORS[i],
        pointBorderColor: Colors.BORDER_COLORS[i],
        pointBackgroundColor: Colors.BACKGROUND_COLORS[i]
      });
    }

    talliesChartData.labels = labels;
    cancelledTalliesChartData.labels = labels;
    talliesVolumeChartData.labels = labels;
    cancelledTalliesVolumeChartData.labels = labels;

    talliesChartData.datasets = talliesDataSets;
    cancelledTalliesChartData.datasets = cancelledTalliesDataSets;
    talliesVolumeChartData.datasets = talliesVolumeDataSets;
    cancelledTalliesVolumeChartData.datasets = cancelledTalliesVolumeDataSets;

    this.talliesChartData = talliesChartData;
    this.cancelledTalliesChartData = cancelledTalliesChartData;
    this.talliesVolumeChartData = talliesVolumeChartData;
    this.cancelledTalliesVolumeChartData = cancelledTalliesVolumeChartData;
  }

  protected abstract getItems(): T[];
  protected abstract getProducts(): Product[];
  protected abstract getSettings(): Settings;
  protected abstract getSettingsHandlerService(): SettingsHandlerService;

  protected abstract getTallies(): Tally[];

  protected abstract getTalliesByItem(item: T): Tally[];

  protected abstract getCancelledTallies(item: T): Tally[];

  protected abstract getLabel(item: T): string;

  protected abstract getPaginator(): MatPaginator;

  getVolumeByItem(item: T): number {
    let tallies = this.getTalliesByItem(item);
    return tallies.map(t => this.getTallyPrice(t)).reduce((a,b) => a+b, 0);
  }

  getCancelledVolumeByItem(item: T): number {
    let tallies = this.getCancelledTallies(item);
    return tallies.map(t => this.getTallyPrice(t)).reduce((a,b) => a+b, 0);
  }

  protected getTallyPrice(tally: Tally): number {
    return this.getProducts().find(p => p.id == tally.productId)?.price || 0
  }
}
