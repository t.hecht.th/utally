import {Observable, of} from "rxjs";
import {Error} from "../model/error";
import {SessionService} from "../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../service/message-handler.service";

export abstract class AbstractHttpHandler {
  protected constructor(
    protected sessionService: SessionService,
    protected router: Router,
    protected messageHandlerService: MessageHandlerService) {
  }

  protected performLogout() {
    this.sessionService.logout();
    this.router.navigate(['login']);
  }

  protected handleError<T>(result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      if (error.status === 401) {
        this.performLogout();
      }

      if (error.status === 403) {
        this.performLogout();
      }

      if (error.status === 0) {
        this.messageHandlerService.onError('Connection error');
      } else {
        const errorModel = error.error as Error;
        this.messageHandlerService.onError(errorModel.message + ' (' + errorModel.error + '): ' + errorModel.detail);
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
