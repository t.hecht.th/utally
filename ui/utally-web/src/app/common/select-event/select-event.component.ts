import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Event} from "../../model/event";
import {UntypedFormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-select-event',
  templateUrl: './select-event.component.html',
  styleUrls: ['./select-event.component.scss']
})
export class SelectEventComponent implements OnInit {

  eventSelectionControl = new UntypedFormControl('', [Validators.required]);

  constructor() { }

  ngOnInit(): void {
  }

  @Input() events: Event[] = [];
  @Input() showProgressIndicator = false;
  @Output() eventChange = new EventEmitter<Event>();
  @Output() onSubmit = new EventEmitter<boolean>();
}
