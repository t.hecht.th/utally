import {SessionService} from "../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {LogoutPinDialogComponent} from "../logout-pin-dialog/logout-pin-dialog.component";
import {LogoutConfirmDialogComponent} from "../logout-confirm-dialog/logout-confirm-dialog.component";
import {AbstractHttpHandler} from "./abstract-http-handler";

export abstract class AbstractSpace extends AbstractHttpHandler {
  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    protected dialog: MatDialog) {
    super(sessionService, router, messageHandlerService);
  }

  logout() {
    if (this.sessionService.hasPin()) {
      const dialogRef = this.dialog.open(LogoutPinDialogComponent, {
        width: '400px',
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result != null) {
          if (result == this.sessionService.pin()) {
            this.performLogout();
          } else {
            this.messageHandlerService.onError('PIN is not correct.')
          }
        }
      });
    } else {
      const dialogRef = this.dialog.open(LogoutConfirmDialogComponent, {
        width: '250px',
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.performLogout();
        }
      });
    }
  }
}
