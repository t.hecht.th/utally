import {ChartConfiguration, ChartOptions} from "chart.js";
import {StatisticEntry} from "../model/statisticEntry";
import {Team} from "../model/team";
import {Product} from "../model/product";
import {Colors} from "./colors";

export class AbstractStatistics {

  pieChartOptions: ChartOptions<'pie'> = {
    responsive: true,
  };
  pieChartLabels: string[];
  pieChartDatasets: {
    label: string,
    data: number[],
    backgroundColor: string[],
    borderColor: string[],
    hoverBackgroundColor: string[],
    hoverBorderColor: string[],
    borderWidth: number
  }[];
  pieChartLegend = true;
  pieChartPlugins = [];

  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartConfiguration<'bar'>['data'];

  barChartOptions: ChartConfiguration<'bar'>['options'] = {
    responsive: true,
    indexAxis: "y",
    maintainAspectRatio: false
  };

  showStatistics(productStatistics: StatisticEntry[], product: Product, teams: Team[]) {
    let labels = productStatistics.map(s => this.getTeamById(s.teamId, teams).shortName);
    let dataSet = productStatistics.map(s => s.count);
    this.pieChartLabels = labels;
    this.pieChartDatasets = [{
      label: product.name,
      data: dataSet,
      backgroundColor: Colors.BACKGROUND_COLORS,
      borderColor: Colors.BORDER_COLORS,
      hoverBackgroundColor: Colors.BORDER_COLORS,
      hoverBorderColor: Colors.BORDER_COLORS,
      borderWidth: 1
    }];

    this.barChartData = {
      labels: labels,
      datasets: [{
        label: product.name,
        data: dataSet,
        backgroundColor: Colors.BACKGROUND_COLORS,
        borderColor: Colors.BORDER_COLORS,
        hoverBackgroundColor: Colors.BORDER_COLORS,
        hoverBorderColor: Colors.BORDER_COLORS,
        borderWidth: 1
      }]
    };
  }

  private getTeamById(teamId: number, teams: Team[]): Team {
    return teams.find(t => t.id == teamId)!;
  }
}
