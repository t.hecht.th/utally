import {Component} from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {faHashtag} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-logout-pin-dialog',
  templateUrl: './logout-pin-dialog.component.html',
  styleUrls: ['./logout-pin-dialog.component.scss']
})
export class LogoutPinDialogComponent {

  pinControl = new UntypedFormControl('', [Validators.required]);

  constructor(public dialogRef: MatDialogRef<LogoutPinDialogComponent>) { }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    if (this.pinControl.invalid) {
      return;
    }

    this.dialogRef.close(this.pinControl.value);
  }

    protected readonly faHashtag = faHashtag;
}
