import {Component, OnInit} from '@angular/core';
import {SessionService} from "../service/session.service";
import {Router} from "@angular/router";
import {AbstractSpace} from "../common/abstract-space";
import {MessageHandlerService} from "../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {Event} from "../model/event";
import {catchError} from "rxjs";
import {EventHttpAdapter} from "../adapter/event-http-adapter";
import {LocationHttpAdapter} from "../adapter/location-http-adapter";
import {TeamHttpAdapter} from "../adapter/team-http-adapter";
import {PlayerHttpAdapter} from "../adapter/player-http-adapter";
import {ProductHttpAdapter} from "../adapter/product-http-adapter";
import {SettingsHandlerService} from "../service/settings-handler.service";
import {TallyHttpAdapter} from "../adapter/tally-http-adapter";
import {TimeslotHttpAdapter} from "../adapter/timeslot-http-adapter";
import {Location} from "../model/location";
import {Team} from "../model/team";
import {PatchPlayerAsOperator, PatchPlayerComment, Player} from "../model/player";
import {PostTally, Tally} from "../model/tally";
import {Timeslot} from "../model/timeslot";
import {Product} from "../model/product";
import {Settings} from "../model/settings";
import {Payment} from "../model/payment";
import {faArrowRightFromBracket} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-operator-space',
  templateUrl: './operator-space.component.html',
  styleUrls: ['./operator-space.component.scss']
})
export class OperatorSpaceComponent extends AbstractSpace implements OnInit {
  faArrowRightFromBracket = faArrowRightFromBracket;

  toolBarTitle = "Utally - Bill";

  events: Event[] = [];
  locations: Location[] = [];
  teams: Team[] = [];
  players: Player[] = [];
  tallies: Tally[] = [];
  timeslots: Timeslot[] = [];
  products: Product[] = [];

  settings: Settings;
  payment: Payment;

  selectedEvent: Event;
  showEventSelectionProgressIndicator = false;
  showLoadDataProgressIndicator = false;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    dialog: MatDialog,
    private eventHttpAdapter: EventHttpAdapter,
    private locationHttpAdapter: LocationHttpAdapter,
    private teamHttpAdapter: TeamHttpAdapter,
    private playerHttpAdapter: PlayerHttpAdapter,
    private productHttpAdapter: ProductHttpAdapter,
    private tallyHttpAdapter: TallyHttpAdapter,
    private timeslotHttpAdapter: TimeslotHttpAdapter,
    private settingsHandlerService: SettingsHandlerService,
  ) {
    super(sessionService, router, messageHandlerService, dialog);
  }

  ngOnInit(): void {
    this.loadEvent();
  }

  isEventSelected(): boolean {
    return !Number.isNaN(this.sessionService.getEventId());
  }

  onEventSelected(event: Event) {
    this.selectedEvent = event;
  }

  onSelectEventSubmit() {
    if (this.selectedEvent) {
      this.sessionService.selectEvent(this.selectedEvent.id);
      this.afterLoadEvent();
    }
  }

  loadEvent() {
    this.showEventSelectionProgressIndicator = true;
    this.eventHttpAdapter.getEvents().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.showEventSelectionProgressIndicator = false;
      this.events = r.sort((a,b) => a.title.localeCompare(b.title));
      if (this.sessionService.getEventId()) {
        let selectedEvent = this.events.find(e => e.id == this.sessionService.getEventId());
        if (selectedEvent) {
          this.selectedEvent = selectedEvent;
          this.afterLoadEvent();
        }
      }
    });
  }

  afterLoadEvent() {
    this.updateToolbarTitle();
    this.showLoadDataProgressIndicator = true;
    this.loadSettings();
  }

  afterPlayerStateChanged() {
    this.showLoadDataProgressIndicator = true;
    this.loadTallies();
  }

  loadSettings() {
    this.settingsHandlerService.getSettings().pipe(
      catchError(this.handleError(null))
    ).subscribe(r => {
      if (r) {
        this.settings = r;
        this.loadTeams();
      }
    })
  }

  loadTeams() {
    this.teamHttpAdapter.getTeamsByEvent(this.selectedEvent.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.teams = r.sort((a,b) => a.shortName.localeCompare(b.shortName));
      this.loadProducts();
    });
  }

  loadProducts() {
    this.productHttpAdapter.getProductsByEvent(this.selectedEvent.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.products = r.sort((a,b) => a.name.localeCompare(b.name));
      this.loadLocations();
    });
  }

  loadLocations() {
    this.locationHttpAdapter.getLocationsByEvent(this.selectedEvent.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.locations = r.sort((a,b) => a.name.localeCompare(b.name));
      this.loadTimeslots();
    });
  }

  loadTimeslots() {
    this.timeslotHttpAdapter.getTimeslotsByEvent(this.selectedEvent.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.timeslots = r.sort((a, b) => new Date(a.start).getTime() - new Date(b.start).getTime());
      this.loadTallies();
    });
  }

  loadTallies() {
    this.tallyHttpAdapter.getTalliesByEvent(this.selectedEvent.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.tallies = r;
      this.loadPlayers();
    });
  }

  loadPlayers() {
    this.playerHttpAdapter.getPlayersByEvent(this.selectedEvent.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.players = r.sort((a,b) => this.compareTeamShortName(a.teamId, b.teamId) || a.name.localeCompare(b.name));
      this.loadPayment();
    });
  }

  loadPayment() {
    this.settingsHandlerService.getPayment().pipe(
      catchError(this.handleError(null))
    ).subscribe(r => {
      if (r) {
        this.payment = r;
        this.showLoadDataProgressIndicator = false;
      }
    })
  }

  compareTeamShortName(teamId1: number, teamId2: number) : number {
    let teamShortName1 = this.teams.find(t => t.id == teamId1)?.shortName || '';
    let teamShortName2 = this.teams.find(t => t.id == teamId2)?.shortName || '';
    return teamShortName1.localeCompare(teamShortName2);
  }

  updateToolbarTitle() {
    this.toolBarTitle = "Utally - Bill"
    if (this.selectedEvent) {
      this.toolBarTitle = this.toolBarTitle + " - " + this.selectedEvent.title;
    }
  }

  onRevertPayment(player: Player) {
    this.playerHttpAdapter.revertPayment(player.id).pipe(
      catchError(this.handleError(null))
    ).subscribe(_ => this.afterPlayerStateChanged());
  }

  onChangeComment(player: PatchPlayerComment) {
    this.playerHttpAdapter.patchPlayerComment(player).pipe(
      catchError(this.handleError(null))
    ).subscribe(_ => this.afterPlayerStateChanged());
  }

  onPay(player: PatchPlayerAsOperator) {
    this.playerHttpAdapter.patchPlayerAsOperator(player).pipe(
      catchError(this.handleError(null))
    ).subscribe(_ => this.afterPlayerStateChanged());
  }

  onPostTally(tally: PostTally) {
    this.tallyHttpAdapter.postTally(tally).pipe(
      catchError(this.handleError(null))
    ).subscribe(_ => this.afterPlayerStateChanged());
  }

  onTallyCancelled(tally: Tally) {
    this.tallyHttpAdapter.cancelTally(tally.id, !tally.cancelled).pipe(
      catchError(this.handleError(null))
    ).subscribe(_ => this.afterPlayerStateChanged());
  }
}
