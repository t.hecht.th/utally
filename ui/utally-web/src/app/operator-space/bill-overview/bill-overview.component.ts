import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Event} from "../../model/event";
import {Location} from "../../model/location";
import {Team} from "../../model/team";
import {PatchPlayerAsOperator, PatchPlayerComment, Player} from "../../model/player";
import {PostTally, Tally} from "../../model/tally";
import {Timeslot} from "../../model/timeslot";
import {Product} from "../../model/product";
import {Settings} from "../../model/settings";
import {PaymentDialogComponent} from "./payment-dialog/payment-dialog.component";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {Payment} from "../../model/payment";

@Component({
  selector: 'app-bill-overview',
  templateUrl: './bill-overview.component.html',
  styleUrls: ['./bill-overview.component.scss']
})
export class BillOverviewComponent implements OnInit {

  @Input() event: Event;
  @Input() locations: Location[] = [];
  @Input() teams: Team[] = [];
  @Input() timeslots: Timeslot[] = [];
  @Input() products: Product[] = [];

  @Input() showLoadDataProgressIndicator = false;
  @Input() settings: Settings;
  @Input() payment: Payment;

  @Output() onChangeComment = new EventEmitter<PatchPlayerComment>();
  @Output() onPay = new EventEmitter<PatchPlayerAsOperator>();
  @Output() onRevertPayment = new EventEmitter<Player>();
  @Output() onPostTally = new EventEmitter<PostTally>();
  @Output() onTallyCancelled = new EventEmitter<Tally>();

  _players: Player[] = [];
  get players(): Player[] {
    return this._players;
  }
  @Input() set players(value: Player[]) {
    this._players = value;
    this.updateFilteredPlayers();
  }

  _tallies: Tally[] = [];
  get tallies(): Tally[] {
    return this._tallies;
  }
  @Input() set tallies(value: Tally[]) {
    this._tallies = value;
    if (this.dialogRef) {
      this.dialogRef.componentInstance.tallies = this._tallies.filter(t => t.playerId == this.dialogRef?.componentInstance.player.id);
      this.dialogRef.componentInstance.calculateSalesSum();
    }
  }

  selectedTeams: Team[] = [];
  filteredPlayers: Player[] = [];

  private dialogRef: MatDialogRef<PaymentDialogComponent> | null

  constructor(
    private dialog: MatDialog,
    private settingsHandlerService: SettingsHandlerService
  ) { }

  ngOnInit(): void {
  }

  onSelectTeam(team: Team) {
    if (this.selectedTeams.includes(team)) {
      this.selectedTeams = this.selectedTeams.filter(t => t.id != team.id)
    } else {
      this.selectedTeams.push(team)
    }

    this.updateFilteredPlayers();
  }

  private updateFilteredPlayers() {
    if (this.selectedTeams.length > 0) {
      let teamIds = this.selectedTeams.map(t => t.id);
      this.filteredPlayers = this.players.filter(p => teamIds.includes(p.teamId))
    } else {
      this.filteredPlayers = this.players;
    }
  }

  getPaidPercentage() : number {
    if (this.filteredPlayers.length > 0) {
      let playerCount = this.filteredPlayers.length;
      let paidPlayersCount = this.filteredPlayers.filter(p => p.paid).length
      return (100*paidPlayersCount)/playerCount;
    } else {
      return 0;
    }
  }

  onPayClick(player: Player) {
    if (player.paid) {
      this.onRevertPayment.emit(player);
    } else {
      this.dialogRef = this.dialog.open(PaymentDialogComponent);
      let instance = this.dialogRef.componentInstance;
      instance.event = this.event;
      instance.player = player;
      instance.tallies = this.tallies.filter(t => t.playerId == player.id);
      instance.products = this.products;
      instance.locations = this.locations;
      instance.settings = this.settings;
      instance.payment = this.payment;
      instance.settingsHandler = this.settingsHandlerService;
      instance.teamName = this.teams.find(t => t.id == player.teamId)?.fullName || "";
      instance.calculateSalesSum();
      let commentSubscription = instance.onChangeComment.subscribe(r => this.onChangeComment.emit(r));
      let paySubscription = instance.onPay.subscribe(r => this.onPay.emit(r));
      let postTallySubscription = instance.onPostTally.subscribe(r => this.onPostTally.emit(r));
      let tallyCancelledSubscription = instance.onTallyCancelled.subscribe(r => this.onTallyCancelled.emit(r));

      this.dialogRef.afterClosed().subscribe(_ => {
        commentSubscription.unsubscribe();
        paySubscription.unsubscribe();
        postTallySubscription.unsubscribe();
        tallyCancelledSubscription.unsubscribe();
        this.dialogRef = null;
      });
    }
  }
}
