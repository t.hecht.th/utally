import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Team} from "../../../model/team";
import {Player} from "../../../model/player";
import {Settings} from "../../../model/settings";
import {MatTableDataSource} from "@angular/material/table";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {faCheck, faXmark} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-bill-table',
  templateUrl: './bill-table.component.html',
  styleUrls: ['./bill-table.component.scss']
})
export class BillTableComponent implements OnInit {

  @Input() teams: Team[] = [];
  @Input() settings: Settings;

  _players: Player[] = [];
  get players(): Player[] {
    return this._players;
  }
  @Input() set players(value: Player[]) {
    this._players = value;
    if (this.dataSource) {
      this.dataSource.data = this._players;
    }
  }

  @Output() pay = new EventEmitter<Player>();

  displayedColumns = ['name', 'team', 'state', 'withpf', 'pfamount', 'paytime', 'comment', 'pay'];
  dataSource: MatTableDataSource<Player>;

  constructor(public settingsHandler: SettingsHandlerService) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.players);
  }

  getTeamById(id: number): Team | undefined {
    return this.teams.find(t => t.id == id)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  protected readonly faCheck = faCheck;
  protected readonly faXmark = faXmark;
}
