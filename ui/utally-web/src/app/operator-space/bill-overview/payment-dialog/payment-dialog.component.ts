import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PatchPlayerAsOperator, PatchPlayerComment, Player} from "../../../model/player";
import {Product} from "../../../model/product";
import {Location} from "../../../model/location";
import {PostTally, Tally} from "../../../model/tally";
import {MatDialogRef} from "@angular/material/dialog";
import {Settings} from "../../../model/settings";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {Event} from "../../../model/event";
import {Payment} from "../../../model/payment";
import {faComment, faEnvelope} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-payment-dialog',
  templateUrl: './payment-dialog.component.html',
  styleUrls: ['./payment-dialog.component.scss']
})
export class PaymentDialogComponent implements OnInit {

  @Input() teamName = "";
  @Input() event: Event;
  @Input() player: Player;
  @Input() products: Product[] = [];
  @Input() locations: Location[] = [];
  @Input() tallies: Tally[] = [];
  @Input() settings: Settings;
  @Input() payment: Payment;
  @Input() settingsHandler: SettingsHandlerService;

  @Output() onChangeComment = new EventEmitter<PatchPlayerComment>();
  @Output() onPay = new EventEmitter<PatchPlayerAsOperator>();
  @Output() onPostTally = new EventEmitter<PostTally>();
  @Output() onTallyCancelled = new EventEmitter<Tally>();

  sliderValue = 100;
  comment = '';
  salesSum = 0;

  bankConfigured = false;

  constructor(public dialogRef: MatDialogRef<PaymentDialogComponent>) {
    console.log("Opening dialog")
  }

  ngOnInit(): void {
    this.bankConfigured = this.payment?.bankName != null || this.payment?.bankBic != null || this.payment?.bankIban != null || this.payment?.bankRecipient != null;
    this.comment = this.player.comment;
  }

  calculateSalesSum() {
    this.salesSum = this
      .tallies
      .filter(t => !t.cancelled)
      .map(t => this.products.find(p => p.id == t.productId)?.price || 0)
      .reduce((previousValue, currentValue) => previousValue + currentValue, 0);
  }

  onProductSelected(product: Product, location: Location) {
    this.onPostTally.emit({
      productId: product.id,
      locationId: location.id,
      playerId: this.player.id
    });
  }

  getProductsByLocation(location: Location) {
    return this.products.filter(p => location.productIds.includes(p.id))
  }

  getTallyCountByProduct(product: Product) {
    return this.tallies.filter(t => !t.cancelled && t.productId == product.id).length;
  }

  getPlayersFee() {
    return this.event.playersFee*(this.sliderValue/100);
  }

  onShortButtonClick(percentage: number) {
    this.sliderValue = percentage;
  }

  onCommentClick() {
    this.onChangeComment.emit({
      id: this.player.id,
      comment: this.comment
    });
    this.dialogRef.close(null);
  }

  onCloseClick() {
    this.dialogRef.close(null);
  }

  onSubmitClick() {
    let patchPlayer : PatchPlayerAsOperator = {
      id: this.player.id,
      paidPlayersFee: this.getPlayersFee(),
      comment: this.comment,
      withPlayersFee: this.getPlayersFee() > 0
    }
    this.onPay.emit(patchPlayer);
    this.dialogRef.close(null);
  }

  getPaypalMeUrl(): string {
    if (this.payment == null || this.payment.paypalMe == null) {
      return "";
    } else {
      return `https://paypal.me/${this.payment.paypalMe}/${this.settingsHandler.getAmountFormattedWithoutCurrencyCode(this.getPlayersFee() + this.salesSum)}`;
    }
  }

  getEpcQrPayload(): string {
    let description = `${this.event.title} ${this.teamName} ${this.player.name}`;
    return `BCD\n002\n1\nSCT\n${this.payment.bankBic}\n${this.payment.bankRecipient}\n${this.payment.bankIban}\n${this.settingsHandler.getCurrencyCode()}${this.settingsHandler.getAmountFormattedWithoutCurrencyCode(this.getPlayersFee() + this.salesSum)}\n\n\n${description}\n\n`;
  }

  protected readonly faEnvelope = faEnvelope;
  protected readonly faComment = faComment;
}
