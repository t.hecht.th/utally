export interface Product {
  id: number;
  name: string;
  price: number;
  imageName: string;
  category: ProductCategory;
  enabled: boolean;
  creationTime: Date;
  modificationTime: Date;
  description: string;
  displayStatistic: boolean;
}

export enum ProductCategory {
  FOOD = 'FOOD', // Category for oof products
  DRINK = 'DRINK', // Category for drinks
  MDSE = 'MDSE', // Category for merchandise products
  MISC = 'MISC' // Category for the rest
}
