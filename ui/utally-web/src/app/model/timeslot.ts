export interface Timeslot {
  id: number;
  label: string;
  start: Date;
  end: Date;
  enabled: boolean;
  creationTime: Date;
  modificationTime: Date;
  description: string;
  eventId: number;
}
