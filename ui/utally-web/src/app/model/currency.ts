export interface Currency {
  currencyCode: string;
  symbol: string;
  displayName: string;
}
