export interface Player {
  id: number;
  name: string;
  enabled: boolean;
  creationTime: Date;
  modificationTime: Date;
  paid: boolean;
  withPlayersFee: boolean;
  comment: string;
  payTime: Date;
  paidPlayersFee: number;
  preferences: Map<string, string>;
  teamId: number;
  eventId: number;
}

export interface PatchPlayerAsOperator {
  id: number;
  withPlayersFee: boolean;
  comment: string;
  paidPlayersFee: number;
}

export interface PatchPlayerComment {
  id: number;
  comment: string;
}
