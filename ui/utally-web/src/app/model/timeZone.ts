export interface TimeZone {
  zoneId: string;
  abbreviation: string;
  offset: string;
}
