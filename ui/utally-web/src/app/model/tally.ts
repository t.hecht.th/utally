export interface Tally {
  id: number;
  playerId: number;
  locationId: number;
  time: Date;
  productId: number;
  cancelled: boolean;
}

export interface PostTally {
  playerId: number;
  locationId: number;
  productId: number;
}
