export interface Payment {
  bankRecipient: string;
  bankName: string;
  bankIban: string;
  bankBic: string;
  paypalMe: string;
  paypalAccount: string;
}
