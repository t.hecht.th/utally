export interface Location {
  id: number;
  name: string;
  enabled: boolean;
  creationTime: Date;
  modificationTime: Date;
  description: string;
  productIds: number[];
}
