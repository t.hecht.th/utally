export interface Team {
  id: number;
  fullName: string;
  city: string;
  shortName: string;
  enabled: boolean;
  creationTime: Date;
  modificationTime: Date;
  description: string;
}
