export interface Event {
  id: number;
  title: string;
  playersFee: number;
  enabled: boolean;
  creationTime: Date;
  modificationTime: Date;
  description: string;
  locationIds: number[];
  teamIds: number[];
}
