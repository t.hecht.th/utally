export interface StatisticEntry {
  productId: number,
  teamId: number,
  count: number
}
