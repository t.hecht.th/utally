export interface Error {
  error: string;
  message: string;
  detail: string;
}
