import {TimeZone} from "./timeZone";

export interface Settings {
  growlDelay: number;
  currencyCode: string;
  timeZone: TimeZone;
  dateTimeFormat: string;
  keepAliveTimeout: number;
  selfRegistration: boolean;
  dataPoints: number;
  liveStatisticsInterval: number;
}

export interface PatchGrowlDelaySettings {
  growlDelay: number;
}

export interface PatchCurrencySettings {
  currencyCode: string;
}

export interface PatchTimeZoneSettings {
  timeZone: {
    zoneId: string;
  };
}

export interface PatchDateTimeFormatSettings {
  dateTimeFormat: string;
}

export interface PatchSelfRegistrationSettings {
  selfRegistration: boolean;
}

export interface PatchDataPointsSettings {
  dataPoints: number;
}

export interface PatchLiveStatisticsIntervalSettings {
  liveStatisticsInterval: number;
}
