import {Component, Input, OnInit} from '@angular/core';
import {AbstractStatistics} from "../../common/abstract-statistics";
import {Product} from "../../model/product";
import {Team} from "../../model/team";
import {StatisticEntry} from "../../model/statisticEntry";

@Component({
  selector: 'app-party-statistics',
  templateUrl: './party-statistics.component.html',
  styleUrls: ['./party-statistics.component.scss']
})
export class PartyStatisticsComponent extends AbstractStatistics implements OnInit {
  @Input() product: Product;
  @Input() teams: Team[];
  @Input() _statistics: StatisticEntry[];

  get statistics(): StatisticEntry[] {
    return this._statistics;
  }

  @Input() set statistics(value: StatisticEntry[]) {
    this._statistics = value;
    this.updateCharts();
  }

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

  private updateCharts() {
    let productStatistics = this.statistics
      .filter(s => s.productId == this.product.id)
      .sort((a, b) => b.count - a.count);

    this.showStatistics(productStatistics, this.product, this.teams);
  }
}
