import {Component, OnDestroy, OnInit} from '@angular/core';
import {SessionService} from "../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {AbstractSpace} from "../common/abstract-space";
import {TeamHttpAdapter} from "../adapter/team-http-adapter";
import {ProductHttpAdapter} from "../adapter/product-http-adapter";
import {SettingsHandlerService} from "../service/settings-handler.service";
import {Team} from "../model/team";
import {Settings} from "../model/settings";
import {catchError} from "rxjs";
import {Product} from "../model/product";
import {EventHttpAdapter} from "../adapter/event-http-adapter";
import {StatisticEntry} from "../model/statisticEntry";
import {Event} from "../model/event";
import {faArrowRightFromBracket} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-party-space',
  templateUrl: './party-space.component.html',
  styleUrls: ['./party-space.component.scss']
})
export class PartySpaceComponent extends AbstractSpace implements OnInit, OnDestroy {
  faArrowRightFromBracket = faArrowRightFromBracket;

  events: Event[] = [];
  teams: Team[] = [];
  private products: Product[] = [];
  settings: Settings;
  statisticsEntries: StatisticEntry[] = [];

  toolBarTitle: string = "Utally - Party";
  selectedEvent: Event;
  currentProductIndex = 0;
  currentProduct: Product;
  showEventSelectionProgressIndicator = false;

  private timerId = 0;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    dialog: MatDialog,
    private teamHttpAdapter: TeamHttpAdapter,
    private productHttpAdapter: ProductHttpAdapter,
    private settingsHandlerService: SettingsHandlerService,
    private eventHttpAdapter: EventHttpAdapter,
  ) {
    super(sessionService, router, messageHandlerService, dialog);
  }

  ngOnInit(): void {
    this.loadEvent();
  }

  ngOnDestroy() {
    this.clearTimer();
  }

  loadEvent() {
    this.showEventSelectionProgressIndicator = true;
    this.eventHttpAdapter.getEvents().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.showEventSelectionProgressIndicator = false;
      this.events = r.sort((a,b) => a.title.localeCompare(b.title));
      if (this.sessionService.getEventId()) {
        let selectedEvent = this.events.find(e => e.id == this.sessionService.getEventId());
        if (selectedEvent) {
          this.selectedEvent = selectedEvent;
          this.afterLoadEvent();
        }
      }
    });
  }

  afterLoadEvent() {
    this.updateToolbarTitle();
    this.loadSettings();
  }

  isEventSelected(): boolean {
    return !Number.isNaN(this.sessionService.getEventId());
  }

  onEventSelected(event: Event) {
    this.selectedEvent = event;
  }

  onSelectEventSubmit() {
    if (this.selectedEvent) {
      this.sessionService.selectEvent(this.selectedEvent.id);
      this.afterLoadEvent();
    }
  }

  loadSettings() {
    this.settingsHandlerService.getSettings().pipe(
      catchError(this.handleError(null))
    ).subscribe(r => {
        if (r) {
          this.settings = r;
          this.loadTeams();
        }
      }
    );
  }

  loadTeams() {
    this.teamHttpAdapter.getTeamsByEvent(this.sessionService.getEventId()).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
        this.teams = r;
        this.loadProducts();
      }
    );
  }

  loadProducts() {
    this.productHttpAdapter.getProductsByEvent(this.sessionService.getEventId()).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
        this.products = r.filter(p => p.enabled && p.displayStatistic);
        this.loadStatistics(r => {
          this.statisticsEntries = r;
          this.startTimer();
        });
      }
    );
  }

  loadStatistics(next: (value: (StatisticEntry[] | never[])) => void) {
    this.eventHttpAdapter.getStatistics(this.sessionService.getEventId()).pipe(
      catchError(this.handleError([]))
    ).subscribe({
        next: r => next(r),
        error: _ => this.clearTimer()
      }
    );
  }

  startTimer() {
    this.showCurrentProduct();
    this.timerId = window.setInterval(() => this.showNextProduct(), this.settings.liveStatisticsInterval);
  }

  showNextProduct() {
    if (this.products.length > 0) {
      this.currentProductIndex = this.currentProductIndex + 1;
      if (this.currentProductIndex >= this.products.length) {
        this.currentProductIndex = 0;
      }
      this.showCurrentProduct();
      this.updateToolbarTitle();
    }
  }

  showCurrentProduct() {
    this.currentProduct = this.products[this.currentProductIndex];
    this.loadStatistics(r => {
      this.statisticsEntries = r;
    });
  }

  updateToolbarTitle() {
    this.toolBarTitle = "Utally - Party"
    if (this.selectedEvent) {
      this.toolBarTitle = this.toolBarTitle + " - " + this.selectedEvent.title;
    }
    if (this.currentProduct) {
      this.toolBarTitle = this.toolBarTitle + " - " + this.currentProduct.name;
    }
  }

  private clearTimer() {
    if (this.timerId) {
      console.info("Clear timer: " + this.timerId);
      window.clearInterval(this.timerId);
    }
  }
}
