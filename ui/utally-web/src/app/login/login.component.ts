import {Component, OnInit} from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {SessionService, Space} from "../service/session.service";
import {Router} from "@angular/router";
import {faEye, faEyeSlash, faHashtag, faUser} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  pinControl = new UntypedFormControl('');
  spaceControl = new UntypedFormControl('', [Validators.required]);
  usernameControl = new UntypedFormControl('', [Validators.required]);
  passwordControl = new UntypedFormControl('', [Validators.required]);

  constructor(private sessionService: SessionService, private router: Router) {
  }

  ngOnInit() {
    if (this.sessionService.space()) {
      this.performRedirect();
    }
  }

  onSubmit() {
    if (this.spaceControl.invalid || this.usernameControl.invalid || this.passwordControl.invalid) {
      return;
    }

    this.sessionService.login(
      this.usernameControl.value,
      this.passwordControl.value,
      this.spaceControl.value,
      this.pinControl.value
    );

    this.performRedirect();
  }

  private performRedirect() {
    switch (this.sessionService.space()) {
      case Space.ADMIN: {
        this.router.navigate(['admin'])
        break;
      }
      case Space.OPERATOR: {
        this.router.navigate(['operator'])
        break;
      }
      case Space.USER: {
        this.router.navigate(['user'])
        break;
      }
      case Space.PARTY: {
        this.router.navigate(['party'])
        break;
      }
    }
  }

  protected readonly faUser = faUser;
  protected readonly faEyeSlash = faEyeSlash;
  protected readonly faEye = faEye;
  protected readonly faHashtag = faHashtag;
}
