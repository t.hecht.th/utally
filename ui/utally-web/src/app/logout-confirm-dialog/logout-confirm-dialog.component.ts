import {Component} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-logout-confirm-dialog',
  templateUrl: './logout-confirm-dialog.component.html',
  styleUrls: ['./logout-confirm-dialog.component.scss']
})
export class LogoutConfirmDialogComponent {

  constructor(public dialogRef: MatDialogRef<LogoutConfirmDialogComponent>) { }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  onSubmitClick() {
    this.dialogRef.close(true);
  }
}
