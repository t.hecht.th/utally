import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {AdminSpaceComponent} from "./admin-space/admin-space.component";
import {OperatorSpaceComponent} from "./operator-space/operator-space.component";
import {UserSpaceComponent} from "./user-space/user-space.component";
import {AuthGuardService} from "./service/auth-guard.service";
import {PartySpaceComponent} from "./party-space/party-space.component";

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'admin', component: AdminSpaceComponent, canActivate: [AuthGuardService] },
  { path: 'operator', component: OperatorSpaceComponent, canActivate: [AuthGuardService] },
  { path: 'user', component: UserSpaceComponent, canActivate: [AuthGuardService] },
  { path: 'party', component: PartySpaceComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
