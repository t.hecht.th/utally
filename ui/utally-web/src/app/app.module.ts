import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatButtonModule} from "@angular/material/button";
import {MatToolbarModule} from "@angular/material/toolbar";
import {HttpClientModule} from "@angular/common/http";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserSpaceComponent} from './user-space/user-space.component';
import {OperatorSpaceComponent} from './operator-space/operator-space.component';
import {AdminSpaceComponent} from './admin-space/admin-space.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {LogoutPinDialogComponent} from './logout-pin-dialog/logout-pin-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {LogoutConfirmDialogComponent} from './logout-confirm-dialog/logout-confirm-dialog.component';
import {SelectLocationComponent} from './user-space/select-location/select-location.component';
import {SelectPlayerComponent} from './user-space/select-player/select-player.component';
import {SelectProductComponent} from './user-space/select-product/select-product.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatTabsModule} from "@angular/material/tabs";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatListModule} from "@angular/material/list";
import {NewPlayerDialogComponent} from './user-space/new-player-dialog/new-player-dialog.component';
import { ProductCardComponent } from './user-space/select-product/product-card/product-card.component';
import {MatBadgeModule} from "@angular/material/badge";
import { TimeslotTableComponent } from './user-space/select-product/timeslot-table/timeslot-table.component';
import {MatTableModule} from "@angular/material/table";
import { ProductImageComponent } from './user-space/select-product/product-image/product-image.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { TallyTableComponent } from './user-space/select-product/tally-table/tally-table.component';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatRippleModule} from "@angular/material/core";
import { ProductStatisticsComponent } from './user-space/product-statistics/product-statistics.component';
import {NgChartsModule} from "ng2-charts";
import {CurrencyPipe} from "@angular/common";
import { SelectEventComponent } from './common/select-event/select-event.component';
import { BillOverviewComponent } from './operator-space/bill-overview/bill-overview.component';
import { BillTableComponent } from './operator-space/bill-overview/bill-table/bill-table.component';
import { PaymentDialogComponent } from './operator-space/bill-overview/payment-dialog/payment-dialog.component';
import {MatSliderModule} from "@angular/material/slider";
import {MatExpansionModule} from "@angular/material/expansion";
import { PartySpaceComponent } from './party-space/party-space.component';
import { PartyStatisticsComponent } from './party-space/party-statistics/party-statistics.component';
import { FooterComponent } from './common/footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {MatSidenavModule} from "@angular/material/sidenav";
import { AdminProductComponent } from './admin-space/admin-product/admin-product.component';
import { AdminLocationComponent } from './admin-space/admin-location/admin-location.component';
import { AdminTeamComponent } from './admin-space/admin-team/admin-team.component';
import { AdminEventComponent } from './admin-space/admin-event/admin-event.component';
import { AdminSettingComponent } from './admin-space/admin-setting/admin-setting.component';
import { AdminTitleComponent } from './admin-space/admin-title/admin-title.component';
import { ProductCategoryComponent } from './admin-space/admin-product/product-category/product-category.component';
import { NewProductDialogComponent } from './admin-space/admin-product/new-product-dialog/new-product-dialog.component';
import {MatRadioModule} from "@angular/material/radio";
import {MatCheckboxModule} from "@angular/material/checkbox";
import { EditProductDialogComponent } from './admin-space/admin-product/edit-product-dialog/edit-product-dialog.component';
import { DeleteProductDialogComponent } from './admin-space/admin-product/delete-product-dialog/delete-product-dialog.component';
import { AddLocationToProductDialogComponent } from './admin-space/admin-product/add-location-to-product-dialog/add-location-to-product-dialog.component';
import { UploadImageDialogComponent } from './admin-space/admin-product/upload-image-dialog/upload-image-dialog.component';
import {ImageCropperModule} from "ngx-image-cropper";
import { NewLocationDialogComponent } from './admin-space/admin-location/new-location-dialog/new-location-dialog.component';
import { EditLocationDialogComponent } from './admin-space/admin-location/edit-location-dialog/edit-location-dialog.component';
import { AddProductToLocationDialogComponent } from './admin-space/admin-location/add-product-to-location-dialog/add-product-to-location-dialog.component';
import { DeleteLocationDialogComponent } from './admin-space/admin-location/delete-location-dialog/delete-location-dialog.component';
import { NewTeamDialogComponent } from './admin-space/admin-team/new-team-dialog/new-team-dialog.component';
import { EditTeamDialogComponent } from './admin-space/admin-team/edit-team-dialog/edit-team-dialog.component';
import { DeleteTeamDialogComponent } from './admin-space/admin-team/delete-team-dialog/delete-team-dialog.component';
import { AdminTeamPlayersComponent } from './admin-space/admin-team-players/admin-team-players.component';
import {
  NewPlayerAdminDialogComponent
} from "./admin-space/admin-team-players/new-player-dialog/new-player-admin-dialog.component";
import { EditPlayerAdminDialogComponent } from './admin-space/admin-team-players/edit-player-admin-dialog/edit-player-admin-dialog.component';
import { DeletePlayerAdminDialogComponent } from './admin-space/admin-team-players/delete-player-admin-dialog/delete-player-admin-dialog.component';
import { MergePlayerDialogComponent } from './admin-space/admin-team-players/merge-player-dialog/merge-player-dialog.component';
import { NewEventDialogComponent } from './admin-space/admin-event/new-event-dialog/new-event-dialog.component';
import { EditEventDialogComponent } from './admin-space/admin-event/edit-event-dialog/edit-event-dialog.component';
import { DeleteEventDialogComponent } from './admin-space/admin-event/delete-event-dialog/delete-event-dialog.component';
import { AddLocationToEventDialogComponent } from './admin-space/admin-event/add-location-to-event-dialog/add-location-to-event-dialog.component';
import { AddTeamToEventDialogComponent } from './admin-space/admin-event/add-team-to-event-dialog/add-team-to-event-dialog.component';
import { AdminTimeslotComponent } from './admin-space/admin-timeslot/admin-timeslot.component';
import { NewTimeslotDialogComponent } from './admin-space/admin-timeslot/new-timeslot-dialog/new-timeslot-dialog.component';
import { EditTimeslotDialogComponent } from './admin-space/admin-timeslot/edit-timeslot-dialog/edit-timeslot-dialog.component';
import { DeleteTimeslotDialogComponent } from './admin-space/admin-timeslot/delete-timeslot-dialog/delete-timeslot-dialog.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import { AdminEventStatisticsComponent } from './admin-space/admin-event-statistics/admin-event-statistics.component';
import {MatGridListModule} from "@angular/material/grid-list";
import { SalesVolumeStatisticsComponent } from './admin-space/admin-event-statistics/sales-volume-statistics/sales-volume-statistics.component';
import { EventProductStatisticsComponent } from './admin-space/admin-event-statistics/event-product-statistics/event-product-statistics.component';
import { EventTeamStatisticsComponent } from './admin-space/admin-event-statistics/event-team-statistics/event-team-statistics.component';
import { EventLocationStatisticsComponent } from './admin-space/admin-event-statistics/event-location-statistics/event-location-statistics.component';
import { EventTimeslotStatisticsComponent } from './admin-space/admin-event-statistics/event-timeslot-statistics/event-timeslot-statistics.component';
import {QRCodeModule} from "angularx-qrcode";
import { AdminTimeslotStatisticsComponent } from './admin-space/admin-timeslot-statistics/admin-timeslot-statistics.component';
import { TimeslotSalesVolumeStatisticsComponent } from './admin-space/admin-timeslot-statistics/timeslot-sales-volume-statistics/timeslot-sales-volume-statistics.component';
import { TimeslotProductStatisticsComponent } from './admin-space/admin-timeslot-statistics/timeslot-product-statistics/timeslot-product-statistics.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserSpaceComponent,
    OperatorSpaceComponent,
    AdminSpaceComponent,
    LogoutPinDialogComponent,
    LogoutConfirmDialogComponent,
    SelectLocationComponent,
    SelectPlayerComponent,
    SelectProductComponent,
    NewPlayerDialogComponent,
    ProductCardComponent,
    TimeslotTableComponent,
    ProductImageComponent,
    TallyTableComponent,
    ProductStatisticsComponent,
    SelectEventComponent,
    BillOverviewComponent,
    BillTableComponent,
    PaymentDialogComponent,
    PartySpaceComponent,
    PartyStatisticsComponent,
    FooterComponent,
    AdminProductComponent,
    AdminLocationComponent,
    AdminTeamComponent,
    AdminEventComponent,
    AdminSettingComponent,
    AdminTitleComponent,
    ProductCategoryComponent,
    NewProductDialogComponent,
    EditProductDialogComponent,
    DeleteProductDialogComponent,
    AddLocationToProductDialogComponent,
    UploadImageDialogComponent,
    NewLocationDialogComponent,
    EditLocationDialogComponent,
    AddProductToLocationDialogComponent,
    DeleteLocationDialogComponent,
    NewTeamDialogComponent,
    EditTeamDialogComponent,
    DeleteTeamDialogComponent,
    AdminTeamPlayersComponent,
    NewPlayerAdminDialogComponent,
    EditPlayerAdminDialogComponent,
    DeletePlayerAdminDialogComponent,
    MergePlayerDialogComponent,
    NewEventDialogComponent,
    EditEventDialogComponent,
    DeleteEventDialogComponent,
    AddLocationToEventDialogComponent,
    AddTeamToEventDialogComponent,
    AdminTimeslotComponent,
    NewTimeslotDialogComponent,
    EditTimeslotDialogComponent,
    DeleteTimeslotDialogComponent,
    AdminEventStatisticsComponent,
    SalesVolumeStatisticsComponent,
    EventProductStatisticsComponent,
    EventTeamStatisticsComponent,
    EventLocationStatisticsComponent,
    EventTimeslotStatisticsComponent,
    AdminTimeslotStatisticsComponent,
    TimeslotSalesVolumeStatisticsComponent,
    TimeslotProductStatisticsComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatDialogModule,
    MatProgressBarModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatListModule,
    MatBadgeModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatRippleModule,
    NgChartsModule,
    MatSliderModule,
    MatExpansionModule,
    FontAwesomeModule,
    MatSidenavModule,
    MatRadioModule,
    MatCheckboxModule,
    ImageCropperModule,
    MatSlideToggleModule,
    MatGridListModule,
    QRCodeModule
  ],
  providers: [CurrencyPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
