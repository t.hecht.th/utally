import {Component, Inject, OnInit} from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Player} from "../../../model/player";

@Component({
  selector: 'app-edit-player-admin-dialog',
  templateUrl: './edit-player-admin-dialog.component.html',
  styleUrls: ['./edit-player-admin-dialog.component.scss']
})
export class EditPlayerAdminDialogComponent implements OnInit {

  nameControl = new UntypedFormControl('', [Validators.required]);
  enabled = true;

  player: Player;
  constructor(
    public dialogRef: MatDialogRef<EditPlayerAdminDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { player: Player }
  ) {
    this.player = data.player;
  }

  ngOnInit() {
    this.enabled = this.player.enabled;
    this.nameControl.setValue(this.player.name);
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    if (this.nameControl.invalid) {
      return;
    }

    this.dialogRef.close({
      id: this.player.id,
      name: this.nameControl.value,
      enabled: this.enabled,
    } as Player);
  }
}
