import {Component, Inject} from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Team} from "../../../model/team";
import {Event} from "../../../model/event";
import {Player} from "../../../model/player";

@Component({
  selector: 'app-new-player-admin-dialog',
  templateUrl: './new-player-admin-dialog.component.html',
  styleUrls: ['./new-player-admin-dialog.component.scss']
})
export class NewPlayerAdminDialogComponent {

  nameControl = new UntypedFormControl('', [Validators.required]);
  selectFormControl = new UntypedFormControl('', [Validators.required]);
  enabled = true;

  teams: Team[] = [];
  events: Event[] = [];

  selectedEvent: Event;
  selectedTeam: Team;

  constructor(
    public dialogRef: MatDialogRef<NewPlayerAdminDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { teams: Team[] | null, events: Event[] | null, team: Team | null, event: Event | null },
  ) {
    if (data.teams && data.event) {
      this.teams = data.teams;
      this.selectedEvent = data.event;
    } else if (data.events && data.team) {
      this.events = data.events;
      this.selectedTeam = data.team;
    }
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    if (this.nameControl.invalid) {
      return;
    }

    if (this.selectFormControl.invalid) {
      return;
    }

    if (this.data.teams) {
      this.selectedTeam = this.selectFormControl.value;
    }

    if (this.data.events) {
      this.selectedEvent = this.selectFormControl.value;
    }

    this.dialogRef.close({
      name: this.nameControl.value,
      enabled: this.enabled,
      teamId: this.selectedTeam.id,
      eventId: this.selectedEvent.id
    } as Player);
  }
}
