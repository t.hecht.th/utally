import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {Player} from "../../model/player";
import {Team} from "../../model/team";
import {Event} from "../../model/event";
import {Tally} from "../../model/tally";
import {Product} from "../../model/product";
import {AdminTableSpace} from "../admin-table/admin-table-space";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {TeamHttpAdapter} from "../../adapter/team-http-adapter";
import {PlayerHttpAdapter} from "../../adapter/player-http-adapter";
import {EventHttpAdapter} from "../../adapter/event-http-adapter";
import {TallyHttpAdapter} from "../../adapter/tally-http-adapter";
import {ProductHttpAdapter} from "../../adapter/product-http-adapter";
import {MatPaginator} from "@angular/material/paginator";
import {catchError} from "rxjs";
import {NewPlayerAdminDialogComponent} from "./new-player-dialog/new-player-admin-dialog.component";
import {EditPlayerAdminDialogComponent} from "./edit-player-admin-dialog/edit-player-admin-dialog.component";
import {DeletePlayerAdminDialogComponent} from "./delete-player-admin-dialog/delete-player-admin-dialog.component";
import {MergePlayerDialogComponent} from "./merge-player-dialog/merge-player-dialog.component";
import {
  faArrowLeft,
  faCheck,
  faCodeMerge,
  faEdit,
  faPlus,
  faTrashCan,
  faXmark
} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-admin-team-players',
  templateUrl: './admin-team-players.component.html',
  styleUrls: ['./admin-team-players.component.scss']
})
export class AdminTeamPlayersComponent extends AdminTableSpace<Player> {

  @Input() team: Team;
  @Input() event: Event;
  teams: Team[] = [];
  events: Event[] = [];
  tallies: Tally[] = [];
  products: Product[] = [];

  displayedColumns = ['name', 'team', 'event', 'tallies', 'sales', 'enabled', 'creationtime'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  title = 'Players';

  @Output() onBackClick = new EventEmitter<void>();

  constructor(sessionService: SessionService,
              router: Router,
              messageHandlerService: MessageHandlerService,
              dialog: MatDialog,
              settingsHandlerService: SettingsHandlerService,
              private teamHttpAdapter: TeamHttpAdapter,
              private playerHttpAdapter: PlayerHttpAdapter,
              private eventHttpAdapter: EventHttpAdapter,
              private tallyHttpAdapter: TallyHttpAdapter,
              private productHttpAdapter: ProductHttpAdapter,
  ) {
    super(sessionService, router, messageHandlerService, dialog, settingsHandlerService);
  }

  override afterLoadSettings() {
    this.loadProducts();
  }

  loadProducts() {
    this.productHttpAdapter.getProducts().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.products = r.sort((a,b) => a.name.localeCompare(b.name));
      this.afterLoadProducts();
    });
  }

  afterLoadProducts() {
    if (this.team) {
      this.title = this.team.fullName + ' - Players';
      this.teams = [this.team];
      this.loadEvents();
    } else if (this.event) {
      this.title = this.event.title + ' - Players';
      this.events = [this.event];
      this.loadTeams();
    }
  }

  loadEvents() {
    this.eventHttpAdapter.getEvents().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.events = r.sort((a,b) => a.title.localeCompare(b.title));
      this.loadTalliesByTeam();
    });
  }

  loadTalliesByTeam() {
    this.tallyHttpAdapter.getTalliesByTeam(this.team.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.tallies = r;
      this.loadPlayersByTeam();
    });
  }

  loadPlayersByTeam() {
    this.playerHttpAdapter.getPlayersByTeam(this.team.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.clickedRows.clear();
      this.setItems(r);
      this.showProgressIndicator = false;
    })
  }

  loadTeams() {
    this.teamHttpAdapter.getTeams().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.teams = r.sort((a,b) => a.fullName.localeCompare(b.fullName));
      this.loadTalliesByEvent();
    });
  }

  loadTalliesByEvent() {
    this.tallyHttpAdapter.getTalliesByEvent(this.event.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.tallies = r;
      this.loadPlayersByEvent();
    });
  }

  loadPlayersByEvent() {
    this.playerHttpAdapter.getPlayersByEvent(this.event.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.clickedRows.clear();
      this.setItems(r);
      this.showProgressIndicator = false;
    })
  }

  loadPlayers() {
    this.showProgressIndicator = true;
    if (this.team) {
      this.loadPlayersByTeam();
    } else if (this.event) {
      this.loadPlayersByEvent();
    }
  }

  getPaginator(): MatPaginator {
    return this.paginator;
  }

  showNewDialog() {
    let dialogRef;
    if (this.team) {
      dialogRef = this.dialog.open(NewPlayerAdminDialogComponent, {
        width: '500px',
        data: {
          events: this.events,
          team: this.team
        }
      });
    } else if (this.event) {
      dialogRef = this.dialog.open(NewPlayerAdminDialogComponent, {
        width: '500px',
        data: {
          teams: this.teams,
          event: this.event
        }
      });
    } else {
      return;
    }

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.playerHttpAdapter.postPlayer(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Player created")
              this.loadPlayers();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showEditDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(EditPlayerAdminDialogComponent, {
        width: '500px',
        data: {
          player: this.clickedRows.values().next().value
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.playerHttpAdapter.patchPlayerAsAdmin(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Player edited")
              this.loadPlayers();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showMergeDialog() {
    if (this.clickedRows.size <= 1) {
      return;
    }

    let clickedRowValues = this.clickedRows.values();
    let player1 = clickedRowValues.next().value;
    let player2 = clickedRowValues.next().value;
    const dialogRef = this.dialog.open(MergePlayerDialogComponent, {
      width: '500px',
      data: {
        player1: player1,
        player2: player2,
        sum1: this.getSumOfTallies(player1),
        sum2: this.getSumOfTallies(player2),
        settings: this.settings
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != 0) {
        this.showProgressIndicator = true;
        let source = result > 0 ? player1 : player2;
        let target = result > 0 ? player2 : player1;
        this.playerHttpAdapter.mergePlayers(source.id, target.id).pipe(
          catchError(this.handleError(null))
        ).subscribe(_ => {
            this.messageHandlerService.onInfo("Players merged")
            this.loadPlayers();
          }
        );
      }
    });
  }

  showDeleteDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(DeletePlayerAdminDialogComponent, {
      width: '500px',
      data: {
        player: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.playerHttpAdapter.deletePlayer(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(_ => {
            this.messageHandlerService.onInfo("Player deleted")
            this.loadPlayers();
          }
        )
      }
    });
  }

  getTeamByPlayer(player: Player): Team | undefined {
    return this.teams.find(t => player.teamId == t.id);
  }

  getEventByPlayer(player: Player): Event | undefined {
    return this.events.find(e => player.eventId == e.id);
  }

  getTalliesCountByPlayer(player: Player): number {
    return this.tallies.filter(t => t.playerId == player.id && !t.cancelled).length;
  }

  getSumOfTallies(player: Player): number {
    if (this.tallies && this.tallies.length > 0 && this.products) {
      return this
        .tallies
        .filter(t => !t.cancelled && t.playerId == player.id)
        .map(t => this.getProductById(t.productId))
        .map(p => p ? p.price : 0)
        .reduce((p, c) => p+c, 0);
    } else {
      return 0;
    }
  }

  getProductById(productId: number): Product | undefined {
    return this.products.find(p => p.id == productId)
  }

    protected readonly faPlus = faPlus;
  protected readonly faEdit = faEdit;
  protected readonly faTrashCan = faTrashCan;
  protected readonly faCodeMerge = faCodeMerge;
  protected readonly faArrowLeft = faArrowLeft;
  protected readonly faCheck = faCheck;
  protected readonly faXmark = faXmark;
}
