import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Player} from "../../../model/player";

@Component({
  selector: 'app-delete-player-admin-dialog',
  templateUrl: './delete-player-admin-dialog.component.html',
  styleUrls: ['./delete-player-admin-dialog.component.scss']
})
export class DeletePlayerAdminDialogComponent {

  player: Player;

  constructor(
    public dialogRef: MatDialogRef<DeletePlayerAdminDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { player: Player }
  ) {
    this.player = data.player;
  }


}
