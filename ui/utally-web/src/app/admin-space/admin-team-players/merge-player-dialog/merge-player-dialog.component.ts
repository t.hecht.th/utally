import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Player} from "../../../model/player";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {Settings} from "../../../model/settings";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-merge-player-dialog',
  templateUrl: './merge-player-dialog.component.html',
  styleUrls: ['./merge-player-dialog.component.scss']
})
export class MergePlayerDialogComponent {

  constructor(
    public settingsHandlerService: SettingsHandlerService,
    @Inject(MAT_DIALOG_DATA) public data: { player1: Player, sum1: number, player2: Player, sum2: number, settings: Settings | null }
  ) { }

  protected readonly faChevronLeft = faChevronLeft;
  protected readonly faChevronRight = faChevronRight;
}
