import {AbstractSpace} from "../../common/abstract-space";
import {AfterViewInit, Injectable, OnInit} from "@angular/core";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {catchError} from "rxjs";
import {Settings} from "../../model/settings";

@Injectable()
export abstract class AdminTableSpace<T> extends AbstractSpace implements OnInit, AfterViewInit {

  protected items: T[] = [];

  protected clickedRows = new Set<T>();

  protected showProgressIndicator = false;

  protected dataSource: MatTableDataSource<T>;

  settings: Settings | null;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    dialog: MatDialog,
    public settingsHandlerService: SettingsHandlerService,
  ) {
    super(sessionService, router, messageHandlerService, dialog);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.items);
    this.loadSettings();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.getPaginator();
  }

  protected setItems(items: T[]) {
    this.items = items;
    this.dataSource.data = items;
  }

  loadSettings() {
    this.showProgressIndicator = true;
    this.settingsHandlerService.getSettings().pipe(
      catchError(this.handleError(null))
    ).subscribe(r => {
      if (r) {
        this.settings = r;
        this.afterLoadSettings();
      }
    })
  }

  protected onRowClicked(item: T) {
    if (this.clickedRows.has(item)) {
      this.clickedRows.delete(item);
    } else {
      this.clickedRows.add(item);
    }
  }

  abstract afterLoadSettings() : void;

  abstract getPaginator(): MatPaginator;
}
