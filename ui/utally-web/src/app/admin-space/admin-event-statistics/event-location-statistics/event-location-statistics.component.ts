import {Component, Input, ViewChild} from '@angular/core';
import {Product} from "../../../model/product";
import {Tally} from "../../../model/tally";
import {Settings} from "../../../model/settings";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from "../../../model/location";
import {AbstractEventStatistics} from "../../../common/abstract-event-statistics";

@Component({
  selector: 'app-event-location-statistics',
  templateUrl: './event-location-statistics.component.html',
  styleUrls: ['./event-location-statistics.component.scss']
})
export class EventLocationStatisticsComponent extends AbstractEventStatistics<Location> {

  @Input() locations: Location[] = [];
  @Input() products: Product[] = [];
  @Input() tallies: Tally[] = [];

  @Input() settings: Settings;
  @Input() settingsHandlerService: SettingsHandlerService;

  displayedColumns = ['name', 'count', 'volume', 'cancelled', 'cancelledvol'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  protected override getPaginator(): MatPaginator {
    return this.paginator;
  }

  protected override getSettings(): Settings {
    return this.settings;
  }

  protected override getSettingsHandlerService(): SettingsHandlerService {
    return this.settingsHandlerService;
  }

  protected override getProducts(): Product[] {
    return this.products;
  }

  protected override getItems(): Location[] {
    return this.locations;
  }

  protected override getTallies(): Tally[] {
    return this.tallies;
  }

  protected override getTalliesByItem(item: Location): Tally[] {
    return this.tallies.filter(t => t.locationId == item.id).filter(t => !t.cancelled);
  }

  protected override getCancelledTallies(item: Location): Tally[] {
    return this.tallies.filter(t => t.locationId == item.id).filter(t => t.cancelled);
  }

  protected override getLabel(item: Location): string {
    return item.name;
  }
}
