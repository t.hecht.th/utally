import {Component, Input, ViewChild} from '@angular/core';
import {Product} from "../../../model/product";
import {Tally} from "../../../model/tally";
import {Settings} from "../../../model/settings";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {Team} from "../../../model/team";
import {Player} from "../../../model/player";
import {MatPaginator} from "@angular/material/paginator";
import {AbstractEventStatistics} from "../../../common/abstract-event-statistics";

@Component({
  selector: 'app-event-team-statistics',
  templateUrl: './event-team-statistics.component.html',
  styleUrls: ['./event-team-statistics.component.scss']
})
export class EventTeamStatisticsComponent extends AbstractEventStatistics<Team> {
  @Input() products: Product[] = [];
  @Input() players: Player[] = [];
  @Input() teams: Team[] = [];
  @Input() tallies: Tally[] = [];

  @Input() settings: Settings;
  @Input() settingsHandlerService: SettingsHandlerService;

  displayedColumns = ['name', 'players', 'paid', 'count', 'volume', 'cancelled', 'cancelledvol'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  getPlayersByTeam(team: Team): Player[] {
    return this.players.filter(p => p.teamId == team.id);
  }

  getPaidPlayersByTeam(team: Team): Player[] {
    return this.getPlayersByTeam(team).filter(p => p.paid);
  }

  protected override getPaginator(): MatPaginator {
    return this.paginator;
  }

  protected override getItems(): Team[] {
    return this.teams;
  }

  protected override getLabel(item: Team): string {
    return item.shortName;
  }

  protected override getTallies(): Tally[] {
    return this.tallies;
  }

  protected override getTalliesByItem(item: Team): Tally[] {
    let playerIds = this.players.filter(p => p.teamId == item.id).map(p => p.id);
    return this.tallies.filter(t => playerIds.includes(t.playerId)).filter(t => !t.cancelled);
  }

  protected override getCancelledTallies(item: Team): Tally[] {
    let playerIds = this.players.filter(p => p.teamId == item.id).map(p => p.id);
    return this.tallies.filter(t => playerIds.includes(t.playerId)).filter(t => t.cancelled);
  }

  protected override getSettings(): Settings {
    return this.settings;
  }

  protected override getSettingsHandlerService(): SettingsHandlerService {
    return this.settingsHandlerService;
  }

  protected override getProducts(): Product[] {
    return this.products;
  }
}
