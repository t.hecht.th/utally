import {Component, Input, OnInit} from '@angular/core';
import {Event} from "../../../model/event";
import {Timeslot} from "../../../model/timeslot";
import {Player} from "../../../model/player";
import {Team} from "../../../model/team";
import {Product} from "../../../model/product";
import {Location} from "../../../model/location";
import {Tally} from "../../../model/tally";
import {ChartConfiguration, ChartOptions} from "chart.js";
import {Colors} from "../../../common/colors";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {Settings} from "../../../model/settings";

@Component({
  selector: 'app-sales-volume-statistics',
  templateUrl: './sales-volume-statistics.component.html',
  styleUrls: ['./sales-volume-statistics.component.scss']
})
export class SalesVolumeStatisticsComponent implements OnInit {

  @Input() event: Event;
  @Input() timeslots: Timeslot[] = [];
  @Input() players: Player[] = [];
  @Input() teams: Team[] = [];
  @Input() products: Product[] = [];
  @Input() locations: Location[] = [];
  @Input() tallies: Tally[] = [];

  @Input() settings: Settings;
  @Input() settingsHandlerService: SettingsHandlerService;

  pieChartOptions: ChartOptions<'pie'> = {
    responsive: true,
  };
  lineChartOptions: ChartOptions<'line'> = {
    responsive: true,
  };
  chartLegend = true;

  locationPieChartLabels: string[];
  locationPieChartDatasets: {
    label: string,
    data: number[],
    backgroundColor: string[],
    borderColor: string[],
    hoverBackgroundColor: string[],
    hoverBorderColor: string[],
    borderWidth: number
  }[];

  timeslotPieChartLabels: string[];
  timeslotPieChartDatasets: {
    label: string,
    data: number[],
    backgroundColor: string[],
    borderColor: string[],
    hoverBackgroundColor: string[],
    hoverBorderColor: string[],
    borderWidth: number
  }[];

  teamPieChartLabels: string[];
  teamPieChartDatasets: {
    label: string,
    data: number[],
    backgroundColor: string[],
    borderColor: string[],
    hoverBackgroundColor: string[],
    hoverBorderColor: string[],
    borderWidth: number
  }[];

  productPieChartLabels: string[];
  productPieChartDatasets: {
    label: string,
    data: number[],
    backgroundColor: string[],
    borderColor: string[],
    hoverBackgroundColor: string[],
    hoverBorderColor: string[],
    borderWidth: number
  }[];

  lineChartData: ChartConfiguration<'line'>['data'] = {
    labels: [],
    datasets: [
      {
        data: [],
        label: 'Sales Volume',
        fill: true,
        tension: 0.5,
        borderColor: Colors.BORDER_COLORS[0],
        backgroundColor: Colors.BACKGROUND_COLORS[0],
        pointBorderColor: Colors.BORDER_COLORS[0],
        pointBackgroundColor: Colors.BACKGROUND_COLORS[0]
      },
      {
        data: [],
        label: 'Cancelled Sales Volume',
        fill: true,
        tension: 0.5,
        borderColor: Colors.BORDER_COLORS[1],
        backgroundColor: Colors.BACKGROUND_COLORS[1],
        pointBorderColor: Colors.BORDER_COLORS[1],
        pointBackgroundColor: Colors.BACKGROUND_COLORS[1]
      }
    ]
  };

  constructor(

  ) { }

  ngOnInit(): void {
    this.updateLineCharts();
    this.updatePieCharts();
  }

  updateLineCharts() {
    let sortedTallies = this.tallies.sort((a,b) => new Date(a.time).getTime() - new Date(b.time).getTime());
    if (sortedTallies.length < 2) {
      return;
    }

    let numberOfDataPoints = this.settings.dataPoints;

    let startTime = new Date(sortedTallies[0].time);
    let endTime = new Date(sortedTallies[sortedTallies.length-1].time);
    let stepSize = Math.ceil((endTime.getTime() - startTime.getTime())/numberOfDataPoints);

    let labels: string[] = [];
    let salesVolumeData: number[] = [];
    let cancelledSalesVolumeData: number[] = [];

    labels.push(this.settingsHandlerService.getTimestampFormatted(startTime));
    salesVolumeData.push(0);
    cancelledSalesVolumeData.push(0);

    for (let i = 0; i < numberOfDataPoints; i++) {
      let lowerBoundary = startTime.getTime() + i * stepSize;
      let upperBoundary = lowerBoundary + stepSize;
      labels.push(this.settingsHandlerService.getTimestampFormatted(new Date(upperBoundary)));

      let salesVolume = salesVolumeData[salesVolumeData.length-1];
      let cancelledSalesVolume = cancelledSalesVolumeData[cancelledSalesVolumeData.length-1];
      let tallies = this.tallies
        .filter(t => new Date(t.time).getTime() >= lowerBoundary && new Date(t.time).getTime() < upperBoundary);
      let validTallies = tallies.filter(t => !t.cancelled);
      let cancelledTallies = tallies.filter(t => t.cancelled);

      let salesVolumeDelta = this.settingsHandlerService.getFractionDigits(this.calculateSalesVolume(validTallies));
      let cancelledSalesVolumeDelta = this.settingsHandlerService.getFractionDigits(this.calculateSalesVolume(cancelledTallies));

      salesVolumeData.push(salesVolume + salesVolumeDelta);
      cancelledSalesVolumeData.push(cancelledSalesVolume + cancelledSalesVolumeDelta);
    }

    let lineChartData = this.lineChartData;
    lineChartData.labels = labels;
    lineChartData.datasets[0].data = salesVolumeData;
    lineChartData.datasets[1].data = cancelledSalesVolumeData;
    this.lineChartData = lineChartData;
  }

  updatePieCharts() {
    let locationLabels = this.locations.map(l => l.name);
    let locationDataSets = this.locations
      .map(l => l.id)
      .map(id => this.tallies.filter(t => !t.cancelled).filter(t => t.locationId == id))
      .map(t => this.calculateSalesVolume(t))
      .map(v => this.settingsHandlerService.getFractionDigits(v));
    this.locationPieChartLabels = locationLabels;
    this.locationPieChartDatasets = [{
      label: "By Locations",
      data: locationDataSets,
      backgroundColor: Colors.BACKGROUND_COLORS,
      borderColor: Colors.BORDER_COLORS,
      hoverBackgroundColor: Colors.BORDER_COLORS,
      hoverBorderColor: Colors.BORDER_COLORS,
      borderWidth: 1
    }];

    let timeslotLabels = this.timeslots.map(t => t.label);
    let timeslotDataSets = this.timeslots
      .map(ts => this.tallies.filter(t => !t.cancelled).filter(t => new Date(t.time).getTime() >= new Date(ts.start).getTime() && new Date(t.time).getTime() < new Date(ts.end).getTime()))
      .map(t => this.calculateSalesVolume(t))
      .map(v => this.settingsHandlerService.getFractionDigits(v));
    this.timeslotPieChartLabels = timeslotLabels;
    this.timeslotPieChartDatasets = [{
      label: "By Timeslots",
      data: timeslotDataSets,
      backgroundColor: Colors.BACKGROUND_COLORS,
      borderColor: Colors.BORDER_COLORS,
      hoverBackgroundColor: Colors.BORDER_COLORS,
      hoverBorderColor: Colors.BORDER_COLORS,
      borderWidth: 1
    }];

    let teamLabels = this.teams.map(t => t.shortName);
    let teamDataSets = this.teams
      .map(team => this.tallies.filter(t => !t.cancelled).filter(t => this.isTallyOfTeam(t, team)))
      .map(t => this.calculateSalesVolume(t))
      .map(v => this.settingsHandlerService.getFractionDigits(v));
    this.teamPieChartLabels = teamLabels;
    this.teamPieChartDatasets = [{
      label: "By Team",
      data: teamDataSets,
      backgroundColor: Colors.BACKGROUND_COLORS,
      borderColor: Colors.BORDER_COLORS,
      hoverBackgroundColor: Colors.BORDER_COLORS,
      hoverBorderColor: Colors.BORDER_COLORS,
      borderWidth: 1
    }];

    let productLabels = this.products.map(p => p.name);
    let productDataSets = this.products
      .map(p => this.tallies.filter(t => !t.cancelled).filter(t => t.productId == p.id))
      .map(t => this.calculateSalesVolume(t))
      .map(v => this.settingsHandlerService.getFractionDigits(v));
    this.productPieChartLabels = productLabels;
    this.productPieChartDatasets = [{
      label: "By Product",
      data: productDataSets,
      backgroundColor: Colors.BACKGROUND_COLORS,
      borderColor: Colors.BORDER_COLORS,
      hoverBackgroundColor: Colors.BORDER_COLORS,
      hoverBorderColor: Colors.BORDER_COLORS,
      borderWidth: 1
    }];
  }

  isTallyOfTeam(tally: Tally, team: Team): boolean {
    let player = this.players.find(p => p.id == tally.playerId);
    if (!player) {
      return false;
    }
    return player.teamId == team.id;
  }

  calculateSalesVolume(tallies: Tally[]): number {
    let volume = 0;
    for (const tally of tallies) {
      let product = this.findProductById(tally.productId);
      if (product) {
        volume += product.price;
      }
    }
    return volume;
  }

  findProductById(id: number): Product | undefined {
    return this.products.find(p => p.id == id);
  }
}
