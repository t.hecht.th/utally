import {Component, Input, ViewChild} from '@angular/core';
import {Tally} from "../../../model/tally";
import {MatPaginator} from "@angular/material/paginator";
import {Timeslot} from "../../../model/timeslot";
import {AbstractEventStatistics} from "../../../common/abstract-event-statistics";
import {Product} from "../../../model/product";
import {Settings} from "../../../model/settings";
import {SettingsHandlerService} from "../../../service/settings-handler.service";

@Component({
  selector: 'app-event-timeslot-statistics',
  templateUrl: './event-timeslot-statistics.component.html',
  styleUrls: ['./event-timeslot-statistics.component.scss']
})
export class EventTimeslotStatisticsComponent extends AbstractEventStatistics<Timeslot> {

  @Input() timeslots: Timeslot[] = [];
  @Input() products: Product[] = [];
  @Input() tallies: Tally[] = [];

  @Input() settings: Settings;
  @Input() settingsHandlerService: SettingsHandlerService;

  displayedColumns = ['name', 'count', 'volume', 'cancelled', 'cancelledvol'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  protected override getPaginator(): MatPaginator {
    return this.paginator;
  }

  protected override getTalliesByItem(item: Timeslot): Tally[] {
    return this.tallies.filter(t => new Date(t.time).getTime() >= new Date(item.start).getTime() && new Date(t.time).getTime() < new Date(item.end).getTime()).filter(t => !t.cancelled);
  }

  protected override getCancelledTallies(item: Timeslot): Tally[] {
    return this.tallies.filter(t => new Date(t.time).getTime() >= new Date(item.start).getTime() && new Date(t.time).getTime() < new Date(item.end).getTime()).filter(t => t.cancelled);
  }

  protected override getLabel(item: Timeslot): string {
    return item.label;
  }

  protected override getSettings(): Settings {
    return this.settings;
  }

  protected override getItems(): Timeslot[] {
    return this.timeslots;
  }

  protected override getSettingsHandlerService(): SettingsHandlerService {
    return this.settingsHandlerService;
  }

  protected override getProducts(): Product[] {
    return this.products;
  }

  protected override getTallies(): Tally[] {
    return this.tallies;
  }
}
