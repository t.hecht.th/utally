import {Component, Input, ViewChild} from '@angular/core';
import {Tally} from "../../../model/tally";
import {Settings} from "../../../model/settings";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {Product} from "../../../model/product";
import {MatPaginator} from "@angular/material/paginator";
import {AbstractEventStatistics} from "../../../common/abstract-event-statistics";

@Component({
  selector: 'app-event-product-statistics',
  templateUrl: './event-product-statistics.component.html',
  styleUrls: ['./event-product-statistics.component.scss']
})
export class EventProductStatisticsComponent extends AbstractEventStatistics<Product> {

  @Input() products: Product[] = [];
  @Input() tallies: Tally[] = [];

  @Input() settings: Settings;
  @Input() settingsHandlerService: SettingsHandlerService;

  displayedColumns = ['name', 'price', 'count', 'volume', 'cancelled', 'cancelledvol'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  protected override getPaginator(): MatPaginator {
    return this.paginator;
  }

  protected override getSettings(): Settings {
    return this.settings;
  }

  protected override getSettingsHandlerService(): SettingsHandlerService {
    return this.settingsHandlerService;
  }

  protected override getProducts(): Product[] {
    return this.products;
  }

  protected override getTallies(): Tally[] {
    return this.tallies;
  }

  protected override getItems(): Product[] {
    return this.getProducts();
  }

  protected override getLabel(item: Product): string {
    return item.name;
  }

  protected override getTalliesByItem(item: Product): Tally[] {
    return this.tallies.filter(t => t.productId == item.id).filter(t => !t.cancelled);
  }

  protected override getCancelledTallies(item: Product): Tally[] {
    return this.tallies.filter(t => t.productId == item.id).filter(t => t.cancelled);
  }
}
