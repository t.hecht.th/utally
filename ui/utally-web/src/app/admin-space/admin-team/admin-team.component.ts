import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {AdminTableSpace} from "../admin-table/admin-table-space";
import {Team} from "../../model/team";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {TeamHttpAdapter} from "../../adapter/team-http-adapter";
import {catchError} from "rxjs";
import {DeleteTeamDialogComponent} from "./delete-team-dialog/delete-team-dialog.component";
import {EditTeamDialogComponent} from "./edit-team-dialog/edit-team-dialog.component";
import {NewTeamDialogComponent} from "./new-team-dialog/new-team-dialog.component";
import {MatPaginator} from "@angular/material/paginator";
import {faCheck, faEdit, faPlus, faTrashCan, faUser, faXmark} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-admin-team',
  templateUrl: './admin-team.component.html',
  styleUrls: ['./admin-team.component.scss']
})
export class AdminTeamComponent extends AdminTableSpace<Team> {

  displayedColumns = ['fullname', 'shortname', 'city', 'description', 'enabled', 'creationtime'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  @Output() showPlayersByTeam = new EventEmitter<Team>();

  constructor(sessionService: SessionService,
              router: Router,
              messageHandlerService: MessageHandlerService,
              dialog: MatDialog,
              settingsHandlerService: SettingsHandlerService,
              private teamHttpAdapter: TeamHttpAdapter) {
    super(sessionService, router, messageHandlerService, dialog, settingsHandlerService);
  }

  override afterLoadSettings() {
    this.loadTeams();
  }

  override getPaginator(): MatPaginator {
    return this.paginator;
  }

  loadTeams() {
    this.teamHttpAdapter.getTeams().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.clickedRows.clear();
      this.setItems(r.sort((a,b) => a.fullName.localeCompare(b.fullName)))
      this.showProgressIndicator = false;
    });
  }

  showNewDialog() {
    const dialogRef = this.dialog.open(NewTeamDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.teamHttpAdapter.postTeam(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Team created")
              this.loadTeams();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showEditDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(EditTeamDialogComponent, {
      width: '500px',
      data: {
        team: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.teamHttpAdapter.putTeam(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Team edited")
              this.loadTeams();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showDeleteDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(DeleteTeamDialogComponent, {
      width: '500px',
      data: {
        team: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showProgressIndicator = true;
        this.teamHttpAdapter.deleteTeam(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(_ => {
            this.messageHandlerService.onInfo("Team deleted")
            this.loadTeams();
          }
        )
      }
    });
  }

  showPlayers() {
    this.showPlayersByTeam.emit(this.clickedRows.values().next().value);
  }

    protected readonly faCheck = faCheck;
    protected readonly faXmark = faXmark;
  protected readonly faPlus = faPlus;
  protected readonly faEdit = faEdit;
  protected readonly faUser = faUser;
  protected readonly faTrashCan = faTrashCan;
}
