import {Component, Inject, OnInit} from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Team} from "../../../model/team";

@Component({
  selector: 'app-edit-team-dialog',
  templateUrl: './edit-team-dialog.component.html',
  styleUrls: ['./edit-team-dialog.component.scss']
})
export class EditTeamDialogComponent implements OnInit {

  nameControl = new UntypedFormControl('', [Validators.required]);
  shortNameControl = new UntypedFormControl('', [Validators.required]);
  cityControl = new UntypedFormControl('', [Validators.required]);
  enabled = true;
  description = "";

  team: Team;

  constructor(public dialogRef: MatDialogRef<EditTeamDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { team: Team },
  ) {
    this.team = data.team;
  }

  ngOnInit() {
    this.nameControl.setValue(this.team.fullName);
    this.shortNameControl.setValue(this.team.shortName);
    this.cityControl.setValue(this.team.city);
    this.enabled = this.team.enabled;
    this.description = this.team.description;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    let valid = true;

    if (this.nameControl.invalid) {
      valid = false;
    }

    if (this.shortNameControl.invalid) {
      valid = false;
    }

    if (this.cityControl.invalid) {
      valid = false;
    }

    if (!valid) {
      return;
    }

    this.dialogRef.close({
      id: this.team.id,
      fullName: this.nameControl.value,
      shortName: this.shortNameControl.value,
      city: this.cityControl.value,
      enabled: this.enabled,
      description: this.description,
    } as Team);
  }
}
