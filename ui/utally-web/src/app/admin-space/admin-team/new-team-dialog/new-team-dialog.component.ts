import { Component } from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {Team} from "../../../model/team";

@Component({
  selector: 'app-new-team-dialog',
  templateUrl: './new-team-dialog.component.html',
  styleUrls: ['./new-team-dialog.component.scss']
})
export class NewTeamDialogComponent {

  nameControl = new UntypedFormControl('', [Validators.required]);
  shortNameControl = new UntypedFormControl('', [Validators.required]);
  cityControl = new UntypedFormControl('', [Validators.required]);
  enabled = true;
  description = "";
  constructor(public dialogRef: MatDialogRef<NewTeamDialogComponent>,) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    let valid = true;

    if (this.nameControl.invalid) {
      valid = false;
    }

    if (this.shortNameControl.invalid) {
      valid = false;
    }

    if (this.cityControl.invalid) {
      valid = false;
    }

    if (!valid) {
      return;
    }

    this.dialogRef.close({
      fullName: this.nameControl.value,
      shortName: this.shortNameControl.value,
      city: this.cityControl.value,
      enabled: this.enabled,
      description: this.description,
    } as Team);
  }

}
