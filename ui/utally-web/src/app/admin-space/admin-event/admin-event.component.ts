import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {AdminTableSpace} from "../admin-table/admin-table-space";
import {Event} from "../../model/event";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {MatPaginator} from "@angular/material/paginator";
import {catchError} from "rxjs";
import {EventHttpAdapter} from "../../adapter/event-http-adapter";
import {NewEventDialogComponent} from "./new-event-dialog/new-event-dialog.component";
import {EditEventDialogComponent} from "./edit-event-dialog/edit-event-dialog.component";
import {DeleteEventDialogComponent} from "./delete-event-dialog/delete-event-dialog.component";
import {Location} from "../../model/location";
import {Team} from "../../model/team";
import {TeamHttpAdapter} from "../../adapter/team-http-adapter";
import {LocationHttpAdapter} from "../../adapter/location-http-adapter";
import {AddLocationToEventDialogComponent} from "./add-location-to-event-dialog/add-location-to-event-dialog.component";
import {AddTeamToEventDialogComponent} from "./add-team-to-event-dialog/add-team-to-event-dialog.component";
import {
  faChartPie, faCheck,
  faClock,
  faEdit,
  faMapPin,
  faPlus,
  faTrashCan,
  faUser,
  faUserGroup, faXmark
} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-admin-event',
  templateUrl: './admin-event.component.html',
  styleUrls: ['./admin-event.component.scss']
})
export class AdminEventComponent extends AdminTableSpace<Event> {

  displayedColumns = ['title', 'description', 'playersfee', 'enabled', 'creationtime'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  locations: Location[] = [];
  teams: Team[] = [];

  @Output() showPlayersByEvent = new EventEmitter<Event>();
  @Output() showTimeslotsByEvent = new EventEmitter<Event>();
  @Output() showStatisticsByEvent = new EventEmitter<Event>();

  constructor(sessionService: SessionService,
              router: Router,
              messageHandlerService: MessageHandlerService,
              dialog: MatDialog,
              settingsHandlerService: SettingsHandlerService,
              private eventHttpAdapter: EventHttpAdapter,
              private teamHttpAdapter: TeamHttpAdapter,
              private locationHttpAdapter: LocationHttpAdapter,
  ) {
    super(sessionService, router, messageHandlerService, dialog, settingsHandlerService);
  }

  override afterLoadSettings() {
    this.loadEvents(false);
  }

  loadEvents(onlyEvents: boolean = true) {
    this.eventHttpAdapter.getEvents().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.clickedRows.clear();
      this.setItems(r.sort((a,b) => a.title.localeCompare(b.title)));
      if (onlyEvents) {
        this.showProgressIndicator = false;
      } else {
        this.loadLocations();
      }
    });
  }

  loadLocations() {
    this.locationHttpAdapter.getLocations().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.locations = r.sort((a,b) => a.name.localeCompare(b.name));
      this.loadTeams();
    });
  }

  loadTeams() {
    this.teamHttpAdapter.getTeams().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.teams = r.sort((a,b) => a.fullName.localeCompare(b.fullName));
      this.showProgressIndicator = false;
    });
  }

  getPaginator(): MatPaginator {
    return this.paginator;
  }

  showNewDialog() {
    const dialogRef = this.dialog.open(NewEventDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.eventHttpAdapter.postEvent(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Event created")
              this.loadEvents();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showEditDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(EditEventDialogComponent, {
      width: '500px',
      data: {
        event: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.eventHttpAdapter.putEvent(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Event edited")
              this.loadEvents();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showDeleteDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(DeleteEventDialogComponent, {
      width: '500px',
      data: {
        event: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showProgressIndicator = true;
        this.eventHttpAdapter.deleteEvent(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(_ => {
            this.messageHandlerService.onInfo("Event deleted")
            this.loadEvents();
          }
        )
      }
    });
  }

  showPlayers() {
    this.showPlayersByEvent.emit(this.clickedRows.values().next().value);
  }

  showAddLocationDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(AddLocationToEventDialogComponent, {
      width: '500px',
      data: {
        event: this.clickedRows.values().next().value,
        locations: this.locations
      }
    });

    dialogRef.afterClosed().subscribe(_ => this.loadEvents());
  }

  showAddTeamDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(AddTeamToEventDialogComponent, {
      width: '500px',
      data: {
        event: this.clickedRows.values().next().value,
        teams: this.teams
      }
    });

    dialogRef.afterClosed().subscribe(_ => this.loadEvents());
  }

  showTimeslots() {
    this.showTimeslotsByEvent.emit(this.clickedRows.values().next().value);
  }

  showStatistics() {
    this.showStatisticsByEvent.emit(this.clickedRows.values().next().value);
  }

    protected readonly faPlus = faPlus;
  protected readonly faEdit = faEdit;
  protected readonly faUser = faUser;
  protected readonly faMapPin = faMapPin;
  protected readonly faUserGroup = faUserGroup;
  protected readonly faChartPie = faChartPie;
  protected readonly faTrashCan = faTrashCan;
  protected readonly faClock = faClock;
  protected readonly faCheck = faCheck;
  protected readonly faXmark = faXmark;
}
