import {Component, Inject, OnInit} from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Event} from "../../../model/event";

@Component({
  selector: 'app-edit-event-dialog',
  templateUrl: './edit-event-dialog.component.html',
  styleUrls: ['./edit-event-dialog.component.scss']
})
export class EditEventDialogComponent implements OnInit {

  titleControl = new UntypedFormControl('', [Validators.required]);
  pfControl = new UntypedFormControl('', [Validators.required, Validators.min(0)]);

  enabled = true;

  description = "";

  constructor(
    public dialogRef: MatDialogRef<EditEventDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {event: Event},
  ) { }

  ngOnInit(): void {
    this.titleControl.setValue(this.data.event.title);
    this.pfControl.setValue(this.data.event.playersFee);
    this.enabled = this.data.event.enabled;
    this.description = this.data.event.description;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    let valid = true;
    if (this.titleControl.invalid) {
      valid = false;
    }

    if (this.pfControl.invalid) {
      valid = false;
    }

    if (!valid) {
      return;
    }

    this.dialogRef.close({
      id: this.data.event.id,
      title: this.titleControl.value,
      playersFee: this.pfControl.value,
      enabled: this.enabled,
      description: this.description,
    } as Event);
  }
}
