import {Component, Inject} from '@angular/core';
import {Location} from "../../../model/location";
import {AbstractHttpHandler} from "../../../common/abstract-http-handler";
import {SessionService} from "../../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../../service/message-handler.service";
import {EventHttpAdapter} from "../../../adapter/event-http-adapter";
import {catchError} from "rxjs";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Event} from "../../../model/event";

@Component({
  selector: 'app-add-location-to-event-dialog',
  templateUrl: './add-location-to-event-dialog.component.html',
  styleUrls: ['./add-location-to-event-dialog.component.scss']
})
export class AddLocationToEventDialogComponent extends AbstractHttpHandler {

  showProgressIndicator = false;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    private eventHttpAdapter: EventHttpAdapter,
    @Inject(MAT_DIALOG_DATA) public data: {event: Event, locations: Location[]},
  ) {
    super(sessionService, router, messageHandlerService);
  }

  addLocationToEvent(location: Location, value: boolean) {
    this.showProgressIndicator = true;
    if (value) {
      this.eventHttpAdapter.addLocationToEvent(location.id, this.data.event.id).pipe(
        catchError(this.handleError(null))
      ).subscribe(_ => this.showProgressIndicator = false);
    } else {
      this.eventHttpAdapter.removeLocationFromEvent(location.id, this.data.event.id).pipe(
        catchError(this.handleError(null))
      ).subscribe(_ => this.showProgressIndicator = false);
    }
  }
}
