import { Component } from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {Event} from "../../../model/event";

@Component({
  selector: 'app-new-event-dialog',
  templateUrl: './new-event-dialog.component.html',
  styleUrls: ['./new-event-dialog.component.scss']
})
export class NewEventDialogComponent {

  titleControl = new UntypedFormControl('', [Validators.required]);
  pfControl = new UntypedFormControl('', [Validators.required, Validators.min(0)]);

  enabled = true;

  description = "";

  constructor(public dialogRef: MatDialogRef<NewEventDialogComponent>,) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    let valid = true;
    if (this.titleControl.invalid) {
      valid = false;
    }

    if (this.pfControl.invalid) {
      valid = false;
    }

    if (!valid) {
      return;
    }

    this.dialogRef.close({
      title: this.titleControl.value,
      playersFee: this.pfControl.value,
      enabled: this.enabled,
      description: this.description,
    } as Event);
  }
}
