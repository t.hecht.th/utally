import {Component, Inject} from '@angular/core';
import {SessionService} from "../../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../../service/message-handler.service";
import {EventHttpAdapter} from "../../../adapter/event-http-adapter";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Event} from "../../../model/event";
import {catchError} from "rxjs";
import {Team} from "../../../model/team";
import {AbstractHttpHandler} from "../../../common/abstract-http-handler";

@Component({
  selector: 'app-add-team-to-event-dialog',
  templateUrl: './add-team-to-event-dialog.component.html',
  styleUrls: ['./add-team-to-event-dialog.component.scss']
})
export class AddTeamToEventDialogComponent extends AbstractHttpHandler {

  showProgressIndicator = false;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    private eventHttpAdapter: EventHttpAdapter,
    @Inject(MAT_DIALOG_DATA) public data: {event: Event, teams: Team[]},
  ) {
    super(sessionService, router, messageHandlerService);
  }

  addTeamToEvent(team: Team, value: boolean) {
    this.showProgressIndicator = true;
    if (value) {
      this.eventHttpAdapter.addTeamToEvent(team.id, this.data.event.id).pipe(
        catchError(this.handleError(null))
      ).subscribe(_ => this.showProgressIndicator = false);
    } else {
      this.eventHttpAdapter.removeTeamFromEvent(team.id, this.data.event.id).pipe(
        catchError(this.handleError(null))
      ).subscribe(_ => this.showProgressIndicator = false);
    }
  }

}
