import {Component, Inject} from '@angular/core';
import {Location} from "../../../model/location";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-delete-location-dialog',
  templateUrl: './delete-location-dialog.component.html',
  styleUrls: ['./delete-location-dialog.component.scss']
})
export class DeleteLocationDialogComponent {

  location: Location;
  constructor(
    public dialogRef: MatDialogRef<DeleteLocationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {location: Location}
  ) {
    this.location = data.location;
  }
}
