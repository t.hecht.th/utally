import {Component, Inject, OnInit} from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {Location} from "../../../model/location";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-edit-location-dialog',
  templateUrl: './edit-location-dialog.component.html',
  styleUrls: ['./edit-location-dialog.component.scss']
})
export class EditLocationDialogComponent implements OnInit {

  nameControl = new UntypedFormControl('', [Validators.required]);

  enabled = true;

  description = "";

  location: Location;

  constructor(
    public dialogRef: MatDialogRef<EditLocationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {location: Location}
  ) {
    this.location = data.location;
  }

  ngOnInit(): void {
    this.nameControl.setValue(this.location.name);
    this.enabled = this.location.enabled;
    this.description = this.location.description;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    if (this.nameControl.invalid) {
      return;
    }

    this.dialogRef.close({
      id: this.location.id,
      name: this.nameControl.value,
      enabled: this.enabled,
      description: this.description,
    } as Location);
  }
}
