import {Component, ViewChild} from '@angular/core';
import {AdminTableSpace} from "../admin-table/admin-table-space";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {ProductHttpAdapter} from "../../adapter/product-http-adapter";
import {LocationHttpAdapter} from "../../adapter/location-http-adapter";
import {catchError} from "rxjs";
import {Product} from "../../model/product";
import {Location} from "../../model/location";
import {NewLocationDialogComponent} from "./new-location-dialog/new-location-dialog.component";
import {EditLocationDialogComponent} from "./edit-location-dialog/edit-location-dialog.component";
import {
  AddProductToLocationDialogComponent
} from "./add-product-to-location-dialog/add-product-to-location-dialog.component";
import {DeleteLocationDialogComponent} from "./delete-location-dialog/delete-location-dialog.component";
import {MatPaginator} from "@angular/material/paginator";
import {faCheck, faEdit, faMartiniGlass, faPlus, faTrashCan, faXmark} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-admin-location',
  templateUrl: './admin-location.component.html',
  styleUrls: ['./admin-location.component.scss']
})
export class AdminLocationComponent extends AdminTableSpace<Location> {

  products: Product[] = [];

  displayedColumns = ['name', 'description', 'enabled', 'creationtime'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    dialog: MatDialog,
    settingsHandlerService: SettingsHandlerService,
    private productHttpAdapter: ProductHttpAdapter,
    private locationHttpAdapter: LocationHttpAdapter,
  ) {
    super(sessionService, router, messageHandlerService, dialog, settingsHandlerService);
  }

  override afterLoadSettings(): void {
    this.loadProducts(true);
  }

  override getPaginator(): MatPaginator {
    return this.paginator;
  }

  loadProducts(loadLocations: boolean) {
    this.showProgressIndicator = true;
    this.productHttpAdapter.getProducts().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.products = r.sort((a,b) => a.name.localeCompare(b.name));
      if (loadLocations) {
        this.loadLocations();
      } else {
        this.showProgressIndicator = false;
      }
    });
  }

  loadLocations() {
    this.locationHttpAdapter.getLocations().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.clickedRows.clear();
      this.setItems(r.sort((a, b) => a.name.localeCompare(b.name)));
      this.showProgressIndicator = false;
    });
  }

  showNewDialog() {
    const dialogRef = this.dialog.open(NewLocationDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.locationHttpAdapter.postLocation(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Location created")
              this.loadLocations();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showEditDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(EditLocationDialogComponent, {
      width: '500px',
      data: {
        location: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.locationHttpAdapter.putLocation(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Location edited")
              this.loadLocations();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showAddProductDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(AddProductToLocationDialogComponent, {
      width: '500px',
      data: {
        location: this.clickedRows.values().next().value,
        products: this.products
      }
    });

    dialogRef.afterClosed().subscribe(_ => this.loadProducts(false));
  }

  showDeleteDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(DeleteLocationDialogComponent, {
      width: '500px',
      data: {
        location: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showProgressIndicator = true;
        this.locationHttpAdapter.deleteLocation(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(_ => {
            this.messageHandlerService.onInfo("Location deleted")
            this.loadLocations();
          }
        )
      }
    });
  }

    protected readonly faPlus = faPlus;
  protected readonly faEdit = faEdit;
  protected readonly faTrashCan = faTrashCan;
  protected readonly faMartiniGlass = faMartiniGlass;
  protected readonly faCheck = faCheck;
  protected readonly faXmark = faXmark;
}
