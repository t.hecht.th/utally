import { Component } from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {Location} from "../../../model/location";

@Component({
  selector: 'app-new-location-dialog',
  templateUrl: './new-location-dialog.component.html',
  styleUrls: ['./new-location-dialog.component.scss']
})
export class NewLocationDialogComponent {

  nameControl = new UntypedFormControl('', [Validators.required]);

  enabled = true;

  description = "";
  constructor(public dialogRef: MatDialogRef<NewLocationDialogComponent>,) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    if (this.nameControl.invalid) {
      return;
    }

    this.dialogRef.close({
      name: this.nameControl.value,
      enabled: this.enabled,
      description: this.description,
    } as Location);
  }
}
