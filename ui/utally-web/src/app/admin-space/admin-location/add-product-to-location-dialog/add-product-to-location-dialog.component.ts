import {Component, Inject, OnInit} from '@angular/core';
import {Product} from "../../../model/product";
import {Location} from "../../../model/location";
import {SessionService} from "../../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../../service/message-handler.service";
import {LocationHttpAdapter} from "../../../adapter/location-http-adapter";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {catchError} from "rxjs";
import {AbstractHttpHandler} from "../../../common/abstract-http-handler";

@Component({
  selector: 'app-add-product-to-location-dialog',
  templateUrl: './add-product-to-location-dialog.component.html',
  styleUrls: ['./add-product-to-location-dialog.component.scss']
})
export class AddProductToLocationDialogComponent extends AbstractHttpHandler implements OnInit {
  products: Product[] = [];
  location: Location;
  showProgressIndicator = false;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    private locationHttpAdapter: LocationHttpAdapter,
    @Inject(MAT_DIALOG_DATA) public data: {location: Location, products: Product[]}
  ) {
    super(sessionService, router, messageHandlerService);
    this.products = data.products;
    this.location = data.location;
  }

  ngOnInit(): void {
  }

  addProductToLocation(product: Product, value: boolean) {
    this.showProgressIndicator = true;
    if (value) {
      this.locationHttpAdapter.addProductToLocation(product.id, this.location.id).pipe(
        catchError(this.handleError(null))
      ).subscribe(_ => this.showProgressIndicator = false);
    } else {
      this.locationHttpAdapter.removeProductFromLocation(product.id, this.location.id).pipe(
        catchError(this.handleError(null))
      ).subscribe(_ => this.showProgressIndicator = false);
    }

  }
}
