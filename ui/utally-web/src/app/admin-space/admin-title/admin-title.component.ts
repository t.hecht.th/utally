import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-admin-title',
  templateUrl: './admin-title.component.html',
  styleUrls: ['./admin-title.component.scss']
})
export class AdminTitleComponent implements OnInit {

  @Input() title = "";
  @Input() showProgressIndicator = false;

  constructor() { }

  ngOnInit(): void {
  }

}
