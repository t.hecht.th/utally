import {Component} from '@angular/core';
import {AbstractSpace} from "../common/abstract-space";
import {SessionService} from "../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {faSquareFacebook, faTwitterSquare, faGitlabSquare, faLinkedin} from "@fortawesome/free-brands-svg-icons";
import {
  faArrowRightFromBracket, faCalendarDays, faCircleInfo,
  faEnvelope, faGears,
  faMapPin,
  faMartiniGlass,
  faUserGroup
} from "@fortawesome/free-solid-svg-icons";
import {Team} from "../model/team";
import {Event} from "../model/event";
import {Timeslot} from "../model/timeslot";

@Component({
  selector: 'app-admin-space',
  templateUrl: './admin-space.component.html',
  styleUrls: ['./admin-space.component.scss']
})
export class AdminSpaceComponent extends AbstractSpace {
  faSquareFacebook = faSquareFacebook;
  faTwitterSquare = faTwitterSquare;
  faGitlabSquare = faGitlabSquare;
  faLinkedin = faLinkedin;
  faEnvelope = faEnvelope;
  faArrowRightFromBracket = faArrowRightFromBracket;

  state = 1;

  team: Team;
  event: Event;
  timeslot: Timeslot;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    dialog: MatDialog
  ) {
    super(sessionService, router, messageHandlerService, dialog);
  }

  changeState(state: number) {
    this.state = state;
  }

  showPlayersByTeam(team: Team) {
    this.team = team;
    this.state = 6;
  }

  showPlayersByEvent(event: Event) {
    this.event = event;
    this.state = 7;
  }

  showTimeslotsByEvent(event: Event) {
    this.event = event;
    this.state = 8;
  }

  showStatisticsByEvent(event: Event) {
    this.event = event;
    this.state = 9;
  }

  showStatisticsByTimeslot(timeslot: Timeslot) {
    this.timeslot = timeslot;
    this.state = 10;
  }

  openInstallationManual() {
    window.open("/utally/docs/installation.html", "_blank");
  }

  openAdministrationManual() {
    window.open("/utally/docs/administration.html", "_blank");
  }

  protected readonly faMartiniGlass = faMartiniGlass;
  protected readonly faMapPin = faMapPin;
  protected readonly faUserGroup = faUserGroup;
  protected readonly faCalendarDays = faCalendarDays;
  protected readonly faGears = faGears;
  protected readonly faCircleInfo = faCircleInfo;
}
