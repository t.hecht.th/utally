import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractSpace} from "../../common/abstract-space";
import {Event} from "../../model/event";
import {Timeslot} from "../../model/timeslot";
import {Player} from "../../model/player";
import {Team} from "../../model/team";
import {Product} from "../../model/product";
import {Location} from "../../model/location";
import {Tally} from "../../model/tally";
import {Settings} from "../../model/settings";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {LocationHttpAdapter} from "../../adapter/location-http-adapter";
import {TeamHttpAdapter} from "../../adapter/team-http-adapter";
import {PlayerHttpAdapter} from "../../adapter/player-http-adapter";
import {ProductHttpAdapter} from "../../adapter/product-http-adapter";
import {TallyHttpAdapter} from "../../adapter/tally-http-adapter";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {catchError} from "rxjs";

@Component({
  selector: 'app-admin-timeslot-statistics',
  templateUrl: './admin-timeslot-statistics.component.html',
  styleUrls: ['./admin-timeslot-statistics.component.scss']
})
export class AdminTimeslotStatisticsComponent extends AbstractSpace implements OnInit {
  @Input() timeslot: Timeslot;
  @Input() event: Event;

  @Output() onBackClick = new EventEmitter<void>();

  protected showProgressIndicator = false;

  players: Player[] = [];
  teams: Team[] = [];
  products: Product[] = [];
  locations: Location[] = [];
  tallies: Tally[] = [];

  settings: Settings;

  constructor(sessionService: SessionService,
              router: Router,
              messageHandlerService: MessageHandlerService,
              dialog: MatDialog,
              private locationHttpAdapter: LocationHttpAdapter,
              private teamHttpAdapter: TeamHttpAdapter,
              private playerHttpAdapter: PlayerHttpAdapter,
              private productHttpAdapter: ProductHttpAdapter,
              private tallyHttpAdapter: TallyHttpAdapter,
              public settingsHandlerService: SettingsHandlerService,
  ){
    super(sessionService, router, messageHandlerService, dialog);
  }

  ngOnInit(): void {
    this.loadSettings();
  }

  loadSettings() {
    this.showProgressIndicator = true;
    this.settingsHandlerService.getSettings().pipe(
      catchError(this.handleError(null))
    ).subscribe(r => {
      if (r) {
        this.settings = r;
        this.loadTeams();
      }
    })
  }

  loadTeams() {
    this.teamHttpAdapter.getTeamsByEvent(this.event.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.teams = r.sort((a,b) => a.shortName.localeCompare(b.shortName));
      this.loadProducts();
    });
  }

  loadProducts() {
    this.productHttpAdapter.getProductsByEvent(this.event.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.products = r.sort((a,b) => a.name.localeCompare(b.name));
      this.loadLocations();
    });
  }

  loadLocations() {
    this.locationHttpAdapter.getLocationsByEvent(this.event.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.locations = r.sort((a,b) => a.name.localeCompare(b.name));
      this.loadTallies();
    });
  }

  loadTallies() {
    this.tallyHttpAdapter.getTalliesByTimeslot(this.timeslot.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.tallies = r;
      this.loadPlayers();
    });
  }

  loadPlayers() {
    this.playerHttpAdapter.getPlayersByEvent(this.event.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.players = r;
      this.showProgressIndicator = false;
    });
  }

  getPaidPlayersFee(): number {
    return this.players.filter(p => p.paid).map(p => p.paidPlayersFee).reduce((a,b) => a+b, 0);
  }

  getNumberOfPaidPlayers(): number {
    return this.players.filter(p => p.paid).length;
  }

  getCancelledTallies(): Tally[] {
    return this.tallies.filter(t => t.cancelled);
  }

  getValidTallies(): Tally[] {
    return this.tallies.filter(t => !t.cancelled);
  }

  getSalesVolume(): number {
    return this.calculateSalesVolume(this.getValidTallies());
  }

  getCancelledSalesVolume(): number {
    return this.calculateSalesVolume(this.getCancelledTallies());
  }

  calculateSalesVolume(tallies: Tally[]): number {
    let volume = 0;
    for (const tally of tallies) {
      let product = this.findProductById(tally.productId);
      if (product) {
        volume += product.price;
      }
    }
    return volume;
  }

  findProductById(id: number): Product | undefined {
    return this.products.find(p => p.id == id);
  }
}
