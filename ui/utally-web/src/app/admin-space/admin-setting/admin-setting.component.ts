import { Component, OnInit } from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {
  PatchCurrencySettings,
  PatchDataPointsSettings,
  PatchDateTimeFormatSettings,
  PatchGrowlDelaySettings,
  PatchLiveStatisticsIntervalSettings, PatchSelfRegistrationSettings,
  Settings
} from "../../model/settings";
import {catchError} from "rxjs";
import {AbstractSpace} from "../../common/abstract-space";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {SettingsHttpAdapter} from "../../adapter/settings-http-adapter";
import {Currency} from "../../model/currency";
import {TimeZone} from "../../model/timeZone";

@Component({
  selector: 'app-admin-setting',
  templateUrl: './admin-setting.component.html',
  styleUrls: ['./admin-setting.component.scss']
})
export class AdminSettingComponent extends AbstractSpace implements OnInit {

  snackBarDelayControl = new UntypedFormControl('', [Validators.required, Validators.min(0)]);
  liveStatisticsIntervalControl = new UntypedFormControl('', [Validators.required, Validators.min(0)]);
  dataPointsControl = new UntypedFormControl('', [Validators.required, Validators.min(0)]);
  dateTimeFormatControl = new UntypedFormControl('', [Validators.required]);
  currencyControl = new UntypedFormControl('', [Validators.required]);
  selfRegistration = true;

  showProgressIndicator = false;

  currencies: Currency[] = [];
  timeZones: TimeZone[] = [];

  settings: Settings | null;
  now = new Date();

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    dialog: MatDialog,
    public settingsHandlerService: SettingsHandlerService,
    private settingsHttpAdapter: SettingsHttpAdapter,
    private snackBar: MatSnackBar,
  ) {
    super(sessionService, router, messageHandlerService, dialog);
  }

  ngOnInit(): void {
    this.loadCurrencies();
  }

  loadCurrencies() {
    this.showProgressIndicator = true;
    this.settingsHttpAdapter.getCurrencies().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.currencies = r.sort((a, b) => a.displayName.localeCompare(b.displayName));
      this.loadTimeZones();
    });
  }

  loadTimeZones() {
    this.settingsHttpAdapter.getTimeZones().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.timeZones = r.sort((a, b) => a.zoneId.localeCompare(b.zoneId));
      this.loadSettings();
    });
  }

  loadSettings() {
    this.settingsHandlerService.getSettings().pipe(
      catchError(this.handleError(null))
    ).subscribe(r => {
      if (r) {
        this.settings = r;
      }
      this.showProgressIndicator = false;
      this.afterLoadSettings();
    });
  }

  afterLoadSettings() {
    this.snackBarDelayControl.setValue(this.settings?.growlDelay || 5000);
    this.liveStatisticsIntervalControl.setValue(this.settings?.liveStatisticsInterval || 10000);
    this.dataPointsControl.setValue(this.settings?.dataPoints || 30);
    this.dateTimeFormatControl.setValue(this.settings?.dateTimeFormat || 'yyyy-MM-dd HH:mm');
    this.currencyControl.setValue(this.settings?.currencyCode || 'EUR');
    this.selfRegistration = this.settings == null ? true : this.settings.selfRegistration;
  }

  patchSettings(patch: any) {
    this.showProgressIndicator = true;
    this.settingsHandlerService.patchSettings(patch).pipe(
      catchError(this.handleError(null))
    ).subscribe(r => {
      if (r) {
        this.settings = r;
        this.messageHandlerService.onInfo("Settings updated");
      }
      this.showProgressIndicator = false;
      this.afterLoadSettings();
    });
  }

  onSnackbarDelayTestClick() {
    if (this.snackBarDelayControl.invalid) {
      return;
    }
    this.snackBar.open("Snack Bar Delay Test", 'OK', {
      duration: this.snackBarDelayControl.value
    });
  }

  onSnackbarDelayApplyClick() {
    if (this.snackBarDelayControl.invalid) {
      return;
    }

    let updatedSettings: PatchGrowlDelaySettings = {
      growlDelay: this.snackBarDelayControl.value
    };

    this.patchSettings(updatedSettings);
  }

  onDateTimeFormatApplyClick() {
    if (this.dateTimeFormatControl.invalid) {
      return;
    }

    let updatedSettings: PatchDateTimeFormatSettings = {
      dateTimeFormat: this.dateTimeFormatControl.value
    };

    this.patchSettings(updatedSettings);
  }

  onLiveStatisticsIntervalApplyClick() {
    if (this.liveStatisticsIntervalControl.invalid) {
      return;
    }

    let updatedSettings: PatchLiveStatisticsIntervalSettings = {
      liveStatisticsInterval: this.liveStatisticsIntervalControl.value
    };

    this.patchSettings(updatedSettings);
  }

  onDataPointsApplyClick() {
    if (this.dataPointsControl.invalid) {
      return;
    }

    let updatedSettings: PatchDataPointsSettings = {
      dataPoints: this.dataPointsControl.value
    };

    this.patchSettings(updatedSettings);
  }

  onSelfRegistrationApplyClick() {
    let updatedSettings: PatchSelfRegistrationSettings = {
      selfRegistration: this.selfRegistration
    };

    this.patchSettings(updatedSettings);
  }

  onCurrencyApplyClick() {
    if (this.currencyControl.invalid) {
      return;
    }

    let updatedSettings: PatchCurrencySettings = {
      currencyCode: this.currencyControl.value
    };

    this.patchSettings(updatedSettings);
  }
}
