import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Timeslot} from "../../../model/timeslot";
import {UntypedFormControl, Validators} from "@angular/forms";
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-edit-timeslot-dialog',
  templateUrl: './edit-timeslot-dialog.component.html',
  styleUrls: ['./edit-timeslot-dialog.component.scss']
})
export class EditTimeslotDialogComponent implements OnInit {

  labelControl = new UntypedFormControl('', [Validators.required]);
  startControl = new UntypedFormControl('', [Validators.required]);
  endControl = new UntypedFormControl('', [Validators.required]);

  enabled = true;
  description = "";

  constructor(public dialogRef: MatDialogRef<EditTimeslotDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { timeslot: Timeslot },) { }

  ngOnInit(): void {
    this.labelControl.setValue(this.data.timeslot.label);
    this.startControl.setValue(formatDate(this.data.timeslot.start,'yyyy-MM-ddTHH:mm','en'));
    this.endControl.setValue(formatDate(this.data.timeslot.end,'yyyy-MM-ddTHH:mm','en'));
    this.enabled = this.data.timeslot.enabled;
    this.description = this.data.timeslot.description;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    let valid = true;

    if (this.labelControl.invalid) {
      valid = false;
    }

    if (this.startControl.invalid) {
      valid = false;
    }

    if (this.endControl.invalid) {
      valid = false;
    }

    if (!valid) {
      return;
    }

    this.dialogRef.close({
      id: this.data.timeslot.id,
      label: this.labelControl.value,
      enabled: this.enabled,
      description: this.description,
      start: new Date(this.startControl.value),
      end: new Date(this.endControl.value),
      eventId: this.data.timeslot.eventId
    } as Timeslot);
  }
}
