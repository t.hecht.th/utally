import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Timeslot} from "../../../model/timeslot";
import {Event} from "../../../model/event";
import {UntypedFormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-new-timeslot-dialog',
  templateUrl: './new-timeslot-dialog.component.html',
  styleUrls: ['./new-timeslot-dialog.component.scss']
})
export class NewTimeslotDialogComponent {

  labelControl = new UntypedFormControl('', [Validators.required]);
  startControl = new UntypedFormControl('', [Validators.required]);
  endControl = new UntypedFormControl('', [Validators.required]);

  enabled = true;
  description = "";

  constructor(
    public dialogRef: MatDialogRef<NewTimeslotDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { event: Event },
  ) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    let valid = true;

    if (this.labelControl.invalid) {
      valid = false;
    }

    if (this.startControl.invalid) {
      valid = false;
    }

    if (this.endControl.invalid) {
      valid = false;
    }

    if (!valid) {
      return;
    }

    this.dialogRef.close({
      label: this.labelControl.value,
      enabled: this.enabled,
      description: this.description,
      start: new Date(this.startControl.value),
      end: new Date(this.endControl.value),
      eventId: this.data.event.id
    } as Timeslot);
  }
}
