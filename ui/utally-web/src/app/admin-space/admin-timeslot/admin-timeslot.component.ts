import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {AdminTableSpace} from "../admin-table/admin-table-space";
import {Timeslot} from "../../model/timeslot";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {MatPaginator} from "@angular/material/paginator";
import {TimeslotHttpAdapter} from "../../adapter/timeslot-http-adapter";
import {catchError} from "rxjs";
import {Event} from "../../model/event";
import {DeleteTimeslotDialogComponent} from "./delete-timeslot-dialog/delete-timeslot-dialog.component";
import {EditTimeslotDialogComponent} from "./edit-timeslot-dialog/edit-timeslot-dialog.component";
import {NewTimeslotDialogComponent} from "./new-timeslot-dialog/new-timeslot-dialog.component";
import {faArrowLeft, faChartPie, faCheck, faEdit, faPlus, faTrashCan, faXmark} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-admin-timeslot',
  templateUrl: './admin-timeslot.component.html',
  styleUrls: ['./admin-timeslot.component.scss']
})
export class AdminTimeslotComponent extends AdminTableSpace<Timeslot> {

  displayedColumns = ['label', 'description', 'start', 'end', 'enabled', 'creationtime'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  @Input() event: Event;

  @Output() onBackClick = new EventEmitter<void>();
  @Output() showStatisticsByTimeslot = new EventEmitter<Timeslot>();

  constructor(sessionService: SessionService,
              router: Router,
              messageHandlerService: MessageHandlerService,
              dialog: MatDialog,
              settingsHandlerService: SettingsHandlerService,
              private timeslotHttpAdapter: TimeslotHttpAdapter,
  ) {
    super(sessionService, router, messageHandlerService, dialog, settingsHandlerService);
  }

  afterLoadSettings(): void {
    this.loadTimeslots();
  }

  loadTimeslots() {
    this.timeslotHttpAdapter.getTimeslotsByEvent(this.event.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.clickedRows.clear();
      this.setItems(r.sort((a, b) => new Date(a.start).getTime() - new Date(b.start).getTime()));
      this.showProgressIndicator = false;
    });
  }

  getPaginator(): MatPaginator {
    return this.paginator;
  }

  showNewDialog() {
    const dialogRef = this.dialog.open(NewTimeslotDialogComponent, {
      width: '500px',
      data: {
        event: this.event
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showProgressIndicator = true;
        this.timeslotHttpAdapter.postTimeslot(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Timeslot created")
              this.loadTimeslots();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showEditDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(EditTimeslotDialogComponent, {
      width: '500px',
      data: {
        timeslot: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showProgressIndicator = true;
        this.timeslotHttpAdapter.putTimeslot(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Timeslot edited")
              this.loadTimeslots();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showDeleteDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(DeleteTimeslotDialogComponent, {
      width: '500px',
      data: {
        timeslot: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showProgressIndicator = true;
        this.timeslotHttpAdapter.deleteTimeslot(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(_ => {
            this.messageHandlerService.onInfo("Timeslot deleted")
            this.loadTimeslots();
          }
        )
      }
    });
  }

  showStatistics() {
    this.showStatisticsByTimeslot.emit(this.clickedRows.values().next().value);
  }

    protected readonly faPlus = faPlus;
  protected readonly faEdit = faEdit;
  protected readonly faTrashCan = faTrashCan;
  protected readonly faChartPie = faChartPie;
  protected readonly faArrowLeft = faArrowLeft;
  protected readonly faCheck = faCheck;
  protected readonly faXmark = faXmark;
}
