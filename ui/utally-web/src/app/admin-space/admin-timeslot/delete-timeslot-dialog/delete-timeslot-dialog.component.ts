import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Timeslot} from "../../../model/timeslot";

@Component({
  selector: 'app-delete-timeslot-dialog',
  templateUrl: './delete-timeslot-dialog.component.html',
  styleUrls: ['./delete-timeslot-dialog.component.scss']
})
export class DeleteTimeslotDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteTimeslotDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { timeslot: Timeslot },) { }

}
