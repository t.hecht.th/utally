import {Component, Input, OnInit} from '@angular/core';
import {faBurger, faGlobe, faMartiniGlassCitrus} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.scss']
})
export class ProductCategoryComponent implements OnInit {

  @Input() category: string;

  constructor() { }

  ngOnInit(): void {
  }

  protected readonly faBurger = faBurger;
  protected readonly faMartiniGlassCitrus = faMartiniGlassCitrus;
  protected readonly faGlobe = faGlobe;
}
