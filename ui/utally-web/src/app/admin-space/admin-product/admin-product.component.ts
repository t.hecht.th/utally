import {Component, ViewChild} from '@angular/core';
import {Location} from "../../model/location";
import {Product} from "../../model/product";
import {SessionService} from "../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {ProductHttpAdapter} from "../../adapter/product-http-adapter";
import {LocationHttpAdapter} from "../../adapter/location-http-adapter";
import {SettingsHandlerService} from "../../service/settings-handler.service";
import {catchError} from "rxjs";
import {NewProductDialogComponent} from "./new-product-dialog/new-product-dialog.component";
import {EditProductDialogComponent} from "./edit-product-dialog/edit-product-dialog.component";
import {DeleteProductDialogComponent} from "./delete-product-dialog/delete-product-dialog.component";
import {
  AddLocationToProductDialogComponent
} from "./add-location-to-product-dialog/add-location-to-product-dialog.component";
import {UploadImageDialogComponent} from "./upload-image-dialog/upload-image-dialog.component";
import {AdminTableSpace} from "../admin-table/admin-table-space";
import {MatPaginator} from "@angular/material/paginator";
import {faCheck, faEdit, faImage, faMapPin, faPlus, faTrashCan, faXmark} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.scss']
})
export class AdminProductComponent extends AdminTableSpace<Product> {

  locations: Location[] = [];

  displayedColumns = ['name', 'image', 'description', 'price', 'category', 'enabled', 'statistics', 'creationtime'];

  @ViewChild(MatPaginator)
  protected paginator: MatPaginator;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    dialog: MatDialog,
    settingsHandlerService: SettingsHandlerService,
    private productHttpAdapter: ProductHttpAdapter,
    private locationHttpAdapter: LocationHttpAdapter,
  ) {
    super(sessionService, router, messageHandlerService, dialog, settingsHandlerService);
  }

  override afterLoadSettings() {
    this.loadLocations(true);
  }

  override getPaginator(): MatPaginator {
    return this.paginator;
  }

  loadLocations(loadProducts: boolean) {
    this.showProgressIndicator = true;
    this.locationHttpAdapter.getLocations().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.locations = r.sort((a,b) => a.name.localeCompare(b.name));
      if (loadProducts) {
        this.loadProducts();
      } else {
        this.showProgressIndicator = false;
      }
    });
  }

  loadProducts() {
    this.productHttpAdapter.getProducts().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.clickedRows.clear();
      this.setItems(r.sort((a, b) => a.name.localeCompare(b.name)));
      this.showProgressIndicator = false;
    });
  }

  showNewDialog() {
    const dialogRef = this.dialog.open(NewProductDialogComponent, {
      width: '650px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.productHttpAdapter.postProduct(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Product created")
              this.loadProducts();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showEditDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(EditProductDialogComponent, {
      width: '500px',
      data: {
        product: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.showProgressIndicator = true;
        this.productHttpAdapter.putProduct(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(r => {
            if (r != null) {
              this.messageHandlerService.onInfo("Product edited")
              this.loadProducts();
            } else {
              this.showProgressIndicator = false;
            }
          }
        )
      }
    });
  }

  showDeleteDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(DeleteProductDialogComponent, {
      width: '500px',
      data: {
        product: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showProgressIndicator = true;
        this.productHttpAdapter.deleteProduct(result).pipe(
          catchError(this.handleError(null))
        ).subscribe(_ => {
            this.messageHandlerService.onInfo("Product deleted")
            this.loadProducts();
          }
        )
      }
    });
  }

  showAddLocationDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(AddLocationToProductDialogComponent, {
      width: '500px',
      data: {
        product: this.clickedRows.values().next().value,
        locations: this.locations
      }
    });

    dialogRef.afterClosed().subscribe(_ => this.loadLocations(false));
  }

  showUploadImageDialog() {
    if (this.clickedRows.size <= 0) {
      return;
    }

    const dialogRef = this.dialog.open(UploadImageDialogComponent, {
      width: '850px',
      data: {
        product: this.clickedRows.values().next().value
      }
    });

    dialogRef.afterClosed().subscribe(_ => this.loadProducts());
  }

    protected readonly faCheck = faCheck;
    protected readonly faXmark = faXmark;
  protected readonly faPlus = faPlus;
  protected readonly faEdit = faEdit;
  protected readonly faImage = faImage;
  protected readonly faTrashCan = faTrashCan;
  protected readonly faMapPin = faMapPin;
}
