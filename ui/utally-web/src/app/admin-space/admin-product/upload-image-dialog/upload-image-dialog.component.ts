import {Component, Inject, OnInit} from '@angular/core';
import {Product} from "../../../model/product";
import {AbstractHttpHandler} from "../../../common/abstract-http-handler";
import {SessionService} from "../../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../../service/message-handler.service";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {catchError} from "rxjs";
import {ImageHttpAdapter} from "../../../adapter/image-http-adapter";
import {ImageHandlerService} from "../../../service/image-handler.service";
import {ProductHttpAdapter} from "../../../adapter/product-http-adapter";
import {base64ToFile, ImageCroppedEvent} from "ngx-image-cropper";

@Component({
  selector: 'app-upload-image-dialog',
  templateUrl: './upload-image-dialog.component.html',
  styleUrls: ['./upload-image-dialog.component.scss']
})
export class UploadImageDialogComponent extends AbstractHttpHandler implements OnInit {

  product: Product;
  showProgressIndicator = false;
  showUploadError = false;
  fileToUpload: File | null = null;
  imageUrl = "";
  croppedImage: any = '';
  imageChangedEvent: any = '';

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    @Inject(MAT_DIALOG_DATA) public data: {product: Product},
    private imageHttpAdapter: ImageHttpAdapter,
    private productHttpAdapter: ProductHttpAdapter,
    private imageHandler: ImageHandlerService,
  ) {
    super(sessionService, router, messageHandlerService);
    this.product = data.product;
  }

  ngOnInit(): void {
    this.loadImage();
  }

  handleFileInput(event: Event) {
    let file = (event.target as HTMLInputElement)?.files?.item(0);
    if (file) {
      this.imageChangedEvent = event;
      this.fileToUpload = file;
      this.showUploadError = false;
    }
  }

  onUploadClick() {
    if (this.fileToUpload) {
      let filename = this.fileToUpload.name.replace(/\.[^/.]+$/, "") + ".png";
      let imageFile = new File([base64ToFile(this.croppedImage)], filename, { type: 'image/png' });

      this.showUploadError = false;
      this.imageHttpAdapter.uploadImage(imageFile).pipe(
        catchError(this.handleError(null))
      ).subscribe({
        next: _ => {
          if (this.fileToUpload) {
            this.product.imageName = filename;
            this.productHttpAdapter.putProduct(this.product).pipe(
              catchError(this.handleError(null))
            ).subscribe({
              next: _ => this.loadImage(),
              error: _ => this.showProgressIndicator = false
            })
          }
        },
        error: _ => {
          this.showUploadError = true;
          this.showProgressIndicator = false;
        }
      });
    }
  }

  loadImage() {
    this.showProgressIndicator = true;
    this.imageHandler.getImageAsBase64(this.product.imageName).pipe(
      catchError(this.handleError(""))
    ).subscribe( r => {
      this.imageUrl = r;
      this.showProgressIndicator = false;
    })
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
}
