import { Component, OnInit } from '@angular/core';
import {UntypedFormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {Product, ProductCategory} from "../../../model/product";
import {ImageHttpAdapter} from "../../../adapter/image-http-adapter";
import {catchError} from "rxjs";
import {AbstractHttpHandler} from "../../../common/abstract-http-handler";
import {SessionService} from "../../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../../service/message-handler.service";
import {base64ToFile, ImageCroppedEvent} from "ngx-image-cropper";

@Component({
  selector: 'app-new-product-dialog',
  templateUrl: './new-product-dialog.component.html',
  styleUrls: ['./new-product-dialog.component.scss']
})
export class NewProductDialogComponent extends AbstractHttpHandler implements OnInit {

  nameControl = new UntypedFormControl('', [Validators.required]);
  priceControl = new UntypedFormControl('', [Validators.required, Validators.min(1)]);
  showCategoryError = false;
  showFileError = false;
  showUploadError = false;
  showInsertError = false;
  enabled = true;
  displayStatistics = false;
  description = "";

  categoryValue: ProductCategory;
  fileToUpload: File | null = null;
  croppedImage: any = '';
  imageChangedEvent: any = '';

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    public dialogRef: MatDialogRef<NewProductDialogComponent>,
    private imageHttpAdapter: ImageHttpAdapter
  ) {
    super(sessionService, router, messageHandlerService);
  }

  ngOnInit(): void {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    this.showCategoryError = false;
    this.showFileError = false;
    let valid = true;
    if (this.nameControl.invalid) {
      valid = false;
    }
    if (this.priceControl.invalid) {
      valid = false;
    }
    if (this.categoryValue == null) {
      this.showCategoryError = true;
      valid = false;
    }
    if (this.fileToUpload == null) {
      this.showFileError = true;
      valid = false;
    }

    if (!valid) {
      return;
    }

    if (this.fileToUpload) {
      let filename = this.fileToUpload.name.replace(/\.[^/.]+$/, "") + ".png";
      let imageFile = new File([base64ToFile(this.croppedImage)], filename, { type: 'image/png' });

      this.imageHttpAdapter.uploadImage(imageFile).pipe(
        catchError(this.handleError(null))
      ).subscribe({
        next: _ => this.dialogRef.close({
          name: this.nameControl.value,
          enabled: this.enabled,
          displayStatistic: this.displayStatistics,
          price: this.priceControl.value,
          description: this.description,
          imageName: filename,
          category: this.categoryValue
        } as Product),
        error: _ => this.showUploadError = true
      });
    }
  }

  handleFileInput(event: Event) {
    let file = (event.target as HTMLInputElement)?.files?.item(0);
    if (file) {
      this.imageChangedEvent = event;
      this.fileToUpload = file;
      this.showFileError = false;
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
}
