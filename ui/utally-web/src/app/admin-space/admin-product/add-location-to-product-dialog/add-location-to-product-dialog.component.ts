import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Product} from "../../../model/product";
import {Location} from "../../../model/location";
import {AbstractHttpHandler} from "../../../common/abstract-http-handler";
import {SessionService} from "../../../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../../../service/message-handler.service";
import {LocationHttpAdapter} from "../../../adapter/location-http-adapter";
import {catchError} from "rxjs";

@Component({
  selector: 'app-add-location-to-product-dialog',
  templateUrl: './add-location-to-product-dialog.component.html',
  styleUrls: ['./add-location-to-product-dialog.component.scss']
})
export class AddLocationToProductDialogComponent extends AbstractHttpHandler {

  product: Product;
  locations: Location[] = [];
  showProgressIndicator = false;

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    private locationHttpAdapter: LocationHttpAdapter,
    @Inject(MAT_DIALOG_DATA) public data: {product: Product, locations: Location[]}
  ) {
    super(sessionService, router, messageHandlerService);
    this.product = data.product;
    this.locations = data.locations;
  }

  addProductToLocation(location: Location, value: boolean) {
    this.showProgressIndicator = true;
    if (value) {
      this.locationHttpAdapter.addProductToLocation(this.product.id, location.id).pipe(
        catchError(this.handleError(null))
      ).subscribe(_ => this.showProgressIndicator = false);
    } else {
      this.locationHttpAdapter.removeProductFromLocation(this.product.id, location.id).pipe(
        catchError(this.handleError(null))
      ).subscribe(_ => this.showProgressIndicator = false);
    }
  }
}
