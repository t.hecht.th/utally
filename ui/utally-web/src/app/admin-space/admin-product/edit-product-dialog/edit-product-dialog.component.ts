import {Component, Inject, OnInit} from '@angular/core';
import {Product, ProductCategory} from "../../../model/product";
import {UntypedFormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-edit-product-dialog',
  templateUrl: './edit-product-dialog.component.html',
  styleUrls: ['./edit-product-dialog.component.scss']
})
export class EditProductDialogComponent implements OnInit {

  nameControl = new UntypedFormControl('', [Validators.required]);
  priceControl = new UntypedFormControl('', [Validators.required, Validators.min(1)]);
  showCategoryError = false;
  showInsertError = false;
  enabled = true;
  displayStatistics = false;
  description = "";

  categoryValue: ProductCategory;
  product: Product;

  constructor(
    public dialogRef: MatDialogRef<EditProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {product: Product}
  ) {
    this.product = data.product;
  }

  ngOnInit(): void {
    this.nameControl.setValue(this.product.name);
    this.priceControl.setValue(this.product.price);
    this.enabled = this.product.enabled;
    this.displayStatistics = this.product.displayStatistic;
    this.description = this.product.description;
    this.categoryValue = this.product.category;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    this.showCategoryError = false;
    let valid = true;
    if (this.nameControl.invalid) {
      valid = false;
    }
    if (this.priceControl.invalid) {
      valid = false;
    }
    if (this.categoryValue == null) {
      this.showCategoryError = true;
      valid = false;
    }

    if (!valid) {
      return;
    }

    this.dialogRef.close({
      id: this.product.id,
      name: this.nameControl.value,
      enabled: this.enabled,
      displayStatistic: this.displayStatistics,
      price: this.priceControl.value,
      description: this.description,
      category: this.categoryValue,
      imageName: this.product.imageName
    } as Product)
  }
}
