import {Component, Input, OnInit} from '@angular/core';
import {Timeslot} from "../../../model/timeslot";
import {Product} from "../../../model/product";
import {Tally} from "../../../model/tally";

@Component({
  selector: 'app-timeslot-table',
  templateUrl: './timeslot-table.component.html',
  styleUrls: ['./timeslot-table.component.scss']
})
export class TimeslotTableComponent implements OnInit {

  @Input() timeslots: Timeslot[];
  @Input() products: Product[];

  _tallies: Tally[];
  get tallies(): Tally[] {
    return this._tallies;
  }
  @Input() set tallies(value: Tally[]) {
    this._tallies = value;
    let newProductList = this.products.filter(p => this.getNumberOfTalliesByProduct(p) > 0);
    if (this.displayedProducts && this.displayedProducts.length != newProductList.length) {
      this.displayedProducts = newProductList;
    }
  }

  displayedColumns = new Array<string>();
  timeslotColumns = new Array<string>();

  displayedProducts: Product[];

  constructor() { }

  ngOnInit(): void {
    this.displayedProducts = this.products.filter(p => this.getNumberOfTalliesByProduct(p) > 0);

    this.displayedColumns.push("Image");
    for (const timeslot of this.timeslots) {
      this.timeslotColumns.push(timeslot.id.toString());
      this.displayedColumns.push(timeslot.id.toString());
    }
    this.displayedColumns.push("Sum");
  }

  getTimeslotById(timeslotId: number) : Timeslot | undefined {
    return this.timeslots.find(t => t.id == +timeslotId);
  }

  getCellContent(product: Product, timeslotId: number) : number | null {
    let timeslot = this.timeslots.find(t => t.id == timeslotId);
    if (!timeslot) {
      return null;
    }
    return this.tallies.filter(t =>
      t.productId == product.id
      && new Date(timeslot!.start).getTime() <= new Date(t.time).getTime()
      && new Date(timeslot!.end).getTime() > new Date(t.time).getTime()
    ).length;
  }

  getNumberOfTalliesByProduct(product: Product) : number {
    return this.tallies.filter(t =>
      !t.cancelled && t.productId == product.id
    ).length;
  }
}
