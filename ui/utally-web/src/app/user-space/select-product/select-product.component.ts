import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../model/product";
import {Tally} from "../../model/tally";
import {Timeslot} from "../../model/timeslot";

@Component({
  selector: 'app-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.scss']
})
export class SelectProductComponent implements OnInit {

  @Input() showLoadProductsProgressIndicator = false;
  @Input() productsByLocation: Product[] = [];
  @Input() productsByEvent: Product[] = [];
  @Input() tallies: Tally[];
  @Input() timeslots: Timeslot[];
  @Output() onProductSelect = new EventEmitter<number>();
  @Output() onTallyCancel = new EventEmitter<Tally>();

  enabledProductsByLocation: Product[] = [];

  constructor() { }

  ngOnInit(): void {
    this.enabledProductsByLocation = this.productsByLocation.filter(p => p.enabled);
  }

  getTallyCountByProduct(productId: number) : number {
    return this.tallies.filter(t => !t.cancelled && t.productId == productId).length;
  }

  onProductSelected(productId: number) {
    this.onProductSelect.emit(productId);
  }

  onTallyCancelled(tally: Tally) {
    this.onTallyCancel.emit(tally);
  }
}
