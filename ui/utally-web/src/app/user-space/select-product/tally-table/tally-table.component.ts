import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Product} from "../../../model/product";
import {Tally} from "../../../model/tally";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {Settings} from "../../../model/settings";
import {MatPaginator} from "@angular/material/paginator";
import {MatTable, MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-tally-table',
  templateUrl: './tally-table.component.html',
  styleUrls: ['./tally-table.component.scss']
})
export class TallyTableComponent implements OnInit, AfterViewInit {

  @Input() products: Product[] = [];
  @Output() onTallyCancelled = new EventEmitter<Tally>();

  _tallies: Tally[];
  get tallies(): Tally[] {
    return this._tallies;
  }
  @Input() set tallies(value: Tally[]) {
    this._tallies = value;
    if (this.dataSource) {
      this.dataSource.data = this.tallies;
    }
  }

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns = ['time', 'product', 'price', 'button'];
  settings: Settings | null;
  dataSource: MatTableDataSource<Tally>;

  constructor(public settingsHandler: SettingsHandlerService) { }

  ngOnInit(): void {
    this.loadSettings();
    this.dataSource = new MatTableDataSource(this.tallies);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  loadSettings() {
    this.settingsHandler.getSettings().subscribe(
      s => this.settings = s
    );
  }

  getProductByTally(tally: Tally) : Product | undefined {
    return this.products.find(p => p.id == tally.productId);
  }
}
