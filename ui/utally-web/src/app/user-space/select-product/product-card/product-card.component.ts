import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../../model/product";
import {ImageHandlerService} from "../../../service/image-handler.service";
import {SettingsHandlerService} from "../../../service/settings-handler.service";
import {MessageHandlerService} from "../../../service/message-handler.service";

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;
  @Input() tallyCount = 0;
  @Output() onProductSelect = new EventEmitter<number>();

  imageUrl: string;
  showProgressBar = false;

  constructor(
    private imageHandler: ImageHandlerService,
    public settingsHandler: SettingsHandlerService,
    private messageHandlerService: MessageHandlerService,
  ) { }

  ngOnInit(): void {
    this.loadImage();
  }

  loadImage() {
    this.showProgressBar = true;
    this.imageHandler.getImageAsBase64(this.product.imageName).subscribe( r => {
      this.imageUrl = r;
      this.showProgressBar = false;
    })
  }

  onClick() {
    this.onProductSelect.emit(this.product.id);
    this.messageHandlerService.onInfo("Enjoy your " + this.product.name);
  }
}
