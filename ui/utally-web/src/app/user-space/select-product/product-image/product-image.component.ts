import {Component, Input, OnInit} from '@angular/core';
import {Product} from "../../../model/product";
import {ImageHandlerService} from "../../../service/image-handler.service";

@Component({
  selector: 'app-product-image',
  templateUrl: './product-image.component.html',
  styleUrls: ['./product-image.component.scss']
})
export class ProductImageComponent implements OnInit {

  @Input() product: Product;

  imageUrl: string;
  showProgressBar = false;

  constructor(private imageHandler: ImageHandlerService,) { }

  ngOnInit(): void {
    this.loadImage();
  }

  loadImage() {
    this.showProgressBar = true;
    this.imageHandler.getImageAsBase64(this.product.imageName).subscribe( r => {
      this.imageUrl = r;
      this.showProgressBar = false;
    })
  }
}
