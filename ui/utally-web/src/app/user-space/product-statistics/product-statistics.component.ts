import {Component, Input, OnInit} from '@angular/core';
import {Product} from "../../model/product";
import {Team} from "../../model/team";
import {StatisticEntry} from "../../model/statisticEntry";
import {AbstractStatistics} from "../../common/abstract-statistics";

@Component({
  selector: 'app-product-statistics',
  templateUrl: './product-statistics.component.html',
  styleUrls: ['./product-statistics.component.scss']
})
export class ProductStatisticsComponent extends AbstractStatistics implements OnInit {

  @Input() product: Product;
  @Input() teams: Team[];
  @Input() statistics: StatisticEntry[];

  constructor() {
    super()
  }

  ngOnInit(): void {
    let productStatistics = this.statistics
      .filter(s => s.count > 0)
      .filter(s => s.productId == this.product.id)
      .sort((a, b) => b.count - a.count)
      .slice(0, 10);

    this.showStatistics(productStatistics, this.product, this.teams);
  }
}
