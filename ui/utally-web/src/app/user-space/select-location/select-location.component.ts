import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Event} from "../../model/event";
import {Location} from "../../model/location";
import {UntypedFormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-select-location',
  templateUrl: './select-location.component.html',
  styleUrls: ['./select-location.component.scss']
})
export class SelectLocationComponent {

  eventSelectionControl = new UntypedFormControl('', [Validators.required]);
  locationSelectionControl = new UntypedFormControl('', [Validators.required]);

  @Input() events: Event[] = [];
  @Input() locations: Location[] = [];
  @Input() showProgressIndicator = false;
  @Output() eventChange = new EventEmitter<Event>();
  @Output() locationChange = new EventEmitter<Location>();
  @Output() onSubmit = new EventEmitter<boolean>();
}


