import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Team} from "../../model/team";
import {Player} from "../../model/player";
import {MatDialog} from "@angular/material/dialog";
import {NewPlayerDialogComponent} from "../new-player-dialog/new-player-dialog.component";
import {faUserPlus} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-select-player',
  templateUrl: './select-player.component.html',
  styleUrls: ['./select-player.component.scss']
})
export class SelectPlayerComponent {

  @Input() teams: Team[] = [];
  @Input() players: Player[] = [];
  @Input() showLoadPlayersProgressIndicator = false;
  @Input() showLoadTeamsProgressIndicator = false;
  @Input() selfRegistration = true;
  selectedTeam: Team | undefined;
  @Output() selectedTeamChange = new EventEmitter<Team>();
  @Output() selectedPlayerChange = new EventEmitter<Player>();
  @Output() newPlayerSubmission = new EventEmitter<string>();

  constructor(
    private dialog: MatDialog
  ) {
  }

  onSelectTeam(team: Team) {
    if (this.selectedTeam == team) {
      this.selectedTeam = undefined;
      this.selectedTeamChange.emit();
    } else {
      this.selectedTeam = team;
      this.selectedTeamChange.emit(team);
    }
  }

  showNewPlayerDialog() {
    const dialogRef = this.dialog.open(NewPlayerDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.newPlayerSubmission.emit(result);
      }
    });
  }

  onSelectPlayer(player: Player) {
    if (!player.paid) {
      this.selectedPlayerChange.emit(player);
    }
  }

    protected readonly faUserPlus = faUserPlus;
}
