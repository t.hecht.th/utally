import {Component} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {UntypedFormControl, Validators} from "@angular/forms";
import {faUserPlus} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-new-player-dialog',
  templateUrl: './new-player-dialog.component.html',
  styleUrls: ['./new-player-dialog.component.scss']
})
export class NewPlayerDialogComponent {

  nameControl = new UntypedFormControl('', [Validators.required])

  constructor(public dialogRef: MatDialogRef<NewPlayerDialogComponent>) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick() {
    if (this.nameControl.invalid) {
      return;
    }
    this.dialogRef.close(this.nameControl.value);
  }

  protected readonly faUserPlus = faUserPlus;
}
