import {Component, OnInit} from '@angular/core';
import {AbstractSpace} from "../common/abstract-space";
import {SessionService} from "../service/session.service";
import {Router} from "@angular/router";
import {MessageHandlerService} from "../service/message-handler.service";
import {MatDialog} from "@angular/material/dialog";
import {Event} from "../model/event";
import {EventHttpAdapter} from "../adapter/event-http-adapter";
import {catchError} from "rxjs";
import {LocationHttpAdapter} from "../adapter/location-http-adapter";
import {Location} from "../model/location";
import {Team} from "../model/team";
import {TeamHttpAdapter} from "../adapter/team-http-adapter";
import {PlayerHttpAdapter} from "../adapter/player-http-adapter";
import {Player} from "../model/player";
import {Product} from "../model/product";
import {ProductHttpAdapter} from "../adapter/product-http-adapter";
import {TallyHttpAdapter} from "../adapter/tally-http-adapter";
import {PostTally, Tally} from "../model/tally";
import {Timeslot} from "../model/timeslot";
import {TimeslotHttpAdapter} from "../adapter/timeslot-http-adapter";
import {StatisticEntry} from "../model/statisticEntry";
import {SettingsHandlerService} from "../service/settings-handler.service";
import {faArrowRightFromBracket} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-user-space',
  templateUrl: './user-space.component.html',
  styleUrls: ['./user-space.component.scss']
})
export class UserSpaceComponent extends AbstractSpace implements OnInit {
  faArrowRightFromBracket = faArrowRightFromBracket;

  events: Event[] = [];
  locations: Location[] = [];
  selectedEvent: Event;
  selectedLocation: Location;
  showLocationSelectionProgressIndicator = false;

  teams: Team[];
  players: Player[];
  selectedTeam: Team | null;
  selectedPlayer: Player | null;
  showLoadTeamsProgressIndicator = false;
  showLoadPlayersProgressIndicator = false;

  productsByLocation: Product[];
  productsByLocationForStatistics: Product[];
  productsByEvent: Product[];
  showLoadProductsProgressIndicator = false;

  selfRegistration = true;

  tallies: Tally[] | null;
  timeslots: Timeslot[];

  statisticEntries: StatisticEntry[] = [];

  toolBarTitle = "Utally";

  constructor(
    sessionService: SessionService,
    router: Router,
    messageHandlerService: MessageHandlerService,
    dialog: MatDialog,
    private eventHttpAdapter: EventHttpAdapter,
    private locationHttpAdapter: LocationHttpAdapter,
    private teamHttpAdapter: TeamHttpAdapter,
    private playerHttpAdapter: PlayerHttpAdapter,
    private productHttpAdapter: ProductHttpAdapter,
    private settingsHandlerService: SettingsHandlerService,
    private tallyHttpAdapter: TallyHttpAdapter,
    private timeslotHttpAdapter: TimeslotHttpAdapter,
  ) {
    super(sessionService, router, messageHandlerService, dialog);
  }

  ngOnInit(): void {
    this.loadEvent();
    if (this.isLocationSelected()) {
      this.onPlayerDeselected();
    }
    this.settingsHandlerService.getSettings().pipe(
      catchError(this.handleError(null))
    ).subscribe(r => this.selfRegistration = r?.selfRegistration == true);
  }

  isLocationSelected(): boolean {
    return !Number.isNaN(this.sessionService.getLocationId());
  }

  isPlayerSelected(): boolean {
    return !Number.isNaN(this.sessionService.getPlayerId());
  }

  loadEvent() {
    this.showLocationSelectionProgressIndicator = true;
    this.eventHttpAdapter.getEvents().pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.showLocationSelectionProgressIndicator = false;
      this.events = r.sort((a,b) => a.title.localeCompare(b.title));
      if (this.sessionService.getEventId()) {
        let selectedEvent = this.events.find(e => e.id == this.sessionService.getEventId());
        if (selectedEvent) {
          this.selectedEvent = selectedEvent;
          this.loadLocation();
          this.loadTimeslots();
          this.loadStatistics();
        }
      }
    });
  }

  onEventSelected(event: Event) {
    this.selectedEvent = event;
    this.showLocationSelectionProgressIndicator = true;
    this.locationHttpAdapter.getLocationsByEvent(event.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.showLocationSelectionProgressIndicator = false;
      this.locations = r.sort((a,b) => a.name.localeCompare(b.name));
    });
  }

  onLocationSelected(location: Location) {
    this.selectedLocation = location;
  }

  onSelectLocationSubmit() {
    if (this.selectedEvent && this.selectedLocation) {
      this.sessionService.selectLocation(this.selectedEvent.id, this.selectedLocation.id);
      this.updateToolbarTitle();
      this.loadTeams();
      this.loadStatistics();
      this.loadTimeslots();
    }
  }

  loadLocation() {
    if (this.isLocationSelected()) {
      this.loadLocationById(this.sessionService.getLocationId());
    }
  }

  loadLocationById(locationId: number) {
    this.locationHttpAdapter.getLocation(locationId).pipe(
      catchError(this.handleError(null))
    ).subscribe(r => {
      if (r != null) {
        this.onLocationSelected(r);
        this.loadTeams();
      }
    });
  }

  loadTeams() {
    this.showLoadTeamsProgressIndicator = true;
    this.teamHttpAdapter.getTeamsByEvent(this.selectedEvent.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.showLoadTeamsProgressIndicator = false;
      this.teams = r.sort((a,b) => a.shortName.localeCompare(b.shortName));
      this.updateToolbarTitle();
      this.loadProducts();
    });
  }

  onSelectTeam(team: Team) {
    this.selectedTeam = team;
    this.updateToolbarTitle();
    this.loadPlayers();
  }

  loadPlayers() {
    if (this.selectedTeam) {
      this.showLoadPlayersProgressIndicator = true;
      this.playerHttpAdapter.getPlayersByTeamAndEvent(this.selectedTeam.id, this.sessionService.getEventId()).pipe(
        catchError(this.handleError([]))
      ).subscribe(r => {
        this.showLoadPlayersProgressIndicator = false;
        this.players = r.sort((a,b) => a.name.localeCompare(b.name));
      });
    } else {
      this.players = [];
    }
  }

  onPlayerSelected(player: Player) {
    if (player) {
      this.showLoadProductsProgressIndicator = true;
      this.selectedPlayer = player;
      this.updateToolbarTitle();
      this.sessionService.setPlayer(player.id);
      this.loadTallies();
      this.statisticEntries = [];
    }
  }

  onPlayerDeselected() {
    this.sessionService.removePlayer();
    this.selectedPlayer = null;
    this.selectedTeam = null;
    this.tallies = null;
    this.players = [];
    this.updateToolbarTitle();
    this.loadStatistics();
  }

  postNewPlayer(name: string) {
    if (this.selectedTeam && name) {
      let player : Partial<Player> = {
        name: name,
        enabled: true,
        teamId: this.selectedTeam.id,
        eventId: this.sessionService.getEventId()
      }
      this.playerHttpAdapter.postPlayer(player).pipe(
        catchError(this.handleError([]))
      ).subscribe(_r => this.loadPlayers());
    }
  }

  loadProducts() {
    this.showLoadProductsProgressIndicator = true;
    this.productHttpAdapter.getProductsByEvent(this.selectedEvent.id).pipe(
      catchError(this.handleError([]))
    ).subscribe(r => {
      this.showLoadProductsProgressIndicator = false;
      this.productsByEvent = r.sort((a, b) => a.name.localeCompare(b.name));
      this.productsByLocation = this.productsByEvent.filter(p => this.selectedLocation.productIds.includes(p.id));
      this.productsByLocationForStatistics = this.productsByLocation.filter(p => p.displayStatistic);
    });
  }

  updateToolbarTitle() {
    this.toolBarTitle = "Utally"
    if (this.selectedEvent && !this.selectedPlayer) {
      this.toolBarTitle = this.toolBarTitle + " - " + this.selectedEvent.title;
    }
    if (this.selectedLocation && !this.selectedPlayer) {
      this.toolBarTitle = this.toolBarTitle + " - " + this.selectedLocation.name;
    }
    if (this.selectedTeam) {
      if (this.selectedPlayer) {
        this.toolBarTitle = this.toolBarTitle + " - " + this.selectedPlayer.name + ", " + this.selectedTeam.shortName;
      } else {
        this.toolBarTitle = this.toolBarTitle + " - " + this.selectedTeam.shortName;
      }
    }
    if (this.tallies) {
      this.toolBarTitle = this.toolBarTitle + " - " + this.settingsHandlerService.getAmountFormatted(this.getSumOfTallies());
    }
  }

  getSumOfTallies(): number {
    if (this.tallies && this.tallies.length > 0 && this.productsByEvent) {
      return this
        .tallies
        .filter(t => !t.cancelled).map(t => this.getProductById(t.productId))
        .map(p => p ? p.price : 0)
        .reduce((p, c) => p+c, 0);
    } else {
      return 0;
    }
  }

  getProductById(productId: number): Product | undefined {
    return this.productsByEvent.find(p => p.id == productId)
  }

  loadTallies() {
    if (this.selectedPlayer) {
      this.tallyHttpAdapter.getTalliesByPlayer(this.selectedPlayer.id).pipe(
        catchError(this.handleError([]))
      ).subscribe(r => {
        this.tallies = r.sort((a, b) => new Date(b.time).getTime() - new Date(a.time).getTime());
        this.updateToolbarTitle();
        this.showLoadProductsProgressIndicator = false;
      });
    }
  }

  onProductSelected(productId: number) {
    if (this.selectedLocation && this.selectedPlayer) {
      this.showLoadProductsProgressIndicator = true;
      let tally : PostTally = {
        locationId: this.selectedLocation.id,
        playerId: this.selectedPlayer.id,
        productId: productId,
      };
      this.tallyHttpAdapter.postTally(tally).pipe(
        catchError(this.handleError(null))
      ).subscribe(_r => this.loadTallies());
    }
  }

  loadTimeslots() {
    if (this.selectedEvent) {
      this.timeslotHttpAdapter.getTimeslotsByEvent(this.selectedEvent.id).pipe(
        catchError(this.handleError([]))
      ).subscribe(r => {
        this.timeslots = r.sort((a, b) => new Date(a.start).getTime() - new Date(b.start).getTime());
      });
    }
  }

  onTallyCancelled(tally: Tally) {
    this.showLoadProductsProgressIndicator = true;
    this.tallyHttpAdapter.cancelTally(tally.id, !tally.cancelled).pipe(
      catchError(this.handleError(null))
    ).subscribe(_r => this.loadTallies());
  }

  loadStatistics() {
    if (this.selectedEvent) {
      this.eventHttpAdapter.getStatistics(this.selectedEvent.id).pipe(
        catchError(this.handleError([]))
      ).subscribe(r => this.statisticEntries = r);
    }
  }
}
