import {Injectable} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {SettingsHandlerService} from "./settings-handler.service";

@Injectable({
  providedIn: 'root'
})
export class MessageHandlerService {

  constructor(
    private snackBar: MatSnackBar,
    private settingsHandlerService: SettingsHandlerService
  ) { }

  onError(message: string) {
    console.error(message);
    this.openSnackBar(message);
  }

  onInfo(message: string) {
    console.info(message);
    this.openSnackBar(message);
  }

  openSnackBar(message: string) {
    this.settingsHandlerService.getSettings().subscribe(settings =>
      this.snackBar.open(message, 'OK', {
        duration: settings.growlDelay
      })
    );
  }
}
