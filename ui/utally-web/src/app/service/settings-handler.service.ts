import { Injectable } from '@angular/core';
import {Settings} from "../model/settings";
import {map, Observable, of} from "rxjs";
import {SettingsHttpAdapter} from "../adapter/settings-http-adapter";
import {CurrencyPipe, formatDate, formatNumber, getNumberOfCurrencyDigits} from "@angular/common";
import {Payment} from "../model/payment";

@Injectable({
  providedIn: 'root'
})
export class SettingsHandlerService {

  private settings: Settings | null = null;
  private payment: Payment | null = null;

  constructor(
    private settingsHttpAdapter : SettingsHttpAdapter,
    private cp: CurrencyPipe
  ) { }

  getSettings(): Observable<Settings> {
    if (this.settings) {
      return of(this.settings);
    }
    return this.settingsHttpAdapter.getSettings().pipe(
      map(r => {
        this.settings = r
        return r;
      })
    );
  }

  getCurrencyCode(): string {
    return this.settings?.currencyCode || "EUR";
  }

  getPayment(): Observable<Payment> {
    if (this.payment) {
      return of(this.payment);
    }
    return this.settingsHttpAdapter.getPayment().pipe(
      map(r => {
        this.payment = r
        return r;
      })
    );
  }

  patchSettings(patch: any): Observable<Settings> {
    return this.settingsHttpAdapter.patchSettings(patch).pipe(
      map(r => {
        this.settings = r
        return r;
      })
    );
  }

  getFractionDigits(amount: number): number {
    let digits = getNumberOfCurrencyDigits(this.settings?.currencyCode || 'EUR');
    return amount/Math.pow(10, digits);
  }

  getAmountFormatted(amount: number): string {
    let currencyCode = this.settings?.currencyCode || "EUR";
    return this.cp.transform(this.getFractionDigits(amount), currencyCode) || "";
  }

  getAmountFormattedWithoutCurrencyCode(amount: number): string {
    let digits = getNumberOfCurrencyDigits(this.settings?.currencyCode || 'EUR');
    return formatNumber(this.getFractionDigits(amount), "en", `1.${digits}-${digits}`);
  }

  getTimestampFormatted(date: Date | null): string {
    if (!date) {
      return "";
    }

    let format = this.settings?.dateTimeFormat || "yyyy-MM-dd HH:mm";

    return formatDate(date, format,"en-US");
  }
}
