import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private static UTALLY_UN = 'Utally-UN';
  private static UTALLY_PW = 'Utally-PW';
  private static UTALLY_SPACE = 'Utally-SPACE';
  private static UTALLY_PIN = 'Utally-PIN';
  private static UTALLY_EVENT = 'Utally-EVENT';
  private static UTALLY_LOCATION = 'Utally-LOCATION';
  private static UTALLY_PLAYER = 'Utally-PLAYER';

  public username(): string {
    return localStorage.getItem(SessionService.UTALLY_UN) || '';
  }

  public password(): string {
    return localStorage.getItem(SessionService.UTALLY_PW) || '';
  }

  public space(): Space {
    let space = localStorage.getItem(SessionService.UTALLY_SPACE);
    if (space == null) {
      return Space.NOT_SELECTED;
    }
    return Space[space as keyof typeof Space];
  }

  public pin(): string {
    return localStorage.getItem(SessionService.UTALLY_PIN) || '';
  }

  public isLoggedIn(): boolean {
    return this.username().length > 0;
  }

  public spaceSelected(): boolean {
    return this.space() != Space.NOT_SELECTED;
  }

  public hasPin(): boolean {
    return this.pin().length > 0;
  }

  public login(username: string, password: string, space: Space, pin: string) {
    localStorage.setItem(SessionService.UTALLY_UN, username);
    localStorage.setItem(SessionService.UTALLY_PW, password);
    localStorage.setItem(SessionService.UTALLY_PIN, pin);
    if (space != Space.NOT_SELECTED) {
      localStorage.setItem(SessionService.UTALLY_SPACE, space.toString());
    } else {
      localStorage.removeItem(SessionService.UTALLY_SPACE);
    }
  }

  public selectEvent(eventId: number) {
    localStorage.setItem(SessionService.UTALLY_EVENT, eventId.toString());
  }

  public selectLocation(eventId: number, locationId: number) {
    localStorage.setItem(SessionService.UTALLY_EVENT, eventId.toString());
    localStorage.setItem(SessionService.UTALLY_LOCATION, locationId.toString());
  }

  public getEventId(): number {
    return SessionService.getIdByKey(SessionService.UTALLY_EVENT);
  }

  public getLocationId(): number {
    return SessionService.getIdByKey(SessionService.UTALLY_LOCATION);
  }

  public setPlayer(playerId: number) {
    localStorage.setItem(SessionService.UTALLY_PLAYER, playerId.toString());
  }

  public removePlayer() {
    localStorage.removeItem(SessionService.UTALLY_PLAYER);
  }

  public getPlayerId(): number {
    return SessionService.getIdByKey(SessionService.UTALLY_PLAYER);
  }

  public logout() {
    localStorage.removeItem(SessionService.UTALLY_UN);
    localStorage.removeItem(SessionService.UTALLY_PW);
    localStorage.removeItem(SessionService.UTALLY_SPACE);
    localStorage.removeItem(SessionService.UTALLY_PIN);
    localStorage.removeItem(SessionService.UTALLY_EVENT);
    localStorage.removeItem(SessionService.UTALLY_LOCATION);
    localStorage.removeItem(SessionService.UTALLY_PLAYER);
  }

  private static getIdByKey(key: string): number {
    let id = localStorage.getItem(key);
    if (id) {
      return Number(id);
    } else {
      return Number.NaN;
    }
  }
}

export enum Space {
  NOT_SELECTED,
  ADMIN,
  OPERATOR,
  USER,
  PARTY
}
