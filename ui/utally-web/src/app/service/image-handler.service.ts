import { Injectable } from '@angular/core';
import {ImageHttpAdapter} from "../adapter/image-http-adapter";
import {mergeMap, Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ImageHandlerService {
  private images : Map<string, Blob> = new Map<string, Blob>();

  constructor(private imageHttpAdapter: ImageHttpAdapter) { }

  getImageAsBase64(filename: string): Observable<string> {
    let blob = this.images.get(filename);
    if (blob) {
      return this.readFile(blob);
    }
    return this.imageHttpAdapter.downloadImage(filename).pipe(
      mergeMap(blob => {
        if (blob) {
          this.images.set(filename, blob);
        }
        return this.readFile(blob);
      })
    );
  }

  private readFile(blob: Blob): Observable<string> {
    const sub = new Subject<string>();
    const reader = new FileReader();

    reader.onload = () => {
      const content: string = reader.result as string;
      sub.next(content);
      sub.complete();
    };

    reader.readAsDataURL(blob);
    return sub.asObservable();
  }
}
