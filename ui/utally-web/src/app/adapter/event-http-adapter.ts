import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {Observable} from "rxjs";
import {Event} from "../model/event";
import {StatisticEntry} from "../model/statisticEntry";

@Injectable({
  providedIn: 'root'
})
export class EventHttpAdapter extends AbstractHttpAdapter {
  private static EVENT_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/events";
  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  getEventsByTeam(teamId: number): Observable<Event[]> {
    return this.http.get<Event[]>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/team/${teamId}`, this.createHeaders());
  }

  getEventsByLocation(locationId: number): Observable<Event[]> {
    return this.http.get<Event[]>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/location/${locationId}`, this.createHeaders());
  }

  getEventsByProduct(productId: number): Observable<Event[]> {
    return this.http.get<Event[]>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/product/${productId}`, this.createHeaders());
  }

  getEvent(eventId: number): Observable<Event> {
    return this.http.get<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/${eventId}`, this.createHeaders());
  }

  getEventByTimeslot(timeslotId: number): Observable<Event> {
    return this.http.get<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/timeslot/${timeslotId}`, this.createHeaders());
  }

  postEvent(event: Event): Observable<Event> {
    return this.http.post<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/`, event, this.createHeaders());
  }

  putEvent(event: Event): Observable<Event> {
    return this.http.put<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/${event.id}`, event, this.createHeaders());
  }

  deleteEvent(eventId: number): Observable<object> {
    return this.http.delete(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/${eventId}`, this.createHeaders());
  }

  addLocationToEvent(locationId: number, eventId: number): Observable<Event> {
    return this.http.patch<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/${eventId}/addlocation/${locationId}`, null, this.createHeaders());
  }

  removeLocationFromEvent(locationId: number, eventId: number): Observable<Event> {
    return this.http.patch<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/${eventId}/removelocation/${locationId}`, null, this.createHeaders());
  }

  removeLocationFromAllEvents(locationId: number): Observable<Event> {
    return this.http.patch<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/removelocation/${locationId}`, null, this.createHeaders());
  }

  addTeamToEvent(teamId: number, eventId: number): Observable<Event> {
    return this.http.patch<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/${eventId}/addteam/${teamId}`, null, this.createHeaders());
  }

  removeTeamFromEvent(teamId: number, eventId: number): Observable<Event> {
    return this.http.patch<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/${eventId}/removeteam/${teamId}`, null, this.createHeaders());
  }

  removeTeamFromAllEvents(teamId: number): Observable<Event> {
    return this.http.patch<Event>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/removeteam/${teamId}`, null, this.createHeaders());
  }

  getStatistics(eventId: number): Observable<StatisticEntry[]> {
    return this.http.get<StatisticEntry[]>(`${EventHttpAdapter.EVENT_SERVICE_BASE_PATH}/statistics/${eventId}`, this.createHeaders());
  }
}
