import {Injectable} from "@angular/core";
import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ImageHttpAdapter extends AbstractHttpAdapter {
  private static IMAGE_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/images";
  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getImageUrl(filename: string) {
    return `${ImageHttpAdapter.IMAGE_SERVICE_BASE_PATH}/${filename}`;
  }

  getImageFilenames(): Observable<string[]> {
    return this.http.get<string[]>(`${ImageHttpAdapter.IMAGE_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  downloadImage(filename: string): Observable<Blob> {
    return this
      .http
      .get(
        this.getImageUrl(filename),
        {
          headers: this.addAuthorizationHeader(new HttpHeaders()),
          responseType: 'blob'
        }
      );
  }

  uploadImage(fileToUpload: File): Observable<object> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(`${ImageHttpAdapter.IMAGE_SERVICE_BASE_PATH}/`, formData, this.createHeaders());
  }

  deleteImage(filename: string): Observable<object> {
    return this.http.delete(this.getImageUrl(filename), this.createHeaders());
  }
}
