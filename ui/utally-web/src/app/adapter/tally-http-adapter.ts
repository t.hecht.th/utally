import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {Observable} from "rxjs";
import {PostTally, Tally} from "../model/tally";

@Injectable({
  providedIn: 'root'
})
export class TallyHttpAdapter extends AbstractHttpAdapter {
  private static TALLY_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/tallies";
  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getTallies(): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  getTalliesByEvent(eventId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/event/${eventId}`, this.createHeaders());
  }

  getTalliesByTimeslot(eventId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/timeslot/${eventId}`, this.createHeaders());
  }

  getTalliesByLocation(locationId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/location/${locationId}`, this.createHeaders());
  }

  getTalliesByPlayer(playerId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/player/${playerId}`, this.createHeaders());
  }

  getTalliesByProduct(productId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/product/${productId}`, this.createHeaders());
  }

  getTalliesByTeam(teamId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/team/${teamId}`, this.createHeaders());
  }

  getTalliesByProductAndPlayer(productId: number, playerId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/product/${productId}/player/${playerId}`, this.createHeaders());
  }

  getTalliesByLocationAndPlayer(locationId: number, playerId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/location/${locationId}/player/${playerId}`, this.createHeaders());
  }

  getTalliesByLocationAndEvent(locationId: number, eventId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/location/${locationId}/event/${eventId}`, this.createHeaders());
  }

  getTalliesByProductAndEvent(productId: number, eventId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/product/${productId}/event/${eventId}`, this.createHeaders());
  }

  getTalliesByTeamAndEvent(teamId: number, eventId: number): Observable<Tally[]> {
    return this.http.get<Tally[]>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/team/${teamId}/event/${eventId}`, this.createHeaders());
  }

  getTally(tallyId: number): Observable<Tally> {
    return this.http.get<Tally>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/${tallyId}`, this.createHeaders());
  }

  postTally(tally: PostTally): Observable<Tally> {
    return this.http.post<Tally>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/`, tally, this.createHeaders());
  }

  cancelTally(tallyId: number, cancelled: boolean): Observable<Tally> {
    return this.http.patch<Tally>(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/${tallyId}/cancelled/${cancelled}`, null, this.createHeaders());
  }

  deleteTally(tallyId: number): Observable<object> {
    return this.http.delete(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/${tallyId}`, this.createHeaders());
  }

  deleteTalliesByProduct(productId: number): Observable<object> {
    return this.http.delete(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/product/${productId}`, this.createHeaders());
  }

  deleteTalliesByPlayer(playerId: number): Observable<object> {
    return this.http.delete(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/player/${playerId}`, this.createHeaders());
  }

  deleteTalliesByLocation(locationId: number): Observable<object> {
    return this.http.delete(`${TallyHttpAdapter.TALLY_SERVICE_BASE_PATH}/location/${locationId}`, this.createHeaders());
  }
}
