import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {Observable} from "rxjs";
import {Team} from "../model/team";

@Injectable({
  providedIn: 'root'
})
export class TeamHttpAdapter extends AbstractHttpAdapter {
  private static TEAM_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/teams";
  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(`${TeamHttpAdapter.TEAM_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  getTeamsByEvent(eventId: number): Observable<Team[]> {
    return this.http.get<Team[]>(`${TeamHttpAdapter.TEAM_SERVICE_BASE_PATH}/event/${eventId}`, this.createHeaders());
  }

  getTeam(teamId: number): Observable<Team> {
    return this.http.get<Team>(`${TeamHttpAdapter.TEAM_SERVICE_BASE_PATH}/${teamId}`, this.createHeaders());
  }

  postTeam(team: Team): Observable<Team> {
    return this.http.post<Team>(`${TeamHttpAdapter.TEAM_SERVICE_BASE_PATH}/`, team, this.createHeaders());
  }

  putTeam(team: Team): Observable<Team> {
    return this.http.put<Team>(`${TeamHttpAdapter.TEAM_SERVICE_BASE_PATH}/${team.id}`, team, this.createHeaders());
  }

  deleteTeam(teamId: number): Observable<object> {
    return this.http.delete(`${TeamHttpAdapter.TEAM_SERVICE_BASE_PATH}/${teamId}`, this.createHeaders());
  }
}
