import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {Observable} from "rxjs";
import {Location} from "../model/location";

@Injectable({
  providedIn: 'root'
})
export class LocationHttpAdapter extends AbstractHttpAdapter {
  private static LOCATION_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/locations";
  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getLocations(): Observable<Location[]> {
    return this.http.get<Location[]>(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  getLocationsByEvent(eventId: number): Observable<Location[]> {
    return this.http.get<Location[]>(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/event/${eventId}`, this.createHeaders());
  }

  getLocation(locationId: number): Observable<Location> {
    return this.http.get<Location>(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/${locationId}`, this.createHeaders());
  }

  postLocation(location: Location): Observable<Location> {
    return this.http.post<Location>(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/`, location, this.createHeaders());
  }

  putLocation(location: Location): Observable<Location> {
    return this.http.put<Location>(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/${location.id}`, location, this.createHeaders());
  }

  deleteLocation(locationId: number): Observable<object> {
    return this.http.delete(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/${locationId}`, this.createHeaders());
  }

  addProductToLocation(productId: number, locationId: number): Observable<Location> {
    return this.http.patch<Location>(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/${locationId}/addproduct/${productId}`, null, this.createHeaders());
  }

  removeProductFromLocation(productId: number, locationId: number): Observable<Location> {
    return this.http.patch<Location>(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/${locationId}/removeproduct/${productId}`, null, this.createHeaders());
  }

  removeProductFromAllLocations(productId: number): Observable<Location> {
    return this.http.patch<Location>(`${LocationHttpAdapter.LOCATION_SERVICE_BASE_PATH}/removeproduct/${productId}`, null, this.createHeaders());
  }
}
