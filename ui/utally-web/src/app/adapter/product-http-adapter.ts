import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {Observable} from "rxjs";
import {Product} from "../model/product";

@Injectable({
  providedIn: 'root'
})
export class ProductHttpAdapter extends AbstractHttpAdapter {
  private static PRODUCT_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/products";
  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${ProductHttpAdapter.PRODUCT_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  getProductsByEvent(eventId: number): Observable<Product[]> {
    return this.http.get<Product[]>(`${ProductHttpAdapter.PRODUCT_SERVICE_BASE_PATH}/event/${eventId}`, this.createHeaders());
  }

  getProductsByLocation(locationId: number): Observable<Product[]> {
    return this.http.get<Product[]>(`${ProductHttpAdapter.PRODUCT_SERVICE_BASE_PATH}/location/${locationId}`, this.createHeaders());
  }

  getProduct(productId: number): Observable<Product> {
    return this.http.get<Product>(`${ProductHttpAdapter.PRODUCT_SERVICE_BASE_PATH}/${productId}`, this.createHeaders());
  }

  postProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(`${ProductHttpAdapter.PRODUCT_SERVICE_BASE_PATH}/`, product, this.createHeaders());
  }

  putProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(`${ProductHttpAdapter.PRODUCT_SERVICE_BASE_PATH}/${product.id}`, product, this.createHeaders());
  }

  deleteProduct(productId: number): Observable<object> {
    return this.http.delete(`${ProductHttpAdapter.PRODUCT_SERVICE_BASE_PATH}/${productId}`, this.createHeaders());
  }
}
