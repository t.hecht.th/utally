import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {Observable} from "rxjs";
import {PatchPlayerAsOperator, PatchPlayerComment, Player} from "../model/player";

@Injectable({
  providedIn: 'root'
})
export class PlayerHttpAdapter extends AbstractHttpAdapter {
  private static PLAYER_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/players";
  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  getPlayersByEvent(eventId: number): Observable<Player[]> {
    return this.http.get<Player[]>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/event/${eventId}`, this.createHeaders());
  }

  getPlayersByTeam(teamId: number): Observable<Player[]> {
    return this.http.get<Player[]>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/team/${teamId}`, this.createHeaders());
  }

  getPlayersByTeamAndEvent(teamId: number, eventId: number): Observable<Player[]> {
    return this.http.get<Player[]>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/team/${teamId}/event/${eventId}`, this.createHeaders());
  }

  getPlayer(playerId: number): Observable<Player> {
    return this.http.get<Player>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/${playerId}`, this.createHeaders());
  }

  postPlayer(player: Partial<Player>): Observable<Player> {
    return this.http.post<Player>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/`, player, this.createHeaders());
  }

  patchPlayerAsAdmin(player: Player): Observable<Player> {
    return this.http.patch<Player>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/${player.id}`, player, this.createHeaders());
  }

  patchPlayerAsUser(player: Player): Observable<Player> {
    return this.http.patch<Player>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/${player.id}/update`, player, this.createHeaders());
  }

  patchPlayerComment(player: PatchPlayerComment): Observable<Player> {
    return this.http.patch<Player>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/${player.id}/comment`, player, this.createHeaders());
  }

  patchPlayerAsOperator(player: PatchPlayerAsOperator): Observable<Player> {
    return this.http.patch<Player>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/${player.id}/pay`, player, this.createHeaders());
  }

  revertPayment(playerId: number): Observable<Player> {
    return this.http.patch<Player>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/${playerId}/revert`, null, this.createHeaders());
  }

  mergePlayers(sourceId: number, destinationId: number): Observable<Player> {
    return this.http.patch<Player>(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/merge/${sourceId}/${destinationId}`, null, this.createHeaders());
  }

  deletePlayer(playerId: number): Observable<object> {
    return this.http.delete(`${PlayerHttpAdapter.PLAYER_SERVICE_BASE_PATH}/${playerId}`, this.createHeaders());
  }
}
