import {SessionService} from "../service/session.service";
import {HttpHeaders} from "@angular/common/http";

export class AbstractHttpAdapter {

  protected static SERVICE_PATH = "/utally/api";

  constructor(protected userService: SessionService) {
  }

  protected createHeaders(): {headers: HttpHeaders} {
    return {
      headers: this.addAuthorizationHeader(new HttpHeaders())
    };
  }

  protected addAuthorizationHeader(headers: HttpHeaders): HttpHeaders {
    return headers.set('Authorization',  `Basic ${btoa(this.userService.username() + ":" + this.userService.password())}`);
  }
}
