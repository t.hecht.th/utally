import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {Observable} from "rxjs";
import {Timeslot} from "../model/timeslot";

@Injectable({
  providedIn: 'root'
})
export class TimeslotHttpAdapter extends AbstractHttpAdapter {
  private static TIMESLOT_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/timeslots";
  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getTimeslots(): Observable<Timeslot[]> {
    return this.http.get<Timeslot[]>(`${TimeslotHttpAdapter.TIMESLOT_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  getTimeslotsByEvent(eventId: number): Observable<Timeslot[]> {
    return this.http.get<Timeslot[]>(`${TimeslotHttpAdapter.TIMESLOT_SERVICE_BASE_PATH}/event/${eventId}`, this.createHeaders());
  }

  getTimeslot(timeslotId: number): Observable<Timeslot> {
    return this.http.get<Timeslot>(`${TimeslotHttpAdapter.TIMESLOT_SERVICE_BASE_PATH}/${timeslotId}`, this.createHeaders());
  }

  postTimeslot(timeslot: Timeslot): Observable<Timeslot> {
    return this.http.post<Timeslot>(`${TimeslotHttpAdapter.TIMESLOT_SERVICE_BASE_PATH}/`, timeslot, this.createHeaders());
  }

  putTimeslot(timeslot: Timeslot): Observable<Timeslot> {
    return this.http.put<Timeslot>(`${TimeslotHttpAdapter.TIMESLOT_SERVICE_BASE_PATH}/${timeslot.id}`, timeslot, this.createHeaders());
  }

  deleteTimeslot(timeslotId: number): Observable<object> {
    return this.http.delete(`${TimeslotHttpAdapter.TIMESLOT_SERVICE_BASE_PATH}/${timeslotId}`, this.createHeaders());
  }
}
