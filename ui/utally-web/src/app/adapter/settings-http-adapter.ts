import {Injectable} from "@angular/core";
import {AbstractHttpAdapter} from "./abstract-http-adapter";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../service/session.service";
import {Observable} from "rxjs";
import {Settings} from "../model/settings";
import {Currency} from "../model/currency";
import {TimeZone} from "../model/timeZone";
import {Payment} from "../model/payment";

@Injectable({
  providedIn: 'root'
})
export class SettingsHttpAdapter extends AbstractHttpAdapter {
  private static SETTINGS_SERVICE_BASE_PATH = AbstractHttpAdapter.SERVICE_PATH + "/settings";

  constructor(private http: HttpClient, userService: SessionService) {
    super(userService);
  }

  getSettings(): Observable<Settings> {
    return this.http.get<Settings>(`${SettingsHttpAdapter.SETTINGS_SERVICE_BASE_PATH}/`, this.createHeaders());
  }

  getCurrencies(): Observable<Currency[]> {
    return this.http.get<Currency[]>(`${SettingsHttpAdapter.SETTINGS_SERVICE_BASE_PATH}/currencies`, this.createHeaders());
  }

  getTimeZones(): Observable<TimeZone[]> {
    return this.http.get<TimeZone[]>(`${SettingsHttpAdapter.SETTINGS_SERVICE_BASE_PATH}/timezones`, this.createHeaders());
  }

  patchSettings(settings: any): Observable<Settings> {
    return this.http.patch<Settings>(`${SettingsHttpAdapter.SETTINGS_SERVICE_BASE_PATH}/`, settings, this.createHeaders());
  }

  getPayment(): Observable<Payment> {
    return this.http.get<Payment>(`${SettingsHttpAdapter.SETTINGS_SERVICE_BASE_PATH}/payment`, this.createHeaders());
  }
}
