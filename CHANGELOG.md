## 2.0.0

### Material Design UI

- New UI based on Google Material Design
- Support for EPC QR codes
- Support for Paypal.me QR codes
- Removed InfluxDB client
- A lot of bug fixes and improvements in security

## 1.1.0

### REST API, Java 17 support and Docker

- Added a REST api to support custom client implementations like web and mobile apps.
- Added Java 17 support
- Players cannot add more tallies after payment
- Improved the bill view
- Added a Dockerfile to build a docker image
- Removed JMX network interface
- A lot of bug fixes and improvements in security

## 1.0.0

### Initial Release

- This is the initial release of Utally
