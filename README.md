Utally
======

Open Source Digital Tally List for Ultimate Tournaments.

Utally is maintained by [Tobias Hecht](https://twitter.com/Tbs_Hcht)

Features
--------

- Digital Tally List
- Self Registration
- Multiple Teams
- Multiple Locations
- Mulitple Events
- Teamranking
- Live Party Statistics
- Reusage of configured Teams
- Player Merge
- Password Protection
- Administration Views
- Administration Log
- Statistics
- Configurable Currencies
- Automatic Backups
- SSL/TLS Support
- REST API for custom clients
- and a lot more

Links
-----

* [Facebook](https://www.facebook.com/utallyproject/)
* [Twitter](https://twitter.com/utallyproject)

Download
--------

You can download the latest releases by clicking on the following links:

### 2.0.0

- [utally-2.0.0.tgz](https://dev-hecht.de/download/utally/releases/utally-2.0.0.tgz)
- [utally-2.0.0.zip](https://dev-hecht.de/download/utally/releases/utally-2.0.0.zip)
- [utally-docker-amd64-2.0.0.tar](https://dev-hecht.de/download/utally/releases/utally-docker-amd64-2.0.0.tar)
- [utally-docker-arm64-2.0.0.tar](https://dev-hecht.de/download/utally/releases/utally-docker-arm64-2.0.0.tar)

### 1.1.0

- [utally-1.1.0.tgz](https://dev-hecht.de/download/utally/releases/utally-1.1.0.tgz)
- [utally-1.1.0.zip](https://dev-hecht.de/download/utally/releases/utally-1.1.0.zip)
- [utally-docker-1.1.0.tar](https://dev-hecht.de/download/utally/releases/utally-docker-1.1.0.tar)

### 1.0.0

- [utally-1.0.0.tgz](https://dev-hecht.de/download/utally/releases/utally-1.0.0.tgz)
- [utally-1.0.0.zip](https://dev-hecht.de/download/utally/releases/utally-1.0.0.zip)

Building
--------

You need JDK 17 (or newer), Gradle 8.1.1 (or newer) and git installed.

    java -version
    gradle -version
    git --version

First clone the Utally repository:
    
    git clone https://gitlab.com/t.hecht.th/utally.git
    cd utally

To build Utally run:

    gradle ngBuild
    gradle build asciidoctor copyLicense
    gradle dists

This will build the artifact, documentation, the zip- and the tar-ball. You will find the distributions in `build/distributions`.

### Building the Docker image

To build the docker image for amd64 run in the project root:

    docker buildx build --platform linux/amd64 -t utally -f docker/Dockerfile .

To build for arm64 run instead:

    docker buildx build --platform linux/arm64 -t utally -f docker/Dockerfile .

Starting Utally
---------------

### Start without Docker

First unpack the distribution package. If you want to use the tar ball use the following command:

    tar xfz Utally-<version>.tgz
    cd utally-<version>

Or if you use the zip ball:

    unzip Utally-<version>.tgz
    cd utally-<version>

Now, you can start Utally by running the startup script:

    bin/utallytstart.sh
    
Follow the log by running the logtail script:

    bin/utallytlogtail.sh

Utally is running if the ps script dumps any output:

    bin/utallytps.sh

Stop Utally by running the stop script:

    bin/utallytstop.sh

### Start with Docker on remote machine
Commands to install Utally on remote machine using docker:

    # On local machine
    docker save utally > utally.tar
    scp utally.tar ${REMOTE_HOST}
    ssh ${REMOTE_HOST}
    # On ${REMOTE_HOST}
    docker stop utally
    docker rm utally
    docker load < ~/utally.tar
    docker run -d -it --restart=always -p ${PORT}:8080 --name=utally -v ${DATA_DIRECTORY}:/utally/data -v ${LOG_DIRECTORY}:/utally/log -v ${IMAGES_DIRECTORY}:/utally/images -v ${CONFIG_DIRECTORY}:/utally/config utally

When running this command the first time, the start script will copy all configuration files info the `${CONFIG_DIRECTORY}`. After editing the configuration, you must rerun the container.

### Admin page
After successful startup, you can administrate Utally in the administration views. Open a browser and visit:

    http://localhost:8080/utally/index.xhtml

The initial login credentials are `admin` with the password `#admin6ws`.

Configure Utally
----------------

You can configure Utally by editing the properties file:

    etc/utally.properties

Guides
------

### Installation Manual

You find the installation guide (only in German) in the distribution package under the following path:

    docs/manual/installation.html

Or visit the [online installation guide](https://dev-hecht.de/docs/utally/html5/installation.html).

### Administration Manual

The administration manual (only in German) can be found under the following location:

    docs/manual/administration.html

Or visit the [online administration manual](https://dev-hecht.de/docs/utally/html5/administration.html).

### Swagger UI

The Swagger UI is located under the following URL:

    http://localhost:8080/utally/swagger-ui.html

Feedback
--------

If you have any suggestions, questions, problems or found any bugs, feel free to write me an [e-mail](mailto:tobi3000@gmx.net).

License
-------

Utally is Open Source software released under the
* [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
